#include <filesystem>
#include <format>
#include <string>
#include <cxxopts.hpp>
#include <SDL2/SDL.h>
#include <kiln/logging.hpp>
#include <kiln/platform.hpp>
#include <kiln/shaders.hpp>
#include <kiln/utilities.hpp>
#include <kiln/vulkan/vk_instance_config.hpp>
#include "demo/demo.hpp"

int main(const int argc, char* argv[])
{
    kiln::virtual_terminal::enable();

    kiln::logging::configure_for_console();

    const auto log = kiln::logging::get_default_logger();

    cxxopts::Options options("kiln_demo", "Demo application for the Kiln Vulkan renderer.");
    options.allow_unrecognised_options();
    options.add_options()
        ("w,width", "The width of the window.",
            cxxopts::value<int>()->default_value("800"))
        ("h,height", "The height of the window.",
            cxxopts::value<int>()->default_value("600"))
        ("debug", "Enables debug features.",
            cxxopts::value<bool>())
        ("demo", "The demo to run.",
            cxxopts::value<std::string>()->default_value(""))
        ("assets", "The directory where assets should be loaded from.",
            cxxopts::value<std::string>()->default_value(""))
        ("wait", "Don't close the log output window right away.",
            cxxopts::value<bool>());

    cxxopts::ParseResult result;
    try
    {
        result = options.parse(argc, argv);
    }
    catch (std::exception& ex)
    {
        log->error("Failed to parse command line. Error: {}",
            std::string(ex.what()));
        return 1;
    }

    auto instance_config = kiln::vulkan::vk_instance_config();
    instance_config.include_vulkan_monitor();
#ifdef _DEBUG
    instance_config.debug_enabled() = true;
#else
    instance_config.debug_enabled() = result["debug"].as<bool>();
#endif

    const auto window_width = result["width"].as<int>();
    const auto window_height = result["height"].as<int>();

    const auto init_result = SDL_Init(SDL_INIT_EVENTS | SDL_INIT_VIDEO);
    if (init_result != 0)
    {
        log->critical("Failed to initialize SDL. Error: {} ({})",
            std::string(SDL_GetError()), init_result);
        return 1;
    }

    auto window = SDL_CreateWindow("Kiln Demo",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        window_width, window_height,
        SDL_WINDOW_RESIZABLE | SDL_WINDOW_VULKAN | SDL_WINDOW_ALLOW_HIGHDPI);

    auto assets_dir = result["assets"].as<std::string>();
    if (assets_dir.empty())
    {
        assets_dir = (kiln::get_root_directory() / L"assets").string();
        log->info("No assets directory specified. Defaulting to: {}", assets_dir);
    }

    if (!std::filesystem::exists(assets_dir))
    {
        log->error("Assets directory was not found at {}", assets_dir);

#if defined(OS_WINDOWS)
        // Show a dialog box reporting the problem.
        {
            const auto message = std::format(
                "Assets directory does not appear to exist. Checked at:\n"
                "\n"
                "{}\n"
                "\n"
                "Ensure this directory exists or provide your own assets directory with the \"--assets\" switch.\n"
                "Application will now exit.",
                assets_dir);

            MessageBoxA(nullptr, message.c_str(), "kiln_demo", MB_OK | MB_ICONERROR);
        }
#endif
        return 1;
    }

    const auto assets_dir_path = std::filesystem::path(assets_dir);
    const auto shaders_dir = assets_dir_path / L"shaders";
    kiln::shaders::initialize(shaders_dir);
    kiln::shaders::load_all();

    auto demo = std::make_unique<kiln::demo>(window, &instance_config, assets_dir_path);
    const auto exit_code = demo->run();
    demo.reset();
    demo = nullptr;

    SDL_DestroyWindow(window);
    window = nullptr;

    SDL_Quit();
    kiln::virtual_terminal::disable();
    return exit_code;
}