#pragma once
#include <filesystem>
#include <memory>
#include <string>
#include <SDL2/SDL.h>
#include <kiln/vulkan/vk_config.h>
#include <kiln/vulkan/vk_instance_config.hpp>
#include <kiln/vulkan/vk_instance.h>
#include <kiln/vulkan/vk_physical_device.h>
#include <kiln/vulkan/vk_renderer.h>
#include <kiln/vulkan/vk_assets_manager.h>
#include <kiln/vulkan/vk_asset_types.h>

namespace kiln
{
    class demo
    {
        SDL_Window* _window;

        vulkan::vk_instance_config* _instance_config;

        vulkan::vk_config _config = { };

        bool _run = false;

        bool _ui_show_background_window = false;
        bool _ui_show_extensions_window = false;
        bool _ui_show_imgui_demo = false;

        struct
        {
            bool enabled = false;

            bool show_vulkan_instance_window = false;
            bool show_vulkan_gpu_window      = false;

            bool show_camera_window = false;
            bool show_meshes_window = false;
        } _ui;

        struct
        {
            uint32_t            index = 0;
            std::string         name;
            vulkan::mesh_asset* mesh  = nullptr;
            bool                apply_world_transform = false;
        } _selected_mesh;

    public:
        demo(SDL_Window* window, vulkan::vk_instance_config* instance_config,
            const std::filesystem::path& assets_root_dir);
        virtual ~demo();
        demo(const demo&) = default;
        demo(demo&&) = default;

        demo& operator=(const demo&) = default;
        demo& operator=(demo&&) = default;

        [[nodiscard]] const screen_mode& screen_mode() const;
        void screen_mode(kiln::screen_mode mode);

        int run();

    private:
        void draw_mesh(vulkan::vk_renderer* render, const std::vector<std::shared_ptr<vulkan::mesh_asset>>& meshes);
        void draw_ui(vulkan::vk_renderer* render);

        /**
        *\brief Poll events that have occurred since the last frame was drawn.
        */
        void poll_events(SDL_Event& e, vulkan::vk_renderer* render);

        void ui_show_background_window(vulkan::vk_renderer* render);
        void ui_show_extensions_window(vulkan::vk_renderer* render);

        void draw_window_camera(const renderer* render);
        void draw_window_meshes(const std::vector<std::shared_ptr<vulkan::mesh_asset>>& meshes);
        void draw_window_vulkan_instance(vulkan::vk_instance* instance);
        void draw_window_vulkan_gpu(vulkan::vk_physical_device* gpu);
    };
}
