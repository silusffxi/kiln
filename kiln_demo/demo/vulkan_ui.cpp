#include <algorithm>
#include <string>
#include <sstream>
#include <vulkan/vulkan.h>
#include <vulkan/vk_enum_string_helper.h>
#include <kiln/imgui/imgui.h>
#include <kiln/vulkan/vk_physical_device.h>
//--------------------
#include "vulkan_ui.h"

using namespace kiln;

namespace
{
    std::string str_pipe_to_new_line(std::string& str)
    {
        std::ranges::replace(str, '|', '\n');
        return str;
    }
}

namespace ImGui
{
    inline void VkSampleCountFlagsText(VkSampleCountFlags sample_counts)
    {
        auto str = string_VkSampleCountFlags(sample_counts);
        Text("%s", str_pipe_to_new_line(str).c_str());
    }
}

void vulkan_ui::draw_instance_info_window(vulkan::vk_instance* instance, bool* is_open)
{
    if (ImGui::Begin("Vulkan Instance", is_open))
    {
        ImGui::SeparatorText("Vulkan API Version");

        ImGui::Text("Header:     %s", vulkan::api_version_header.string().c_str());
        if (ImGui::IsItemHovered())
        {
            if (ImGui::BeginTooltip())
            {
                ImGui::Text("Vulkan API version as reported by the vulkan.h header.");
                ImGui::EndTooltip();
            }
        }

        ImGui::Text("Vulkan API: %s", vulkan::api_version_direct.string().c_str());
        if (ImGui::IsItemHovered())
        {
            if (ImGui::BeginTooltip())
            {
                ImGui::Text("Vulkan API as reported by Vulkan itself.");
                ImGui::EndTooltip();
            }
        }

        ImGui::Text("Platform:   %s", vulkan::api_version_platform.string().c_str());
        if (ImGui::IsItemHovered())
        {
            if (ImGui::BeginTooltip())
            {
                ImGui::Text("Vulkan API as reported by the OS/platform.");
                ImGui::EndTooltip();
            }
        }

        ImGui::Separator();

        if (ImGui::CollapsingHeader("Extensions"))
        {
            for (const auto& ext : instance->extensions())
            {
                ImGui::BulletText("%s (%d)", ext.name.c_str(), ext.spec_version);
            }
        }

        if (ImGui::CollapsingHeader("Layers"))
        {
            const auto& available_layers = instance->available_layers();
            const auto& layers_in_use    = instance->layers_in_use();

            for (const auto& layer : available_layers)
            {
                ImGui::BulletText("%s - (%s)", layer.name.c_str(), layer.desc.c_str());
            }

            ImGui::Separator();

            for (const auto& [name, layer] : layers_in_use)
            {
                ImGui::BulletText("%s", name.c_str());
            }
        }
    }
    ImGui::End();
}

void vulkan_ui::draw_gpu_info_window(vulkan::vk_physical_device* gpu, bool* is_open)
{
    const auto api_ver = gpu->api_version();

    if (ImGui::Begin("Vulkan GPU", is_open))
    {
        ImGui::Text("%s", gpu->name().c_str());
        ImGui::SeparatorText("Queues");
        ImGui::Separator();
        ImGui::Text("Compute Queue Family: %d (%d queues)", gpu->compute_queue_family(), gpu->get_compute_queue_count());
        ImGui::Text("Graphics Queue Family: %d (%d queues)", gpu->graphics_queue_family(), gpu->get_graphics_queue_count());

        if (ImGui::CollapsingHeader("Properties"))
        {
            auto* props = gpu->properties();
            if (ImGui::BeginTable("gpu_props", 2))
            {
                ImGui::TableSetupColumn("Name");
                ImGui::TableSetupColumn("Value");
                ImGui::TableHeadersRow();

                ImGui::TableNextColumn();
                ImGui::Text("apiVersion");
                ImGui::TableNextColumn();
                ImGui::Text("%d", props->apiVersion);

                ImGui::TableNextColumn();
                ImGui::Text("driverVersion");
                ImGui::TableNextColumn();
                ImGui::Text("%d", props->driverVersion);

                ImGui::TableNextColumn();
                ImGui::Text("vendorID");
                ImGui::TableNextColumn();
                ImGui::Text("%d", props->vendorID);

                ImGui::TableNextColumn();
                ImGui::Text("deviceID");
                ImGui::TableNextColumn();
                ImGui::Text("%d", props->deviceID);

                ImGui::TableNextColumn();
                ImGui::Text("deviceName");
                ImGui::TableNextColumn();
                ImGui::Text("%s", props->deviceName);

                ImGui::TableNextColumn();
                ImGui::Text("limits");
                ImGui::TableNextColumn();
                ImGui::Text("Refer to the \"Limits\" section");

                ImGui::EndTable();
            }
        }

        if (ImGui::CollapsingHeader("Limits"))
        {
            const auto* limits = gpu->limits();
            if (ImGui::BeginTable("gpu_limits", 2))
            {
                ImGui::TableSetupColumn("Name");
                ImGui::TableSetupColumn("Value");
                ImGui::TableHeadersRow();

                ImGui::TableNextColumn();
                ImGui::Text("maxImageDimension1D");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxImageDimension1D);

                ImGui::TableNextColumn();
                ImGui::Text("maxImageDimension2D");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxImageDimension2D);

                ImGui::TableNextColumn();
                ImGui::Text("maxImageDimension3D");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxImageDimension3D);

                ImGui::TableNextColumn();
                ImGui::Text("maxImageDimensionCube");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxImageDimensionCube);

                ImGui::TableNextColumn();
                ImGui::Text("maxImageArrayLayers");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxImageArrayLayers);

                ImGui::TableNextColumn();
                ImGui::Text("maxTexelBufferElements");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxTexelBufferElements);

                ImGui::TableNextColumn();
                ImGui::Text("maxUniformBufferRange");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxUniformBufferRange);

                ImGui::TableNextColumn();
                ImGui::Text("maxStorageBufferRange");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxStorageBufferRange);

                ImGui::TableNextColumn();
                ImGui::Text("maxPushConstantsSize");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxPushConstantsSize);

                ImGui::TableNextColumn();
                ImGui::Text("maxMemoryAllocationCount");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxMemoryAllocationCount);

                ImGui::TableNextColumn();
                ImGui::Text("maxSamplerAllocationCount");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxSamplerAllocationCount);

                ImGui::TableNextColumn();
                ImGui::Text("bufferImageGranularity");
                ImGui::TableNextColumn();
                ImGui::Text("%zu", limits->bufferImageGranularity);

                ImGui::TableNextColumn();
                ImGui::Text("sparseAddressSpaceSize");
                ImGui::TableNextColumn();
                ImGui::Text("%zu", limits->sparseAddressSpaceSize);

                ImGui::TableNextColumn();
                ImGui::Text("maxBoundDescriptorSets");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxBoundDescriptorSets);

                ImGui::TableNextColumn();
                ImGui::Text("maxPerStageDescriptorSamplers");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxPerStageDescriptorSamplers);

                ImGui::TableNextColumn();
                ImGui::Text("maxPerStageDescriptorUniformBuffers");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxPerStageDescriptorUniformBuffers);

                ImGui::TableNextColumn();
                ImGui::Text("maxPerStageDescriptorStorageBuffers");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxPerStageDescriptorStorageBuffers);

                ImGui::TableNextColumn();
                ImGui::Text("maxPerStageDescriptorSampledImages");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxPerStageDescriptorSampledImages);

                ImGui::TableNextColumn();
                ImGui::Text("maxPerStageDescriptorStorageImages");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxPerStageDescriptorStorageImages);

                ImGui::TableNextColumn();
                ImGui::Text("maxPerStageDescriptorInputAttachments");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxPerStageDescriptorInputAttachments);

                ImGui::TableNextColumn();
                ImGui::Text("maxPerStageResources");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxPerStageResources);

                ImGui::TableNextColumn();
                ImGui::Text("maxDescriptorSetSamplers");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxDescriptorSetSamplers);

                ImGui::TableNextColumn();
                ImGui::Text("maxDescriptorSetUniformBuffers");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxDescriptorSetUniformBuffers);

                ImGui::TableNextColumn();
                ImGui::Text("maxDescriptorSetUniformBuffersDynamic");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxDescriptorSetUniformBuffersDynamic);

                ImGui::TableNextColumn();
                ImGui::Text("maxDescriptorSetStorageBuffers");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxDescriptorSetStorageBuffers);

                ImGui::TableNextColumn();
                ImGui::Text("maxDescriptorSetStorageBuffersDynamic");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxDescriptorSetStorageBuffersDynamic);

                ImGui::TableNextColumn();
                ImGui::Text("maxDescriptorSetSampledImages");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxDescriptorSetSampledImages);

                ImGui::TableNextColumn();
                ImGui::Text("maxDescriptorSetStorageImages");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxDescriptorSetStorageImages);

                ImGui::TableNextColumn();
                ImGui::Text("maxDescriptorSetInputAttachments");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxDescriptorSetInputAttachments);

                ImGui::TableNextColumn();
                ImGui::Text("maxVertexInputAttributes");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxVertexInputAttributes);

                ImGui::TableNextColumn();
                ImGui::Text("maxVertexInputBindings");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxVertexInputBindings);

                ImGui::TableNextColumn();
                ImGui::Text("maxVertexInputAttributeOffset");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxVertexInputAttributeOffset);

                ImGui::TableNextColumn();
                ImGui::Text("maxVertexInputBindingStride");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxVertexInputBindingStride);

                ImGui::TableNextColumn();
                ImGui::Text("maxVertexOutputComponents");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxVertexOutputComponents);

                ImGui::TableNextColumn();
                ImGui::Text("maxTessellationGenerationLevel");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxTessellationGenerationLevel);

                ImGui::TableNextColumn();
                ImGui::Text("maxTessellationPatchSize");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxTessellationPatchSize);

                ImGui::TableNextColumn();
                ImGui::Text("maxTessellationControlPerVertexInputComponents");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxTessellationControlPerVertexInputComponents);

                ImGui::TableNextColumn();
                ImGui::Text("maxTessellationControlPerVertexOutputComponents");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxTessellationControlPerVertexOutputComponents);

                ImGui::TableNextColumn();
                ImGui::Text("maxTessellationControlPerPatchOutputComponents");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxTessellationControlPerPatchOutputComponents);

                ImGui::TableNextColumn();
                ImGui::Text("maxTessellationControlTotalOutputComponents");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxTessellationControlTotalOutputComponents);

                ImGui::TableNextColumn();
                ImGui::Text("maxTessellationEvaluationInputComponents");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxTessellationEvaluationInputComponents);

                ImGui::TableNextColumn();
                ImGui::Text("maxTessellationEvaluationOutputComponents");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxTessellationEvaluationOutputComponents);

                ImGui::TableNextColumn();
                ImGui::Text("maxGeometryShaderInvocations");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxGeometryShaderInvocations);

                ImGui::TableNextColumn();
                ImGui::Text("maxGeometryInputComponents");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxGeometryInputComponents);

                ImGui::TableNextColumn();
                ImGui::Text("maxGeometryOutputComponents");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxGeometryOutputComponents);

                ImGui::TableNextColumn();
                ImGui::Text("maxGeometryOutputVertices");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxGeometryOutputVertices);

                ImGui::TableNextColumn();
                ImGui::Text("maxGeometryTotalOutputComponents");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxGeometryTotalOutputComponents);

                ImGui::TableNextColumn();
                ImGui::Text("maxFragmentInputComponents");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxFragmentInputComponents);

                ImGui::TableNextColumn();
                ImGui::Text("maxFragmentOutputAttachments");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxFragmentOutputAttachments);

                ImGui::TableNextColumn();
                ImGui::Text("maxFragmentDualSrcAttachments");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxFragmentDualSrcAttachments);

                ImGui::TableNextColumn();
                ImGui::Text("maxFragmentCombinedOutputResources");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxFragmentCombinedOutputResources);

                ImGui::TableNextColumn();
                ImGui::Text("maxComputeSharedMemorySize");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxComputeSharedMemorySize);

                ImGui::TableNextColumn();
                ImGui::Text("maxComputeWorkGroupCount");
                ImGui::TableNextColumn();
                ImGui::Text("[0] = %d", limits->maxComputeWorkGroupCount[0]);
                ImGui::Text("[1] = %d", limits->maxComputeWorkGroupCount[1]);
                ImGui::Text("[2] = %d", limits->maxComputeWorkGroupCount[2]);

                ImGui::TableNextColumn();
                ImGui::Text("maxComputeWorkGroupInvocations");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxComputeWorkGroupInvocations);

                ImGui::TableNextColumn();
                ImGui::Text("maxComputeWorkGroupSize");
                ImGui::TableNextColumn();
                ImGui::Text("[0] = %d", limits->maxComputeWorkGroupSize[0]);
                ImGui::Text("[1] = %d", limits->maxComputeWorkGroupSize[1]);
                ImGui::Text("[2] = %d", limits->maxComputeWorkGroupSize[2]);

                ImGui::TableNextColumn();
                ImGui::Text("subPixelPrecisionBits");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->subPixelPrecisionBits);

                ImGui::TableNextColumn();
                ImGui::Text("subTexelPrecisionBits");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->subTexelPrecisionBits);

                ImGui::TableNextColumn();
                ImGui::Text("mipmapPrecisionBits");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->mipmapPrecisionBits);

                ImGui::TableNextColumn();
                ImGui::Text("maxDrawIndexedIndexValue");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxDrawIndexedIndexValue);

                ImGui::TableNextColumn();
                ImGui::Text("maxDrawIndirectCount");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxDrawIndirectCount);

                ImGui::TableNextColumn();
                ImGui::Text("maxSamplerLodBias");
                ImGui::TableNextColumn();
                ImGui::Text("%.03f", limits->maxSamplerLodBias);

                ImGui::TableNextColumn();
                ImGui::Text("maxSamplerAnisotropy");
                ImGui::TableNextColumn();
                ImGui::Text("%.03f", limits->maxSamplerAnisotropy);

                ImGui::TableNextColumn();
                ImGui::Text("maxViewports");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxViewports);

                ImGui::TableNextColumn();
                ImGui::Text("maxViewportDimensions");
                ImGui::TableNextColumn();
                ImGui::Text("min: %d", limits->maxViewportDimensions[0]);
                ImGui::Text("max: %d", limits->maxViewportDimensions[1]);

                ImGui::TableNextColumn();
                ImGui::Text("viewportBoundsRange");
                ImGui::TableNextColumn();
                ImGui::Text("min: %.03f", limits->viewportBoundsRange[0]);
                ImGui::Text("max: %.03f", limits->viewportBoundsRange[1]);

                ImGui::TableNextColumn();
                ImGui::Text("viewportSubPixelBits");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->viewportSubPixelBits);

                ImGui::TableNextColumn();
                ImGui::Text("minMemoryMapAlignment");
                ImGui::TableNextColumn();
                ImGui::Text("%zu", limits->minMemoryMapAlignment);

                ImGui::TableNextColumn();
                ImGui::Text("minTexelBufferOffsetAlignment");
                ImGui::TableNextColumn();
                ImGui::Text("%zu", limits->minTexelBufferOffsetAlignment);

                ImGui::TableNextColumn();
                ImGui::Text("minUniformBufferOffsetAlignment");
                ImGui::TableNextColumn();
                ImGui::Text("%zu", limits->minUniformBufferOffsetAlignment);

                ImGui::TableNextColumn();
                ImGui::Text("minStorageBufferOffsetAlignment");
                ImGui::TableNextColumn();
                ImGui::Text("%zu", limits->minStorageBufferOffsetAlignment);

                ImGui::TableNextColumn();
                ImGui::Text("minTexelOffset");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->minTexelOffset);

                ImGui::TableNextColumn();
                ImGui::Text("maxTexelOffset");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxTexelOffset);

                ImGui::TableNextColumn();
                ImGui::Text("minTexelGatherOffset");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->minTexelGatherOffset);

                ImGui::TableNextColumn();
                ImGui::Text("maxTexelGatherOffset");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxTexelGatherOffset);

                ImGui::TableNextColumn();
                ImGui::Text("minInterpolationOffset");
                ImGui::TableNextColumn();
                ImGui::Text("%.03f", limits->minInterpolationOffset);

                ImGui::TableNextColumn();
                ImGui::Text("maxInterpolationOffset");
                ImGui::TableNextColumn();
                ImGui::Text("%.03f", limits->maxInterpolationOffset);

                ImGui::TableNextColumn();
                ImGui::Text("subPixelInterpolationOffsetBits");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->subPixelInterpolationOffsetBits);

                ImGui::TableNextColumn();
                ImGui::Text("maxFramebufferWidth");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxFramebufferWidth);

                ImGui::TableNextColumn();
                ImGui::Text("maxFramebufferHeight");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxFramebufferHeight);

                ImGui::TableNextColumn();
                ImGui::Text("maxFramebufferLayers");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxFramebufferLayers);

                ImGui::TableNextColumn();
                ImGui::Text("framebufferColorSampleCounts");
                ImGui::TableNextColumn();
                ImGui::VkSampleCountFlagsText(limits->framebufferColorSampleCounts);

                ImGui::TableNextColumn();
                ImGui::Text("framebufferDepthSampleCounts");
                ImGui::TableNextColumn();
                ImGui::VkSampleCountFlagsText(limits->framebufferDepthSampleCounts);

                ImGui::TableNextColumn();
                ImGui::Text("framebufferStencilSampleCounts");
                ImGui::TableNextColumn();
                ImGui::VkSampleCountFlagsText(limits->framebufferStencilSampleCounts);

                ImGui::TableNextColumn();
                ImGui::Text("framebufferNoAttachmentsSampleCounts");
                ImGui::TableNextColumn();
                ImGui::VkSampleCountFlagsText(limits->framebufferNoAttachmentsSampleCounts);

                ImGui::TableNextColumn();
                ImGui::Text("maxColorAttachments");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxColorAttachments);

                ImGui::TableNextColumn();
                ImGui::Text("sampledImageColorSampleCounts");
                ImGui::TableNextColumn();
                ImGui::VkSampleCountFlagsText(limits->sampledImageColorSampleCounts);

                ImGui::TableNextColumn();
                ImGui::Text("sampledImageIntegerSampleCounts");
                ImGui::TableNextColumn();
                ImGui::VkSampleCountFlagsText(limits->sampledImageIntegerSampleCounts);

                ImGui::TableNextColumn();
                ImGui::Text("sampledImageDepthSampleCounts");
                ImGui::TableNextColumn();
                ImGui::VkSampleCountFlagsText(limits->sampledImageDepthSampleCounts);

                ImGui::TableNextColumn();
                ImGui::Text("sampledImageStencilSampleCounts");
                ImGui::TableNextColumn();
                ImGui::VkSampleCountFlagsText(limits->sampledImageStencilSampleCounts);

                ImGui::TableNextColumn();
                ImGui::Text("storageImageSampleCounts");
                ImGui::TableNextColumn();
                ImGui::VkSampleCountFlagsText(limits->storageImageSampleCounts);

                ImGui::TableNextColumn();
                ImGui::Text("maxSampleMaskWords");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxSampleMaskWords);

                ImGui::TableNextColumn();
                ImGui::Text("timestampComputeAndGraphics");
                ImGui::TableNextColumn();
                ImGui::Text("%s", limits->timestampComputeAndGraphics ? "true" : "false");

                ImGui::TableNextColumn();
                ImGui::Text("timestampPeriod");
                ImGui::TableNextColumn();
                ImGui::Text("%.03f", limits->timestampPeriod);

                ImGui::TableNextColumn();
                ImGui::Text("maxClipDistances");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxClipDistances);

                ImGui::TableNextColumn();
                ImGui::Text("maxCullDistances");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxCullDistances);

                ImGui::TableNextColumn();
                ImGui::Text("maxCombinedClipAndCullDistances");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->maxCombinedClipAndCullDistances);

                ImGui::TableNextColumn();
                ImGui::Text("discreteQueuePriorities");
                ImGui::TableNextColumn();
                ImGui::Text("%d", limits->discreteQueuePriorities);

                ImGui::TableNextColumn();
                ImGui::Text("pointSizeRange");
                ImGui::TableNextColumn();
                ImGui::Text("min: %.03f", limits->pointSizeRange[0]);
                ImGui::Text("max: %.03f", limits->pointSizeRange[1]);

                ImGui::TableNextColumn();
                ImGui::Text("lineWidthRange");
                ImGui::TableNextColumn();
                ImGui::Text("min: %.03f", limits->lineWidthRange[0]);
                ImGui::Text("max: %.03f", limits->lineWidthRange[1]);

                ImGui::TableNextColumn();
                ImGui::Text("pointSizeGranularity");
                ImGui::TableNextColumn();
                ImGui::Text("%.03f", limits->pointSizeGranularity);

                ImGui::TableNextColumn();
                ImGui::Text("lineWidthGranularity");
                ImGui::TableNextColumn();
                ImGui::Text("%.03f", limits->lineWidthGranularity);

                ImGui::TableNextColumn();
                ImGui::Text("strictLines");
                ImGui::TableNextColumn();
                ImGui::Text("%s", limits->strictLines ? "true" : "false");

                ImGui::TableNextColumn();
                ImGui::Text("standardSampleLocations");
                ImGui::TableNextColumn();
                ImGui::Text("%s", limits->standardSampleLocations ? "true" : "false");

                ImGui::TableNextColumn();
                ImGui::Text("optimalBufferCopyOffsetAlignment");
                ImGui::TableNextColumn();
                ImGui::Text("%zu", limits->optimalBufferCopyOffsetAlignment);

                ImGui::TableNextColumn();
                ImGui::Text("optimalBufferCopyRowPitchAlignment");
                ImGui::TableNextColumn();
                ImGui::Text("%zu", limits->optimalBufferCopyRowPitchAlignment);

                ImGui::TableNextColumn();
                ImGui::Text("nonCoherentAtomSize");
                ImGui::TableNextColumn();
                ImGui::Text("%zu", limits->nonCoherentAtomSize);

                ImGui::EndTable();
            }
        }

        if (ImGui::CollapsingHeader("Features"))
        {
            const auto* features = gpu->features();
            if (ImGui::BeginTable("gpu_features", 2))
            {
                ImGui::TableSetupColumn("Name");
                ImGui::TableSetupColumn("Value");
                ImGui::TableHeadersRow();

                ImGui::TableNextColumn();
                ImGui::Text("robustBufferAccess");
                ImGui::TableNextColumn();
                bool robustBufferAccess = features->robustBufferAccess;
                ImGui::Checkbox("##gpu_features_robustBufferAccess", &robustBufferAccess);

                ImGui::TableNextColumn();
                ImGui::Text("fullDrawIndexUint32");
                ImGui::TableNextColumn();
                bool fullDrawIndexUint32 = features->fullDrawIndexUint32;
                ImGui::Checkbox("##gpu_features_fullDrawIndexUint32", &fullDrawIndexUint32);

                ImGui::TableNextColumn();
                ImGui::Text("imageCubeArray");
                ImGui::TableNextColumn();
                bool imageCubeArray = features->imageCubeArray;
                ImGui::Checkbox("##gpu_features_imageCubeArray", &imageCubeArray);

                ImGui::TableNextColumn();
                ImGui::Text("independentBlend");
                ImGui::TableNextColumn();
                bool independentBlend = features->independentBlend;
                ImGui::Checkbox("##gpu_features_independentBlend", &independentBlend);

                ImGui::TableNextColumn();
                ImGui::Text("geometryShader");
                ImGui::TableNextColumn();
                bool geometryShader = features->geometryShader;
                ImGui::Checkbox("##gpu_features_geometryShader", &geometryShader);

                ImGui::TableNextColumn();
                ImGui::Text("tessellationShader");
                ImGui::TableNextColumn();
                bool tessellationShader = features->tessellationShader;
                ImGui::Checkbox("##gpu_features_tessellationShader", &tessellationShader);

                ImGui::TableNextColumn();
                ImGui::Text("sampleRateShading");
                ImGui::TableNextColumn();
                bool sampleRateShading = features->sampleRateShading;
                ImGui::Checkbox("##gpu_features_sampleRateShading", &sampleRateShading);

                ImGui::TableNextColumn();
                ImGui::Text("dualSrcBlend");
                ImGui::TableNextColumn();
                bool dualSrcBlend = features->dualSrcBlend;
                ImGui::Checkbox("##gpu_features_dualSrcBlend", &dualSrcBlend);

                ImGui::TableNextColumn();
                ImGui::Text("logicOp");
                ImGui::TableNextColumn();
                bool logicOp = features->logicOp;
                ImGui::Checkbox("##gpu_features_logicOp", &logicOp);

                ImGui::TableNextColumn();
                ImGui::Text("multiDrawIndirect");
                ImGui::TableNextColumn();
                bool multiDrawIndirect = features->multiDrawIndirect;
                ImGui::Checkbox("##gpu_features_multiDrawIndirect", &multiDrawIndirect);

                ImGui::TableNextColumn();
                ImGui::Text("drawIndirectFirstInstance");
                ImGui::TableNextColumn();
                bool drawIndirectFirstInstance = features->drawIndirectFirstInstance;
                ImGui::Checkbox("##gpu_features_drawIndirectFirstInstance", &drawIndirectFirstInstance);

                ImGui::TableNextColumn();
                ImGui::Text("depthClamp");
                ImGui::TableNextColumn();
                bool depthClamp = features->depthClamp;
                ImGui::Checkbox("##gpu_features_depthClamp", &depthClamp);

                ImGui::TableNextColumn();
                ImGui::Text("depthBiasClamp");
                ImGui::TableNextColumn();
                bool depthBiasClamp = features->depthBiasClamp;
                ImGui::Checkbox("##gpu_features_depthBiasClamp", &depthBiasClamp);

                ImGui::TableNextColumn();
                ImGui::Text("fillModeNonSolid");
                ImGui::TableNextColumn();
                bool fillModeNonSolid = features->fillModeNonSolid;
                ImGui::Checkbox("##gpu_features_fillModeNonSolid", &fillModeNonSolid);

                ImGui::TableNextColumn();
                ImGui::Text("depthBounds");
                ImGui::TableNextColumn();
                bool depthBounds = features->depthBounds;
                ImGui::Checkbox("##gpu_features_depthBounds", &depthBounds);

                ImGui::TableNextColumn();
                ImGui::Text("wideLines");
                ImGui::TableNextColumn();
                bool wideLines = features->wideLines;
                ImGui::Checkbox("##gpu_features_wideLines", &wideLines);

                ImGui::TableNextColumn();
                ImGui::Text("largePoints");
                ImGui::TableNextColumn();
                bool largePoints = features->largePoints;
                ImGui::Checkbox("##gpu_features_largePoints", &largePoints);

                ImGui::TableNextColumn();
                ImGui::Text("alphaToOne");
                ImGui::TableNextColumn();
                bool alphaToOne = features->alphaToOne;
                ImGui::Checkbox("##gpu_features_alphaToOne", &alphaToOne);

                ImGui::TableNextColumn();
                ImGui::Text("multiViewport");
                ImGui::TableNextColumn();
                bool multiViewport = features->multiViewport;
                ImGui::Checkbox("##gpu_features_multiViewport", &multiViewport);

                ImGui::TableNextColumn();
                ImGui::Text("samplerAnisotropy");
                ImGui::TableNextColumn();
                bool samplerAnisotropy = features->samplerAnisotropy;
                ImGui::Checkbox("##gpu_features_samplerAnisotropy", &samplerAnisotropy);

                ImGui::TableNextColumn();
                ImGui::Text("textureCompressionETC2");
                ImGui::TableNextColumn();
                bool textureCompressionETC2 = features->textureCompressionETC2;
                ImGui::Checkbox("##gpu_features_textureCompressionETC2", &textureCompressionETC2);

                ImGui::TableNextColumn();
                ImGui::Text("textureCompressionASTC_LDR");
                ImGui::TableNextColumn();
                bool textureCompressionASTC_LDR = features->textureCompressionASTC_LDR;
                ImGui::Checkbox("##gpu_features_textureCompressionASTC_LDR", &textureCompressionASTC_LDR);

                ImGui::TableNextColumn();
                ImGui::Text("textureCompressionBC");
                ImGui::TableNextColumn();
                bool textureCompressionBC = features->textureCompressionBC;
                ImGui::Checkbox("##gpu_features_textureCompressionBC", &textureCompressionBC);

                ImGui::TableNextColumn();
                ImGui::Text("occlusionQueryPrecise");
                ImGui::TableNextColumn();
                bool occlusionQueryPrecise = features->occlusionQueryPrecise;
                ImGui::Checkbox("##gpu_features_occlusionQueryPrecise", &occlusionQueryPrecise);

                ImGui::TableNextColumn();
                ImGui::Text("pipelineStatisticsQuery");
                ImGui::TableNextColumn();
                bool pipelineStatisticsQuery = features->pipelineStatisticsQuery;
                ImGui::Checkbox("##gpu_features_pipelineStatisticsQuery", &pipelineStatisticsQuery);

                ImGui::TableNextColumn();
                ImGui::Text("vertexPipelineStoresAndAtomics");
                ImGui::TableNextColumn();
                bool vertexPipelineStoresAndAtomics = features->vertexPipelineStoresAndAtomics;
                ImGui::Checkbox("##gpu_features_vertexPipelineStoresAndAtomics", &vertexPipelineStoresAndAtomics);

                ImGui::TableNextColumn();
                ImGui::Text("fragmentStoresAndAtomics");
                ImGui::TableNextColumn();
                bool fragmentStoresAndAtomics = features->fragmentStoresAndAtomics;
                ImGui::Checkbox("##gpu_features_fragmentStoresAndAtomics", &fragmentStoresAndAtomics);

                ImGui::TableNextColumn();
                ImGui::Text("shaderTessellationAndGeometryPointSize");
                ImGui::TableNextColumn();
                bool shaderTessellationAndGeometryPointSize = features->shaderTessellationAndGeometryPointSize;
                ImGui::Checkbox("##gpu_features_shaderTessellationAndGeometryPointSize", &shaderTessellationAndGeometryPointSize);

                ImGui::TableNextColumn();
                ImGui::Text("shaderImageGatherExtended");
                ImGui::TableNextColumn();
                bool shaderImageGatherExtended = features->shaderImageGatherExtended;
                ImGui::Checkbox("##gpu_features_shaderImageGatherExtended", &shaderImageGatherExtended);

                ImGui::TableNextColumn();
                ImGui::Text("shaderStorageImageExtendedFormats");
                ImGui::TableNextColumn();
                bool shaderStorageImageExtendedFormats = features->shaderStorageImageExtendedFormats;
                ImGui::Checkbox("##gpu_features_shaderStorageImageExtendedFormats", &shaderStorageImageExtendedFormats);

                ImGui::TableNextColumn();
                ImGui::Text("shaderStorageImageMultisample");
                ImGui::TableNextColumn();
                bool shaderStorageImageMultisample = features->shaderStorageImageMultisample;
                ImGui::Checkbox("##gpu_features_shaderStorageImageMultisample", &shaderStorageImageMultisample);

                ImGui::TableNextColumn();
                ImGui::Text("shaderStorageImageReadWithoutFormat");
                ImGui::TableNextColumn();
                bool shaderStorageImageReadWithoutFormat = features->shaderStorageImageReadWithoutFormat;
                ImGui::Checkbox("##gpu_features_shaderStorageImageReadWithoutFormat", &shaderStorageImageReadWithoutFormat);

                ImGui::TableNextColumn();
                ImGui::Text("shaderStorageImageWriteWithoutFormat");
                ImGui::TableNextColumn();
                bool shaderStorageImageWriteWithoutFormat = features->shaderStorageImageWriteWithoutFormat;
                ImGui::Checkbox("##gpu_features_shaderStorageImageWriteWithoutFormat", &shaderStorageImageWriteWithoutFormat);

                ImGui::TableNextColumn();
                ImGui::Text("shaderUniformBufferArrayDynamicIndexing");
                ImGui::TableNextColumn();
                bool shaderUniformBufferArrayDynamicIndexing = features->shaderUniformBufferArrayDynamicIndexing;
                ImGui::Checkbox("##gpu_features_shaderUniformBufferArrayDynamicIndexing", &shaderUniformBufferArrayDynamicIndexing);

                ImGui::TableNextColumn();
                ImGui::Text("shaderSampledImageArrayDynamicIndexing");
                ImGui::TableNextColumn();
                bool shaderSampledImageArrayDynamicIndexing = features->shaderSampledImageArrayDynamicIndexing;
                ImGui::Checkbox("##gpu_features_shaderSampledImageArrayDynamicIndexing", &shaderSampledImageArrayDynamicIndexing);

                ImGui::TableNextColumn();
                ImGui::Text("shaderStorageBufferArrayDynamicIndexing");
                ImGui::TableNextColumn();
                bool shaderStorageBufferArrayDynamicIndexing = features->shaderStorageBufferArrayDynamicIndexing;
                ImGui::Checkbox("##gpu_features_shaderStorageBufferArrayDynamicIndexing", &shaderStorageBufferArrayDynamicIndexing);

                ImGui::TableNextColumn();
                ImGui::Text("shaderStorageImageArrayDynamicIndexing");
                ImGui::TableNextColumn();
                bool shaderStorageImageArrayDynamicIndexing = features->shaderStorageImageArrayDynamicIndexing;
                ImGui::Checkbox("##gpu_features_shaderStorageImageArrayDynamicIndexing", &shaderStorageImageArrayDynamicIndexing);

                ImGui::TableNextColumn();
                ImGui::Text("shaderClipDistance");
                ImGui::TableNextColumn();
                bool shaderClipDistance = features->shaderClipDistance;
                ImGui::Checkbox("##gpu_features_shaderClipDistance", &shaderClipDistance);

                ImGui::TableNextColumn();
                ImGui::Text("shaderCullDistance");
                ImGui::TableNextColumn();
                bool shaderCullDistance = features->shaderCullDistance;
                ImGui::Checkbox("##gpu_features_shaderCullDistance", &shaderCullDistance);

                ImGui::TableNextColumn();
                ImGui::Text("shaderFloat64");
                ImGui::TableNextColumn();
                bool shaderFloat64 = features->shaderFloat64;
                ImGui::Checkbox("##gpu_features_shaderFloat64", &shaderFloat64);

                ImGui::TableNextColumn();
                ImGui::Text("shaderInt64");
                ImGui::TableNextColumn();
                bool shaderInt64 = features->shaderInt64;
                ImGui::Checkbox("##gpu_features_shaderInt64", &shaderInt64);

                ImGui::TableNextColumn();
                ImGui::Text("shaderInt16");
                ImGui::TableNextColumn();
                bool shaderInt16 = features->shaderInt16;
                ImGui::Checkbox("##gpu_features_shaderInt16", &shaderInt16);

                ImGui::TableNextColumn();
                ImGui::Text("shaderResourceResidency");
                ImGui::TableNextColumn();
                bool shaderResourceResidency = features->shaderResourceResidency;
                ImGui::Checkbox("##gpu_features_shaderResourceResidency", &shaderResourceResidency);

                ImGui::TableNextColumn();
                ImGui::Text("shaderResourceMinLod");
                ImGui::TableNextColumn();
                bool shaderResourceMinLod = features->shaderResourceMinLod;
                ImGui::Checkbox("##gpu_features_shaderResourceMinLod", &shaderResourceMinLod);

                ImGui::TableNextColumn();
                ImGui::Text("sparseBinding");
                ImGui::TableNextColumn();
                bool sparseBinding = features->sparseBinding;
                ImGui::Checkbox("##gpu_features_sparseBinding", &sparseBinding);

                ImGui::TableNextColumn();
                ImGui::Text("sparseResidencyBuffer");
                ImGui::TableNextColumn();
                bool sparseResidencyBuffer = features->sparseResidencyBuffer;
                ImGui::Checkbox("##gpu_features_sparseResidencyBuffer", &sparseResidencyBuffer);

                ImGui::TableNextColumn();
                ImGui::Text("sparseResidencyImage2D");
                ImGui::TableNextColumn();
                bool sparseResidencyImage2D = features->sparseResidencyImage2D;
                ImGui::Checkbox("##gpu_features_sparseResidencyImage2D", &sparseResidencyImage2D);

                ImGui::TableNextColumn();
                ImGui::Text("sparseResidencyImage3D");
                ImGui::TableNextColumn();
                bool sparseResidencyImage3D = features->sparseResidencyImage3D;
                ImGui::Checkbox("##gpu_features_sparseResidencyImage3D", &sparseResidencyImage3D);

                ImGui::TableNextColumn();
                ImGui::Text("sparseResidency2Samples");
                ImGui::TableNextColumn();
                bool sparseResidency2Samples = features->sparseResidency2Samples;
                ImGui::Checkbox("##gpu_features_sparseResidency2Samples", &sparseResidency2Samples);

                ImGui::TableNextColumn();
                ImGui::Text("sparseResidency4Samples");
                ImGui::TableNextColumn();
                bool sparseResidency4Samples = features->sparseResidency4Samples;
                ImGui::Checkbox("##gpu_features_sparseResidency4Samples", &sparseResidency4Samples);

                ImGui::TableNextColumn();
                ImGui::Text("sparseResidency8Samples");
                ImGui::TableNextColumn();
                bool sparseResidency8Samples = features->sparseResidency8Samples;
                ImGui::Checkbox("##gpu_features_sparseResidency8Samples", &sparseResidency8Samples);

                ImGui::TableNextColumn();
                ImGui::Text("sparseResidency16Samples");
                ImGui::TableNextColumn();
                bool sparseResidency16Samples = features->sparseResidency16Samples;
                ImGui::Checkbox("##gpu_features_sparseResidency16Samples", &sparseResidency16Samples);

                ImGui::TableNextColumn();
                ImGui::Text("sparseResidencyAliased");
                ImGui::TableNextColumn();
                bool sparseResidencyAliased = features->sparseResidencyAliased;
                ImGui::Checkbox("##gpu_features_sparseResidencyAliased", &sparseResidencyAliased);

                ImGui::TableNextColumn();
                ImGui::Text("variableMultisampleRate");
                ImGui::TableNextColumn();
                bool variableMultisampleRate = features->variableMultisampleRate;
                ImGui::Checkbox("##gpu_features_variableMultisampleRate", &variableMultisampleRate);

                ImGui::TableNextColumn();
                ImGui::Text("inheritedQueries");
                ImGui::TableNextColumn();
                bool inheritedQueries = features->inheritedQueries;
                ImGui::Checkbox("##gpu_features_inheritedQueries", &inheritedQueries);

                ImGui::EndTable();
            }
        }

        if (api_ver >= vulkan::api_1_1)
        {
            if (ImGui::CollapsingHeader("Properties (Vulkan 1.1)"))
            {
                const auto* vk_11_props = gpu->properties_vulkan_11();
                if (ImGui::BeginTable("gpu_props_vk_11", 2))
                {
                    ImGui::TableSetupColumn("Name");
                    ImGui::TableSetupColumn("Value");
                    ImGui::TableHeadersRow();

                    ImGui::TableNextColumn();
                    ImGui::Text("deviceUUID");
                    ImGui::TableNextColumn();
                    ImGui::Text("<TODO>");

                    ImGui::TableNextColumn();
                    ImGui::Text("driverUUID");
                    ImGui::TableNextColumn();
                    ImGui::Text("<TODO>");

                    ImGui::TableNextColumn();
                    ImGui::Text("deviceLUID");
                    ImGui::TableNextColumn();
                    ImGui::Text("<TODO>");

                    ImGui::TableNextColumn();
                    ImGui::Text("deviceNodeMask");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", vk_11_props->deviceNodeMask);

                    ImGui::TableNextColumn();
                    ImGui::Text("deviceLUIDValid");
                    ImGui::TableNextColumn();
                    ImGui::Text("%s", vk_11_props->deviceLUIDValid ? "true" : "false");

                    ImGui::TableNextColumn();
                    ImGui::Text("subgroupSize");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", vk_11_props->subgroupSize);

                    ImGui::TableNextColumn();
                    ImGui::Text("subgroupSupportedStages");
                    ImGui::TableNextColumn();
                    auto subgroup_supported_stages_str = string_VkShaderStageFlags(vk_11_props->subgroupSupportedStages);
                    ImGui::TextWrapped("%s", str_pipe_to_new_line(subgroup_supported_stages_str).c_str());

                    ImGui::TableNextColumn();
                    ImGui::Text("subgroupSupportedOperations");
                    ImGui::TableNextColumn();
                    auto subgroup_supported_ops_str = string_VkSubgroupFeatureFlags(vk_11_props->subgroupSupportedOperations);
                    ImGui::TextWrapped("%s", str_pipe_to_new_line(subgroup_supported_ops_str).c_str());

                    ImGui::TableNextColumn();
                    ImGui::Text("subgroupQuadOperationsInAllStages");
                    ImGui::TableNextColumn();
                    ImGui::Text("%s", vk_11_props->subgroupQuadOperationsInAllStages ? "true" : "false");

                    ImGui::TableNextColumn();
                    ImGui::Text("pointClippingBehavior");
                    ImGui::TableNextColumn();
                    ImGui::TextWrapped("%s", string_VkPointClippingBehavior(vk_11_props->pointClippingBehavior));

                    ImGui::TableNextColumn();
                    ImGui::Text("maxMultiviewViewCount");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", vk_11_props->maxMultiviewViewCount);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxMultiviewInstanceIndex");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", vk_11_props->maxMultiviewInstanceIndex);

                    ImGui::TableNextColumn();
                    ImGui::Text("protectedNoFault");
                    ImGui::TableNextColumn();
                    ImGui::Text("%s", vk_11_props->protectedNoFault ? "true" : "false");

                    ImGui::TableNextColumn();
                    ImGui::Text("maxPerSetDescriptors");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", vk_11_props->maxPerSetDescriptors);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxMemoryAllocationSize");
                    ImGui::TableNextColumn();
                    ImGui::Text("%zu", vk_11_props->maxMemoryAllocationSize);

                    ImGui::EndTable();
                }
            }

            if (ImGui::CollapsingHeader("Features (Vulkan 1.1)"))
            {
                const auto* vk_11_feats = gpu->features_vulkan_11();
                if (ImGui::BeginTable("gpu_props_vk_11", 2))
                {
                    ImGui::TableSetupColumn("Name");
                    ImGui::TableSetupColumn("Value");
                    ImGui::TableHeadersRow();

                    ImGui::TableNextColumn();
                    ImGui::Text("storageBuffer16BitAccess");
                    ImGui::TableNextColumn();
                    bool storageBuffer16BitAccess = vk_11_feats->storageBuffer16BitAccess;
                    ImGui::Checkbox("##gpu_features_storageBuffer16BitAccess", &storageBuffer16BitAccess);

                    ImGui::TableNextColumn();
                    ImGui::Text("uniformAndStorageBuffer16BitAccess");
                    ImGui::TableNextColumn();
                    bool uniformAndStorageBuffer16BitAccess = vk_11_feats->uniformAndStorageBuffer16BitAccess;
                    ImGui::Checkbox("##gpu_features_uniformAndStorageBuffer16BitAccess", &uniformAndStorageBuffer16BitAccess);

                    ImGui::TableNextColumn();
                    ImGui::Text("storagePushConstant16");
                    ImGui::TableNextColumn();
                    bool storagePushConstant16 = vk_11_feats->storagePushConstant16;
                    ImGui::Checkbox("##gpu_features_storagePushConstant16", &storagePushConstant16);

                    ImGui::TableNextColumn();
                    ImGui::Text("storageInputOutput16");
                    ImGui::TableNextColumn();
                    bool storageInputOutput16 = vk_11_feats->storageInputOutput16;
                    ImGui::Checkbox("##gpu_features_storageInputOutput16", &storageInputOutput16);

                    ImGui::TableNextColumn();
                    ImGui::Text("multiview");
                    ImGui::TableNextColumn();
                    bool multiview = vk_11_feats->multiview;
                    ImGui::Checkbox("##gpu_features_multiview", &multiview);

                    ImGui::TableNextColumn();
                    ImGui::Text("multiviewGeometryShader");
                    ImGui::TableNextColumn();
                    bool multiviewGeometryShader = vk_11_feats->multiviewGeometryShader;
                    ImGui::Checkbox("##gpu_features_multiviewGeometryShader", &multiviewGeometryShader);

                    ImGui::TableNextColumn();
                    ImGui::Text("multiviewTessellationShader");
                    ImGui::TableNextColumn();
                    bool multiviewTessellationShader = vk_11_feats->multiviewTessellationShader;
                    ImGui::Checkbox("##gpu_features_multiviewTessellationShader", &multiviewTessellationShader);

                    ImGui::TableNextColumn();
                    ImGui::Text("variablePointersStorageBuffer");
                    ImGui::TableNextColumn();
                    bool variablePointersStorageBuffer = vk_11_feats->variablePointersStorageBuffer;
                    ImGui::Checkbox("##gpu_features_variablePointersStorageBuffer", &variablePointersStorageBuffer);

                    ImGui::TableNextColumn();
                    ImGui::Text("variablePointers");
                    ImGui::TableNextColumn();
                    bool variablePointers = vk_11_feats->variablePointers;
                    ImGui::Checkbox("##gpu_features_variablePointers", &variablePointers);

                    ImGui::TableNextColumn();
                    ImGui::Text("protectedMemory");
                    ImGui::TableNextColumn();
                    bool protectedMemory = vk_11_feats->protectedMemory;
                    ImGui::Checkbox("##gpu_features_protectedMemory", &protectedMemory);

                    ImGui::TableNextColumn();
                    ImGui::Text("samplerYcbcrConversion");
                    ImGui::TableNextColumn();
                    bool samplerYcbcrConversion = vk_11_feats->samplerYcbcrConversion;
                    ImGui::Checkbox("##gpu_features_samplerYcbcrConversion", &samplerYcbcrConversion);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderDrawParameters");
                    ImGui::TableNextColumn();
                    bool shaderDrawParameters = vk_11_feats->shaderDrawParameters;
                    ImGui::Checkbox("##gpu_features_shaderDrawParameters", &shaderDrawParameters);


                    ImGui::EndTable();
                }
            }
        }

        if (api_ver >= vulkan::api_1_2)
        {
            if (ImGui::CollapsingHeader("Properties (Vulkan 1.2)"))
            {
                const auto* props = gpu->properties_vulkan_12();
                if (ImGui::BeginTable("gpu_props_vk_12", 2))
                {
                    ImGui::TableSetupColumn("Name");
                    ImGui::TableSetupColumn("Value");
                    ImGui::TableHeadersRow();

                    ImGui::TableNextColumn();
                    ImGui::Text("driverID");
                    ImGui::TableNextColumn();
                    ImGui::Text("%s", string_VkDriverId(props->driverID));

                    ImGui::TableNextColumn();
                    ImGui::Text("driverName");
                    ImGui::TableNextColumn();
                    ImGui::Text("%s", props->driverName);

                    ImGui::TableNextColumn();
                    ImGui::Text("driverInfo");
                    ImGui::TableNextColumn();
                    ImGui::Text("%s", props->driverInfo);

                    ImGui::TableNextColumn();
                    ImGui::Text("conformanceVersion");
                    ImGui::TableNextColumn();
                    {
                        auto ver = props->conformanceVersion;
                        ImGui::Text("%d.%d.%d.%d", ver.major, ver.minor, ver.patch, ver.subminor);
                    }

                    ImGui::TableNextColumn();
                    ImGui::Text("denormBehaviorIndependence");
                    ImGui::TableNextColumn();
                    ImGui::Text("%s", string_VkShaderFloatControlsIndependence(props->denormBehaviorIndependence));

                    ImGui::TableNextColumn();
                    ImGui::Text("roundingModeIndependence");
                    ImGui::TableNextColumn();
                    ImGui::Text("%s", string_VkShaderFloatControlsIndependence(props->roundingModeIndependence));

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderSignedZeroInfNanPreserveFloat16");
                    ImGui::TableNextColumn();
                    bool shaderSignedZeroInfNanPreserveFloat16 = props->shaderSignedZeroInfNanPreserveFloat16;
                    ImGui::Checkbox("##gpu_props_vk12_shaderSignedZeroInfNanPreserveFloat16", &shaderSignedZeroInfNanPreserveFloat16);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderSignedZeroInfNanPreserveFloat32");
                    ImGui::TableNextColumn();
                    bool shaderSignedZeroInfNanPreserveFloat32 = props->shaderSignedZeroInfNanPreserveFloat32;
                    ImGui::Checkbox("##gpu_props_vk12_shaderSignedZeroInfNanPreserveFloat32", &shaderSignedZeroInfNanPreserveFloat32);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderSignedZeroInfNanPreserveFloat64");
                    ImGui::TableNextColumn();
                    bool shaderSignedZeroInfNanPreserveFloat64 = props->shaderSignedZeroInfNanPreserveFloat64;
                    ImGui::Checkbox("##gpu_props_vk12_shaderSignedZeroInfNanPreserveFloat64", &shaderSignedZeroInfNanPreserveFloat64);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderDenormPreserveFloat16");
                    ImGui::TableNextColumn();
                    bool shaderDenormPreserveFloat16 = props->shaderDenormPreserveFloat16;
                    ImGui::Checkbox("##gpu_props_vk12_shaderDenormPreserveFloat16", &shaderDenormPreserveFloat16);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderDenormPreserveFloat32");
                    ImGui::TableNextColumn();
                    bool shaderDenormPreserveFloat32 = props->shaderDenormPreserveFloat32;
                    ImGui::Checkbox("##gpu_props_vk12_shaderDenormPreserveFloat32", &shaderDenormPreserveFloat32);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderDenormPreserveFloat64");
                    ImGui::TableNextColumn();
                    bool shaderDenormPreserveFloat64 = props->shaderDenormPreserveFloat64;
                    ImGui::Checkbox("##gpu_props_vk12_shaderDenormPreserveFloat64", &shaderDenormPreserveFloat64);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderDenormFlushToZeroFloat16");
                    ImGui::TableNextColumn();
                    bool shaderDenormFlushToZeroFloat16 = props->shaderDenormFlushToZeroFloat16;
                    ImGui::Checkbox("##gpu_props_vk12_shaderDenormFlushToZeroFloat16", &shaderDenormFlushToZeroFloat16);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderDenormFlushToZeroFloat32");
                    ImGui::TableNextColumn();
                    bool shaderDenormFlushToZeroFloat32 = props->shaderDenormFlushToZeroFloat32;
                    ImGui::Checkbox("##gpu_props_vk12_shaderDenormFlushToZeroFloat32", &shaderDenormFlushToZeroFloat32);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderDenormFlushToZeroFloat64");
                    ImGui::TableNextColumn();
                    bool shaderDenormFlushToZeroFloat64 = props->shaderDenormFlushToZeroFloat64;
                    ImGui::Checkbox("##gpu_props_vk12_shaderDenormFlushToZeroFloat64", &shaderDenormFlushToZeroFloat64);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderRoundingModeRTEFloat16");
                    ImGui::TableNextColumn();
                    bool shaderRoundingModeRTEFloat16 = props->shaderRoundingModeRTEFloat16;
                    ImGui::Checkbox("##gpu_props_vk12_shaderRoundingModeRTEFloat16", &shaderRoundingModeRTEFloat16);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderRoundingModeRTEFloat32");
                    ImGui::TableNextColumn();
                    bool shaderRoundingModeRTEFloat32 = props->shaderRoundingModeRTEFloat32;
                    ImGui::Checkbox("##gpu_props_vk12_shaderRoundingModeRTEFloat32", &shaderRoundingModeRTEFloat32);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderRoundingModeRTEFloat64");
                    ImGui::TableNextColumn();
                    bool shaderRoundingModeRTEFloat64 = props->shaderRoundingModeRTEFloat64;
                    ImGui::Checkbox("##gpu_props_vk12_shaderRoundingModeRTEFloat64", &shaderRoundingModeRTEFloat64);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderRoundingModeRTZFloat16");
                    ImGui::TableNextColumn();
                    bool shaderRoundingModeRTZFloat16 = props->shaderRoundingModeRTZFloat16;
                    ImGui::Checkbox("##gpu_props_vk12_shaderRoundingModeRTZFloat16", &shaderRoundingModeRTZFloat16);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderRoundingModeRTZFloat32");
                    ImGui::TableNextColumn();
                    bool shaderRoundingModeRTZFloat32 = props->shaderRoundingModeRTZFloat32;
                    ImGui::Checkbox("##gpu_props_vk12_shaderRoundingModeRTZFloat32", &shaderRoundingModeRTZFloat32);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderRoundingModeRTZFloat64");
                    ImGui::TableNextColumn();
                    bool shaderRoundingModeRTZFloat64 = props->shaderRoundingModeRTZFloat64;
                    ImGui::Checkbox("##gpu_props_vk12_shaderRoundingModeRTZFloat64", &shaderRoundingModeRTZFloat64);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxUpdateAfterBindDescriptorsInAllPools");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxUpdateAfterBindDescriptorsInAllPools);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderUniformBufferArrayNonUniformIndexingNative");
                    ImGui::TableNextColumn();
                    bool shaderUniformBufferArrayNonUniformIndexingNative = props->shaderUniformBufferArrayNonUniformIndexingNative;
                    ImGui::Checkbox("##gpu_props_vk12_shaderUniformBufferArrayNonUniformIndexingNative", &shaderUniformBufferArrayNonUniformIndexingNative);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderSampledImageArrayNonUniformIndexingNative");
                    ImGui::TableNextColumn();
                    bool shaderSampledImageArrayNonUniformIndexingNative = props->shaderSampledImageArrayNonUniformIndexingNative;
                    ImGui::Checkbox("##gpu_props_vk12_shaderSampledImageArrayNonUniformIndexingNative", &shaderSampledImageArrayNonUniformIndexingNative);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderStorageBufferArrayNonUniformIndexingNative");
                    ImGui::TableNextColumn();
                    bool shaderStorageBufferArrayNonUniformIndexingNative = props->shaderStorageBufferArrayNonUniformIndexingNative;
                    ImGui::Checkbox("##gpu_props_vk12_shaderStorageBufferArrayNonUniformIndexingNative", &shaderStorageBufferArrayNonUniformIndexingNative);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderStorageImageArrayNonUniformIndexingNative");
                    ImGui::TableNextColumn();
                    bool shaderStorageImageArrayNonUniformIndexingNative = props->shaderStorageImageArrayNonUniformIndexingNative;
                    ImGui::Checkbox("##gpu_props_vk12_shaderStorageImageArrayNonUniformIndexingNative", &shaderStorageImageArrayNonUniformIndexingNative);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderInputAttachmentArrayNonUniformIndexingNative");
                    ImGui::TableNextColumn();
                    bool shaderInputAttachmentArrayNonUniformIndexingNative = props->shaderInputAttachmentArrayNonUniformIndexingNative;
                    ImGui::Checkbox("##gpu_props_vk12_shaderInputAttachmentArrayNonUniformIndexingNative", &shaderInputAttachmentArrayNonUniformIndexingNative);

                    ImGui::TableNextColumn();
                    ImGui::Text("robustBufferAccessUpdateAfterBind");
                    ImGui::TableNextColumn();
                    bool robustBufferAccessUpdateAfterBind = props->robustBufferAccessUpdateAfterBind;
                    ImGui::Checkbox("##gpu_props_vk12_robustBufferAccessUpdateAfterBind", &robustBufferAccessUpdateAfterBind);

                    ImGui::TableNextColumn();
                    ImGui::Text("quadDivergentImplicitLod");
                    ImGui::TableNextColumn();
                    bool quadDivergentImplicitLod = props->quadDivergentImplicitLod;
                    ImGui::Checkbox("##gpu_props_vk12_quadDivergentImplicitLod", &quadDivergentImplicitLod);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxPerStageDescriptorUpdateAfterBindSamplers");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxPerStageDescriptorUpdateAfterBindSamplers);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxPerStageDescriptorUpdateAfterBindUniformBuffers");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxPerStageDescriptorUpdateAfterBindUniformBuffers);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxPerStageDescriptorUpdateAfterBindStorageBuffers");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxPerStageDescriptorUpdateAfterBindStorageBuffers);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxPerStageDescriptorUpdateAfterBindSampledImages");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxPerStageDescriptorUpdateAfterBindSampledImages);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxPerStageDescriptorUpdateAfterBindStorageImages");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxPerStageDescriptorUpdateAfterBindStorageImages);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxPerStageDescriptorUpdateAfterBindInputAttachments");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxPerStageDescriptorUpdateAfterBindInputAttachments);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxPerStageUpdateAfterBindResources");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxPerStageUpdateAfterBindResources);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxDescriptorSetUpdateAfterBindSamplers");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxDescriptorSetUpdateAfterBindSamplers);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxDescriptorSetUpdateAfterBindUniformBuffers");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxDescriptorSetUpdateAfterBindUniformBuffers);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxDescriptorSetUpdateAfterBindUniformBuffersDynamic");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxDescriptorSetUpdateAfterBindUniformBuffersDynamic);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxDescriptorSetUpdateAfterBindStorageBuffers");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxDescriptorSetUpdateAfterBindStorageBuffers);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxDescriptorSetUpdateAfterBindStorageBuffersDynamic");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxDescriptorSetUpdateAfterBindStorageBuffersDynamic);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxDescriptorSetUpdateAfterBindSampledImages");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxDescriptorSetUpdateAfterBindSampledImages);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxDescriptorSetUpdateAfterBindStorageImages");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxDescriptorSetUpdateAfterBindStorageImages);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxDescriptorSetUpdateAfterBindInputAttachments");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxDescriptorSetUpdateAfterBindInputAttachments);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxDescriptorSetUpdateAfterBindInputAttachments");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxDescriptorSetUpdateAfterBindInputAttachments);

                    ImGui::TableNextColumn();
                    ImGui::Text("supportedDepthResolveModes");
                    ImGui::TableNextColumn();
                    auto supported_depth_resolve_mods = string_VkResolveModeFlags(props->supportedDepthResolveModes);
                    ImGui::Text("%s", str_pipe_to_new_line(supported_depth_resolve_mods).c_str());

                    ImGui::TableNextColumn();
                    ImGui::Text("supportedStencilResolveModes");
                    ImGui::TableNextColumn();
                    auto supported_stencil_resolve_mods = string_VkResolveModeFlags(props->supportedStencilResolveModes);
                    ImGui::Text("%s", str_pipe_to_new_line(supported_stencil_resolve_mods).c_str());

                    ImGui::TableNextColumn();
                    ImGui::Text("independentResolveNone");
                    ImGui::TableNextColumn();
                    bool independentResolveNone = props->independentResolveNone;
                    ImGui::Checkbox("##gpu_props_vk12_independentResolveNone", &independentResolveNone);

                    ImGui::TableNextColumn();
                    ImGui::Text("independentResolve");
                    ImGui::TableNextColumn();
                    bool independentResolve = props->independentResolve;
                    ImGui::Checkbox("##gpu_props_vk12_independentResolve", &independentResolve);

                    ImGui::TableNextColumn();
                    ImGui::Text("filterMinmaxSingleComponentFormats");
                    ImGui::TableNextColumn();
                    bool filterMinmaxSingleComponentFormats = props->filterMinmaxSingleComponentFormats;
                    ImGui::Checkbox("##gpu_props_vk12_filterMinmaxSingleComponentFormats", &filterMinmaxSingleComponentFormats);

                    ImGui::TableNextColumn();
                    ImGui::Text("filterMinmaxImageComponentMapping");
                    ImGui::TableNextColumn();
                    bool filterMinmaxImageComponentMapping = props->filterMinmaxImageComponentMapping;
                    ImGui::Checkbox("##gpu_props_vk12_filterMinmaxImageComponentMapping", &filterMinmaxImageComponentMapping);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxTimelineSemaphoreValueDifference");
                    ImGui::TableNextColumn();
                    ImGui::Text("%zu", props->maxTimelineSemaphoreValueDifference);

                    ImGui::TableNextColumn();
                    ImGui::Text("framebufferIntegerColorSampleCounts");
                    ImGui::TableNextColumn();
                    auto frame_buffer_int_color_sample_counts = string_VkSampleCountFlags(props->framebufferIntegerColorSampleCounts);
                    ImGui::TextWrapped("%s", str_pipe_to_new_line(frame_buffer_int_color_sample_counts).c_str());

                    ImGui::EndTable();
                }
            }

            if (ImGui::CollapsingHeader("Features (Vulkan 1.2)"))
            {
                const auto* feats = gpu->features_vulkan_12();
                if (ImGui::BeginTable("gpu_feats_vk_12", 2))
                {
                    ImGui::TableSetupColumn("Name");
                    ImGui::TableSetupColumn("Value");
                    ImGui::TableHeadersRow();

                    ImGui::TableNextColumn();
                    ImGui::Text("samplerMirrorClampToEdge");
                    ImGui::TableNextColumn();
                    bool samplerMirrorClampToEdge = feats->samplerMirrorClampToEdge;
                    ImGui::Checkbox("##gpu_feats_vk12_samplerMirrorClampToEdge", &samplerMirrorClampToEdge);

                    ImGui::TableNextColumn();
                    ImGui::Text("drawIndirectCount");
                    ImGui::TableNextColumn();
                    bool drawIndirectCount = feats->drawIndirectCount;
                    ImGui::Checkbox("##gpu_feats_vk12_drawIndirectCount", &drawIndirectCount);

                    ImGui::TableNextColumn();
                    ImGui::Text("storageBuffer8BitAccess");
                    ImGui::TableNextColumn();
                    bool storageBuffer8BitAccess = feats->storageBuffer8BitAccess;
                    ImGui::Checkbox("##gpu_feats_vk12_storageBuffer8BitAccess", &storageBuffer8BitAccess);

                    ImGui::TableNextColumn();
                    ImGui::Text("uniformAndStorageBuffer8BitAccess");
                    ImGui::TableNextColumn();
                    bool uniformAndStorageBuffer8BitAccess = feats->uniformAndStorageBuffer8BitAccess;
                    ImGui::Checkbox("##gpu_feats_vk12_uniformAndStorageBuffer8BitAccess", &uniformAndStorageBuffer8BitAccess);

                    ImGui::TableNextColumn();
                    ImGui::Text("storagePushConstant8");
                    ImGui::TableNextColumn();
                    bool storagePushConstant8 = feats->storagePushConstant8;
                    ImGui::Checkbox("##gpu_feats_vk12_storagePushConstant8", &storagePushConstant8);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderBufferInt64Atomics");
                    ImGui::TableNextColumn();
                    bool shaderBufferInt64Atomics = feats->shaderBufferInt64Atomics;
                    ImGui::Checkbox("##gpu_feats_vk12_shaderBufferInt64Atomics", &shaderBufferInt64Atomics);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderSharedInt64Atomics");
                    ImGui::TableNextColumn();
                    bool shaderSharedInt64Atomics = feats->shaderSharedInt64Atomics;
                    ImGui::Checkbox("##gpu_feats_vk12_shaderSharedInt64Atomics", &shaderSharedInt64Atomics);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderFloat16");
                    ImGui::TableNextColumn();
                    bool shaderFloat16 = feats->shaderFloat16;
                    ImGui::Checkbox("##gpu_feats_vk12_shaderFloat16", &shaderFloat16);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderInt8");
                    ImGui::TableNextColumn();
                    bool shaderInt8 = feats->shaderInt8;
                    ImGui::Checkbox("##gpu_feats_vk12_shaderInt8", &shaderInt8);

                    ImGui::TableNextColumn();
                    ImGui::Text("descriptorIndexing");
                    ImGui::TableNextColumn();
                    bool descriptorIndexing = feats->descriptorIndexing;
                    ImGui::Checkbox("##gpu_feats_vk12_descriptorIndexing", &descriptorIndexing);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderInputAttachmentArrayDynamicIndexing");
                    ImGui::TableNextColumn();
                    bool shaderInputAttachmentArrayDynamicIndexing = feats->shaderInputAttachmentArrayDynamicIndexing;
                    ImGui::Checkbox("##gpu_feats_vk12_shaderInputAttachmentArrayDynamicIndexing", &shaderInputAttachmentArrayDynamicIndexing);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderUniformTexelBufferArrayDynamicIndexing");
                    ImGui::TableNextColumn();
                    bool shaderUniformTexelBufferArrayDynamicIndexing = feats->shaderUniformTexelBufferArrayDynamicIndexing;
                    ImGui::Checkbox("##gpu_feats_vk12_shaderUniformTexelBufferArrayDynamicIndexing", &shaderUniformTexelBufferArrayDynamicIndexing);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderStorageTexelBufferArrayDynamicIndexing");
                    ImGui::TableNextColumn();
                    bool shaderStorageTexelBufferArrayDynamicIndexing = feats->shaderStorageTexelBufferArrayDynamicIndexing;
                    ImGui::Checkbox("##gpu_feats_vk12_shaderStorageTexelBufferArrayDynamicIndexing", &shaderStorageTexelBufferArrayDynamicIndexing);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderUniformBufferArrayNonUniformIndexing");
                    ImGui::TableNextColumn();
                    bool shaderUniformBufferArrayNonUniformIndexing = feats->shaderUniformBufferArrayNonUniformIndexing;
                    ImGui::Checkbox("##gpu_feats_vk12_shaderUniformBufferArrayNonUniformIndexing", &shaderUniformBufferArrayNonUniformIndexing);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderSampledImageArrayNonUniformIndexing");
                    ImGui::TableNextColumn();
                    bool shaderSampledImageArrayNonUniformIndexing = feats->shaderSampledImageArrayNonUniformIndexing;
                    ImGui::Checkbox("##gpu_feats_vk12_shaderSampledImageArrayNonUniformIndexing", &shaderSampledImageArrayNonUniformIndexing);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderStorageBufferArrayNonUniformIndexing");
                    ImGui::TableNextColumn();
                    bool shaderStorageBufferArrayNonUniformIndexing = feats->shaderStorageBufferArrayNonUniformIndexing;
                    ImGui::Checkbox("##gpu_feats_vk12_shaderStorageBufferArrayNonUniformIndexing", &shaderStorageBufferArrayNonUniformIndexing);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderStorageImageArrayNonUniformIndexing");
                    ImGui::TableNextColumn();
                    bool shaderStorageImageArrayNonUniformIndexing = feats->shaderStorageImageArrayNonUniformIndexing;
                    ImGui::Checkbox("##gpu_feats_vk12_shaderStorageImageArrayNonUniformIndexing", &shaderStorageImageArrayNonUniformIndexing);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderInputAttachmentArrayNonUniformIndexing");
                    ImGui::TableNextColumn();
                    bool shaderInputAttachmentArrayNonUniformIndexing = feats->shaderInputAttachmentArrayNonUniformIndexing;
                    ImGui::Checkbox("##gpu_feats_vk12_shaderInputAttachmentArrayNonUniformIndexing", &shaderInputAttachmentArrayNonUniformIndexing);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderUniformTexelBufferArrayNonUniformIndexing");
                    ImGui::TableNextColumn();
                    bool shaderUniformTexelBufferArrayNonUniformIndexing = feats->shaderUniformTexelBufferArrayNonUniformIndexing;
                    ImGui::Checkbox("##gpu_feats_vk12_shaderUniformTexelBufferArrayNonUniformIndexing", &shaderUniformTexelBufferArrayNonUniformIndexing);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderStorageTexelBufferArrayNonUniformIndexing");
                    ImGui::TableNextColumn();
                    bool shaderStorageTexelBufferArrayNonUniformIndexing = feats->shaderStorageTexelBufferArrayNonUniformIndexing;
                    ImGui::Checkbox("##gpu_feats_vk12_shaderStorageTexelBufferArrayNonUniformIndexing", &shaderStorageTexelBufferArrayNonUniformIndexing);

                    ImGui::TableNextColumn();
                    ImGui::Text("descriptorBindingUniformBufferUpdateAfterBind");
                    ImGui::TableNextColumn();
                    bool descriptorBindingUniformBufferUpdateAfterBind = feats->descriptorBindingUniformBufferUpdateAfterBind;
                    ImGui::Checkbox("##gpu_feats_vk12_descriptorBindingUniformBufferUpdateAfterBind", &descriptorBindingUniformBufferUpdateAfterBind);

                    ImGui::TableNextColumn();
                    ImGui::Text("descriptorBindingSampledImageUpdateAfterBind");
                    ImGui::TableNextColumn();
                    bool descriptorBindingSampledImageUpdateAfterBind = feats->descriptorBindingSampledImageUpdateAfterBind;
                    ImGui::Checkbox("##gpu_feats_vk12_descriptorBindingSampledImageUpdateAfterBind", &descriptorBindingSampledImageUpdateAfterBind);

                    ImGui::TableNextColumn();
                    ImGui::Text("descriptorBindingStorageImageUpdateAfterBind");
                    ImGui::TableNextColumn();
                    bool descriptorBindingStorageImageUpdateAfterBind = feats->descriptorBindingStorageImageUpdateAfterBind;
                    ImGui::Checkbox("##gpu_feats_vk12_descriptorBindingStorageImageUpdateAfterBind", &descriptorBindingStorageImageUpdateAfterBind);

                    ImGui::TableNextColumn();
                    ImGui::Text("descriptorBindingStorageBufferUpdateAfterBind");
                    ImGui::TableNextColumn();
                    bool descriptorBindingStorageBufferUpdateAfterBind = feats->descriptorBindingStorageBufferUpdateAfterBind;
                    ImGui::Checkbox("##gpu_feats_vk12_descriptorBindingStorageBufferUpdateAfterBind", &descriptorBindingStorageBufferUpdateAfterBind);

                    ImGui::TableNextColumn();
                    ImGui::Text("descriptorBindingUniformTexelBufferUpdateAfterBind");
                    ImGui::TableNextColumn();
                    bool descriptorBindingUniformTexelBufferUpdateAfterBind = feats->descriptorBindingUniformTexelBufferUpdateAfterBind;
                    ImGui::Checkbox("##gpu_feats_vk12_descriptorBindingUniformTexelBufferUpdateAfterBind", &descriptorBindingUniformTexelBufferUpdateAfterBind);

                    ImGui::TableNextColumn();
                    ImGui::Text("descriptorBindingStorageTexelBufferUpdateAfterBind");
                    ImGui::TableNextColumn();
                    bool descriptorBindingStorageTexelBufferUpdateAfterBind = feats->descriptorBindingStorageTexelBufferUpdateAfterBind;
                    ImGui::Checkbox("##gpu_feats_vk12_descriptorBindingStorageTexelBufferUpdateAfterBind", &descriptorBindingStorageTexelBufferUpdateAfterBind);

                    ImGui::TableNextColumn();
                    ImGui::Text("descriptorBindingUpdateUnusedWhilePending");
                    ImGui::TableNextColumn();
                    bool descriptorBindingUpdateUnusedWhilePending = feats->descriptorBindingUpdateUnusedWhilePending;
                    ImGui::Checkbox("##gpu_feats_vk12_descriptorBindingUpdateUnusedWhilePending", &descriptorBindingUpdateUnusedWhilePending);

                    ImGui::TableNextColumn();
                    ImGui::Text("descriptorBindingPartiallyBound");
                    ImGui::TableNextColumn();
                    bool descriptorBindingPartiallyBound = feats->descriptorBindingPartiallyBound;
                    ImGui::Checkbox("##gpu_feats_vk12_descriptorBindingPartiallyBound", &descriptorBindingPartiallyBound);

                    ImGui::TableNextColumn();
                    ImGui::Text("descriptorBindingVariableDescriptorCount");
                    ImGui::TableNextColumn();
                    bool descriptorBindingVariableDescriptorCount = feats->descriptorBindingVariableDescriptorCount;
                    ImGui::Checkbox("##gpu_feats_vk12_descriptorBindingVariableDescriptorCount", &descriptorBindingVariableDescriptorCount);

                    ImGui::TableNextColumn();
                    ImGui::Text("runtimeDescriptorArray");
                    ImGui::TableNextColumn();
                    bool runtimeDescriptorArray = feats->runtimeDescriptorArray;
                    ImGui::Checkbox("##gpu_feats_vk12_runtimeDescriptorArray", &runtimeDescriptorArray);

                    ImGui::TableNextColumn();
                    ImGui::Text("samplerFilterMinmax");
                    ImGui::TableNextColumn();
                    bool samplerFilterMinmax = feats->samplerFilterMinmax;
                    ImGui::Checkbox("##gpu_feats_vk12_samplerFilterMinmax", &samplerFilterMinmax);

                    ImGui::TableNextColumn();
                    ImGui::Text("scalarBlockLayout");
                    ImGui::TableNextColumn();
                    bool scalarBlockLayout = feats->scalarBlockLayout;
                    ImGui::Checkbox("##gpu_feats_vk12_scalarBlockLayout", &scalarBlockLayout);

                    ImGui::TableNextColumn();
                    ImGui::Text("imagelessFramebuffer");
                    ImGui::TableNextColumn();
                    bool imagelessFramebuffer = feats->imagelessFramebuffer;
                    ImGui::Checkbox("##gpu_feats_vk12_imagelessFramebuffer", &imagelessFramebuffer);

                    ImGui::TableNextColumn();
                    ImGui::Text("uniformBufferStandardLayout");
                    ImGui::TableNextColumn();
                    bool uniformBufferStandardLayout = feats->uniformBufferStandardLayout;
                    ImGui::Checkbox("##gpu_feats_vk12_uniformBufferStandardLayout", &uniformBufferStandardLayout);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderSubgroupExtendedTypes");
                    ImGui::TableNextColumn();
                    bool shaderSubgroupExtendedTypes = feats->shaderSubgroupExtendedTypes;
                    ImGui::Checkbox("##gpu_feats_vk12_shaderSubgroupExtendedTypes", &shaderSubgroupExtendedTypes);

                    ImGui::TableNextColumn();
                    ImGui::Text("separateDepthStencilLayouts");
                    ImGui::TableNextColumn();
                    bool separateDepthStencilLayouts = feats->separateDepthStencilLayouts;
                    ImGui::Checkbox("##gpu_feats_vk12_separateDepthStencilLayouts", &separateDepthStencilLayouts);

                    ImGui::TableNextColumn();
                    ImGui::Text("hostQueryReset");
                    ImGui::TableNextColumn();
                    bool hostQueryReset = feats->hostQueryReset;
                    ImGui::Checkbox("##gpu_feats_vk12_hostQueryReset", &hostQueryReset);

                    ImGui::TableNextColumn();
                    ImGui::Text("timelineSemaphore");
                    ImGui::TableNextColumn();
                    bool timelineSemaphore = feats->timelineSemaphore;
                    ImGui::Checkbox("##gpu_feats_vk12_timelineSemaphore", &timelineSemaphore);

                    ImGui::TableNextColumn();
                    ImGui::Text("bufferDeviceAddress");
                    ImGui::TableNextColumn();
                    bool bufferDeviceAddress = feats->bufferDeviceAddress;
                    ImGui::Checkbox("##gpu_feats_vk12_bufferDeviceAddress", &bufferDeviceAddress);

                    ImGui::TableNextColumn();
                    ImGui::Text("bufferDeviceAddressCaptureReplay");
                    ImGui::TableNextColumn();
                    bool bufferDeviceAddressCaptureReplay = feats->bufferDeviceAddressCaptureReplay;
                    ImGui::Checkbox("##gpu_feats_vk12_bufferDeviceAddressCaptureReplay", &bufferDeviceAddressCaptureReplay);

                    ImGui::TableNextColumn();
                    ImGui::Text("bufferDeviceAddressMultiDevice");
                    ImGui::TableNextColumn();
                    bool bufferDeviceAddressMultiDevice = feats->bufferDeviceAddressMultiDevice;
                    ImGui::Checkbox("##gpu_feats_vk12_bufferDeviceAddressMultiDevice", &bufferDeviceAddressMultiDevice);

                    ImGui::TableNextColumn();
                    ImGui::Text("vulkanMemoryModel");
                    ImGui::TableNextColumn();
                    bool vulkanMemoryModel = feats->vulkanMemoryModel;
                    ImGui::Checkbox("##gpu_feats_vk12_vulkanMemoryModel", &vulkanMemoryModel);

                    ImGui::TableNextColumn();
                    ImGui::Text("vulkanMemoryModelDeviceScope");
                    ImGui::TableNextColumn();
                    bool vulkanMemoryModelDeviceScope = feats->vulkanMemoryModelDeviceScope;
                    ImGui::Checkbox("##gpu_feats_vk12_vulkanMemoryModelDeviceScope", &vulkanMemoryModelDeviceScope);

                    ImGui::TableNextColumn();
                    ImGui::Text("vulkanMemoryModelAvailabilityVisibilityChains");
                    ImGui::TableNextColumn();
                    bool vulkanMemoryModelAvailabilityVisibilityChains = feats->vulkanMemoryModelAvailabilityVisibilityChains;
                    ImGui::Checkbox("##gpu_feats_vk12_vulkanMemoryModelAvailabilityVisibilityChains", &vulkanMemoryModelAvailabilityVisibilityChains);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderOutputViewportIndex");
                    ImGui::TableNextColumn();
                    bool shaderOutputViewportIndex = feats->shaderOutputViewportIndex;
                    ImGui::Checkbox("##gpu_feats_vk12_shaderOutputViewportIndex", &shaderOutputViewportIndex);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderOutputLayer");
                    ImGui::TableNextColumn();
                    bool shaderOutputLayer = feats->shaderOutputLayer;
                    ImGui::Checkbox("##gpu_feats_vk12_shaderOutputLayer", &shaderOutputLayer);

                    ImGui::TableNextColumn();
                    ImGui::Text("subgroupBroadcastDynamicId");
                    ImGui::TableNextColumn();
                    bool subgroupBroadcastDynamicId = feats->subgroupBroadcastDynamicId;
                    ImGui::Checkbox("##gpu_feats_vk12_subgroupBroadcastDynamicId", &subgroupBroadcastDynamicId);


                    ImGui::EndTable();
                }
            }
        }

        if (api_ver >= vulkan::api_1_3)
        {
            if (ImGui::CollapsingHeader("Properties (Vulkan 1.3)"))
            {
                const auto* props = gpu->properties_vulkan_13();
                if (ImGui::BeginTable("gpu_props_vk_13", 2))
                {
                    ImGui::TableSetupColumn("Name");
                    ImGui::TableSetupColumn("Value");
                    ImGui::TableHeadersRow();

                    ImGui::TableNextColumn();
                    ImGui::Text("minSubgroupSize");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->minSubgroupSize);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxSubgroupSize");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxSubgroupSize);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxComputeWorkgroupSubgroups");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxComputeWorkgroupSubgroups);

                    ImGui::TableNextColumn();
                    ImGui::Text("requiredSubgroupSizeStages");
                    ImGui::TableNextColumn();
                    {
                        auto req_subgroup_size_stages = string_VkShaderStageFlags(props->requiredSubgroupSizeStages);
                        ImGui::TextWrapped("%s", str_pipe_to_new_line(req_subgroup_size_stages).c_str());
                    }

                    ImGui::TableNextColumn();
                    ImGui::Text("maxInlineUniformBlockSize");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxInlineUniformBlockSize);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxPerStageDescriptorInlineUniformBlocks");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxPerStageDescriptorInlineUniformBlocks);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxPerStageDescriptorUpdateAfterBindInlineUniformBlocks");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxPerStageDescriptorUpdateAfterBindInlineUniformBlocks);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxDescriptorSetInlineUniformBlocks");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxDescriptorSetInlineUniformBlocks);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxDescriptorSetUpdateAfterBindInlineUniformBlocks");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxDescriptorSetUpdateAfterBindInlineUniformBlocks);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxInlineUniformTotalSize");
                    ImGui::TableNextColumn();
                    ImGui::Text("%d", props->maxInlineUniformTotalSize);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProduct8BitUnsignedAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProduct8BitUnsignedAccelerated = props->integerDotProduct8BitUnsignedAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProduct8BitUnsignedAccelerated", &integerDotProduct8BitUnsignedAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProduct8BitSignedAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProduct8BitSignedAccelerated = props->integerDotProduct8BitSignedAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProduct8BitSignedAccelerated", &integerDotProduct8BitSignedAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProduct8BitMixedSignednessAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProduct8BitMixedSignednessAccelerated = props->integerDotProduct8BitMixedSignednessAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProduct8BitMixedSignednessAccelerated", &integerDotProduct8BitMixedSignednessAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProduct4x8BitPackedUnsignedAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProduct4x8BitPackedUnsignedAccelerated = props->integerDotProduct4x8BitPackedUnsignedAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProduct4x8BitPackedUnsignedAccelerated", &integerDotProduct4x8BitPackedUnsignedAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProduct4x8BitPackedSignedAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProduct4x8BitPackedSignedAccelerated = props->integerDotProduct4x8BitPackedSignedAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProduct4x8BitPackedSignedAccelerated", &integerDotProduct4x8BitPackedSignedAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProduct4x8BitPackedMixedSignednessAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProduct4x8BitPackedMixedSignednessAccelerated = props->integerDotProduct4x8BitPackedMixedSignednessAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProduct4x8BitPackedMixedSignednessAccelerated", &integerDotProduct4x8BitPackedMixedSignednessAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProduct16BitUnsignedAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProduct16BitUnsignedAccelerated = props->integerDotProduct16BitUnsignedAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProduct16BitUnsignedAccelerated", &integerDotProduct16BitUnsignedAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProduct16BitSignedAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProduct16BitSignedAccelerated = props->integerDotProduct16BitSignedAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProduct16BitSignedAccelerated", &integerDotProduct16BitSignedAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProduct16BitMixedSignednessAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProduct16BitMixedSignednessAccelerated = props->integerDotProduct16BitMixedSignednessAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProduct16BitMixedSignednessAccelerated", &integerDotProduct16BitMixedSignednessAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProduct32BitUnsignedAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProduct32BitUnsignedAccelerated = props->integerDotProduct32BitUnsignedAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProduct32BitUnsignedAccelerated", &integerDotProduct32BitUnsignedAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProduct32BitSignedAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProduct32BitSignedAccelerated = props->integerDotProduct32BitSignedAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProduct32BitSignedAccelerated", &integerDotProduct32BitSignedAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProduct32BitMixedSignednessAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProduct32BitMixedSignednessAccelerated = props->integerDotProduct32BitMixedSignednessAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProduct32BitMixedSignednessAccelerated", &integerDotProduct32BitMixedSignednessAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProduct64BitUnsignedAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProduct64BitUnsignedAccelerated = props->integerDotProduct64BitUnsignedAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProduct64BitUnsignedAccelerated", &integerDotProduct64BitUnsignedAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProduct64BitSignedAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProduct64BitSignedAccelerated = props->integerDotProduct64BitSignedAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProduct64BitSignedAccelerated", &integerDotProduct64BitSignedAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProduct64BitMixedSignednessAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProduct64BitMixedSignednessAccelerated = props->integerDotProduct64BitMixedSignednessAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProduct64BitMixedSignednessAccelerated", &integerDotProduct64BitMixedSignednessAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProductAccumulatingSaturating8BitUnsignedAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProductAccumulatingSaturating8BitUnsignedAccelerated = props->integerDotProductAccumulatingSaturating8BitUnsignedAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProductAccumulatingSaturating8BitUnsignedAccelerated", &integerDotProductAccumulatingSaturating8BitUnsignedAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProductAccumulatingSaturating8BitSignedAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProductAccumulatingSaturating8BitSignedAccelerated = props->integerDotProductAccumulatingSaturating8BitSignedAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProductAccumulatingSaturating8BitSignedAccelerated", &integerDotProductAccumulatingSaturating8BitSignedAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProductAccumulatingSaturating8BitMixedSignednessAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProductAccumulatingSaturating8BitMixedSignednessAccelerated = props->integerDotProductAccumulatingSaturating8BitMixedSignednessAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProductAccumulatingSaturating8BitMixedSignednessAccelerated", &integerDotProductAccumulatingSaturating8BitMixedSignednessAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProductAccumulatingSaturating4x8BitPackedUnsignedAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProductAccumulatingSaturating4x8BitPackedUnsignedAccelerated = props->integerDotProductAccumulatingSaturating4x8BitPackedUnsignedAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProductAccumulatingSaturating4x8BitPackedUnsignedAccelerated", &integerDotProductAccumulatingSaturating4x8BitPackedUnsignedAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProductAccumulatingSaturating4x8BitPackedSignedAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProductAccumulatingSaturating4x8BitPackedSignedAccelerated = props->integerDotProductAccumulatingSaturating4x8BitPackedSignedAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProductAccumulatingSaturating4x8BitPackedSignedAccelerated", &integerDotProductAccumulatingSaturating4x8BitPackedSignedAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProductAccumulatingSaturating4x8BitPackedMixedSignednessAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProductAccumulatingSaturating4x8BitPackedMixedSignednessAccelerated = props->integerDotProductAccumulatingSaturating4x8BitPackedMixedSignednessAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProductAccumulatingSaturating4x8BitPackedMixedSignednessAccelerated", &integerDotProductAccumulatingSaturating4x8BitPackedMixedSignednessAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProductAccumulatingSaturating16BitUnsignedAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProductAccumulatingSaturating16BitUnsignedAccelerated = props->integerDotProductAccumulatingSaturating16BitUnsignedAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProductAccumulatingSaturating16BitUnsignedAccelerated", &integerDotProductAccumulatingSaturating16BitUnsignedAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProductAccumulatingSaturating16BitSignedAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProductAccumulatingSaturating16BitSignedAccelerated = props->integerDotProductAccumulatingSaturating16BitSignedAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProductAccumulatingSaturating16BitSignedAccelerated", &integerDotProductAccumulatingSaturating16BitSignedAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProductAccumulatingSaturating16BitMixedSignednessAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProductAccumulatingSaturating16BitMixedSignednessAccelerated = props->integerDotProductAccumulatingSaturating16BitMixedSignednessAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProductAccumulatingSaturating16BitMixedSignednessAccelerated", &integerDotProductAccumulatingSaturating16BitMixedSignednessAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProductAccumulatingSaturating32BitUnsignedAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProductAccumulatingSaturating32BitUnsignedAccelerated = props->integerDotProductAccumulatingSaturating32BitUnsignedAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProductAccumulatingSaturating32BitUnsignedAccelerated", &integerDotProductAccumulatingSaturating32BitUnsignedAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProductAccumulatingSaturating32BitSignedAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProductAccumulatingSaturating32BitSignedAccelerated = props->integerDotProductAccumulatingSaturating32BitSignedAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProductAccumulatingSaturating32BitSignedAccelerated", &integerDotProductAccumulatingSaturating32BitSignedAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProductAccumulatingSaturating32BitMixedSignednessAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProductAccumulatingSaturating32BitMixedSignednessAccelerated = props->integerDotProductAccumulatingSaturating32BitMixedSignednessAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProductAccumulatingSaturating32BitMixedSignednessAccelerated", &integerDotProductAccumulatingSaturating32BitMixedSignednessAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProductAccumulatingSaturating64BitUnsignedAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProductAccumulatingSaturating64BitUnsignedAccelerated = props->integerDotProductAccumulatingSaturating64BitUnsignedAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProductAccumulatingSaturating64BitUnsignedAccelerated", &integerDotProductAccumulatingSaturating64BitUnsignedAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProductAccumulatingSaturating64BitSignedAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProductAccumulatingSaturating64BitSignedAccelerated = props->integerDotProductAccumulatingSaturating64BitSignedAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProductAccumulatingSaturating64BitSignedAccelerated", &integerDotProductAccumulatingSaturating64BitSignedAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("integerDotProductAccumulatingSaturating64BitMixedSignednessAccelerated");
                    ImGui::TableNextColumn();
                    bool integerDotProductAccumulatingSaturating64BitMixedSignednessAccelerated = props->integerDotProductAccumulatingSaturating64BitMixedSignednessAccelerated;
                    ImGui::Checkbox("##gpu_props_vk12_integerDotProductAccumulatingSaturating64BitMixedSignednessAccelerated", &integerDotProductAccumulatingSaturating64BitMixedSignednessAccelerated);

                    ImGui::TableNextColumn();
                    ImGui::Text("storageTexelBufferOffsetAlignmentBytes");
                    ImGui::TableNextColumn();
                    ImGui::Text("%zu", props->storageTexelBufferOffsetAlignmentBytes);

                    ImGui::TableNextColumn();
                    ImGui::Text("storageTexelBufferOffsetSingleTexelAlignment");
                    ImGui::TableNextColumn();
                    bool storageTexelBufferOffsetSingleTexelAlignment = props->storageTexelBufferOffsetSingleTexelAlignment;
                    ImGui::Checkbox("##gpu_props_vk12_storageTexelBufferOffsetSingleTexelAlignment", &storageTexelBufferOffsetSingleTexelAlignment);

                    ImGui::TableNextColumn();
                    ImGui::Text("uniformTexelBufferOffsetAlignmentBytes");
                    ImGui::TableNextColumn();
                    ImGui::Text("%zu", props->uniformTexelBufferOffsetAlignmentBytes);

                    ImGui::TableNextColumn();
                    ImGui::Text("uniformTexelBufferOffsetSingleTexelAlignment");
                    ImGui::TableNextColumn();
                    bool uniformTexelBufferOffsetSingleTexelAlignment = props->uniformTexelBufferOffsetSingleTexelAlignment;
                    ImGui::Checkbox("##gpu_props_vk12_uniformTexelBufferOffsetSingleTexelAlignment", &uniformTexelBufferOffsetSingleTexelAlignment);

                    ImGui::TableNextColumn();
                    ImGui::Text("maxBufferSize");
                    ImGui::TableNextColumn();
                    ImGui::Text("%zu", props->maxBufferSize);

                    ImGui::EndTable();
                }
            }

            if (ImGui::CollapsingHeader("Features (Vulkan 1.3)"))
            {
                const auto* feats = gpu->features_vulkan_13();
                if (ImGui::BeginTable("gpu_feats_vk_13", 2))
                {
                    ImGui::TableSetupColumn("Name");
                    ImGui::TableSetupColumn("Value");
                    ImGui::TableHeadersRow();

                    ImGui::TableNextColumn();
                    ImGui::Text("robustImageAccess");
                    ImGui::TableNextColumn();
                    bool robustImageAccess = feats->robustImageAccess;
                    ImGui::Checkbox("##gpu_feats_vk13_robustImageAccess", &robustImageAccess);

                    ImGui::TableNextColumn();
                    ImGui::Text("inlineUniformBlock");
                    ImGui::TableNextColumn();
                    bool inlineUniformBlock = feats->inlineUniformBlock;
                    ImGui::Checkbox("##gpu_feats_vk13_inlineUniformBlock", &inlineUniformBlock);

                    ImGui::TableNextColumn();
                    ImGui::Text("descriptorBindingInlineUniformBlockUpdateAfterBind");
                    ImGui::TableNextColumn();
                    bool descriptorBindingInlineUniformBlockUpdateAfterBind = feats->descriptorBindingInlineUniformBlockUpdateAfterBind;
                    ImGui::Checkbox("##gpu_feats_vk13_descriptorBindingInlineUniformBlockUpdateAfterBind", &descriptorBindingInlineUniformBlockUpdateAfterBind);

                    ImGui::TableNextColumn();
                    ImGui::Text("pipelineCreationCacheControl");
                    ImGui::TableNextColumn();
                    bool pipelineCreationCacheControl = feats->pipelineCreationCacheControl;
                    ImGui::Checkbox("##gpu_feats_vk13_pipelineCreationCacheControl", &pipelineCreationCacheControl);

                    ImGui::TableNextColumn();
                    ImGui::Text("privateData");
                    ImGui::TableNextColumn();
                    bool privateData = feats->privateData;
                    ImGui::Checkbox("##gpu_feats_vk13_privateData", &privateData);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderDemoteToHelperInvocation");
                    ImGui::TableNextColumn();
                    bool shaderDemoteToHelperInvocation = feats->shaderDemoteToHelperInvocation;
                    ImGui::Checkbox("##gpu_feats_vk13_shaderDemoteToHelperInvocation", &shaderDemoteToHelperInvocation);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderTerminateInvocation");
                    ImGui::TableNextColumn();
                    bool shaderTerminateInvocation = feats->shaderTerminateInvocation;
                    ImGui::Checkbox("##gpu_feats_vk13_shaderTerminateInvocation", &shaderTerminateInvocation);

                    ImGui::TableNextColumn();
                    ImGui::Text("subgroupSizeControl");
                    ImGui::TableNextColumn();
                    bool subgroupSizeControl = feats->subgroupSizeControl;
                    ImGui::Checkbox("##gpu_feats_vk13_subgroupSizeControl", &subgroupSizeControl);

                    ImGui::TableNextColumn();
                    ImGui::Text("computeFullSubgroups");
                    ImGui::TableNextColumn();
                    bool computeFullSubgroups = feats->computeFullSubgroups;
                    ImGui::Checkbox("##gpu_feats_vk13_computeFullSubgroups", &computeFullSubgroups);

                    ImGui::TableNextColumn();
                    ImGui::Text("synchronization2");
                    ImGui::TableNextColumn();
                    bool synchronization2 = feats->synchronization2;
                    ImGui::Checkbox("##gpu_feats_vk13_synchronization2", &synchronization2);

                    ImGui::TableNextColumn();
                    ImGui::Text("textureCompressionASTC_HDR");
                    ImGui::TableNextColumn();
                    bool textureCompressionASTC_HDR = feats->textureCompressionASTC_HDR;
                    ImGui::Checkbox("##gpu_feats_vk13_textureCompressionASTC_HDR", &textureCompressionASTC_HDR);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderZeroInitializeWorkgroupMemory");
                    ImGui::TableNextColumn();
                    bool shaderZeroInitializeWorkgroupMemory = feats->shaderZeroInitializeWorkgroupMemory;
                    ImGui::Checkbox("##gpu_feats_vk13_shaderZeroInitializeWorkgroupMemory", &shaderZeroInitializeWorkgroupMemory);

                    ImGui::TableNextColumn();
                    ImGui::Text("dynamicRendering");
                    ImGui::TableNextColumn();
                    bool dynamicRendering = feats->dynamicRendering;
                    ImGui::Checkbox("##gpu_feats_vk13_dynamicRendering", &dynamicRendering);

                    ImGui::TableNextColumn();
                    ImGui::Text("shaderIntegerDotProduct");
                    ImGui::TableNextColumn();
                    bool shaderIntegerDotProduct = feats->shaderIntegerDotProduct;
                    ImGui::Checkbox("##gpu_feats_vk13_shaderIntegerDotProduct", &shaderIntegerDotProduct);

                    ImGui::TableNextColumn();
                    ImGui::Text("maintenance4");
                    ImGui::TableNextColumn();
                    bool maintenance4 = feats->maintenance4;
                    ImGui::Checkbox("##gpu_feats_vk13_maintenance4", &maintenance4);

                    ImGui::EndTable();
                }
            }
        }

        if (ImGui::CollapsingHeader("Memory Properties"))
        {
            auto* mem_props = gpu->memory_properties();
            if (ImGui::BeginTable("gpu_mem_props", 2))
            {
                ImGui::TableSetupColumn("Name");
                ImGui::TableSetupColumn("Value");
                ImGui::TableHeadersRow();

                ImGui::TableNextColumn();
                ImGui::Text("memoryHeapCount");
                ImGui::TableNextColumn();
                ImGui::Text("%d", mem_props->memoryHeapCount);

                ImGui::TableNextColumn();
                ImGui::Text("memoryHeaps");
                ImGui::TableNextColumn();
                for (auto idx = 0u; idx < mem_props->memoryHeapCount; idx++)
                {
                    const auto [size, flags] = mem_props->memoryHeaps[idx];
                    ImGui::Text("[%d] Size = %zu", idx, size);
                    ImGui::BulletText("%s", string_VkMemoryHeapFlags(flags).c_str());
                }

                ImGui::TableNextColumn();
                ImGui::Text("memoryTypeCount");
                ImGui::TableNextColumn();
                ImGui::Text("%d", mem_props->memoryTypeCount);
                for (auto idx = 0u; idx < mem_props->memoryTypeCount; idx++)
                {
                    const auto [flags, index] = mem_props->memoryTypes[idx];
                    ImGui::Text("[%d] Index = %d", idx, index);
                    ImGui::BulletText("%s", string_VkMemoryHeapFlags(flags).c_str());
                }

                ImGui::EndTable();
            }
        }
    }

    ImGui::End();
}
