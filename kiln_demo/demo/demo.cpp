#include <array>
#include <memory>
#include <ranges>
#include <kiln/kiln.h>
#include <kiln/renderer.hpp>
#include <kiln/vulkan/imgui_vk_render_extension.h>
#include <kiln/vulkan/vk_types.h>
#include <kiln/imgui/imgui.h>
#include "vulkan_ui.h"
//-----------------
#include "demo.hpp"

using namespace kiln;

namespace
{
    std::array rect_vertices =
    {
        vulkan::vertex{ .position = {  0.5f, -0.5f, 0.f }, .color = {  0.f,  0.f,  0.f,  1.f }, },
        vulkan::vertex{ .position = {  0.5f,  0.5f, 0.f }, .color = {  0.5f, 0.5f, 0.5f, 1.f }, },
        vulkan::vertex{ .position = { -0.5f, -0.5f, 0.f }, .color = {  1.f,  0.f,  0.f,  1.f }, },
        vulkan::vertex{ .position = { -0.5f,  0.5f, 0.f }, .color = {  0.f,  1.f,  0.f,  1.f }, },
    };

    std::array rect_indices =
    {
        0u, 1u, 2u,
        2u, 1u, 3u,
    };
}

demo::demo(SDL_Window* window, vulkan::vk_instance_config* instance_config,
    const std::filesystem::path& assets_root_dir) :
    _window(window), _instance_config(instance_config)
{
    _config.assets_root_directory = assets_root_dir;
    _config.screen_mode           = screen_mode::vsync;

    initialize_for_vulkan(window, &_config, instance_config);
}

demo::~demo()
{
    shutdown();
}

const screen_mode& demo::screen_mode() const
{
    return _config.screen_mode;
}

void demo::screen_mode(const kiln::screen_mode mode)
{
    _config.screen_mode = mode;
}

int demo::run()
{
    auto* render = vulkan::get_renderer();
    auto& clear_color = render->clear_color();
    clear_color.g = 0.5f;
    clear_color.b = 1.0f;

    render->add_extension([&](vulkan::vk_render_extension_create_args& args)
    {
        return std::make_unique<vulkan::imgui_vk_render_extension>(_window, render->immediate(), args);
    });

    //auto rect = render->immediate()->upload_mesh(rect_indices, rect_vertices);

    const auto basic_meshes = vulkan::assets()->load_gltf_meshes(L"vkguide/basicmesh.glb");

    const auto* imgui_ext = render->get_extension("imgui");
    _ui.enabled = imgui_ext != nullptr && imgui_ext->is_enabled();

    SDL_Event e;
    _run = true;
    while (_run)
    {
        poll_events(e, render);

        // If quit was requested, we don't want to continue with any more
        // rendering operations.
        if (!_run) break;
        if (render->is_paused()) continue;

        render->frame_begin();
        draw_ui(render);
        if (_ui.show_meshes_window) draw_window_meshes(basic_meshes.value());

        render->draw_background();
        if (_selected_mesh.mesh != nullptr)
        {
            render->draw_mesh(_selected_mesh.mesh, _selected_mesh.apply_world_transform);
        }

        render->frame_end();
    }

    return 0;
}

void demo::draw_mesh(vulkan::vk_renderer* render, const std::vector<std::shared_ptr<vulkan::mesh_asset>>& meshes)
{
    const auto& suzanne = meshes.back();
    render->draw_mesh(suzanne.get());
}

void demo::draw_ui(vulkan::vk_renderer* render)
{
    const auto ext = render->get_extension("imgui");
    if (ext == nullptr || !ext->is_enabled())
        return;

    // We don't want to attempt to draw the UI if a swapchain
    // rebuild was requested.
    if (vulkan::get_swapchain()->requires_rebuild())
        return;

    if (ImGui::BeginMainMenuBar())
    {
        if (ImGui::BeginMenu("Render"))
        {
            if (ImGui::BeginMenu("Vulkan"))
            {
                ImGui::MenuItem("Instance", nullptr, &_ui.show_vulkan_instance_window);
                ImGui::MenuItem("GPU", nullptr, &_ui.show_vulkan_gpu_window);
                ImGui::EndMenu();
            }
            ImGui::Separator();
            ImGui::MenuItem("Camera", nullptr, &_ui.show_camera_window);
            ImGui::MenuItem("Background", nullptr, &_ui_show_background_window);
            ImGui::MenuItem("Meshes", nullptr, &_ui.show_meshes_window);
            ImGui::Separator();
            ImGui::MenuItem("Extensions", nullptr, &_ui_show_extensions_window);

            ImGui::Separator();
            if (ImGui::MenuItem("Exit"))
                _run = !_run;

            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("Tools"))
        {
            ImGui::MenuItem("ImGui Demo", nullptr, &_ui_show_imgui_demo);
            ImGui::EndMenu();
        }

        ImGui::EndMainMenuBar();
    }

    if (_ui.show_camera_window) draw_window_camera(render);

    if (_ui_show_imgui_demo) ImGui::ShowDemoWindow(&_ui_show_imgui_demo);
    if (_ui_show_background_window) ui_show_background_window(render);
    if (_ui_show_extensions_window) ui_show_extensions_window(render);
    if (_ui.show_vulkan_instance_window) draw_window_vulkan_instance(vulkan::get_instance());
    if (_ui.show_vulkan_gpu_window) draw_window_vulkan_gpu(vulkan::get_gpu());
}

void demo::poll_events(SDL_Event& e, vulkan::vk_renderer* render)
{
    while (SDL_PollEvent(&e))
    {
        if (e.type == SDL_QUIT)
        {
            _run = false;
            break;
        }

        if (e.type == SDL_WINDOWEVENT)
        {
            switch (e.window.event)
            {
            case SDL_WINDOWEVENT_MINIMIZED:
                render->pause();
                break;
            case SDL_WINDOWEVENT_RESTORED:
                render->resume();
                break;
            default:
                break;
            }
        }

        render->handle_event(&e);
    }
}

void demo::ui_show_background_window(vulkan::vk_renderer* render)
{
    ImGui::SetNextWindowSize(ImVec2{ 300, 200 }, ImGuiCond_FirstUseEver);
    if (ImGui::Begin("Background Values", &_ui_show_background_window))
    {
        auto* selected_effect = render->background_effect();
        const auto effect_selected = selected_effect != nullptr;

        const auto& effects = render->background_effects();
        if (ImGui::BeginCombo("Background Effect", effect_selected ? selected_effect->name().c_str() : "none"))
        {
            for (auto& effect : effects)
            {
                if (ImGui::Selectable(effect->name().c_str(), effect_selected && effect->name() == selected_effect->name()))
                {
                    render->set_background_effect(effect.get());
                }
            }

            if (ImGui::Selectable("none", !effect_selected))
            {
                render->set_background_effect(nullptr);
            }

            ImGui::EndCombo();
        }

        if (effect_selected)
        {
            ImGui::Separator();

            auto* values = selected_effect->push_constants();
            ImGui::DragFloat4("data1", reinterpret_cast<float*>(&values->data1), 0.01f, 0.f, 1.f);
            ImGui::DragFloat4("data2", reinterpret_cast<float*>(&values->data2), 0.01f, 0.f, 1.f);
            ImGui::DragFloat4("data3", reinterpret_cast<float*>(&values->data3), 0.01f, 0.f, 1.f);
            ImGui::DragFloat4("data4", reinterpret_cast<float*>(&values->data4), 0.01f, 0.f, 1.f);
        }
        else
        {
            auto& clear = render->clear_color();
            ImGui::ColorEdit4("Clear Color", reinterpret_cast<float*>(&clear));

            //ImGui::Text("No background effect selected.");
        }
    }

    ImGui::End();
}

void demo::ui_show_extensions_window(vulkan::vk_renderer* render)
{
    ImGui::SetNextWindowSize(ImVec2{ 300, 200 }, ImGuiCond_FirstUseEver);
    if (ImGui::Begin("Render Extensions", &_ui_show_extensions_window))
    {
        ImGui::Text("Loaded Extensions");
        ImGui::Separator();

        auto extensions = render->get_extensions();
        for (auto* ext : extensions | std::views::values)
        {
            auto enabled = ext->is_enabled();

            const auto name = ext->name();

            // The ImGui has some special rendering requirements since
            // disabling the module from the UI that it's rendering
            // probably isn't a good idea.
            if (name == "imgui")
            {
                auto dummy_enabled = true;
                ImGui::Checkbox(ext->name().c_str(), &dummy_enabled);
                continue;
            }

            if (ImGui::Checkbox(ext->name().c_str(), &enabled))
            {
                if (enabled)
                    ext->enable();
                else
                    ext->disable();
            }
        }
    }

    ImGui::End();
}

void demo::draw_window_camera(const renderer* render)
{
    if (!_ui.show_camera_window)
        return;

    ImGui::SetNextWindowSize(ImVec2{ 300, 200 }, ImGuiCond_FirstUseEver);
    if (ImGui::Begin("Camera", &_ui.show_camera_window))
    {
        auto* cam = render->default_camera();
        ImGui::DragFloat("X", &cam->pos().x, 0.01f, -1000.f, 1000.f);
        ImGui::DragFloat("Y", &cam->pos().y, 0.01f, -1000.f, 1000.f);
        ImGui::DragFloat("Z", &cam->pos().z, 0.01f, -1000.f, 1000.f);
    }
    ImGui::End();
}

void demo::draw_window_meshes(const std::vector<std::shared_ptr<vulkan::mesh_asset>>& meshes)
{
    if (!_ui.show_meshes_window)
        return;

    ImGui::SetNextWindowSize(ImVec2{ 300, 200 }, ImGuiCond_FirstUseEver);
    if (ImGui::Begin("Meshes", &_ui.show_meshes_window))
    {
        if (ImGui::BeginCombo("Mesh", _selected_mesh.name.c_str()))
        {
            const auto mesh_count = static_cast<uint32_t>(meshes.size());
            for (auto idx = 0u; idx < mesh_count; idx++)
            {
                auto& mesh = meshes.at(idx);
                if (ImGui::Selectable(mesh->name.c_str(), idx == _selected_mesh.index))
                {
                    _selected_mesh.index = idx;
                    _selected_mesh.name  = mesh->name;
                    _selected_mesh.mesh  = mesh.get();
                }

            }
            ImGui::EndCombo();
        }

        ImGui::Checkbox("Apply World Transform", &_selected_mesh.apply_world_transform);
    }

    ImGui::End();
}

void demo::draw_window_vulkan_instance(vulkan::vk_instance* instance)
{
    vulkan_ui::draw_instance_info_window(instance, &_ui.show_vulkan_instance_window);
}

void demo::draw_window_vulkan_gpu(vulkan::vk_physical_device* gpu)
{
    vulkan_ui::draw_gpu_info_window(gpu, &_ui.show_vulkan_gpu_window);
}
