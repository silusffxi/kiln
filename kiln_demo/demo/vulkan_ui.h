#pragma once
#include <kiln/vulkan/vk_instance.h>
#include <kiln/vulkan/vk_physical_device.h>

namespace kiln
{
    class vulkan_ui
    {
    public:
        static void draw_instance_info_window(vulkan::vk_instance* instance, bool* is_open);
        static void draw_gpu_info_window(vulkan::vk_physical_device* gpu, bool* is_open);
    };
}