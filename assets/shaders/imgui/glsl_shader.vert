#version 450 core
layout(location = 0) in vec2 aPos;
layout(location = 1) in vec2 aUV;
layout(location = 2) in vec4 aColor;

layout(push_constant) uniform uPushConstant {
    vec2 uScale;
    vec2 uTranslate;
} pc;

out gl_PerVertex {
    vec4 gl_Position;
};

layout(location = 0) out struct {
    vec4 Color;
    vec2 UV;
} Out;

vec3 rec709_eotf(vec3 v_)
{
    mat2x3 v = mat2x3( (v_/4.5), pow((v_+0.0999)/1.099, vec3(2.22222)) );
    return vec3(
        v[v_[0] < 0.081 ? 0 : 1][0],
        v[v_[1] < 0.081 ? 0 : 1][1],
        v[v_[2] < 0.081 ? 0 : 1][2]
    );
}

void main()
{
    Out.Color = aColor;
    Out.UV = aUV;
    gl_Position = vec4(aPos * pc.uScale + pc.uTranslate, 0, 1);
}
