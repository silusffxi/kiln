#version 450 core
layout(location = 0) out vec4 fColor;

layout(set=0, binding=0) uniform sampler2D sTexture;

layout(location = 0) in struct {
    vec4 Color;
    vec2 UV;
} In;

vec3 rec709_eotf(vec3 v_)
{
    mat2x3 v = mat2x3( (v_/4.5), pow((v_+0.0999)/1.099, vec3(2.22222)) );
    return vec3(
        v[v_[0] < 0.081 ? 0 : 1][0],
        v[v_[1] < 0.081 ? 0 : 1][1],
        v[v_[2] < 0.081 ? 0 : 1][2]
    );
}

void main()
{
    fColor = In.Color * texture(sTexture, In.UV.st);
    fColor = vec4(rec709_eotf(fColor.rgb), fColor.a);
}
