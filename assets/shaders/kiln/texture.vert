// Based on: https://github.com/PaoloMazzon/Vulkan2D/blob/master/shaders/tex.vert
#version 460
#extension GL_ARB_separate_shader_objects : enable

layout (push_constant) uniform PushBuffer {
    vec4 color_mod;
    vec4 texture_coords;
} push_buffer;

layout (location = 1) out vec2 frag_tex_coord;

out gl_PerVertex {
    vec4 gl_Position;
};

vec2 vertices[] = {
    vec2(0.0f, 0.0f),
    vec2(1.0f, 0.0f),
    vec2(1.0f, 1.0f),
    vec2(1.0f, 1.0f),
    vec2(0.0f, 1.0f),
    vec2(0.0f, 0.0f),
};

vec2 tex_coords[] = {
    vec2(0.0f, 0.0f),
    vec2(1.0f, 0.0f),
    vec2(1.0f, 1.0f),
    vec2(1.0f, 1.0f),
    vec2(0.0f, 1.0f),
    vec2(0.0f, 0.0f),
};

void main() {
    vec2 new_pos;
    new_pos.x = vertices[gl_VertexIndex].x * push_buffer.texture_coords.z;
    new_pos.y = vertices[gl_VertexIndex].y * push_buffer.texture_coords.w;

    gl_Position = vec4(new_pos, 1.0, 1.0);
    frag_tex_coord.x = push_buffer.texture_coords.x + (tex_coords[gl_VertexIndex].x * push_buffer.texture_coords.z);
    frag_tex_coord.y = push_buffer.texture_coords.y + (tex_coords[gl_VertexIndex].y * push_buffer.texture_coords.w);
}