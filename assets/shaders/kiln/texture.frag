// Based on: https://github.com/PaoloMazzon/Vulkan2D/blob/master/shaders/tex.frag
#version 460
#extension GL_ARB_separate_shader_objects : enable

layout (set = 1, binding = 1) uniform sampler   tex_sampler;
layout (set = 2, binding = 2) uniform texture2D tex;

layout (push_constant) uniform PushBuffer {
    vec4 color_mod;
    vec4 texture_coords;
} push_buffer;

layout (location = 1) in vec2 frag_tex_coord;

layout (location = 0) out vec4 out_color;

void main() {
    vec4 color = texture(sampler2D(tex, tex_sampler), frag_tex_coord);
    out_color = vec4(
        color.r * push_buffer.color_mod.r,
        color.g * push_buffer.color_mod.g,
        color.b * push_buffer.color_mod.b,
        color.a * push_buffer.color_mod.a
    );
}