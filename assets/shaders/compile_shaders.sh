#!/usr/bin/env bash
# Setup for determining where the script is running from:
# (https://stackoverflow.com/a/246128)
SOURCE=${BASH_SOURCE[0]}
while [ -L "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )
  SOURCE=$(readlink "$SOURCE")
  [[ $SOURCE != /* ]] && SOURCE=$DIR/$SOURCE # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )

glslc -o $DIR/kiln/texture.vert.spv $DIR/kiln/texture.vert
glslc -o $DIR/kiln/texture.frag.spv $DIR/kiln/texture.frag

glslc -o $DIR/vkguide/gradient_color.comp.spv      $DIR/vkguide/gradient_color.comp
glslc -o $DIR/vkguide/gradient1.comp.spv           $DIR/vkguide/gradient1.comp
glslc -o $DIR/vkguide/sky.comp.spv                 $DIR/vkguide/sky.comp
glslc -o $DIR/vkguide/color_triangle.frag.spv      $DIR/vkguide/color_triangle.frag
glslc -o $DIR/vkguide/color_triangle.vert.spv      $DIR/vkguide/color_triangle.vert
glslc -o $DIR/vkguide/color_triangle_mesh.vert.spv $DIR/vkguide/color_triangle_mesh.vert

# 