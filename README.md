# Kiln

A rendering library primarily focused on Vulkan.

## Dependencies

Dependencies required to build the library and integrate it into an application:

| Dependency                                         | Notes |
|----------------------------------------------------|-------|
| [cxxopts](https://github.com/jarro2783/cxxopts)    | This is only used by the demo application. It is **NOT** required to use/build Kiln itself. |
| [glm](https://github.com/g-truc/glm)               |       |
| [SDL2](https://github.com/libsdl-org/SDL/releases) | Used for managing the window, events, and other non-rendering related functions.
| [spdlog](https://github.com/gabime/spdlog)         | Ensure that `SPDLOG_USE_STD_FORMAT` is being defined by your compiler. This will prevent spdlog from using [fmtlib](https://github.com/fmtlib/fmt), and will instead rely on `std::format`. |
| [stb_image](https://github.com/nothings/stb)       | Used for loading images and textures from disk. |
| [Vulkan SDK](https://vulkan.lunarg.com/sdk/home)   | Ensure that the `VULKAN_SDK` environment variable is configured and available to your toolchain. It is recommended that the latest version of the SDK is used. The Visual Studio project(s) will rely on this for locating the Vulkan SDK files. |
| [VulkanMemoryAllocator](https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator) | |

## Gamma Correction and ImGui

By default, the swapchain that Vulkan will be using will be within the sRGB color space. However, ImGui would prefer the swapchain use UNORM. To account for this, the fragment shader that ImGui is using needs to be updated in order to apply gamma correction. More information about this can be found in [this reddit thread](https://www.reddit.com/r/vulkan/comments/s3x6hq/comment/hsoqnie/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button).

To actually apply this gamma correction we'll want to add this function to the **fragment** shader (from the linked Reddit thread):

```glsl
vec3 rec709_eotf(vec3 v_) {
    mat2x3 v = mat2x3( (v_/4.5), pow((v_+0.0999)/1.099, vec3(2.22222)) );
    return vec3(
        v[v_[0] < 0.081 ? 0 : 1][0],
        v[v_[1] < 0.081 ? 0 : 1][1],
        v[v_[2] < 0.081 ? 0 : 1][2]
    );
}
```