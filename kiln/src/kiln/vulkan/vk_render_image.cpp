#include <vulkan/vulkan.h>
#include "kiln/vulkan/vk_utilities.h"
//--------------------------------------
#include "kiln/vulkan/vk_render_image.h"

using namespace kiln;
using namespace vulkan;

void kiln_transition_vk_render_image(kiln_vk_render_image* img, VkCommandBuffer cmd, VkImageLayout next_layout)
{
    if (img == nullptr)
        return;

    // We don't need to do an image transition if the layout of the image is already
    // in the desired layout.
    if (img->layout == next_layout)
        return;

    transition_image(cmd, img->image, img->layout, next_layout);
    img->prev_layout = img->layout;
    img->layout = next_layout;
}

void vk_render_image::transition_to(VkCommandBuffer cmd, VkImageLayout next_layout)
{
    kiln_transition_vk_render_image(this, cmd, next_layout);
}
