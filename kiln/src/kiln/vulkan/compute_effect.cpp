#include <cstdint>
#include <memory>
#include <string>
#include <utility>
#include <vulkan/vulkan.h>
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_shader.h"
#include "kiln/vulkan/vk_types.h"
#include "kiln/vulkan/vk_utilities.h"
#include "vk_init.hpp"
//-------------------------------------
#include "kiln/vulkan/compute_effect.h"

using namespace kiln;
using namespace vulkan;

compute_effect::compute_effect(std::string name, vk_device* device, VkPipelineLayout layout, VkPipeline pipeline) :
    _name(std::move(name)), _device(device), _layout(layout), _pipeline(pipeline)
{
}

compute_effect::~compute_effect()
{
    if (_pipeline != nullptr)
    {
        vkDestroyPipeline(*_device, _pipeline, nullptr);
        _pipeline = nullptr;
    }

    if (_layout != nullptr)
    {
        vkDestroyPipelineLayout(*_device, _layout, nullptr);
        _layout = nullptr;
    }
}

std::string compute_effect::name() const { return _name; }
VkPipelineLayout compute_effect::layout() const { return _layout; }
VkPipeline compute_effect::pipeline() const { return _pipeline; }
compute_push_constants* compute_effect::push_constants() { return &_push_constants; }

void compute_effect::draw(VkCommandBuffer cmd, VkDescriptorSet draw_image_desc_set, const allocated_image* draw_image) const
{
    // Bind the pipeline (shader)
    vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_COMPUTE, _pipeline);

    // Bind the descriptor set containing the draw image for the shader.
    vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_COMPUTE, _layout, 0, 1,
        &draw_image_desc_set, 0, nullptr);

    vkCmdPushConstants(cmd, _layout, VK_SHADER_STAGE_COMPUTE_BIT,
        0, sizeof(compute_push_constants), &_push_constants);

    // Execute the compute pipeline dispatch.
    // This is using a 16x16 workgroup.
    const auto width = static_cast<uint32_t>(std::ceil(draw_image->width_f() / 16.f));
    const auto height = static_cast<uint32_t>(std::ceil(draw_image->height_f() / 16.f));

    vkCmdDispatch(cmd, width, height, 1);
}

std::unique_ptr<compute_effect> compute_effect::create(const std::string& name, vk_device* device,
    VkDescriptorSetLayout desc_set_layout, const vk_shader* shader)
{
    constexpr auto push_constants_range = VkPushConstantRange
    {
        .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
        .offset = 0,
        .size = sizeof(compute_push_constants),
    };

    const auto layout_create_info = vk_init::pipeline::layout_create_info(&desc_set_layout, 1, &push_constants_range, 1);

    VkPipelineLayout layout;
    vk_result_check(vkCreatePipelineLayout(*device, &layout_create_info, nullptr, &layout),
        "Failed to create compute pipeline layout.");

    const auto stage_create_info = VkPipelineShaderStageCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .stage = VK_SHADER_STAGE_COMPUTE_BIT,
        .module = shader->shader_module(),
        .pName = "main",
        .pSpecializationInfo = nullptr,
    };

    const auto create_info = VkComputePipelineCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .stage = stage_create_info,
        .layout = layout,
        //.basePipelineHandle =
        //.basePipelineIndex = 
    };

    VkPipeline pipeline;
    vk_result_check(vkCreateComputePipelines(*device, nullptr, 1, &create_info, nullptr, &pipeline),
        "Failed to create compute pipeline.");

    return std::make_unique<compute_effect>(name, device, layout, pipeline);
}
