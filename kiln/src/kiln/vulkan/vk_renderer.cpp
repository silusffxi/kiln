#include <functional>
#include <memory>
#include <ranges>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <SDL2/SDL.h>
#include <vulkan/vulkan.h>
#include <vulkan/vk_enum_string_helper.h>
#include "kiln/logging.hpp"
#include "kiln/renderer.hpp"
#include "kiln/shaders.hpp"
#include "kiln/types.h"
#include "kiln/utilities.hpp"
#include "kiln/vulkan/vk_instance.h"
#include "kiln/vulkan/vk_physical_device.h"
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_memory_allocator.h"
#include "kiln/vulkan/vk_surface.h"
#include "kiln/vulkan/vk_swapchain.h"
#include "kiln/vulkan/vk_immediate.h"
#include "kiln/vulkan/descriptor_allocator.h"
#include "kiln/vulkan/vk_frame_data.h"
#include "kiln/vulkan/compute_effect.h"
#include "kiln/vulkan/allocated_image.h"
#include "kiln/vulkan/vk_shader.h"
#include "kiln/vulkan/vk_render_extension.h"
#include "kiln/vulkan/sampler_manager.h"
#include "kiln/vulkan/vk_pipeline.h"
#include "kiln/vulkan/vk_types.h"
#include "kiln/vulkan/vk_utilities.h"
#include "vk_init.hpp"
#include "builders/vk_descriptor_layout_builder.hpp"
#include "builders/vk_pipeline_builder.hpp"
//---------------------------------
#include "kiln/vulkan/vk_renderer.h"

using namespace kiln;
using namespace vulkan;

vk_renderer::vk_renderer(const vk_renderer_args& args) :
    renderer(args.window, graphics_api::vulkan), _log(logging::get_logger(log_name::vulkan)),
    _window(args.window), _instance(args.instance), _gpu(args.gpu), _device(args.device),
    _allocator(args.allocator), _surface(args.surface), _swapchain(args.swapchain),
    _immediate(args.immediate), _assets(args.assets)
{
    _samplers = std::make_unique<sampler_manager>(_gpu, _device);

    _frame_data.frames  = vk_frame_data::create_and_populate(_device, max_frames_in_flight);
    _frame_data.current = &_frame_data.frames[_frame_data.current_index];

    _depth_image = allocated_image::create_depth_image(_device, _allocator,
        _surface->width(), _surface->height());

    _draw_image = allocated_image::create_draw_image(_device, _allocator->allocator(),
        _surface->width(), _surface->height());

    initialize_descriptors();
    initialize_pipelines();
    initialize_compute_effects();

    _render_extensions.list.reserve(max_render_extensions);
    _render_extensions.create_args = vk_render_extension_create_args
    {
        .instance  = _instance,
        .gpu       = _gpu,
        .device    = _device,
        .surface   = _surface,
        .swapchain = _swapchain,
    };
}

vk_renderer::~vk_renderer()
{
    free_unique_ptr(_mesh_pipeline);
    free_unique_ptr(_colored_triangle);

    if (!_render_extensions.list.empty())
    {
        for (auto& ext : _render_extensions.list)
        {
            ext.reset();
            ext = nullptr;
        }
        _render_extensions.list.clear();
        _render_extensions.by_name.clear();
    }

    if (!_compute_effects.empty())
    {
        for (auto& effect : _compute_effects)
        {
            effect.reset();
            effect = nullptr;
        }
        _compute_effects.clear();
    }

    free_unique_ptr(_texture_descriptor);
    free_unique_ptr(_sampler_descriptor);
    free_unique_ptr(_draw_image_descriptor);
    free_unique_ptr(_draw_image);
    free_unique_ptr(_depth_image);
    free_unique_ptr(_samplers);
}

void vk_renderer::add_extension(const std::function<std::unique_ptr<vk_render_extension>(vk_render_extension_create_args&)>& ext_factory)
{
    add_extension(ext_factory(_render_extensions.create_args));
}

void vk_renderer::add_extension(std::unique_ptr<vk_render_extension> ext)
{
    _render_extensions.list.emplace_back(std::move(ext));
    auto* added_ext = _render_extensions.list.back().get();

    _render_extensions.by_name.try_emplace(added_ext->name(), added_ext);
}

compute_effect* vk_renderer::background_effect() const
{
    return _selected_background;
}

std::vector<std::unique_ptr<compute_effect>>& vk_renderer::background_effects()
{
    return _compute_effects;
}

void vk_renderer::set_background_effect(compute_effect* effect)
{
    _selected_background = effect;
}

void vk_renderer::draw_background() const
{
    draw_background(_frame_data.current->cmd_buffer);
}

void vk_renderer::draw_geometry() const
{
    draw_geometry(_frame_data.current->cmd_buffer, nullptr);
}

void vk_renderer::draw_geometry(const std::unique_ptr<gpu_mesh_buffers>& mesh) const
{
    draw_geometry(_frame_data.current->cmd_buffer, mesh.get());
}

void vk_renderer::draw_geometry(const gpu_mesh_buffers* mesh) const
{
    draw_geometry(_frame_data.current->cmd_buffer, mesh);
}

void vk_renderer::draw_mesh(const mesh_asset* mesh, bool world_transform) const
{
    draw_mesh(_frame_data.current->cmd_buffer, mesh, world_transform);
}

vk_render_extension* vk_renderer::get_extension(const std::string& name) const
{
    if (!_render_extensions.by_name.contains(name))
        return nullptr;

    return _render_extensions.by_name.at(name);
}

const std::unordered_map<std::string, vk_render_extension*>& vk_renderer::get_extensions() const
{
    return _render_extensions.by_name;
}

vk_immediate* vk_renderer::immediate() const
{
    return _immediate;
}

void vk_renderer::on_event(const SDL_Event* e)
{
    for (const auto& ext : _render_extensions.list)
    {
        ext->handle_event(e);
    }
}

void vk_renderer::on_frame_begin()
{
    static auto cmd_buffer_begin_info = vk_init::command::buffer_begin_info(
        VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT, nullptr);

    // Reset the fence for the current frame
    _device->wait_for_fence(_frame_data.current->render_fence);
    _device->reset_fence(_frame_data.current->render_fence);

    // Acquire the next swapchain image index
    _swapchain_image = _swapchain->acquire_next_image(_frame_data.current->swapchain_semaphore);

    // Swapchain image would only be null if something went wrong with acquiring the next
    // swapchain image or when the swapchain is out of date and needs to be rebuilt.
    if (_swapchain_image == nullptr) return;

    // We can mark the frame as "in progress" at this point.
    _frame_status.in_progress = true;

    const auto cmd = _frame_data.current->cmd_buffer;
    vk_result_check(vkResetCommandBuffer(cmd, 0),
        "Failed to reset command buffer.");

    vk_result_check(vkBeginCommandBuffer(cmd, &cmd_buffer_begin_info),
        "Failed to begin command buffer.");

    _swapchain_image->transition_to(cmd, VK_IMAGE_LAYOUT_GENERAL);

    _draw_image->transition_to(cmd, VK_IMAGE_LAYOUT_GENERAL);
    draw_clear(cmd, _draw_image->image(), _draw_image->layout());

    _draw_image->transition_to(cmd, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
    _depth_image->transition_to(cmd, VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL);
    render_extensions_frame_begin();
}

void vk_renderer::on_frame_end()
{
    // Swapchain image should only be null if something went wrong with acquiring the next
    // swapchain image on frame begin or when the swapchain is out of date and needs to be
    // rebuilt.
    if (_swapchain_image == nullptr) return;

    const auto cmd = _frame_data.current->cmd_buffer;

    _draw_image->transition_to(cmd, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
    _swapchain_image->transition_to(cmd, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

    copy_image_to_image(cmd, _draw_image->image(), _draw_image->extent_2d(),
        _swapchain_image->image, _swapchain_image->extent_2d);

    _swapchain_image->transition_to(cmd, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
    render_extensions_frame_end(cmd);

    _swapchain_image->transition_to(cmd, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);

    vk_result_check(vkEndCommandBuffer(cmd),
        "Failed to end command buffer.");

    queue_submit(cmd, _frame_data.current);
    queue_present(_frame_data.current);

    // We can mark the frame as "done" at this point:
    _frame_status.in_progress = false;

    next_frame();

    _frame_status.count = ++_frame_status.count % 120;

    // If a rebuild was requested after acquiring the previous swapchain image
    // then we'll want to process those rebuild requests here.
    _swapchain->process_rebuild();
}

void vk_renderer::apply_world_transform(glm::mat4& world_matrix) const
{
    constexpr float perspective_near = 10'000.f;
    constexpr float perspective_far  = 0.1f;

    constexpr auto fov_y = glm::radians(70.f);

    // Aspect ratio needs to be re-computed because the size of the draw image
    // may have changed between frame draws.
    const auto aspect_ratio = _draw_image->aspect_ratio();
    const auto view         = glm::translate(default_camera()->pos());

    auto projection = glm::perspective(
        fov_y,            // FOV Y
        aspect_ratio,     // Aspect Ratio
        perspective_near, // Near
        perspective_far   // Far
    );

    // Invert the Y direction on the projection matrix to make things more like OpenGL.
    projection[1][1] *= -1.f;

    world_matrix = projection * view;
}

void vk_renderer::draw_background(VkCommandBuffer cmd) const
{
    if (_selected_background == nullptr || !_frame_status.in_progress)
        return;

    const auto draw_image_layout = _draw_image->layout();
    _draw_image->transition_to(cmd, VK_IMAGE_LAYOUT_GENERAL);
    _selected_background->draw(cmd, _draw_image_descriptor->set(), _draw_image.get());
    _draw_image->transition_to(cmd, draw_image_layout);
}

void vk_renderer::draw_geometry(VkCommandBuffer cmd, const gpu_mesh_buffers* mesh) const
{
    static auto viewport = VkViewport
    {
        .x = 0,
        .y = 0,
        .width = _draw_image->width_f(),
        .height = _draw_image->height_f(),
        .minDepth = 0.f,
        .maxDepth = 1.f,
    };

    static auto scissor = VkRect2D
    {
        .offset = {.x = 0, .y = 0, },
        .extent = {.width = _draw_image->width(), .height = _draw_image->height(), },
    };

    static auto push_constants = gpu_draw_push_constants
    {
        .world_matrix = glm::mat4{ 1.f },
    };

    //-----------------------------------------------------------------------------

    const auto color_attachment = vk_init::rendering::color_attachment_info(_draw_image->view(), nullptr, VK_IMAGE_LAYOUT_GENERAL);
    const auto render_info = vk_init::rendering::info(_draw_image->extent_2d(), &color_attachment, nullptr);

    vkCmdBeginRendering(cmd, &render_info);

    // Update the viewport with the draw image size (in case it has changed):
    viewport.width = _draw_image->width_f();
    viewport.height = _draw_image->height_f();

    vkCmdSetViewport(cmd, 0, 1, &viewport);

    // Update the scissor dimensions with the draw image size (in case it has changed):
    scissor.extent.width = _draw_image->width();
    scissor.extent.height = _draw_image->height();

    vkCmdSetScissor(cmd, 0, 1, &scissor);

    if (mesh == nullptr)
    {
        // Bind the pipeline
        vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, _colored_triangle->pipeline());

        // Issue the draw command with 3 vertices.
        vkCmdDraw(cmd, 3, 1, 0, 0);
    }
    else
    {
        vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, _mesh_pipeline->pipeline());

        push_constants.vertex_buffer = mesh->vertex_buffer_address();

        vkCmdPushConstants(cmd, _mesh_pipeline->layout(), VK_SHADER_STAGE_VERTEX_BIT, 0,
            sizeof(gpu_draw_push_constants), &push_constants);

        vkCmdBindIndexBuffer(cmd, *mesh->index_buffer(), 0, VK_INDEX_TYPE_UINT32);

        vkCmdDrawIndexed(cmd, 6, 1, 0, 0, 0);
    }

    vkCmdEndRendering(cmd);
}

void vk_renderer::draw_mesh(const VkCommandBuffer cmd, const mesh_asset* mesh, bool world_transform) const
{
    static auto viewport = VkViewport
    {
        .x = 0,
        .y = 0,
        .width = _draw_image->width_f(),
        .height = _draw_image->height_f(),
        .minDepth = 0.f,
        .maxDepth = 1.f,
    };

    static auto scissor = VkRect2D
    {
        .offset = {.x = 0, .y = 0, },
        .extent = {.width = _draw_image->width(), .height = _draw_image->height(), },
    };

    static auto push_constants = gpu_draw_push_constants
    {
        .world_matrix = glm::mat4{ 1.f },
    };

    //-----------------------------------------------------------------------------

    const auto color_attachment = vk_init::rendering::color_attachment_info(_draw_image->view(), nullptr, VK_IMAGE_LAYOUT_GENERAL);
    const auto depth_attachment = vk_init::rendering::depth_attachment_info(_depth_image->view(), VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL);
    const auto render_info = vk_init::rendering::info(_draw_image->extent_2d(), &color_attachment, &depth_attachment);

    // Update the viewport with the draw image size (in case it has changed):
    viewport.width = _draw_image->width_f();
    viewport.height = _draw_image->height_f();

    // Update the scissor dimensions with the draw image size (in case it has changed):
    scissor.extent.width = _draw_image->width();
    scissor.extent.height = _draw_image->height();

    vkCmdBeginRendering(cmd, &render_info);

    vkCmdSetViewport(cmd, 0, 1, &viewport);
    vkCmdSetScissor(cmd, 0, 1, &scissor);

    vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, _mesh_pipeline->pipeline());

    push_constants.vertex_buffer = mesh->mesh_buffers->vertex_buffer_address();

    // World Transform:
    if (world_transform)
    {
        apply_world_transform(push_constants.world_matrix);
    }
    else
    {
        push_constants.world_matrix = glm::mat4{ 1.f };
    }

    vkCmdPushConstants(cmd, _mesh_pipeline->layout(), VK_SHADER_STAGE_VERTEX_BIT, 0,
        sizeof(gpu_draw_push_constants), &push_constants);

    vkCmdBindIndexBuffer(cmd, *mesh->mesh_buffers->index_buffer(), 0, VK_INDEX_TYPE_UINT32);

    vkCmdDrawIndexed(cmd,
        mesh->surfaces[0].count,       // Index count
        1,                             // Instance count
        mesh->surfaces[0].start_index, // First index
        0,                             // Vertex offset
        0                              // First instance
    );

    vkCmdEndRendering(cmd);
}

void vk_renderer::draw_clear(VkCommandBuffer cmd, VkImage image, VkImageLayout layout)
{
    static VkClearColorValue       clear_value = { };
    static VkImageSubresourceRange clear_range = vk_init::image::subresource_range(VK_IMAGE_ASPECT_COLOR_BIT);

    const auto& color = clear_color();
    clear_value.float32[0] = color.r;
    clear_value.float32[1] = color.g;
    clear_value.float32[2] = color.b;
    clear_value.float32[3] = color.a;

    vkCmdClearColorImage(cmd, image, layout, &clear_value, 1, &clear_range);
}

void vk_renderer::next_frame()
{
    _frame_data.current_index = ++_frame_data.current_index % static_cast<uint32_t>(_frame_data.frames.size());
    _frame_data.current       = &_frame_data.frames[_frame_data.current_index];
}

void vk_renderer::queue_submit(const VkCommandBuffer cmd, const vk_frame_data* frame) const
{
    const auto cmd_submit_info = vk_init::command::buffer_submit_info(cmd);
    const auto semaphore_wait_info = vk_init::semaphore::submit_info(VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT_KHR,
        frame->swapchain_semaphore);

    const auto semaphore_signal_info = vk_init::semaphore::submit_info(VK_PIPELINE_STAGE_2_ALL_GRAPHICS_BIT,
        frame->renderer_semaphore);

    const auto submit_info = vk_init::submit_info2(&cmd_submit_info, &semaphore_signal_info, &semaphore_wait_info);

    switch (const auto result = vkQueueSubmit2(_device->get_graphics_queue(), 1, &submit_info, frame->render_fence))  // NOLINT(clang-diagnostic-switch-enum)
    {
    case VK_SUCCESS:
        break;
    case VK_ERROR_OUT_OF_DATE_KHR:
        _log->warn("Queue submit failed. Swapchain needs to be rebuilt.");
        _swapchain->request_rebuild();
        break;
    default:
        _log->error("Call to vkQueueSubmit2 failed! Error: {} ({:#x})",
            std::string(string_VkResult(result)), static_cast<int>(result));
        break;
    }
}

void vk_renderer::queue_present(const vk_frame_data* frame) const
{
    auto* swapchain = _swapchain->get_vk_swapchain();

    VkResult result;
    const auto present_info = vk_init::present_info(&swapchain, 1, &_swapchain_image->index,
        &result, &frame->renderer_semaphore, 1);

    switch (const auto present_result = vkQueuePresentKHR(_device->get_graphics_queue(), &present_info))  // NOLINT(clang-diagnostic-switch-enum)
    {
    case VK_SUCCESS:
        break;
    case VK_ERROR_OUT_OF_DATE_KHR:
        _log->warn("Queue present failed. Swapchain needs to be rebuilt.");
        _swapchain->request_rebuild();
        break;
    default:
        _log->error("Call to vkQueuePresentKHR failed! Error: {} ({:#x})",
            std::string(string_VkResult(present_result)), static_cast<int>(present_result));
        break;
    }
}

void vk_renderer::render_extensions_frame_begin() const
{
    for (auto& ext : _render_extensions.list)
    {
        if (ext == nullptr || !ext->is_enabled()) continue;
        ext->frame_begin(_draw_image->render_image(), _swapchain_image);
    }
}

void vk_renderer::render_extensions_frame_end(const VkCommandBuffer cmd) const
{
    for (auto& ext : _render_extensions.list)
    {
        if (ext == nullptr || !ext->is_enabled()) continue;
        ext->frame_end(cmd);
    }
}

void vk_renderer::initialize_compute_effects()
{
    const auto desc_layout = _draw_image_descriptor->layout();

    const auto gradient_color = _assets->get_shader("vkguide/gradient_color.comp.spv");
    _compute_effects.emplace_back(compute_effect::create(
        "gradient_color", _device, desc_layout, gradient_color));

    _compute_effects.back()->push_constants()->data1 = glm::vec4(1.f, 0.f, 0.f, 1.f);
    _compute_effects.back()->push_constants()->data2 = glm::vec4(0.f, 0.f, 1.f, 1.f);

    const auto star_sky = _assets->get_shader("vkguide/sky.comp.spv");
    _compute_effects.emplace_back(compute_effect::create(
        "sky", _device, desc_layout, star_sky));

    _compute_effects.back()->push_constants()->data1 = glm::vec4(0.f, 0.005f, 0.1f, 0.975f);
}

void vk_renderer::initialize_descriptors()
{
    // Create the descriptor set layout for the compute shader.
    {
        std::vector<descriptor_allocator::pool_size_ratio> sizes =
        {
            { VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1 },
        };

        auto draw_image_desc_layout = vk_descriptor_layout_builder()
            .add_binding(0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE)
            .add_shader_stage(VK_SHADER_STAGE_COMPUTE_BIT)
            .build(_device);

        _draw_image_descriptor = std::make_unique<vk_descriptor>(_device, draw_image_desc_layout, 10, sizes);
        _draw_image_descriptor->allocate_set([&](VkDescriptorSet set)
        {
            const auto img_info = VkDescriptorImageInfo
            {
                .sampler = nullptr,
                .imageView = _draw_image->view(),
                .imageLayout = VK_IMAGE_LAYOUT_GENERAL,
            };

            const auto draw_image_write = VkWriteDescriptorSet
            {
                .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .pNext = nullptr,

                .dstSet = set,
                .dstBinding = 0,
                //.dstArrayElement =
                .descriptorCount = 1,
                .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
                .pImageInfo = &img_info,
                //.pBufferInfo =
                //.pTexelBufferView = 
            };

            vkUpdateDescriptorSets(*_device, 1, &draw_image_write, 0, nullptr);
        });
    }

    // Create the descriptor for samplers
    {
        constexpr uint32_t binding = 1;

        auto layout = vk_descriptor_layout_builder()
            .add_binding(binding, VK_DESCRIPTOR_TYPE_SAMPLER)
            .add_shader_stage(VK_SHADER_STAGE_FRAGMENT_BIT)
            .build(_device);

        auto sizes = std::vector<descriptor_allocator::pool_size_ratio>
        {
            { VK_DESCRIPTOR_TYPE_SAMPLER, 4 },
        };

        _sampler_descriptor = std::make_unique<vk_descriptor>(_device, layout, 4, sizes);
        _sampler_descriptor->allocate_set([&](const VkDescriptorSet set)
        {
            const auto img_info = VkDescriptorImageInfo
            {
                .sampler = _samplers->linear(),
            };

            const auto write = vk_init::descriptor_set::write(VK_DESCRIPTOR_TYPE_SAMPLER, binding,
                set, nullptr, 1, &img_info);

            vkUpdateDescriptorSets(*_device, 1, &write, 0, nullptr);
        });
    }

    // Create the descriptor for textures
    {
        auto layout = vk_descriptor_layout_builder()
            .add_binding(1, VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE)
            .add_shader_stage(VK_SHADER_STAGE_FRAGMENT_BIT)
            .build(_device);

        _texture_descriptor = std::make_unique<vk_descriptor>(_device, layout, nullptr);
    }
}

void vk_renderer::initialize_pipelines()
{
    initialize_mesh_pipeline();
    initialize_triangle_pipeline();
}

void vk_renderer::initialize_mesh_pipeline()
{
    _mesh_pipeline = std::make_unique<vk_pipeline>(_device, [&](VkPipelineLayout* layout, VkPipeline* pipeline)
    {
        constexpr auto buffer_range = VkPushConstantRange
        {
            .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
            .offset = 0,
            .size = sizeof(gpu_draw_push_constants),
        };

        const auto layout_create_info = vk_init::pipeline::layout_create_info(nullptr, 0, &buffer_range, 1);

        vk_result_check(vkCreatePipelineLayout(*_device, &layout_create_info, nullptr, layout),
            "Failed to create triangle pipeline layout.");

        const auto& color_triangle_frag = _assets->get_shader("vkguide/color_triangle.frag.spv");
        const auto& color_triangle_vert = _assets->get_shader("vkguide/color_triangle_mesh.vert.spv");

        if (color_triangle_frag == nullptr || color_triangle_vert == nullptr)
        {
            _log->warn("Failed to create triangle pipeline.");
            return;
        }

        *pipeline = vk_pipeline_builder(_device, *layout)
            .add_vertex_shader(color_triangle_vert->shader_module())
            .add_fragment_shader(color_triangle_frag->shader_module())
            .input_topology(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST)
            .polygon_mode(VK_POLYGON_MODE_FILL)
            .cull_mode(VK_CULL_MODE_NONE, VK_FRONT_FACE_CLOCKWISE)
            .multisampling_none()
            .enable_depth_test(true, VK_COMPARE_OP_GREATER_OR_EQUAL)
            .enable_blending(blend_mode::additive)
            .color_attachment_format(_draw_image->format())
            .depth_format(_depth_image->format())
            .build();
    });
}

void vk_renderer::initialize_triangle_pipeline()
{
    _colored_triangle = std::make_unique<vk_pipeline>(_device, [&](VkPipelineLayout* layout, VkPipeline* pipeline)
    {
        const auto layout_create_info = vk_init::pipeline::layout_create_info(nullptr, 0, nullptr, 0);

        vk_result_check(vkCreatePipelineLayout(*_device, &layout_create_info, nullptr, layout),
            "Failed to create triangle pipeline layout.");

        const auto& color_triangle_frag = _assets->get_shader("vkguide/color_triangle.frag.spv");
        const auto& color_triangle_vert = _assets->get_shader("vkguide/color_triangle.vert.spv");

        if (color_triangle_frag == nullptr || color_triangle_vert == nullptr)
        {
            _log->warn("Failed to create triangle pipeline.");
            return;
        }

        *pipeline = vk_pipeline_builder(_device, *layout)
            .add_vertex_shader(color_triangle_vert->shader_module())
            .add_fragment_shader(color_triangle_frag->shader_module())
            .input_topology(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST)
            .polygon_mode(VK_POLYGON_MODE_FILL)
            .cull_mode(VK_CULL_MODE_NONE, VK_FRONT_FACE_CLOCKWISE)
            .multisampling_none()
            .disable_blending()
            .disable_blending()
            .color_attachment_format(_draw_image->format())
            .depth_format(VK_FORMAT_UNDEFINED)
            .build();
    });
}
