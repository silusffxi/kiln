#include <vector>
#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>
#include <vulkan/vulkan.h>
#include "kiln/logging.hpp"
#include "kiln/vulkan/vk_instance.h"
#include "kiln/vulkan/vk_physical_device.h"
//----------------------------
#include "kiln/vulkan/vk_surface.h"

using namespace kiln;
using namespace vulkan;

vk_surface::vk_surface(SDL_Window* window, const std::unique_ptr<vk_instance>& instance, const std::unique_ptr<vk_physical_device>& gpu) :
    vk_surface(window, instance.get(), gpu.get())
{
}

vk_surface::vk_surface(SDL_Window* window, vk_instance* instance, vk_physical_device* gpu) :
    _instance(instance),
    _physical_device(gpu)
{
    if (SDL_Vulkan_CreateSurface(window, *_instance, &_surface) != SDL_TRUE)
        return;

    _supported_formats = enumerate_surface_formats(*_physical_device, _surface);
    _supported_present_modes = enumerate_surface_present_modes(*_physical_device, _surface);

    update_capabilities();

    logging::get_logger(log_name::vulkan)->debug("VkSurfaceKHR created for SDL window.");
}

vk_surface::~vk_surface()
{
    if (_surface != nullptr)
    {
        vkDestroySurfaceKHR(*_instance, _surface, nullptr);
        _surface = nullptr;
        logging::get_logger(log_name::vulkan)->debug("VkSurfaceKHR destroyed.");
    }

    _physical_device = nullptr;
    _instance = nullptr;
}

bool vk_surface::is_present_mode_supported(VkPresentModeKHR mode) const
{
    return std::ranges::any_of(_supported_present_modes,
        [&](VkPresentModeKHR m) { return m == mode; });
}

void vk_surface::update_capabilities()
{
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(*_physical_device, _surface, &_capabilities);
}

std::vector<VkSurfaceFormatKHR> vk_surface::enumerate_surface_formats(VkPhysicalDevice physical_device,
    VkSurfaceKHR surface)
{
    uint32_t count;
    vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, surface, &count, nullptr);

    std::vector<VkSurfaceFormatKHR> formats(count);
    vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device, surface, &count, formats.data());

    return formats;
}

std::vector<VkPresentModeKHR> vk_surface::enumerate_surface_present_modes(VkPhysicalDevice physical_device,
    VkSurfaceKHR surface)
{
    uint32_t count;
    vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, surface, &count, nullptr);

    std::vector<VkPresentModeKHR> present_modes(count);
    vkGetPhysicalDeviceSurfacePresentModesKHR(physical_device, surface, &count, present_modes.data());

    return present_modes;
}