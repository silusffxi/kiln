#include <cstdlib>
#include <atomic>
#include <vulkan/vulkan.h>
#include <vulkan/vk_enum_string_helper.h>
#include "kiln/logging.hpp"
#include "kiln/platform.hpp"
#include "vk_init.hpp"
//-------------------------------------
#include "kiln/vulkan/vk_utilities.h"

namespace
{
    std::atomic_bool abort_on_vulkan_error = false;
    bool image_transition_logging_enabled = false;
}

VkResult kiln_vk_result_check(VkResult result, const char* error_message, const char* src_file, int line, const char* func)
{
    return kiln::vulkan::result_check(result, error_message, src_file, line, func);
}

void kiln::vulkan::copy_image_to_image(VkCommandBuffer cmd, VkImage src, VkExtent2D src_size, VkImage dst, VkExtent2D dst_size)
{
    auto blit_region = VkImageBlit2
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_BLIT_2,
        .pNext = nullptr,
    };

    blit_region.srcOffsets[1].x = static_cast<int32_t>(src_size.width);
    blit_region.srcOffsets[1].y = static_cast<int32_t>(src_size.height);
    blit_region.srcOffsets[1].z = 1;

    blit_region.dstOffsets[1].x = static_cast<int32_t>(dst_size.width);
    blit_region.dstOffsets[1].y = static_cast<int32_t>(dst_size.height);
    blit_region.dstOffsets[1].z = 1;

    blit_region.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    blit_region.srcSubresource.baseArrayLayer = 0;
    blit_region.srcSubresource.layerCount = 1;
    blit_region.srcSubresource.mipLevel = 0;

    blit_region.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    blit_region.dstSubresource.baseArrayLayer = 0;
    blit_region.dstSubresource.layerCount = 1;
    blit_region.dstSubresource.mipLevel = 0;

    const auto blit_info = VkBlitImageInfo2
    {
        .sType = VK_STRUCTURE_TYPE_BLIT_IMAGE_INFO_2,
        .pNext = nullptr,
        .srcImage = src,
        .srcImageLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        .dstImage = dst,
        .dstImageLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        .regionCount = 1,
        .pRegions = &blit_region,
        //.filter = 
    };

    vkCmdBlitImage2(cmd, &blit_info);
}

VkResult kiln::vulkan::result_check(VkResult result, const std::string& error_message,
    const char* src_file, int line, const char* func)
{
    const auto log = logging::get_logger(log_name::vulkan);
    if (result == VK_SUCCESS)
        return result;

    const auto vk_result_str = std::string(string_VkResult(result));

    std::string err_msg = "An error occurred while making a call to the Vulkan API. ";
    if (!error_message.empty())
        err_msg = error_message;

    log->error("{} {}:{} <{}> Error: {} ({:#x})",
        err_msg, std::string(src_file), line, std::string(func),
        vk_result_str, static_cast<int>(result));

    if (abort_on_vulkan_error)
    {
#ifdef OS_WINDOWS
        // If we're on Windows we can pop a dialog box that displays the error information.
        MessageBoxA(nullptr,
            std::format("{}\nError: {} ({:#x})\n\nFile: {}:{}\nFunc: {}",
                err_msg, vk_result_str, static_cast<int>(result),
                std::string(src_file), line, std::string(func)).c_str(),
            "kiln",
            MB_OK | MB_ICONERROR);
#endif
        abort();
    }

    return result;
}

void kiln::vulkan::transition_image(VkCommandBuffer cmd, VkImage image, VkImageLayout current_layout, VkImageLayout next_layout)
{
    //=========================================================================
    // IMPORTANT
    //-------------------------------------------------------------------------
    // In order to get "synchronization 2" to work correctly, we need to actually
    // enable the synchronization 2 features in the VkDevice we're working with.
    //
    // To enable synchronization 2:
    //
    //   1. Create a VkPhysicalDeviceSynchronization2Features struct
    //   2. Assign its "synchronization2" field to VK_TRUE
    //   3. Set the "pNext" filed in the VkDeviceCreateInfo struct to point to the
    //      struct created in step 1.
    //
    //   This will end up looking something like this:
    //
    //   VkDeviceCreateInfo device_create_info = ...
    //
    //   VkPhysicalDeviceSynchronization2Features sync2_features =
    //   {
    //       .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SYNCHRONIZATION_2_FEATURES
    //       .pNext = nullptr,
    //       .synchronization2 = VK_TRUE
    //   };
    //
    //   device_create_info.pNext = &sync2_features;
    //
    //   vkCreateDevice(..., &device_create_info, ...);
    //
    //
    // More information about this can be found here:
    //
    //    - https://stackoverflow.com/a/76472644
    //=========================================================================

    static auto image_barrier = VkImageMemoryBarrier2
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2,
        .pNext = nullptr,

        .srcStageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT,
        .srcAccessMask = VK_ACCESS_2_MEMORY_WRITE_BIT,
        .dstStageMask = VK_PIPELINE_STAGE_2_ALL_COMMANDS_BIT,
        .dstAccessMask = VK_ACCESS_2_MEMORY_WRITE_BIT | VK_ACCESS_2_MEMORY_READ_BIT,

        //.oldLayout = current_layout,
        //.newLayout = next_layout,

        //.srcQueueFamilyIndex =
        //.dstQueueFamilyIndex =

        //.image = image,
        //.subresourceRange = vk_init::image_subresource_range(aspect_mask),
    };

    static auto dep_info = VkDependencyInfo
    {
        .sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO,
        .pNext = nullptr,
        //.dependencyFlags =

        //.memoryBarrierCount =
        //.pMemoryBarriers =

        //.bufferMemoryBarrierCount =
        //.pBufferMemoryBarriers =

        .imageMemoryBarrierCount = 1,
        .pImageMemoryBarriers = &image_barrier,
    };


    const auto aspect_mask = next_layout == VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL
        ? VK_IMAGE_ASPECT_DEPTH_BIT
        : VK_IMAGE_ASPECT_COLOR_BIT;

    image_barrier.oldLayout = current_layout;
    image_barrier.newLayout = next_layout;
    image_barrier.image = image;
    image_barrier.subresourceRange = vk_init::image::subresource_range(aspect_mask);

    vkCmdPipelineBarrier2(cmd, &dep_info);

    if (image_transition_logging_enabled)
    {
        auto ptr_val = reinterpret_cast<std::uintptr_t>(image);
        const auto cur_layout_str = string_VkImageLayout(current_layout);
        const auto next_layout_str = string_VkImageLayout(next_layout);

        logging::get_logger(log_name::vulkan)->debug("Transition image: {:#x}: {} --> {}",
            ptr_val, cur_layout_str, next_layout_str);
    }
}
