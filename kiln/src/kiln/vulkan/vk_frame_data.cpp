//------------------------------------
#include "kiln/vulkan/vk_frame_data.h"

using namespace kiln;
using namespace vulkan;

uint32_t kiln_vulkan_max_frames_in_flight = kiln::vulkan::max_frames_in_flight;

vk_frame_data::vk_frame_data() :
    cmd_pool(nullptr), cmd_buffer(nullptr),
    swapchain_semaphore(nullptr), renderer_semaphore(nullptr),
    render_fence(nullptr)
{
}

vk_frame_data::vk_frame_data(vk_device* device) :
    vk_frame_data()
{
    cmd_pool   = device->create_command_pool(nullptr);
    cmd_buffer = device->create_command_buffer(cmd_pool);

    swapchain_semaphore = device->create_semaphore();
    renderer_semaphore  = device->create_semaphore();

    render_fence = device->create_fence(VK_FENCE_CREATE_SIGNALED_BIT);
}

std::vector<vk_frame_data> vk_frame_data::create_and_populate(vk_device* device, uint32_t count)
{
    std::vector<vk_frame_data> frames;
    frames.reserve(count);
    populate(device, &frames);
    return frames;
}

void vk_frame_data::populate(vk_device* device, std::vector<vk_frame_data>* frames)
{
    if (device == nullptr || frames == nullptr)
        return;

    if (frames->capacity() == 0)
        return;

    // If the vector hasn't been populated with anything yet,
    // but has the capacity to be populated then we'll want
    // to just populate the values.
    if (frames->empty() && frames->capacity() > 0)
    {
        const auto frame_count = static_cast<uint32_t>(frames->capacity());
        for (auto idx = 0u; idx < frame_count; idx++)
        {
            // NOTE: This is implicitly calling the constructor for vk_frame_data because
            // std::vector<T>::emplace_back will do that for you, apparently. This IS NOT
            // populating the list with pointers to device.
            //
            // This is actually:
            //
            //   frames->emplace_back(vk_frame_data(device));
            //
            frames->emplace_back(device);
        }
        return;
    }
    else if (!frames->empty())
    {
        for (auto& frame : *frames)
        {
            frame.cmd_pool = device->create_command_pool(nullptr);
            frame.cmd_buffer = device->create_command_buffer(frame.cmd_pool);

            frame.swapchain_semaphore = device->create_semaphore();
            frame.renderer_semaphore = device->create_semaphore();

            frame.render_fence = device->create_fence(VK_FENCE_CREATE_SIGNALED_BIT);
        }
        return;
    }

    logging::get_logger(log_name::vulkan)->warn("Could not populate frame data collection.");
}
