#include <cstdint>
#include <memory>
#include <utility>
#include <vulkan/vulkan.h>
#include <vma/vk_mem_alloc.h>
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_utilities.h"
#include "vk_init.hpp"
//--------------------------------------
#include "kiln/vulkan/allocated_image.h"

using namespace kiln;
using namespace vulkan;

allocated_image::allocated_image(vk_device* device, VmaAllocator allocator, uint32_t width, uint32_t height,
    VkImage image, VkImageView view, VmaAllocation alloc, const VmaAllocationInfo& alloc_info,
    VkFormat format, VkImageUsageFlags usage) :
    _device(device), _allocator(allocator), _width(width), _height(height),
    _alloc(alloc), _alloc_info(alloc_info), _format(format), _usage(usage)
{
    _internal_image.extent_2d = VkExtent2D{ width, height, };
    _internal_image.extent_3d = VkExtent3D{ width, height, 1 };

    _internal_image.image = image;
    _internal_image.view  = view;
}

allocated_image::~allocated_image()
{
    if (_internal_image.view != nullptr)
    {
        vkDestroyImageView(*_device, _internal_image.view, nullptr);
        _internal_image.view = nullptr;
    }

    if (_internal_image.image != nullptr)
    {
        vmaDestroyImage(_allocator, _internal_image.image, _alloc);
        _internal_image.image = nullptr;
        _alloc = nullptr;
    }
}

float allocated_image::aspect_ratio() const { return width_f() / height_f(); }

uint32_t allocated_image::width() const { return _width; }
uint32_t allocated_image::height() const { return _height; }

float allocated_image::width_f() const { return static_cast<float>(_width); }
float allocated_image::height_f() const { return static_cast<float>(_height); }

VkImage allocated_image::image() const { return _internal_image.image; }
VkImageView allocated_image::view() const { return _internal_image.view; }

const VkExtent2D& allocated_image::extent_2d() const { return _internal_image.extent_2d; }
const VkExtent3D& allocated_image::extent_3d() const { return _internal_image.extent_3d; }

VmaAllocation allocated_image::allocation() const { return _alloc; }
VmaAllocationInfo allocated_image::allocation_info() const { return _alloc_info; }

VkFormat allocated_image::format() const { return _format; }
VkImageUsageFlags allocated_image::usage() const { return _usage; }

vk_render_image* allocated_image::render_image() { return &_internal_image; }

VkImageLayout allocated_image::layout() const { return _internal_image.layout; }

void allocated_image::transition_to(VkCommandBuffer cmd, VkImageLayout next_layout)
{
    _internal_image.transition_to(cmd, next_layout);
}

std::unique_ptr<allocated_image> allocated_image::create_depth_image(vk_device* device, VmaAllocator allocator,
    uint32_t width, uint32_t height)
{
    const auto extent = VkExtent3D{ width, height, 1 };

    const auto create_info = vk_init::image::create_info(extent, DEPTH_IMAGE_FORMAT, DEPTH_IMAGE_USAGE);

    constexpr auto alloc_info = VmaAllocationCreateInfo
    {
        .usage = VMA_MEMORY_USAGE_GPU_ONLY,
        .requiredFlags = static_cast<VkMemoryPropertyFlags>(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT),
    };

    VkImage image;
    VmaAllocation alloc;
    VmaAllocationInfo info;

    vk_result_check(vmaCreateImage(allocator, &create_info, &alloc_info, &image, &alloc, &info),
        "Failed to allocate depth image.");

    const auto view_create_info = vk_init::image::view_create_info(image, DEPTH_IMAGE_FORMAT,
        VK_IMAGE_ASPECT_DEPTH_BIT);

    VkImageView view;
    vk_result_check(vkCreateImageView(*device, &view_create_info, nullptr, &view),
        "Failed to create image view for depth image.");

    return std::make_unique<allocated_image>(device, allocator, width, height, image, view, alloc, info,
        DEPTH_IMAGE_FORMAT, DEPTH_IMAGE_USAGE);
}

std::unique_ptr<allocated_image> allocated_image::create_depth_image(vk_device* device, const vk_memory_allocator* allocator,
    uint32_t width, uint32_t height)
{
    return create_depth_image(device, allocator->allocator(), width, height);
}

std::unique_ptr<allocated_image> allocated_image::create_draw_image(vk_device* device, VmaAllocator allocator,
    uint32_t width, uint32_t height)
{
    const auto extent = VkExtent3D{ width, height, 1 };

    const auto create_info = vk_init::image::create_info(extent, DEFAULT_IMAGE_FORMAT, DEFAULT_IMAGE_USAGE);

    constexpr auto alloc_info = VmaAllocationCreateInfo
    {
        .usage = VMA_MEMORY_USAGE_GPU_ONLY,
        .requiredFlags = static_cast<VkMemoryPropertyFlags>(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT),
    };

    VkImage image;
    VmaAllocation alloc;
    VmaAllocationInfo info;

    vk_result_check(vmaCreateImage(allocator, &create_info, &alloc_info, &image, &alloc, &info),
        "Failed to allocate draw image.");

    const auto view_create_info = vk_init::image::view_create_info(image, DEFAULT_IMAGE_FORMAT,
        VK_IMAGE_ASPECT_COLOR_BIT);

    VkImageView view;
    vk_result_check(vkCreateImageView(*device, &view_create_info, nullptr, &view),
        "Failed to create image view for draw image.");

    return std::make_unique<allocated_image>(device, allocator, width, height, image, view, alloc, info,
        DEFAULT_IMAGE_FORMAT, DEFAULT_IMAGE_USAGE);
}

std::unique_ptr<allocated_image> allocated_image::create_draw_image(vk_device* device, const vk_memory_allocator* allocator,
    uint32_t width, uint32_t height)
{
    return create_draw_image(device, allocator->allocator(), width, height);
}
