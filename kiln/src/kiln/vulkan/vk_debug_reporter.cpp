#include <memory>
#include <spdlog/logger.h>
#include <vulkan/vulkan.h>
#include <vulkan/vk_enum_string_helper.h>
#include "kiln/logging.hpp"
#include "vk_init.hpp"
//--------------------------------
#include "kiln/vulkan/vk_debug_reporter.h"

using namespace kiln::vulkan;

VkBool32 on_debug_report(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT object_type,
    uint64_t object, size_t location, int32_t message_code,
    const char* layer_prefix, const char* message, void* user_data)
{
    if (user_data == nullptr)
        return VK_FALSE;

    const auto* reporter = static_cast<vk_debug_reporter*>(user_data);
    reporter->handle_debug_report(flags, object_type, object, location, message_code,
        layer_prefix, message, nullptr);

    return VK_TRUE;
}

vk_debug_reporter::vk_debug_reporter(const std::unique_ptr<vk_instance>& instance) :
    vk_debug_reporter(instance.get())
{
}

vk_debug_reporter::vk_debug_reporter(vk_instance* instance) :
    _log(logging::get_logger(log_name::vulkan)),
    _instance(instance)
{
    const auto create_info = vk_init::debug::report_callback_create_info_EXT(on_debug_report, this);

    const auto result = _instance->create_debug_report_callback(&create_info, nullptr, &_debug_report_callback);
    if (result != VK_SUCCESS)
    {
        _log->warn("Failed to configure debug reporting. Error: {} ({})",
            string_VkResult(result), static_cast<int>(result));
        return;
    }

    _error_enabled = true;
    _warn_enabled = true;
    _warn_perf_enabled = true;
}

vk_debug_reporter::~vk_debug_reporter()
{
    if (_debug_report_callback != nullptr)
    {
        _instance->destroy_debug_report_callback(_debug_report_callback, nullptr);
        _debug_report_callback = nullptr;
    }

    if (_instance != nullptr)
    {
        _instance = nullptr;
    }
}

void vk_debug_reporter::handle_debug_report(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT object_type,
    uint64_t object, size_t location, int32_t message_code,
    const char* layer_prefix, const char* message, void* user_data) const
{
    auto log_level = spdlog::level::off;

    if (_info_enabled && flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT)
        log_level = spdlog::level::info;

    if ((_warn_enabled && flags & VK_DEBUG_REPORT_WARNING_BIT_EXT) ||
        (_warn_perf_enabled && flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT))
        log_level = spdlog::level::warn;

    if (_error_enabled && flags & VK_DEBUG_REPORT_ERROR_BIT_EXT)
        log_level = spdlog::level::err;

    if (_debug_enabled && flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT)
        log_level = spdlog::level::debug;

    if (log_level == spdlog::level::off)
        return;

    const auto obj_type_str = debug_object_type_to_string(object_type);

    _log->log(log_level, "({}) {}\n[{}]> Object: {:#x} | Address: {:#x}",
        std::string(layer_prefix), std::string(message),
        obj_type_str, object, location);
}

std::string_view vk_debug_reporter::debug_object_type_to_string(VkDebugReportObjectTypeEXT obj_type)
{
    return string_VkDebugReportObjectTypeEXT(obj_type);
}
