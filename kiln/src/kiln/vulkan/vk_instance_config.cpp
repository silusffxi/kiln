#include "kiln/vulkan/vk_instance_config.hpp"

using namespace kiln::vulkan;

bool& vk_instance_config::debug_enabled()
{
    return _debug_enabled;
}

std::vector<std::string> vk_instance_config::get_enabled_optional_layers()
{
    std::vector<std::string> layer_names;
    for (auto& [name, enabled] : _optional_layers)
    {
        if (!enabled || name.empty()) continue;
        layer_names.emplace_back(name);
    }

    return layer_names;
}

void vk_instance_config::include_optional_layer(const std::string& layer_name)
{
    add_or_enable_instance_layer(layer_name);
}

void vk_instance_config::include_eos()
{
    add_or_enable_instance_layer("VK_LAYER_EOS_Overlay");
}

void vk_instance_config::include_nv_optimus()
{
    add_or_enable_instance_layer("VK_LAYER_NV_optimus");
}

void vk_instance_config::include_obs_hook()
{
    add_or_enable_instance_layer("VK_LAYER_OBS_HOOK");
}

void vk_instance_config::include_rgsc()
{
    add_or_enable_instance_layer("VK_LAYER_ROCKSTAR_GAMES_social_club");
}

void vk_instance_config::include_rtss()
{
    add_or_enable_instance_layer("VK_LAYER_RTSS");
}

void vk_instance_config::include_steam()
{
    add_or_enable_instance_layer("VK_LAYER_VALVE_steam_overlay");
    add_or_enable_instance_layer("VK_LAYER_VALVE_steam_fossilize");
}

void vk_instance_config::include_vulkan_api_dump()
{
    add_or_enable_instance_layer("VK_LAYER_LUNARG_api_dump");
}

void vk_instance_config::include_vulkan_monitor()
{
    add_or_enable_instance_layer("VK_LAYER_LUNARG_monitor");
}

void vk_instance_config::include_vulkan_gfx_recconstruct()
{
    add_or_enable_instance_layer("VK_LAYER_LUNARG_gfxreconstruct");
}

void vk_instance_config::include_vulkan_profiles()
{
    add_or_enable_instance_layer("VK_LAYER_KHRONOS_profiles");
}

void vk_instance_config::add_or_enable_instance_layer(const std::string& layer_name)
{
    if (_optional_layers.contains(layer_name))
    {
        _optional_layers[layer_name] = true;
    }
    else
    {
        _optional_layers.try_emplace(layer_name, true);
    }
}
