#include <vulkan/vulkan.h>
#include <vma/vk_mem_alloc.h>
//--------------------------------
#include "kiln/vulkan/vk_buffer.h"

using namespace kiln;
using namespace vulkan;

vk_buffer::vk_buffer(VmaAllocator allocator, VkBuffer buffer, VmaAllocation allocation,
    const VmaAllocationInfo& alloc_info, VkDeviceAddress address) :
    _allocator(allocator), _buffer(buffer), _allocation(allocation),
    _alloc_info(alloc_info), _buffer_address(address)
{
}

vk_buffer::~vk_buffer()
{
    if (_buffer != nullptr && _allocation != nullptr)
    {
        vmaDestroyBuffer(_allocator, _buffer, _allocation);
        _allocator = nullptr;
        _buffer = nullptr;
        _allocation = nullptr;
    }
}

VmaAllocation vk_buffer::allocation() const { return _allocation; }

const VmaAllocationInfo& vk_buffer::allocation_info() const { return _alloc_info; }

VkBuffer vk_buffer::buffer() const { return _buffer; }

VkDeviceAddress vk_buffer::buffer_address() const { return _buffer_address; }

void vk_buffer::set_name(const std::string& name)
{
    vmaSetAllocationName(_allocator, _allocation, name.c_str());
    _alloc_info.pName = name.c_str();
}
