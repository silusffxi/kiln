#include <format>
#include <vector>
#include <vulkan/vulkan.h>
#include <vulkan/vk_enum_string_helper.h>
#include "kiln/logging.hpp"
#include "kiln/types.h"
#include "kiln/vulkan/vk_config.h"
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_physical_device.h"
#include "kiln/vulkan/vk_surface.h"
#include "kiln/vulkan/vk_utilities.h"
#include "vk_init.hpp"
//------------------------------
#include "kiln/vulkan/vk_swapchain.h"

using namespace kiln;
using namespace vulkan;

vk_swapchain::vk_swapchain(const vk_config* config, const std::unique_ptr<vk_physical_device>& gpu,
    const std::unique_ptr<vk_device>& device, const std::unique_ptr<vk_surface>& surface,
    VmaAllocator allocator) :
    vk_swapchain(config, gpu.get(), device.get(), surface.get(), allocator)
{
}

vk_swapchain::vk_swapchain(const vk_config* config, vk_physical_device* physical_device,
    vk_device* device, vk_surface* surface, VmaAllocator allocator) :
    _log(logging::get_logger(log_name::vulkan)),
    _config(config),
    _physical_device(physical_device),
    _device(device),
    _surface(surface),
    _allocator(allocator)
{
    build();
}

vk_swapchain::~vk_swapchain()
{
    destroy();
    _device = nullptr;
}

vk_swapchain_image* vk_swapchain::acquire_next_image(VkSemaphore semaphore)
{
    uint32_t index;
    const auto result = vkAcquireNextImageKHR(*_device, _swapchain, 0, semaphore, nullptr, &index);
    if (result == VK_ERROR_OUT_OF_DATE_KHR)
    {
        if (!requires_rebuild())
        {
            _log->warn("'vkAcquireNextImageKHR' returned 'VK_ERROR_OUT_OF_DATE_KHR'. Swapchain will be rebuilt.");
            rebuild();
            return acquire_next_image(semaphore);
        }
        return nullptr;
    }

    if (result != VK_SUCCESS)
    {
        return nullptr;
    }

    _swapchain_image_index = index;
    auto* image = &_images[_swapchain_image_index];

    // Reset the image layout:
    image->layout = VK_IMAGE_LAYOUT_UNDEFINED;
    image->prev_layout = VK_IMAGE_LAYOUT_UNDEFINED;

    return image;
}

VkFormat vk_swapchain::image_format() const
{
    return _swapchain_image_format;
}

VkExtent2D vk_swapchain::image_extent() const
{
    return _swapchain_extent;
}

std::vector<vk_swapchain_image>& vk_swapchain::images()
{
    return _images;
}

void vk_swapchain::process_rebuild()
{
    if (!requires_rebuild())
        return;

    rebuild();
}

void vk_swapchain::request_rebuild()
{
    ++_rebuild_request_count;
}

void vk_swapchain::build()
{
    const auto& surface_caps = _surface->capabilities();
    auto image_count = std::max(3u, surface_caps.minImageCount);
    if (surface_caps.maxImageCount > 0 && image_count > surface_caps.maxImageCount)
        image_count = surface_caps.maxImageCount;

    switch (_config->screen_mode)
    {
    case screen_mode::immediate:
        _present_mode = VK_PRESENT_MODE_IMMEDIATE_KHR;
        break;
    case screen_mode::vsync:
        _present_mode = VK_PRESENT_MODE_FIFO_KHR;
        break;
    case screen_mode::vsync_relaxed:
        _present_mode = VK_PRESENT_MODE_FIFO_RELAXED_KHR;
        break;
    case screen_mode::triple_buffered:
        _present_mode = VK_PRESENT_MODE_MAILBOX_KHR;
        break;
    default:
        // Screen mode is "unknown" or something else we weren't expecting.
        // We'll just fall back to immediate since that's going to be the
        // simplest thing to do.
        _present_mode = VK_PRESENT_MODE_IMMEDIATE_KHR;
        break;
    }

    // Ensure that the surface is supported.
    //-------------------------------------------------------------------------

    if (!_surface->is_present_mode_supported(_present_mode))
    {
        _log->warn("Present mode \"{}\" is NOT supported.",
            string_VkPresentModeKHR(_present_mode));
    }

    VkBool32 surface_is_supported;
    if (vk_result_check(vkGetPhysicalDeviceSurfaceSupportKHR(*_physical_device,
        _physical_device->graphics_queue_family(), *_surface, &surface_is_supported),
        "Failed to determine if surface is supported (Required for swapchain creation).") != VK_SUCCESS)
    {
        return;
    }

    _swapchain_extent.width = surface_caps.currentExtent.width;
    _swapchain_extent.height = surface_caps.currentExtent.height;

    // Create the swapchain
    //-------------------------------------------------------------------------

    const auto& surface_format = _surface->format();

    const auto swapchain_create_info = vk_init::swapchain::create_info_KHR(
        *_surface, surface_caps, surface_format,
        _surface->width(), _surface->height(),
        _present_mode, nullptr, image_count);

    if (vk_result_check(vkCreateSwapchainKHR(*_device, &swapchain_create_info, nullptr, &_swapchain),
        "Failed to create swapchain.") != VK_SUCCESS)
    {
        return;
    }

    // Assign the swapchain image format based on the surface format.
    _swapchain_image_format = _surface->format().format;

    // Get the swapchain images
    //-------------------------------------------------------------------------

    uint32_t swapchain_image_count;
    vkGetSwapchainImagesKHR(*_device, _swapchain, &swapchain_image_count, nullptr);

    auto images = std::vector<VkImage>(swapchain_image_count);
    vkGetSwapchainImagesKHR(*_device, _swapchain, &swapchain_image_count, images.data());

    // We can populate all the swapchain images now that we have the images from the swapchain.
    _images.reserve(swapchain_image_count);

    uint32_t image_index = 0;
    for (const auto& img : images)
    {
        _images.emplace_back(vk_swapchain_image{ .index = image_index });

        auto& sc_img = _images.back();
        sc_img.extent_2d = VkExtent2D{ _swapchain_extent.width, _swapchain_extent.height };
        sc_img.extent_3d = VkExtent3D{ _swapchain_extent.width, _swapchain_extent.height, 1, };
        sc_img.image = img;

        const auto view_create_info = vk_init::image::view_create_info(img,
            surface_format.format, VK_IMAGE_ASPECT_COLOR_BIT, 1);

        vk_result_check(vkCreateImageView(*_device, &view_create_info, nullptr, &sc_img.view),
            std::format("Failed to create swapchain image view for swapchain image at index {}.", image_index));

        image_index++;
    }

    _log->debug("VkSwapchainKHR created. ({} swapchain images.)", _images.size());
}

void vk_swapchain::destroy()
{
    if (!_images.empty())
    {
        auto count = 0;
        for (const auto& img : _images)
        {
            if (img.view == nullptr) continue;
            vkDestroyImageView(*_device, img.view, nullptr);
            count++;
        }

        _log->debug("Destroyed {} swapchain image views.", count);
        _images.clear();
    }

    if (_swapchain != nullptr)
    {
        vkDestroySwapchainKHR(*_device, _swapchain, nullptr);
        _swapchain = nullptr;
        _log->debug("Destroyed swapchain.");
    }
}

void vk_swapchain::rebuild()
{
    // We're not checking if a rebuild was requested in this method because vk_swapchain should
    // be the only caller for this function. As such, it should be able to force a rebuild without
    // one being requested from an external source.

    // Wait for everything to become idle before attempting to execute the rebuild.
    _device->wait_idle();

    // The capabilities for the surface need to be updated before the swapchain is rebuilt.
    // This is required because we pull the dimensions of the swapchain images from
    // the surface capabilities.
    _surface->update_capabilities();

    _log->debug("Swapchain Rebuild: destroying current swapchain...");
    destroy();

    _log->debug("Swapchain Rebuild: building new swapchain...");
    build();

    _log->debug("Swapchain Rebuild: complete. ({} rebuild requests)",
        static_cast<uint8_t>(_rebuild_request_count));

    // In the event that a rebuild was actually requested,
    // we'll just go ahead and reset the request count here.
    _rebuild_request_count = 0;
}