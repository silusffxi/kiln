#include <cstdint>
#include <span>
#include <vulkan/vulkan.h>
#include "kiln/logging.hpp"
#include "kiln/vulkan/vk_utilities.h"
//--------------------------------------
#include "kiln/vulkan/descriptor_allocator.h"

using namespace kiln;
using namespace vulkan;

descriptor_allocator::descriptor_allocator(vk_device* device, uint32_t max_sets,
    const std::span<pool_size_ratio>& pool_ratios) :
    _log(logging::get_logger(log_name::vulkan)),
    _device(device), _max_sets(max_sets)
{
    std::vector<VkDescriptorPoolSize> pool_sizes;
    for (const auto& ratio : pool_ratios)
    {
        const auto r = static_cast<uint32_t>(ratio.ratio);
        pool_sizes.emplace_back(VkDescriptorPoolSize
        {
            .type = ratio.type,
            .descriptorCount = r * max_sets,
        });
    }

    const auto pool_create_info = VkDescriptorPoolCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .maxSets = max_sets,
        .poolSizeCount = static_cast<uint32_t>(pool_sizes.size()),
        .pPoolSizes = pool_sizes.data(),
    };

    vk_result_check(vkCreateDescriptorPool(*_device, &pool_create_info, nullptr, &_pool),
        "Failed to create descriptor pool.");
}

descriptor_allocator::~descriptor_allocator()
{
    if (!_allocated_sets.empty())
    {
        _allocated_sets.clear();
    }

    if (_pool != nullptr)
    {
        vkDestroyDescriptorPool(*_device, _pool, nullptr);
        _pool = nullptr;
    }
}

VkDescriptorSet descriptor_allocator::allocate(VkDescriptorSetLayout layout)
{
    VkDescriptorSet set;
    allocate(&layout, 1u, &set);
    return set;
}

VkDescriptorSet descriptor_allocator::allocate(const VkDescriptorSetLayout* layout)
{
    VkDescriptorSet set;
    allocate(layout, 1u, &set);
    return set;
}

std::vector<VkDescriptorSet> descriptor_allocator::allocate(const std::vector<VkDescriptorSetLayout>& layouts)
{
    std::vector<VkDescriptorSet> sets(layouts.size());
    allocate(layouts.data(), static_cast<uint32_t>(layouts.size()), sets.data());
    return sets;
}

std::vector<VkDescriptorSet> descriptor_allocator::allocate(const VkDescriptorSetLayout* layouts, uint32_t layouts_count)
{
    std::vector<VkDescriptorSet> sets(layouts_count);
    allocate(layouts, layouts_count, sets.data());
    return sets;
}

VkDescriptorPool descriptor_allocator::pool() const
{
    return _pool;
}

void descriptor_allocator::allocate(const VkDescriptorSetLayout* layouts, uint32_t layouts_count, VkDescriptorSet* sets)
{
    if (layouts_count == 0)
    {
        _log->warn("Cannot allocate descriptor set(s) with a layout count of 0.");
        return;
    }

    if (_allocated_sets.size() >= _max_sets || (_allocated_sets.size() + layouts_count) >= _max_sets)
    {
        _log->warn("Cannot allocate any more descriptor sets from this allocator."
            "Allocating {} additional sets would exceed the maximum number of sets. "
            "({} maximum allocations)",
            layouts_count, _max_sets);
        return;
    }

    const auto alloc_info = VkDescriptorSetAllocateInfo
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .pNext = nullptr,
        .descriptorPool = _pool,
        .descriptorSetCount = layouts_count,
        .pSetLayouts = layouts,
    };

    vk_result_check(vkAllocateDescriptorSets(*_device, &alloc_info, sets),
        "Failed to allocate descriptor sets.");

    for (auto idx = 0u; idx < layouts_count; idx++)
    {
        _allocated_sets.emplace_back(sets[idx]);
    }

    _log->debug("{} descriptor set(s) allocated. ({} total allocated sets.)",
        layouts_count, _allocated_sets.size());
}
