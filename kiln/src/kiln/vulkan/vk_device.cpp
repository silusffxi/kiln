#include <cstdint>
#include <format>
#include <memory>
#include <ranges>
#include <unordered_map>
#include <vector>
#include <vulkan/vulkan.h>
#include <vulkan/vk_enum_string_helper.h>
#include "kiln/logging.hpp"
#include "kiln/vulkan/vk_physical_device.h"
#include "kiln/vulkan/vk_utilities.h"
#include "vk_init.hpp"
//---------------------------
#include "kiln/vulkan/vk_device.h"

using namespace kiln;
using namespace vulkan;

namespace
{
    constexpr VkStructPrototype* to_struct_prototype_ptr(void* ptr)
    {
        return static_cast<VkStructPrototype*>(ptr);
    }

    void link_device_create_info(const std::vector<VkStructPrototype*>& prototypes, VkStructPrototype* proto_create_info)
    {
        for (auto& ptr : prototypes)
        {
            // We don't want to link to ourselves
            if (ptr == proto_create_info)
                continue;

            // We don't want to link to an object
            // that already has a reference.
            if (ptr->pNext != nullptr)
                continue;

            ptr->pNext = proto_create_info;
            return;
        }
    }
}

vk_device::vk_device(const std::unique_ptr<vk_physical_device>& phys_device, bool enable_debug) :
    vk_device(phys_device.get(), enable_debug)
{
}

vk_device::vk_device(vk_physical_device* phys_device, bool enable_debug) :
    _log(logging::get_logger(log_name::vulkan))
{
    _compute_queue_family_index = phys_device->compute_queue_family();
    _graphics_queue_family_index = phys_device->graphics_queue_family();

    const auto requested_compute_queue_count = phys_device->get_compute_queue_count();
    _log->debug("Allocating {} queues for compute.", requested_compute_queue_count);

    const auto requested_graphics_queue_count = phys_device->get_graphics_queue_count();
    _log->debug("Allocating {} queues for graphics.", requested_graphics_queue_count);

    auto compute_queue_priorities = std::vector<float>(requested_compute_queue_count);
    std::ranges::fill(compute_queue_priorities, 1.f);

    auto graphics_queue_priorities = std::vector<float>(requested_graphics_queue_count);
    std::ranges::fill(graphics_queue_priorities, 1.f);

    const auto compute_queue_create_info = vk_init::device::queue_create_info(_compute_queue_family_index,
        compute_queue_priorities.data(), requested_compute_queue_count);

    const auto graphics_queue_create_info = vk_init::device::queue_create_info(_graphics_queue_family_index,
        graphics_queue_priorities.data(), requested_graphics_queue_count);

    const std::vector queues{ graphics_queue_create_info, compute_queue_create_info };

    _device = create_device(phys_device, queues, enable_debug);

    // Fetch all the queues within the family that are available to us.
    for (auto idx = 0u; idx < requested_graphics_queue_count; idx++)
    {
        VkQueue temp;
        vkGetDeviceQueue(_device, _graphics_queue_family_index, idx, &temp);
        _graphics_queues.emplace_back(temp);
    }
    _log->debug("Allocated {} graphics queues.", requested_graphics_queue_count);

    for (auto idx = 0u; idx < requested_compute_queue_count; idx++)
    {
        VkQueue temp;
        vkGetDeviceQueue(_device, _compute_queue_family_index, idx, &temp);
        _compute_queues.emplace_back(temp);
    }
    _log->debug("Allocated {} compute queues.", requested_compute_queue_count);

    if (!_graphics_queues.empty())
    {
        _graphics_queue = _graphics_queues[0];
        _log->debug("Assigned queue {} in queue family {} as the default graphics queue.",
            0, _graphics_queue_family_index);
    }

    if (!_compute_queues.empty())
    {
        _compute_queue = _compute_queues[0];
        _log->debug("Assigned queue {} in queue family {} as the default compute queue.",
            0, _compute_queue_family_index);
    }

    const auto cmd_pool_create_info = vk_init::command::pool_create_info(_graphics_queue_family_index,
        VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT);

    vk_result_check(vkCreateCommandPool(_device, &cmd_pool_create_info, nullptr, &_graphics_cmd_pool),
        "Failed to create graphics command pool.");
}

vk_device::~vk_device()
{
    if (!_shader_modules.empty())
    {
        auto destroyed_count = 0u;
        for (auto& mod : _shader_modules)
        {
            vkDestroyShaderModule(_device, mod, nullptr);
            mod = nullptr;
            destroyed_count++;
        }

        _shader_modules.clear();
        _log->debug("Destroyed {} shader modules.", destroyed_count);
    }

    if (!_semaphores.empty())
    {
        auto destroyed_count = 0u;
        for (auto& semaphore : _semaphores)
        {
            vkDestroySemaphore(_device, semaphore, nullptr);
            semaphore = nullptr;
            destroyed_count++;
        }

        _semaphores.clear();
        _log->debug("Destroyed {} semaphores.", destroyed_count);
    }

    if (!_fences.empty())
    {
        auto destroyed_count = 0u;
        for (auto& fence : _fences)
        {
            vkDestroyFence(_device, fence, nullptr);
            fence = nullptr;
            destroyed_count++;
        }

        _fences.clear();
        _log->debug("Destroyed {} fences.", destroyed_count);
    }

    if (!_cmd_pools.empty())
    {
        auto destroyed_count = 0u;
        for (const auto& pool : _cmd_pools | std::views::values)
        {
            vkDestroyCommandPool(_device, pool, nullptr);
            destroyed_count++;
        }

        _log->debug("Destroyed {} additional command pools.", destroyed_count);
    }

    if (_graphics_cmd_pool != nullptr)
    {
        vkDestroyCommandPool(_device, _graphics_cmd_pool, nullptr);
        _graphics_cmd_pool = nullptr;
        _log->debug("Destroyed graphics command pool.");
    }

    if (!_compute_queues.empty()) _compute_queues.clear();
    if (!_graphics_queues.empty()) _graphics_queues.clear();

    // VkQueue does not have any explicit clean up procedures.
    // The queues will be destroyed as part of the call to vkDestroyDevice.
    if (_graphics_queue != nullptr) _graphics_queue = nullptr;

    if (_device != nullptr)
    {
        vkDestroyDevice(_device, nullptr);
        _device = nullptr;
        _log->debug("VkDevice destroyed.");
    }
}

VkCommandBuffer vk_device::create_command_buffer(VkCommandPool pool, bool primary) const
{
    const auto alloc_info = vk_init::command::buffer_allocate_info(pool, 1, primary);

    VkCommandBuffer cmd_buf;
    vk_result_check(vkAllocateCommandBuffers(_device, &alloc_info, &cmd_buf),
        std::format("Failed to create {} command buffer.", primary ? "primary" : "secondary"));

    return cmd_buf;
}

VkCommandPool vk_device::create_command_pool(const VkAllocationCallbacks* alloc_callbacks)
{
    const auto create_info = vk_init::command::pool_create_info(_graphics_queue_family_index,
        VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT);

    return create_command_pool(create_info, alloc_callbacks);
}

VkCommandPool vk_device::create_command_pool(uint32_t queue_family, const VkAllocationCallbacks* alloc_callbacks)
{
    const auto create_info = vk_init::command::pool_create_info(queue_family,
        VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT);

    return create_command_pool(create_info, alloc_callbacks);
}

VkCommandPool vk_device::create_command_pool(const VkCommandPoolCreateInfo& create_info,
    const VkAllocationCallbacks* alloc_callbacks)
{
    if (_cmd_pools.contains(create_info.queueFamilyIndex))
    {
        // Return the command pool if we already have one
        // for a given queue family index.
        return _cmd_pools.at(create_info.queueFamilyIndex);
    }

    VkCommandPool cmd_pool;
    if (vk_result_check(vkCreateCommandPool(_device, &create_info, alloc_callbacks, &cmd_pool),
        "Failed to create command pool.") != VK_SUCCESS)
    {
        return nullptr;
    }

    _cmd_pools.try_emplace(create_info.queueFamilyIndex, cmd_pool);
    return cmd_pool;
}

VkFence vk_device::create_fence(VkFenceCreateFlags flags)
{
    const auto create_info = vk_init::fence::create_info(flags);

    VkFence fence;
    vk_result_check(vkCreateFence(_device, &create_info, nullptr, &fence),
        "Failed to create fence.");

    _fences.emplace_back(fence);
    return fence;
}

VkCommandBuffer vk_device::create_graphics_command_buffer(bool primary) const
{
    return create_command_buffer(_graphics_cmd_pool, primary);
}

VkSemaphore vk_device::create_semaphore(VkSemaphoreCreateFlags flags)
{
    const auto create_info = vk_init::semaphore::create_info(flags);

    VkSemaphore semaphore;
    vk_result_check(vkCreateSemaphore(_device, &create_info, nullptr, &semaphore),
        "Failed to create semaphore.");

    _semaphores.emplace_back(semaphore);
    return semaphore;
}

VkShaderModule vk_device::create_shader_module(const std::vector<uint32_t>& shader_binary)
{
    auto create_info = VkShaderModuleCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .codeSize = shader_binary.size() * 4LLU,
        .pCode = shader_binary.data(),
    };

    VkShaderModule mod;
    vk_result_check(vkCreateShaderModule(_device, &create_info, nullptr, &mod),
        "Failed to create shader module.");

    _shader_modules.emplace_back(mod);
    return mod;
}

VkQueue vk_device::get_graphics_queue() const { return _graphics_queue; }
VkCommandPool vk_device::get_graphics_command_pool() const { return _graphics_cmd_pool; }

VkQueue vk_device::get_compute_queue(uint32_t index) const
{
    if (index >= static_cast<uint32_t>(_compute_queues.size()))
    {
        return nullptr;
    }

    return _compute_queues.at(index);
}

VkQueue vk_device::get_graphics_queue(uint32_t index) const
{
    if (index >= static_cast<uint32_t>(_graphics_queues.size()))
    {
        return nullptr;
    }

    return _graphics_queues.at(index);
}

void vk_device::reset_fence(VkFence fence) const
{
    const auto result = vkResetFences(_device, 1, &fence);
    if (result == VK_SUCCESS)
        return;

    const auto result_str = string_VkResult(result);
    _log->error("vkResetFences failed. Error: {} ({})",
        result_str, static_cast<int>(result));
}

void vk_device::wait_for_fence(const VkFence fence, const uint32_t wait_seconds/* = 1*/) const
{
    const auto result = vkWaitForFences(_device, 1, &fence, VK_TRUE, wait_seconds * vk_timeout_seconds);
    if (result == VK_SUCCESS)
        return;

    const auto result_str = string_VkResult(result);
    _log->error("vkWaitForFences failed. Error: {} ({})",
        result_str, static_cast<int>(result));
}

void vk_device::wait_for_semaphore(VkSemaphore semaphore)
{
    
}

void vk_device::wait_idle() const
{
    if (_device == nullptr)
        return;

    vkDeviceWaitIdle(_device);
}

void vk_device::wait_queue_idle() const
{
    if (_device == nullptr || _graphics_queue == nullptr)
        return;

    vkQueueWaitIdle(_graphics_queue);
}

VkDevice vk_device::create_device(vk_physical_device* gpu, const std::vector<VkDeviceQueueCreateInfo>& queues,
    bool enable_debug) const
{
    auto device_create_info = vk_init::device::create_info(queues.data(),
        static_cast<uint32_t>(queues.size()),
        gpu->features(),
        enable_debug);

    // Including this with the VkDeviceCreateInfo that is ultimately passed
    // to vkCreateDevice is required in order to get synchronization 2 features
    // working. Refer to: https://stackoverflow.com/a/76472644 for more info.
    auto sync2_features = VkPhysicalDeviceSynchronization2Features
    {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SYNCHRONIZATION_2_FEATURES,
        .pNext = nullptr,
        .synchronization2 = VK_TRUE,
    };

    // Needed to allow for dynamic rendering as long as the GPU actually supports it.
    auto dynamic_rendering_features = VkPhysicalDeviceDynamicRenderingFeatures
    {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DYNAMIC_RENDERING_FEATURES,
        .pNext = nullptr,
        .dynamicRendering = VK_TRUE,
    };

    auto buffer_device_address_features = VkPhysicalDeviceBufferDeviceAddressFeatures
    {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES,
        .pNext = nullptr,

        .bufferDeviceAddress = VK_TRUE,
    };

    // (It's 2:37 AM - why am I manually building a linked list to pass to vkCreateDevice.)
    //------
    // To add additional features that should be included with the call to vkCreateDevice:
    //
    // 1. Add another "auto* ptr_some_feature = to_struct_prototype_ptr(&some_feature_create_info);
    // 2. Include the pointer created in the previous step in the "create_info_pointers" list.
    //
    //      const std::vector create_info_pointers =
    //      {
    //          ...,
    //          ptr_dynamic_rendering,
    //          ptr_some_feature,
    //      };
    //
    // 3. (Optional) Add a check using the GPU to see if the feature you want to add.
    // 4. Make a call to "link_device_create_info" with the "create_info_pointers" and the
    //    pointer to the VkStructPrototype for the feature you want to add.

    auto* ptr_device_create_info = to_struct_prototype_ptr(&device_create_info);
    auto* ptr_sync2 = to_struct_prototype_ptr(&sync2_features);
    auto* ptr_dynamic_rendering = to_struct_prototype_ptr(&dynamic_rendering_features);
    auto* ptr_buffer_dev_address = to_struct_prototype_ptr(&buffer_device_address_features);

    const std::vector create_info_pointers =
    {
        ptr_device_create_info,
        ptr_sync2,
        ptr_dynamic_rendering,
        ptr_buffer_dev_address,
    };

    if (gpu->supports_synchronization2())
    {
        link_device_create_info(create_info_pointers, ptr_sync2);
        _log->debug("vkCreateDevice: included support for synchronization2.");
    }

    if (gpu->supports_dynamic_rendering())
    {
        link_device_create_info(create_info_pointers, ptr_dynamic_rendering);
        _log->debug("vkCreateDevice: included support for dynamic rendering.");
    }

    if (gpu->supports_buffer_device_address())
    {
        link_device_create_info(create_info_pointers, ptr_buffer_dev_address);
        _log->debug("vkCreateDevice: included support for buffer device address.");
    }

    // Safety check to just make sure we don't have a possible circular reference.
    if (create_info_pointers.back()->pNext != nullptr)
    {
        _log->warn("vkCreateDevice: last create info or feature struct included in the chain of structs "
            "passed has a value for \"pNext\". Expected the last item in the chain to have a null "
            "value for \"pNext\". This may indicate a possible circular reference.");
    }

    VkDevice device;
    vk_result_check(vkCreateDevice(*gpu, &device_create_info, nullptr, &device),
        "Failed to create logical device.");

    if (device != nullptr)
    {
        _log->debug("VkDevice created.");
    }

    return device;
}
