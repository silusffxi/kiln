#include <cstdint>
#include <vector>
#include <vulkan/vulkan.h>
#include "kiln/vulkan/vk_instance.h"
#include "kiln/vulkan/vk_physical_device.h"
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_surface.h"
#include "kiln/vulkan/vk_swapchain.h"
#include "kiln/vulkan/vk_frame_data.h"
#include "kiln/vulkan/vk_utilities.h"
#include "vk_init.hpp"
//------------------------------------------
#include "kiln/vulkan/vk_render_extension.h"

using namespace kiln;
using namespace vulkan;

vk_render_extension::vk_render_extension(const std::string& name, const vk_render_extension_create_args& args) :
    vk_render_extension(name, args.instance, args.gpu, args.device, args.surface, args.swapchain)
{
}

vk_render_extension::vk_render_extension(const std::string& name, vk_instance* instance, vk_physical_device* gpu,
    vk_device* device, vk_surface* surface, vk_swapchain* swapchain) :
    _name(name), _instance(instance), _gpu(gpu), _device(device), _surface(surface), _swapchain(swapchain)
{
    _max_supported_msaa = _gpu->get_supported_msaa();

    // Get everything set up so that we can use a parallel command buffer in the extension.
    _cmd =
    {
        .inherit_render_info = vk_init::command::buffer_inheritance_rendering_info(_max_supported_msaa),
        .inherit_info = vk_init::command::buffer_inheritance_info(),
    };

    _cmd.begin_info = vk_init::command::buffer_begin_info(VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT, &_cmd.inherit_info);
    //_cmd.begin_info.pNext = &_cmd.inherit_render_info;

    for (auto idx = 0u; idx < max_frames_in_flight; idx++)
    {
        _cmd.buffers.emplace_back(_device->create_graphics_command_buffer(false));
    }
}

void vk_render_extension::disable() { _enabled = false; }
void vk_render_extension::enable() { _enabled = true; }
bool vk_render_extension::is_enabled() const { return _enabled; }

void vk_render_extension::frame_begin(vk_render_image* target_image, vk_render_image* swapchain_image)
{
    // Do not start another frame if we're in the process of drawing
    // another frame or if the extension is disabled.
    if (_frame_in_progress || !_enabled) return;

    command_buffer_begin();
    _frame_in_progress = true;
    _render_image      = target_image;
    _swapchain_image   = swapchain_image;

    on_frame_begin(current_command_buffer(), _render_image, _swapchain_image);
}

void vk_render_extension::frame_end(VkCommandBuffer parent)
{
    // Don't attempt to end a frame if we haven't actually
    // started drawing a frame.
    // We're not checking whether the extension is enabled,
    // or not, here because we don't want to break frame rendering
    // if the extension was disabled in the middle of frame rendering.
    if (!_frame_in_progress) return;

    const auto cmd = current_command_buffer();

    on_frame_end(cmd, _render_image, _swapchain_image);
    command_buffer_end();
    vkCmdExecuteCommands(parent, 1, &cmd);

    next_frame();
    _render_image      = nullptr;
    _swapchain_image   = nullptr;
    _frame_in_progress = false;
}

void vk_render_extension::handle_event(const SDL_Event* e)
{
    on_event(e);
}

std::string vk_render_extension::name() { return _name; }

inline VkCommandBuffer vk_render_extension::current_command_buffer() const
{
    return _cmd.buffers[_frame_data.current_index];
}

void vk_render_extension::on_event(const SDL_Event* e)
{
}

void vk_render_extension::on_frame_begin(VkCommandBuffer cmd, vk_render_image* target_image, vk_render_image* swapchain_image)
{
}

void vk_render_extension::on_frame_end(VkCommandBuffer cmd, vk_render_image* target_image, vk_render_image* swapchain_image)
{
}

vk_instance*        vk_render_extension::instance() const { return _instance; }
vk_physical_device* vk_render_extension::gpu() const { return _gpu; }
vk_device*          vk_render_extension::device() const { return _device; }
vk_surface*         vk_render_extension::surface() const { return _surface; }
vk_swapchain*       vk_render_extension::swapchain() const { return _swapchain; }
vk_render_image*    vk_render_extension::render_image() const { return _render_image; }

void vk_render_extension::command_buffer_begin() const
{
    const auto cmd = current_command_buffer();

    vk_result_check(vkResetCommandBuffer(cmd, 0),
        "Failed to reset secondary command buffer in render extension.");

    vk_result_check(vkBeginCommandBuffer(cmd, &_cmd.begin_info),
        "Failed to begin secondary command buffer in render extension.");
}

void vk_render_extension::command_buffer_end() const
{
    const auto cmd = current_command_buffer();

    vk_result_check(vkEndCommandBuffer(cmd),
        "Failed to end secondary command buffer in render extension.");
}

void vk_render_extension::next_frame()
{
    _frame_data.current_index = ++_frame_data.current_index % static_cast<uint32_t>(_cmd.buffers.size());
}
