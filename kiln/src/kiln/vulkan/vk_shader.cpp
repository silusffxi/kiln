#include <filesystem>
#include <memory>
//--------------------------------
#include "kiln/vulkan/vk_shader.h"

using namespace kiln;
using namespace vulkan;

vk_shader::vk_shader(vk_device* device, VkShaderModule mod, const shaders::binary& shader_def) :
    _device(device), _mod(mod),
    _name(shader_def.name), _file_path(shader_def.path), _type(shader_def.type)
{
}

std::unique_ptr<vk_shader> vk_shader::from_shader_binary(vk_device* device, const shaders::binary& def)
{
    // We can't create a shader module if we don't have a device or the shader definition
    // doesn't actually have any SPIR-V binary.
    if (device == nullptr || def.spirv.empty())
        return nullptr;

    const auto shader_module = device->create_shader_module(def.spirv);
    return std::make_unique<vk_shader>(device, shader_module, def);
}

VkShaderModule vk_shader::shader_module() const { return _mod; }
const std::string& vk_shader::name() const { return _name; }
const std::filesystem::path& vk_shader::file_path() const { return _file_path; }
const shader_type& vk_shader::type() const { return _type; }
