#pragma once
#include <functional>
#include <memory>
#include <unordered_map>
#include <vector>
#include <vulkan/vulkan.h>
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_types.h"

namespace kiln::vulkan
{
    class vk_pipeline_builder
    {
        /**
        *\brief Blend modes that are available when \c enable_blending is called.
        */
        static std::unordered_map<blend_mode, const VkPipelineColorBlendAttachmentState*> available_blend_modes;

        VkDevice         _device;
        VkPipelineLayout _pipeline_layout;

        // Shaders
        //---------------------------------------------------------------------

        std::vector<VkPipelineShaderStageCreateInfo> _shader_stages;

        // Vertex Data
        //---------------------------------------------------------------------

        VkPipelineVertexInputStateCreateInfo _vertex_input_create_info =
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
        };

        std::vector<VkVertexInputBindingDescription> _vertex_binding_descriptions;
        std::vector<VkVertexInputAttributeDescription> _vertex_attribute_descriptions;

        // Input Assembly
        //---------------------------------------------------------------------

        VkPipelineInputAssemblyStateCreateInfo _input_assembly_create_info =
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
        };

        // Rasterization State
        //---------------------------------------------------------------------

        VkPipelineRasterizationStateCreateInfo _rasterizer_create_info =
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,

            //.depthClampEnable = VK_FALSE,
            //.rasterizerDiscardEnable = VK_FALSE,
            //.polygonMode = VK_POLYGON_MODE_FILL,
            //.cullMode = VK_CULL_MODE_NONE,
            //.frontFace = VK_FRONT_FACE_CLOCKWISE,
            //.depthBiasEnable = VK_FALSE,
            //.depthBiasConstantFactor = 0.0f,
            //.depthBiasClamp = 0.0f,
            //.depthBiasSlopeFactor = 0.0f,
            //.lineWidth = 0.f,
        };

        // Multi-sampling State
        //---------------------------------------------------------------------

        VkPipelineMultisampleStateCreateInfo _multisampling_create_info =
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
        };

        // Depth-stencil State
        //---------------------------------------------------------------------

        VkPipelineDepthStencilStateCreateInfo _depth_stencil_create_info =
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
        };

        // Color Blend State
        //---------------------------------------------------------------------

        VkFormat _color_attachment_format = VK_FORMAT_UNDEFINED;

        const VkPipelineColorBlendAttachmentState* _color_blend_attachment_state = nullptr;
        VkPipelineColorBlendStateCreateInfo _color_blend_state_create_info =
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,

            .logicOpEnable = VK_FALSE,
            .logicOp = VK_LOGIC_OP_COPY,
        };

        // Render Create Info
        //---------------------------------------------------------------------

        VkPipelineRenderingCreateInfo _render_info_create_info =
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_RENDERING_CREATE_INFO,
            .pNext = nullptr,
        };

        // Dynamic States
        //---------------------------------------------------------------------

        std::vector<VkDynamicState> _dynamic_states;

    public:
        vk_pipeline_builder();
        vk_pipeline_builder(const vk_device* device);
        vk_pipeline_builder(const std::unique_ptr<vk_device>& device);
        vk_pipeline_builder(const vk_device* device, VkPipelineLayout layout);
        vk_pipeline_builder(const std::unique_ptr<vk_device>& device, VkPipelineLayout layout);

        vk_pipeline_builder& add_dynamic_state(VkDynamicState state);

        vk_pipeline_builder& add_fragment_shader(VkShaderModule shader);
        vk_pipeline_builder& add_vertex_shader(VkShaderModule shader);

        vk_pipeline_builder& add_vertex_attribute(VkVertexInputAttributeDescription desc);
        vk_pipeline_builder& add_vertex_attribute(uint32_t location, uint32_t binding,
            VkFormat format, uint32_t offset);

        vk_pipeline_builder& add_vertex_binding(uint32_t binding, uint32_t stride,
            VkVertexInputRate input_rate = VK_VERTEX_INPUT_RATE_VERTEX);

        vk_pipeline_builder& color_attachment_format(VkFormat format);

        vk_pipeline_builder& cull_mode(VkCullModeFlags mode, VkFrontFace face);

        vk_pipeline_builder& custom_depth_stencil(const std::function<void(VkPipelineDepthStencilStateCreateInfo*)>& func);

        vk_pipeline_builder& depth_format(VkFormat format);

        vk_pipeline_builder& disable_blending();
        vk_pipeline_builder& enable_blending(blend_mode mode);

        vk_pipeline_builder& disable_depth_test();
        vk_pipeline_builder& enable_depth_test(bool depth_write_enable, VkCompareOp compare_op);

        vk_pipeline_builder& input_topology(VkPrimitiveTopology topology);

        vk_pipeline_builder& multisampling_none();
        vk_pipeline_builder& multisampling(VkSampleCountFlagBits samples);

        vk_pipeline_builder& polygon_mode(VkPolygonMode mode, float line_width = 1.f);

        vk_pipeline_builder& use_layout(VkPipelineLayout layout);

        /**
        *\brief Makes the necessary calls required to build the graphics pipeline.
        *       Returns \c nullptr if building the pipeline fails.
        */
        VkPipeline build();

        void reset();
    };
}
