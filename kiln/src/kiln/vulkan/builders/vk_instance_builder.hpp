#pragma once
#include <string>
#include <vector>
#include <vulkan/vulkan.h>

namespace kiln::vulkan
{
    class instance_builder
    {
        VkApplicationInfo _app_info =
        {
            .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
            .pNext = nullptr,
            .pApplicationName = "",
            .applicationVersion = VK_MAKE_VERSION(0, 1, 0),
            .pEngineName = "",
            .engineVersion = VK_MAKE_VERSION(0, 1, 0),
            .apiVersion = VK_API_VERSION_1_3,
        };

        std::vector<std::string> _extension_names;
        std::vector<std::string> _layer_names;

    public:
        instance_builder& add_extension(const std::string& name);
        instance_builder& add_layer(const std::string& name);
        instance_builder& app_name(const std::string& app_name);
        instance_builder& app_version(uint32_t major, uint32_t minor, uint32_t patch);
        instance_builder& engine_name(const std::string& engine_name);
        instance_builder& engine_version(uint32_t major, uint32_t minor, uint32_t patch);
        instance_builder& use_extensions(const std::vector<std::string>& extensions);
        instance_builder& use_layers(const std::vector<std::string>& layers);
        instance_builder& vulkan_api_100();
        instance_builder& vulkan_api_110();
        instance_builder& vulkan_api_120();
        instance_builder& vulkan_api_130();
        instance_builder& vulkan_api_version(uint32_t api_version);

        [[nodiscard]] VkInstance build() const;
    };
}