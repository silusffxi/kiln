#pragma once
#include <vulkan/vulkan.h>
#include <vector>
#include "kiln/vulkan/vk_device.h"

namespace kiln::vulkan
{
    class vk_descriptor_layout_builder
    {
        std::vector<VkDescriptorSetLayoutBinding> _bindings;

        VkShaderStageFlags _shader_stages = 0;

    public:
        vk_descriptor_layout_builder& add_binding(uint32_t binding, VkDescriptorType type);
        vk_descriptor_layout_builder& add_binding(uint32_t binding, VkDescriptorType type, VkShaderStageFlags shader_stage);
        vk_descriptor_layout_builder& add_shader_stage(VkShaderStageFlagBits stage);

        void clear();
        VkDescriptorSetLayout build(const vk_device* device);
    };
}