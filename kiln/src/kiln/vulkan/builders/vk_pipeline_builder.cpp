#include <vector>
#include <vulkan/vulkan.h>
#include <vulkan/vk_enum_string_helper.h>
#include "kiln/logging.hpp"
#include "kiln/vulkan/vk_types.h"
#include "../vk_init.hpp"
//----------------------------------
#include "vk_pipeline_builder.hpp"

using namespace kiln;
using namespace vulkan;

namespace
{
    VkPipelineColorBlendAttachmentState BLEND_MODE_NONE =
    {
        .blendEnable = VK_FALSE,
        .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
    };

    VkPipelineColorBlendAttachmentState BLEND_MODE_ADDITIVE =
    {
        .blendEnable         = VK_TRUE,
        .srcColorBlendFactor = VK_BLEND_FACTOR_ONE,
        .dstColorBlendFactor = VK_BLEND_FACTOR_DST_ALPHA,
        .colorBlendOp        = VK_BLEND_OP_ADD,
        .srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
        .dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
        .alphaBlendOp        = VK_BLEND_OP_ADD,
        .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
    };

    VkPipelineColorBlendAttachmentState BLEND_MODE_ALPHA_BLEND =
    {
        .blendEnable         = VK_TRUE,
        .srcColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA,
        .dstColorBlendFactor = VK_BLEND_FACTOR_DST_ALPHA,
        .colorBlendOp        = VK_BLEND_OP_ADD,
        .srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
        .dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
        .alphaBlendOp        = VK_BLEND_OP_ADD,
        .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
    };
}

std::unordered_map<blend_mode, const VkPipelineColorBlendAttachmentState*> vk_pipeline_builder::available_blend_modes =
{
    { blend_mode::none,        &BLEND_MODE_NONE },
    { blend_mode::additive,    &BLEND_MODE_ADDITIVE },
    { blend_mode::alpha_blend, &BLEND_MODE_ALPHA_BLEND },
};

vk_pipeline_builder::vk_pipeline_builder() :
    _device(nullptr), _pipeline_layout(nullptr)
{
}

vk_pipeline_builder::vk_pipeline_builder(const vk_device* device) :
    _device(*device), _pipeline_layout(nullptr)
{
}

vk_pipeline_builder::vk_pipeline_builder(const std::unique_ptr<vk_device>& device) :
    _device(*device), _pipeline_layout(nullptr)
{
}

vk_pipeline_builder::vk_pipeline_builder(const vk_device* device, VkPipelineLayout layout) :
    _device(*device), _pipeline_layout(layout)
{
}

vk_pipeline_builder::vk_pipeline_builder(const std::unique_ptr<vk_device>& device, VkPipelineLayout layout) :
    _device(*device), _pipeline_layout(layout)
{
}

vk_pipeline_builder& vk_pipeline_builder::add_dynamic_state(VkDynamicState state)
{
    _dynamic_states.emplace_back(state);
    return *this;
}

vk_pipeline_builder& vk_pipeline_builder::add_fragment_shader(VkShaderModule shader)
{
    _shader_stages.emplace_back(vk_init::pipeline::shader_stage_create_info(
        VK_SHADER_STAGE_FRAGMENT_BIT, shader));

    return *this;
}

vk_pipeline_builder& vk_pipeline_builder::add_vertex_shader(VkShaderModule shader)
{
    _shader_stages.emplace_back(vk_init::pipeline::shader_stage_create_info(
        VK_SHADER_STAGE_VERTEX_BIT, shader));

    return *this;
}

vk_pipeline_builder& vk_pipeline_builder::add_vertex_attribute(VkVertexInputAttributeDescription desc)
{
    _vertex_attribute_descriptions.emplace_back(desc);
    return *this;
}

vk_pipeline_builder& vk_pipeline_builder::add_vertex_attribute(uint32_t location, uint32_t binding,
    VkFormat format, uint32_t offset)
{
    _vertex_attribute_descriptions.emplace_back(VkVertexInputAttributeDescription
        {
            .location = location,
            .binding = binding,
            .format = format,
            .offset = offset,
        });

    return *this;
}

vk_pipeline_builder& vk_pipeline_builder::add_vertex_binding(uint32_t binding, uint32_t stride,
    VkVertexInputRate input_rate)
{
    _vertex_binding_descriptions.emplace_back(binding, stride, input_rate);
    return *this;
}

vk_pipeline_builder& vk_pipeline_builder::color_attachment_format(VkFormat format)
{
    _color_attachment_format = format;
    _render_info_create_info.colorAttachmentCount = 1;
    _render_info_create_info.pColorAttachmentFormats = &_color_attachment_format;

    return *this;
}

vk_pipeline_builder& vk_pipeline_builder::cull_mode(VkCullModeFlags mode, VkFrontFace face)
{
    _rasterizer_create_info.cullMode = mode;
    _rasterizer_create_info.frontFace = face;

    return *this;
}

vk_pipeline_builder& vk_pipeline_builder::custom_depth_stencil(const std::function<void(VkPipelineDepthStencilStateCreateInfo*)>& func)
{
    func(&_depth_stencil_create_info);
    return *this;
}

vk_pipeline_builder& vk_pipeline_builder::depth_format(VkFormat format)
{
    _render_info_create_info.depthAttachmentFormat = format;

    return *this;
}

vk_pipeline_builder& vk_pipeline_builder::disable_blending()
{
    _color_blend_attachment_state = available_blend_modes.at(blend_mode::none);

    return *this;
}

vk_pipeline_builder& vk_pipeline_builder::enable_blending(const blend_mode mode)
{
    _color_blend_attachment_state = available_blend_modes.contains(mode)
        ? available_blend_modes.at(mode)
        : available_blend_modes.at(blend_mode::none);

    //_color_blend_attachment_state.blendEnable = VK_TRUE;
    //_color_blend_attachment_state.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
    //_color_blend_attachment_state.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    //_color_blend_attachment_state.colorBlendOp = VK_BLEND_OP_ADD;
    //_color_blend_attachment_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    //_color_blend_attachment_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    //_color_blend_attachment_state.alphaBlendOp = VK_BLEND_OP_ADD;
    //
    //_color_blend_attachment_state.colorWriteMask =
    //    VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT |
    //    VK_COLOR_COMPONENT_A_BIT;

    //_color_blend_attachment_state.blendEnable = VK_TRUE;
    //_color_blend_attachment_state.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
    //_color_blend_attachment_state.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    //_color_blend_attachment_state.colorBlendOp = VK_BLEND_OP_ADD;
    //_color_blend_attachment_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
    //_color_blend_attachment_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    //_color_blend_attachment_state.alphaBlendOp = VK_BLEND_OP_ADD;
    //
    //_color_blend_attachment_state.colorWriteMask =
    //    VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

    return *this;
}

vk_pipeline_builder& vk_pipeline_builder::disable_depth_test()
{
    _depth_stencil_create_info.depthTestEnable = VK_FALSE;
    _depth_stencil_create_info.depthWriteEnable = VK_FALSE;
    _depth_stencil_create_info.depthCompareOp = VK_COMPARE_OP_NEVER;
    _depth_stencil_create_info.depthBoundsTestEnable = VK_FALSE;
    _depth_stencil_create_info.stencilTestEnable = VK_FALSE;
    _depth_stencil_create_info.front = {};
    _depth_stencil_create_info.back = {};
    _depth_stencil_create_info.minDepthBounds = 0.f;
    _depth_stencil_create_info.maxDepthBounds = 1.f;

    return *this;
}

vk_pipeline_builder& vk_pipeline_builder::enable_depth_test(const bool depth_write_enable, const VkCompareOp compare_op)
{
    _depth_stencil_create_info.depthTestEnable = VK_TRUE;
    _depth_stencil_create_info.depthWriteEnable = depth_write_enable;
    _depth_stencil_create_info.depthCompareOp = compare_op;
    _depth_stencil_create_info.depthBoundsTestEnable = VK_FALSE;
    _depth_stencil_create_info.stencilTestEnable = VK_FALSE;
    _depth_stencil_create_info.front = { };
    _depth_stencil_create_info.back = { };
    _depth_stencil_create_info.minDepthBounds = 0.f;
    _depth_stencil_create_info.maxDepthBounds = 1.f;

    return *this;
}

vk_pipeline_builder& vk_pipeline_builder::input_topology(VkPrimitiveTopology topology)
{
    if (topology == VK_PRIMITIVE_TOPOLOGY_LINE_STRIP || topology == VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY ||
        topology == VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP || topology == VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY)
    {
        _input_assembly_create_info.primitiveRestartEnable = VK_TRUE;
    }
    else
    {
        _input_assembly_create_info.primitiveRestartEnable = VK_FALSE;
    }

    _input_assembly_create_info.topology = topology;

    return *this;
}

vk_pipeline_builder& vk_pipeline_builder::multisampling_none()
{
    return multisampling(VK_SAMPLE_COUNT_1_BIT);
}

vk_pipeline_builder& vk_pipeline_builder::multisampling(VkSampleCountFlagBits samples)
{
    _multisampling_create_info.rasterizationSamples = samples;
    _multisampling_create_info.sampleShadingEnable = VK_FALSE;
    _multisampling_create_info.minSampleShading = 1.f;
    _multisampling_create_info.pSampleMask = nullptr;

    _multisampling_create_info.alphaToCoverageEnable = VK_FALSE;
    _multisampling_create_info.alphaToOneEnable = VK_FALSE;

    return *this;
}

vk_pipeline_builder& vk_pipeline_builder::polygon_mode(VkPolygonMode mode, float line_width/* = 1.f */)
{
    _rasterizer_create_info.polygonMode = mode;
    _rasterizer_create_info.lineWidth = line_width;

    return *this;
}

vk_pipeline_builder& vk_pipeline_builder::use_layout(VkPipelineLayout layout)
{
    _pipeline_layout = layout;
    return *this;
}

VkPipeline vk_pipeline_builder::build()
{
    constexpr auto viewport_create_info = VkPipelineViewportStateCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .pNext = nullptr,
        .viewportCount = 1,
        .pViewports = nullptr,
        .scissorCount = 1,
        .pScissors = nullptr,
    };

    // Color Blending
    //-------------------------------------------
    _color_blend_state_create_info.attachmentCount = 1;
    _color_blend_state_create_info.pAttachments = _color_blend_attachment_state;

    if (_color_blend_attachment_state->blendEnable != VK_FALSE)
    {
        _color_blend_state_create_info.blendConstants[0] = 1.0f;
        _color_blend_state_create_info.blendConstants[1] = 1.0f;
        _color_blend_state_create_info.blendConstants[2] = 1.0f;
        _color_blend_state_create_info.blendConstants[3] = 1.0f;
    }

    // Vertex Input
    //-------------------------------------------
    if (!_vertex_binding_descriptions.empty())
    {
        _vertex_input_create_info.pVertexBindingDescriptions = _vertex_binding_descriptions.data();
        _vertex_input_create_info.vertexBindingDescriptionCount =
            static_cast<uint32_t>(_vertex_binding_descriptions.size());
    }

    if (!_vertex_attribute_descriptions.empty())
    {
        _vertex_input_create_info.pVertexAttributeDescriptions = _vertex_attribute_descriptions.data();
        _vertex_input_create_info.vertexAttributeDescriptionCount =
            static_cast<uint32_t>(_vertex_attribute_descriptions.size());
    }

    // If we don't have any dynamic states, then we'll add some defaults:
    if (_dynamic_states.empty())
    {
        _dynamic_states.emplace_back(VK_DYNAMIC_STATE_VIEWPORT);
        _dynamic_states.emplace_back(VK_DYNAMIC_STATE_SCISSOR);
    }

    const auto dynamic_state_create_info = VkPipelineDynamicStateCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .dynamicStateCount = static_cast<uint32_t>(_dynamic_states.size()),
        .pDynamicStates = _dynamic_states.data(),
    };

    const auto gfx_pipeline_create_info = VkGraphicsPipelineCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        .pNext = &_render_info_create_info,
        .flags = 0,

        .stageCount = static_cast<uint32_t>(_shader_stages.size()),
        .pStages = _shader_stages.data(),
        .pVertexInputState = &_vertex_input_create_info,
        .pInputAssemblyState = &_input_assembly_create_info,
        .pTessellationState = nullptr,
        .pViewportState = &viewport_create_info,
        .pRasterizationState = &_rasterizer_create_info,
        .pMultisampleState = &_multisampling_create_info,
        .pDepthStencilState = &_depth_stencil_create_info,
        .pColorBlendState = &_color_blend_state_create_info,
        .pDynamicState = &dynamic_state_create_info,
        .layout = _pipeline_layout,
        //.renderPass = nullptr,
        //.subpass = 0,
        //.basePipelineHandle = nullptr,
        //.basePipelineIndex = 0,
    };

    const auto log = logging::get_logger(log_name::vulkan);

    VkPipeline pipeline;
    const auto result = vkCreateGraphicsPipelines(_device, nullptr, 1, &gfx_pipeline_create_info,
        nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        const auto result_name = std::string(string_VkResult(result));

        log->error(
            "Failed to create graphics pipeline (via vkCreateGraphicsPipeline). "
            "Error: {} ({:#x}",
            result_name, static_cast<int>(result));
        return nullptr;
    }

    return pipeline;
}

void vk_pipeline_builder::reset()
{
    _shader_stages.clear();

    _input_assembly_create_info = VkPipelineInputAssemblyStateCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
    };

    _rasterizer_create_info = VkPipelineRasterizationStateCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
    };

    _color_blend_attachment_state = nullptr;

    _multisampling_create_info = VkPipelineMultisampleStateCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
    };

    _depth_stencil_create_info = VkPipelineDepthStencilStateCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
    };

    _render_info_create_info = VkPipelineRenderingCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RENDERING_CREATE_INFO,
        .pNext = nullptr,
    };

    _pipeline_layout = nullptr;

    _color_attachment_format = VK_FORMAT_UNDEFINED;

    _vertex_input_create_info.vertexBindingDescriptionCount = 0;
    _vertex_input_create_info.pVertexBindingDescriptions = nullptr;

    _vertex_input_create_info.vertexAttributeDescriptionCount = 0;
    _vertex_input_create_info.pVertexAttributeDescriptions = nullptr;

    _vertex_binding_descriptions.clear();
    _vertex_attribute_descriptions.clear();
}