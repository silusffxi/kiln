#include <vulkan/vulkan.h>
#include <vector>
#include "kiln/vulkan/vk_utilities.h"
#include "../vk_init.hpp"
//-----------------------------------------
#include "vk_descriptor_layout_builder.hpp"

using namespace kiln;
using namespace vulkan;

vk_descriptor_layout_builder& vk_descriptor_layout_builder::add_binding(uint32_t binding, VkDescriptorType type)
{
    _bindings.emplace_back(VkDescriptorSetLayoutBinding
    {
        .binding = binding,
        .descriptorType = type,
        .descriptorCount = 1,
        //.stageFlags =
        //.pImmutableSamplers = 
    });

    return *this;
}

vk_descriptor_layout_builder& vk_descriptor_layout_builder::add_binding(uint32_t binding, VkDescriptorType type,
    VkShaderStageFlags shader_stage)
{
    _bindings.emplace_back(VkDescriptorSetLayoutBinding
    {
        .binding = binding,
        .descriptorType = type,
        .descriptorCount = 1,
        .stageFlags = shader_stage,
        //.pImmutableSamplers = 
    });

    return *this;
}

vk_descriptor_layout_builder& vk_descriptor_layout_builder::add_shader_stage(VkShaderStageFlagBits stage)
{
    _shader_stages |= stage;
    return *this;
}

void vk_descriptor_layout_builder::clear()
{
    _bindings.clear();
}

VkDescriptorSetLayout vk_descriptor_layout_builder::build(const vk_device* device)
{
    for (auto& binding : _bindings)
    {
        if (binding.stageFlags == 0) binding.stageFlags |= _shader_stages;
    }

    const auto create_info = vk_init::descriptor_set::layout_create_info(
        _bindings.data(), static_cast<uint32_t>(_bindings.size()));

    VkDescriptorSetLayout layout;
    vk_result_check(vkCreateDescriptorSetLayout(*device, &create_info, nullptr, &layout),
        "Failed to create descriptor set layout.");

    return layout;
}
