#include <string>
#include <vector>
#include <vulkan/vulkan.h>
#include <vulkan/vk_enum_string_helper.h>
#include "kiln/logging.hpp"
#include "../vk_init.hpp"
//--------------------------------
#include "vk_instance_builder.hpp"

using namespace kiln::vulkan;

instance_builder& instance_builder::add_extension(const std::string& name)
{
    _extension_names.emplace_back(name);
    return *this;
}

instance_builder& instance_builder::add_layer(const std::string& name)
{
    _layer_names.emplace_back(name);
    return *this;
}

instance_builder& instance_builder::app_name(const std::string& app_name)
{
    _app_info.pApplicationName = app_name.c_str();
    return *this;
}

instance_builder& instance_builder::app_version(uint32_t major, uint32_t minor, uint32_t patch)
{
    _app_info.applicationVersion = VK_MAKE_VERSION(major, minor, patch);
    return *this;
}

instance_builder& instance_builder::engine_name(const std::string& engine_name)
{
    _app_info.pEngineName = engine_name.c_str();
    return *this;
}

instance_builder& instance_builder::engine_version(uint32_t major, uint32_t minor, uint32_t patch)
{
    _app_info.engineVersion = VK_MAKE_VERSION(major, minor, patch);
    return *this;
}

instance_builder& instance_builder::use_extensions(const std::vector<std::string>& extensions)
{
    for (auto& name : extensions)
    {
        _extension_names.emplace_back(name);
    }

    return *this;
}

instance_builder& instance_builder::use_layers(const std::vector<std::string>& layers)
{
    for (auto& name : layers)
    {
        _layer_names.emplace_back(name);
    }

    return *this;
}

instance_builder& instance_builder::vulkan_api_100() { return vulkan_api_version(VK_API_VERSION_1_0); }
instance_builder& instance_builder::vulkan_api_110() { return vulkan_api_version(VK_API_VERSION_1_1); }
instance_builder& instance_builder::vulkan_api_120() { return vulkan_api_version(VK_API_VERSION_1_2); }
instance_builder& instance_builder::vulkan_api_130() { return vulkan_api_version(VK_API_VERSION_1_3); }

instance_builder& instance_builder::vulkan_api_version(uint32_t api_version)
{
    _app_info.apiVersion = api_version;
    return *this;
}

VkInstance instance_builder::build() const
{
    const auto log = logging::get_logger(log_name::vulkan);

    // Re-map the string vectors into arrays of const char* so that the extension and layer names
    // can actually be passed to vkCreateInstance.
    //----------

    const auto extensions_count = static_cast<uint32_t>(_extension_names.size());
    const auto layers_count = static_cast<uint32_t>(_layer_names.size());

    auto extension_names = std::vector<const char*>(extensions_count);
    auto layer_names = std::vector<const char*>(layers_count);

    for (auto idx = 0u; idx < extensions_count; idx++)
    {
        extension_names[idx] = _extension_names[idx].c_str();
    }

    std::stringstream layers_message;
    for (auto idx = 0u; idx < layers_count; idx++)
    {
        layer_names[idx] = _layer_names[idx].c_str();
    }

    // Pass everything to the function create the VkInstanceCreateInfo struct.
    //----------

    const auto instance_create_info = vk_init::instance::create_info(&_app_info, layer_names, extension_names);

    // Create the instance.
    //----------

    VkInstance instance;
    const auto result = vkCreateInstance(&instance_create_info, nullptr, &instance);
    if (result != VK_SUCCESS)
    {
        log->critical("Failed to create VkInstance. Error: {} ({})",
            string_VkResult(result), static_cast<int>(result));
        return nullptr;
    }

    return instance;
}
