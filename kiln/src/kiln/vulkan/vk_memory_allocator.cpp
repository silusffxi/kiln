#include <vulkan/vulkan.h>
#include <vma/vk_mem_alloc.h>
#include "kiln/vulkan/vk_instance.h"
#include "kiln/vulkan/vk_physical_device.h"
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_buffer.h"
#include "kiln/vulkan/vk_utilities.h"
#include "vk_init.hpp"
//------------------------------------------
#include "kiln/vulkan/vk_memory_allocator.h"

using namespace kiln;
using namespace vulkan;

vk_memory_allocator::vk_memory_allocator(const std::unique_ptr<vk_instance>& instance,
    const std::unique_ptr<vk_physical_device>& gpu, const std::unique_ptr<vk_device>& device) :
    vk_memory_allocator(instance.get(), gpu.get(), device.get())
{
}

vk_memory_allocator::vk_memory_allocator(vk_instance* instance, vk_physical_device* gpu, vk_device* device) :
    _instance(instance), _gpu(gpu), _device(device)
{
    const auto create_info = VmaAllocatorCreateInfo
    {
        .flags = VMA_ALLOCATOR_CREATE_BUFFER_DEVICE_ADDRESS_BIT,
        .physicalDevice = *gpu,
        .device = *device,
        .instance = *instance,
        .vulkanApiVersion = VK_API_VERSION_1_3,
    };

    vmaCreateAllocator(&create_info, &_allocator);
}

vk_memory_allocator::~vk_memory_allocator()
{
    if (_allocator != nullptr)
    {
        vmaDestroyAllocator(_allocator);
        _allocator = nullptr;
    }
}

VmaAllocator vk_memory_allocator::allocator() const
{
    return _allocator;
}

std::unique_ptr<vk_buffer> vk_memory_allocator::create_buffer(size_t size, VkBufferUsageFlags usage, VmaMemoryUsage memory_usage)
{
    auto create_info = vk_init::buffer::create_info(size, usage);
    // VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT
    create_info.usage |= VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;

    const auto alloc_info = VmaAllocationCreateInfo
    {
        .flags = VMA_ALLOCATION_CREATE_MAPPED_BIT,
        .usage = memory_usage,
        //.requiredFlags =
        //.preferredFlags =
        //.memoryTypeBits =
        //.pool =
        //.pUserData =
        //.priority = 
    };

    VkBuffer buffer;
    VmaAllocation allocation;
    VmaAllocationInfo allocation_info = { };
    vk_result_check(vmaCreateBuffer(_allocator, &create_info, &alloc_info, &buffer, &allocation, &allocation_info),
        "Failed to create buffer.");

    const auto device_address_info = VkBufferDeviceAddressInfo
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO,
        .pNext = nullptr,
        .buffer = buffer,
    };

    const auto buffer_address = vkGetBufferDeviceAddress(*_device, &device_address_info);

    return std::make_unique<vk_buffer>(_allocator, buffer, allocation, allocation_info, buffer_address);
}

std::unique_ptr<vk_buffer> vk_memory_allocator::create_staging_buffer(size_t size, VkBufferUsageFlags usage, VmaMemoryUsage memory_usage)
{
    const auto create_info = vk_init::buffer::create_info(size, usage);
    const auto alloc_info = VmaAllocationCreateInfo
    {
        .flags = VMA_ALLOCATION_CREATE_MAPPED_BIT,
        .usage = memory_usage,
        //.requiredFlags =
        //.preferredFlags =
        //.memoryTypeBits =
        //.pool =
        //.pUserData =
        //.priority =
    };

    VkBuffer buffer;
    VmaAllocation allocation;
    VmaAllocationInfo allocation_info = { };
    vk_result_check(vmaCreateBuffer(_allocator, &create_info, &alloc_info, &buffer, &allocation, &allocation_info),
        "Failed to create staging buffer.");

    const auto device_address_info = VkBufferDeviceAddressInfo
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO,
        .pNext = nullptr,
        .buffer = buffer,
    };

    const auto buffer_address = vkGetBufferDeviceAddress(*_device, &device_address_info);

    auto buf = std::make_unique<vk_buffer>(_allocator, buffer, allocation, allocation_info, buffer_address);
    buf->set_name("staging_buffer");
    return std::move(buf);
}
