#include <vulkan/vulkan.h>
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_types.h"
#include "kiln/vulkan/vk_utilities.h"
#include "vk_init.hpp"
//----------------------------------
#include "kiln/vulkan/vk_pipeline.h"

using namespace kiln;
using namespace vulkan;

vk_pipeline::vk_pipeline(vk_device* device, VkPipelineLayout layout, VkPipeline pipeline) :
    _device(device), _layout(layout), _pipeline(pipeline)
{
}

vk_pipeline::vk_pipeline(vk_device* device, const std::function<void(VkPipelineLayout*, VkPipeline*)>& create_func) :
    _device(device), _layout(nullptr), _pipeline(nullptr)
{
    create_func(&_layout, &_pipeline);
}

vk_pipeline::~vk_pipeline()
{
    if (_pipeline != nullptr)
    {
        vkDestroyPipeline(*_device, _pipeline, nullptr);
        _pipeline = nullptr;
    }

    if (_layout != nullptr)
    {
        vkDestroyPipelineLayout(*_device, _layout, nullptr);
        _layout = nullptr;
    }
}

VkPipelineLayout vk_pipeline::layout() const { return _layout; }
VkPipeline vk_pipeline::pipeline() const { return _pipeline; }

std::unique_ptr<vk_pipeline> vk_pipeline::create_compute_pipeline(vk_device* device, VkDescriptorSetLayout desc_set_layout,
    const vk_shader* shader)
{
    constexpr auto push_constants_range = VkPushConstantRange
    {
        .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
        .offset = 0,
        .size = sizeof(compute_push_constants),
    };

    const auto layout_create_info = vk_init::pipeline::layout_create_info(&desc_set_layout, 1, &push_constants_range, 1);

    VkPipelineLayout layout;
    vk_result_check(vkCreatePipelineLayout(*device, &layout_create_info, nullptr, &layout),
        "Failed to create compute pipeline layout.");

    const auto stage_create_info = VkPipelineShaderStageCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .stage = VK_SHADER_STAGE_COMPUTE_BIT,
        .module = shader->shader_module(),
        .pName = "main",
        .pSpecializationInfo = nullptr,
    };


    const auto create_info = VkComputePipelineCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .stage = stage_create_info,
        .layout = layout,
        //.basePipelineHandle =
        //.basePipelineIndex = 
    };

    VkPipeline pipeline;
    vk_result_check(vkCreateComputePipelines(*device, nullptr, 1, &create_info, nullptr, &pipeline),
        "Failed to create compute pipeline.");

    return std::make_unique<vk_pipeline>(device, layout, pipeline);
}
