#ifdef _DEBUG
#include <cassert>
#endif
#include <cstdint>
#include <format>
#include <string>
#include <vector>
#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>
#include <vulkan/vulkan.h>
#include <kiln/logging.hpp>
#include <kiln/utilities.hpp>
#include "kiln/vulkan/vk_instance_config.hpp"
#include "builders/vk_instance_builder.hpp"
//-----------------------------
#include "kiln/vulkan/vk_instance.h"

using namespace kiln::vulkan;

namespace
{
    /// Instance layers that are required.
    std::vector required_layers =
    {
        std::string("VK_LAYER_KHRONOS_validation"),
        std::string("VK_LAYER_KHRONOS_synchronization2"),
        std::string("VK_LAYER_KHRONOS_shader_object"),
        std::string("VK_LAYER_LUNARG_screenshot"),
    };

    /// Instance extensions that shouldn't ever be included.
    std::vector exclude_extensions =
    {
        std::string("VK_LUNARG_direct_driver_loading"),
    };
}

vk_instance::vk_instance(SDL_Window* window, vk_instance_config* config) :
    _window(window)
{
    const auto log = logging::get_logger(log_name::vulkan);

    populate_instance_extensions(&_instance_extensions, _window, config->debug_enabled());
    populate_instance_layers(&_instance_layers);

    auto builder = instance_builder()
        .app_name("kiln")
        .app_version(0, 1, 0)
        .engine_name("kiln")
        .engine_version(0, 1, 0)
        .vulkan_api_130();

    // Re-map extensions and layers.
    //-------------------------------------------

    // Extensions
    for (auto& ext : _instance_extensions)
    {
        bool skip = false;
        for (auto& exclude : exclude_extensions)
        {
            if (exclude == ext.name)
            {
                log->info("Excluding instance extension {} (explicitly excluded)", ext.name);
                skip = true;
                break;
            }
        }

        if (skip) continue;
        builder.add_extension(ext.name);
    }

    // Layers
    for (const auto& req_layer : required_layers)
    {
        bool found_layer = false;
        for (const auto& layer : _instance_layers)
        {
            if (layer.name == req_layer)
            {
                found_layer = true;
                builder.add_layer(layer.name);
                _layers_in_use.try_emplace(layer.name, layer);
            }
        }

        if (!found_layer)
        {
            log->critical("Missing required Vulkan instance layer \"{}\"!", req_layer);
            return;
        }
    }

    // Optional Layers
    const auto optional_layers = config->get_enabled_optional_layers();
    log->info("Will attempt to enable {} optional instance layer(s)...", optional_layers.size());
    for (const auto& opt_layer : optional_layers)
    {
        bool found_layer = false;
        for (const auto& layer : _instance_layers)
        {
            if (layer.name == opt_layer)
            {
                found_layer = true;
                builder.add_layer(layer.name);
                _layers_in_use.try_emplace(layer.name, layer);
            }
        }

        if (!found_layer)
        {
            log->warn("Missing requested, optional, Vulkan instance layer: \"{}\"",
                opt_layer);
        }
    }

    // Create the VkInstance
    //-------------------------------------------

    _instance = builder.build();

    log->debug("VkInstance created.");

    // As part of our setup, we'll also want to find and capture the addresses of various functions
    // that may be helpful or necessary for the lifetime of the instance.
    setup_function_pointers();
}

vk_instance::~vk_instance()
{
    const auto log = logging::get_logger(log_name::vulkan);

    if (_instance != nullptr)
    {
        vkDestroyInstance(_instance, nullptr);
        log->debug("VkInstance destroyed.");
    }
}

const std::vector<vk_layer_properties>& vk_instance::available_layers() const { return _instance_layers; }
const std::vector<vk_extension_properties>& vk_instance::extensions() const { return _instance_extensions; }
const std::unordered_map<std::string, vk_layer_properties>& vk_instance::layers_in_use() const { return _layers_in_use; }

VkResult vk_instance::create_debug_report_callback(const VkDebugReportCallbackCreateInfoEXT* pCreateInfo,
    const VkAllocationCallbacks* pAllocator, VkDebugReportCallbackEXT* pCallback) const
{
    if (_debug.fn_create_debug_report_callback == nullptr)
        return VK_ERROR_FEATURE_NOT_PRESENT;

    return _debug.fn_create_debug_report_callback(_instance, pCreateInfo, pAllocator, pCallback);
}

void vk_instance::destroy_debug_report_callback(VkDebugReportCallbackEXT callback,
    const VkAllocationCallbacks* pAllocator) const
{
    if (_debug.fn_destroy_debug_report_callback == nullptr)
        return;

    _debug.fn_destroy_debug_report_callback(_instance, callback, pAllocator);
}

void vk_instance::setup_function_pointers()
{
    if (_debug.fn_create_debug_report_callback == nullptr)
    {
        _debug.fn_create_debug_report_callback = get_function_pointer<PFN_vkCreateDebugReportCallbackEXT>(
            "vkCreateDebugReportCallbackEXT");

#ifdef _DEBUG
        assert(_debug.fn_create_debug_report_callback != nullptr);
#endif
    }

    if (_debug.fn_destroy_debug_report_callback == nullptr)
    {
        _debug.fn_destroy_debug_report_callback = get_function_pointer<PFN_vkDestroyDebugReportCallbackEXT>(
            "vkDestroyDebugReportCallbackEXT");

#ifdef _DEBUG
        assert(_debug.fn_destroy_debug_report_callback != nullptr);
#endif
    }
}

void vk_instance::populate_instance_extensions(std::vector<vk_extension_properties>* extensions,
    SDL_Window* window, bool enable_debug)
{
    // Collect all the available instance extensions.
    //-------------------------------------------

    uint32_t count;
    vkEnumerateInstanceExtensionProperties(nullptr, &count, nullptr);

    std::vector<VkExtensionProperties> extension_props(count);
    vkEnumerateInstanceExtensionProperties(nullptr, &count, extension_props.data());

    for (auto idx = 0u; idx < count; idx++)
    {
        const auto& [name, spec_version] = extension_props[idx];

        const auto ext_name = std::string(name);
        extensions->emplace_back(vk_extension_properties
            {
                .spec_version = spec_version,
                .name = ext_name,
            });
    }

    if (window != nullptr)
    {
        uint32_t sdl_ext_count;
        SDL_Vulkan_GetInstanceExtensions(window, &sdl_ext_count, nullptr);

        std::vector<const char*> sdl_extension_names(sdl_ext_count);
        SDL_Vulkan_GetInstanceExtensions(window, &sdl_ext_count, sdl_extension_names.data());

        for (auto idx = 0u; idx < sdl_ext_count; idx++)
        {
            const auto sdl_ext_name = std::string(sdl_extension_names[idx]);

            const auto have_sdl_ext = std::ranges::any_of(*extensions,
                [&](const vk_extension_properties& props) { return props.name == sdl_ext_name; });

            if (have_sdl_ext) continue;

            extensions->emplace_back(vk_extension_properties
                {
                    .spec_version = 0,
                    .name = sdl_ext_name,
                });
        }
    }

    if (enable_debug)
    {
        extensions->emplace_back(vk_extension_properties
            {
                .spec_version = VK_EXT_DEBUG_REPORT_SPEC_VERSION,
                .name = std::string(VK_EXT_DEBUG_REPORT_EXTENSION_NAME),
            });
    }
}

void vk_instance::populate_instance_layers(std::vector<vk_layer_properties>* layers)
{
    uint32_t count;
    vkEnumerateInstanceLayerProperties(&count, nullptr);

    std::vector<VkLayerProperties> layer_props(count);
    vkEnumerateInstanceLayerProperties(&count, layer_props.data());

    for (auto idx = 0u; idx < count; idx++)
    {
        const auto& [name, spec_version, impl_version, desc] = layer_props[idx];
        layers->emplace_back(vk_layer_properties
            {
                .spec_version = spec_version,
                .implementation_version = impl_version,

                .name = std::string(name),
                .desc = std::string(desc),
            });
    }
}
