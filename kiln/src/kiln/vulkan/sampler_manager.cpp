#include <cstdint>
#include <ranges>
#include <string>
#include <sstream>
#include <vector>
#include <vulkan/vulkan.h>
#include <vulkan/vk_enum_string_helper.h>
#include "kiln/logging.hpp"
#include "kiln/vulkan/vk_physical_device.h"
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_types.h"
#include "vk_init.hpp"
//--------------------------------------
#include "kiln/vulkan/sampler_manager.h"

using namespace kiln;
using namespace vulkan;

namespace
{
    std::string create_supported_samples_message(const std::vector<VkSampleCountFlagBits>& supported_sample_counts)
    {
        const auto supported_count = static_cast<uint8_t>(supported_sample_counts.size());

        std::stringstream log_msg;
        log_msg << "Supported sample counts: ";
        for (uint8_t idx = 0u; idx < supported_count; idx++)
        {
            // The "NOLINT" here is complaining about not supporting
            // the "VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM" value in the switch
            // statement. The sample counts shouldn't ever be using that value
            // since it is not valid and is only providing a way to determine
            // what the maximum value of bits would be.
            switch (supported_sample_counts[idx])  // NOLINT(clang-diagnostic-switch-enum)
            {
            case VK_SAMPLE_COUNT_1_BIT:
                log_msg << "1x";
                break;
            case VK_SAMPLE_COUNT_2_BIT:
                log_msg << "2x";
                break;
            case VK_SAMPLE_COUNT_4_BIT:
                log_msg << "4x";
                break;
            case VK_SAMPLE_COUNT_8_BIT:
                log_msg << "8x";
                break;
            case VK_SAMPLE_COUNT_16_BIT:
                log_msg << "16x";
                break;
            case VK_SAMPLE_COUNT_32_BIT:
                log_msg << "32x";
                break;
            case VK_SAMPLE_COUNT_64_BIT:
                log_msg << "64x";
                break;
            default:
                log_msg << "UNKNOWN (" << static_cast<uint32_t>(supported_sample_counts[idx]) << ')';
                break;
            }

            if (idx > 0 && idx % (supported_count - 1) == 0)
                continue;

            log_msg << ", ";
        }

        return log_msg.str();
    }
}

sampler_manager::sampler_manager(vk_physical_device* gpu, vk_device* device) :
    _log(logging::get_logger(log_name::vulkan)), _gpu(gpu), _device(device)
{
    _max_supported = _gpu->get_supported_msaa();

    for (auto& count : vk_sample_counts)
    {
        // Don't add a sample count that isn't supported.
        if (count > _max_supported)
            continue;

        auto [_, added] = _supported_sample_counts.emplace(count);
        if (added) _supported_sample_counts_v.emplace_back(count);
    }

    //------------------------------
    // Log the supported sample counts:
    _log->trace("{}", create_supported_samples_message(_supported_sample_counts_v));
    //------------------------------
}

sampler_manager::~sampler_manager()
{
    if (!_samplers_linear.empty())
    {
        auto destroyed_count = 0u;
        for (auto& sampler : _samplers_linear | std::views::values)
        {
            vkDestroySampler(*_device, sampler, nullptr);
            sampler = nullptr;
            destroyed_count++;
        }
        _samplers_linear.clear();

        _log->debug("Destroyed {} \"linear\" samplers.", destroyed_count);
    }

    if (!_samplers_nearest.empty())
    {
        auto destroyed_count = 0u;
        for (auto& sampler : _samplers_nearest | std::views::values)
        {
            vkDestroySampler(*_device, sampler, nullptr);
            sampler = nullptr;
            destroyed_count++;
        }
        _samplers_nearest.clear();

        _log->debug("Destroyed {} \"nearest\" samplers.", destroyed_count);
    }
}

bool sampler_manager::is_supported(uint32_t sample_count) const
{
    const auto casted = static_cast<VkSampleCountFlagBits>(sample_count);
    return is_supported(casted);
}

bool sampler_manager::is_supported(VkSampleCountFlagBits sample_count) const
{
    return _supported_sample_counts.contains(sample_count);
}

VkSampler sampler_manager::linear(const VkSampleCountFlagBits sample_count)
{
    const auto target_sample_count = sample_count == VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM
        ? _max_supported
        : sample_count;

    if (_samplers_linear.contains(target_sample_count))
        return _samplers_linear.at(target_sample_count);

    const auto create_info = vk_init::sampler::create_info_linear(target_sample_count);

    auto sampler = create_sampler(target_sample_count, &create_info);
    if (sampler == nullptr)
        return nullptr;

    _samplers_linear.try_emplace(target_sample_count, sampler);
    return sampler;
}

VkSampler sampler_manager::nearest(const VkSampleCountFlagBits sample_count)
{
    const auto target_sample_count = sample_count == VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM
        ? _max_supported
        : sample_count;

    if (_samplers_nearest.contains(target_sample_count))
        return _samplers_nearest.at(target_sample_count);

    const auto create_info = vk_init::sampler::create_info_linear(target_sample_count);

    auto sampler = create_sampler(target_sample_count, &create_info);
    if (sampler == nullptr)
        return nullptr;

    _samplers_nearest.try_emplace(target_sample_count, sampler);
    return sampler;
}

const std::vector<VkSampleCountFlagBits>& sampler_manager::supported_sample_counts() const
{
    return _supported_sample_counts_v;
}

VkSampler sampler_manager::create_sampler(VkSampleCountFlagBits sample_count, const VkSamplerCreateInfo* create_info) const
{
    VkSampler sampler;
    const auto result = vkCreateSampler(*_device, create_info, nullptr, &sampler);
    if (result != VK_SUCCESS)
    {
        const auto count_name = std::string(string_VkSampleCountFlagBits(sample_count));
        const auto result_name = std::string(string_VkResult(result));

        _log->error("Failed to create sampler for \"{}\". Error: {} ({:#x})",
            count_name, result_name, static_cast<int>(result));

        return nullptr;
    }

    if (create_info->magFilter == VK_FILTER_LINEAR)
        _log->debug("Created {}x linear sampler.", static_cast<int>(sample_count));

    if (create_info->magFilter == VK_FILTER_NEAREST)
        _log->debug("Created {}x nearest sampler.", static_cast<int>(sample_count));

    return sampler;
}
