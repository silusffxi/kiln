#pragma once
#include <cstdint>
#include <vector>
#include <vulkan/vulkan.h>

namespace vk_init
{
    VkPresentInfoKHR present_info(const VkSwapchainKHR* swapchains, uint32_t swapchains_count,
        const uint32_t* images, VkResult* present_result,
        const VkSemaphore* wait_semaphores, uint32_t wait_semaphores_count);

    VkSubmitInfo submit_info(const VkCommandBuffer* command_buffers, uint32_t command_buffer_count,
        const VkSemaphore* signal_semaphores, uint32_t signal_semaphore_count,
        const VkSemaphore* wait_semaphores, uint32_t wait_semaphore_count,
        const VkPipelineStageFlags* wait_flag);

    VkSubmitInfo2 submit_info2(const VkCommandBufferSubmitInfo* cmd, const VkSemaphoreSubmitInfo* signal_semaphore_info,
        const VkSemaphoreSubmitInfo* wait_semaphore_info);

    namespace application
    {
        VkApplicationInfo info();
    }

    namespace attachments
    {
        VkAttachmentDescription color(VkFormat format);

        VkAttachmentReference color_ref(uint32_t index);
    }

    namespace buffer
    {
        VkBufferCreateInfo create_info(VkDeviceSize size, VkBufferUsageFlags usage);
    }

    namespace command
    {
        VkCommandBufferAllocateInfo buffer_allocate_info(VkCommandPool pool, uint32_t count);

        VkCommandBufferAllocateInfo buffer_allocate_info(VkCommandPool pool, uint32_t count, bool is_primary);

        VkCommandBufferBeginInfo buffer_begin_info(VkCommandBufferUsageFlags flags, const VkCommandBufferInheritanceInfo* inherit_info);

        VkCommandBufferInheritanceInfo buffer_inheritance_info();

        VkCommandBufferInheritanceRenderingInfo buffer_inheritance_rendering_info(VkSampleCountFlagBits sampling = VK_SAMPLE_COUNT_1_BIT);

        VkCommandBufferSubmitInfo buffer_submit_info(VkCommandBuffer cmd);

        VkCommandPoolCreateInfo pool_create_info(uint32_t queue_family_index, VkCommandPoolCreateFlags flags);
    }

    namespace debug
    {
        VkDebugReportCallbackCreateInfoEXT report_callback_create_info_EXT(PFN_vkDebugReportCallbackEXT callback,
            void* user_data = nullptr);
    }

    namespace descriptor_pool
    {
        VkDescriptorPoolCreateInfo create_info(const std::vector<VkDescriptorPoolSize>& pool_sizes, uint32_t max_sets);

        VkDescriptorPoolCreateInfo create_info(const VkDescriptorPoolSize* pool_sizes, uint32_t pool_sizes_count, uint32_t max_sets);
    }

    namespace descriptor_set
    {
        VkDescriptorSetAllocateInfo allocate_info(VkDescriptorPool pool, const VkDescriptorSetLayout* layout);

        VkDescriptorSetAllocateInfo allocate_info(VkDescriptorPool pool, const VkDescriptorSetLayout* layouts, uint32_t layouts_count);

        void destroy_layout(VkDevice device, VkDescriptorSetLayout* layout);

        VkDescriptorSetLayoutBinding layout_binding(uint32_t binding,
            VkDescriptorType types, uint32_t type_count,
            VkShaderStageFlags shader_stage, const VkSampler* sampler);

        VkDescriptorSetLayoutCreateInfo layout_create_info(
            const VkDescriptorSetLayoutBinding* bindings, uint32_t binding_count);

        VkWriteDescriptorSet write(VkDescriptorType type, uint32_t binding, VkDescriptorSet dst_set,
            const VkDescriptorBufferInfo* info, uint32_t descriptor_count, const VkDescriptorImageInfo* images);
    }

    namespace device
    {
        VkDeviceCreateInfo create_info(const VkDeviceQueueCreateInfo* info,
            uint32_t size, const VkPhysicalDeviceFeatures* features, bool debug);

        VkDeviceQueueCreateInfo queue_create_info(uint32_t queue_family_index, const float* priority,
            uint32_t queue_count = 1u);
    }

    namespace frame_buffer
    {
        VkFramebufferCreateInfo create_info(VkRenderPass render_pass, uint32_t surface_width, uint32_t surface_height,
            const std::vector<VkImageView>& attachments);
    }

    namespace fence
    {
        VkFenceCreateInfo create_info(VkFenceCreateFlags flags = 0);
    }

    namespace image
    {
        VkImageCreateInfo create_info(uint32_t image_width, uint32_t image_height, VkFormat format,
            VkImageUsageFlags usage, uint32_t mip_levels, VkSampleCountFlagBits samples);

        VkImageCreateInfo create_info(VkExtent3D extent, VkFormat format, VkImageUsageFlags usage_flags);

        VkImageCreateInfo create_info(VkExtent3D extent, VkFormat format, VkImageUsageFlags usage,
            uint32_t mip_levels, VkSampleCountFlagBits samples);

        VkImageSubresourceRange subresource_range(VkImageAspectFlags aspect_mask);

        VkImageViewCreateInfo view_create_info(VkImage image, VkFormat format,
            VkImageAspectFlags aspect_mask);

        VkImageViewCreateInfo view_create_info(VkImage image, VkFormat format,
            VkImageAspectFlags aspect_mask, uint32_t mip_levels);
    }

    namespace instance
    {
        VkInstanceCreateInfo create_info(const VkApplicationInfo* app_info,
            const std::vector<const char*>& layers, const std::vector<const char*>& extensions);

        VkInstanceCreateInfo create_info(const VkApplicationInfo* app_info,
            const char** layers, uint32_t layer_count, const char** extensions, uint32_t extension_count);
    }

    namespace pipeline
    {
        VkPipelineColorBlendStateCreateInfo color_blend_state_create_info(
            const VkPipelineColorBlendAttachmentState* attachments, uint32_t attachments_count);

        void destroy(VkDevice device, VkPipeline* pipeline);

        void destroy_layout(VkDevice device, VkPipelineLayout* layout);

        VkPipelineDepthStencilStateCreateInfo depth_stencil_state_create_info();

        VkPipelineDynamicStateCreateInfo dynamic_state_create_info(const VkDynamicState* states, uint32_t states_count);

        VkGraphicsPipelineCreateInfo graphics_create_info(
            const VkPipelineShaderStageCreateInfo* shader_stages, uint32_t shader_stages_count,
            const VkPipelineVertexInputStateCreateInfo* vertex_input_info,
            const VkPipelineInputAssemblyStateCreateInfo* input_assembly,
            const VkPipelineViewportStateCreateInfo* viewport,
            const VkPipelineRasterizationStateCreateInfo* rasterizer,
            const VkPipelineMultisampleStateCreateInfo* multisampling,
            const VkPipelineDepthStencilStateCreateInfo* depth_stencil,
            const VkPipelineColorBlendStateCreateInfo* color_blending,
            const VkPipelineDynamicStateCreateInfo* dynamic_state,
            VkPipelineLayout layout,
            VkRenderPass render_pass);

        VkPipelineInputAssemblyStateCreateInfo input_assembly_state_create_info(bool triangles);

        VkPipelineLayoutCreateInfo layout_create_info(const VkDescriptorSetLayout* layouts, uint32_t layouts_count,
            const VkPushConstantRange* push_constants, uint32_t push_constants_count);

        VkPipelineMultisampleStateCreateInfo multisample_state_create_info(VkSampleCountFlagBits samples);

        VkPipelineRasterizationStateCreateInfo rasterization_state_create_info(bool fill);

        VkPipelineShaderStageCreateInfo shader_stage_create_info(VkShaderStageFlagBits stage, VkShaderModule shader,
            const char* entry_name = "main");

        VkPipelineVertexInputStateCreateInfo vertex_input_state_create_info(
            const std::vector<VkVertexInputBindingDescription>& bindings,
            const std::vector<VkVertexInputAttributeDescription>& attributes);

        VkPipelineVertexInputStateCreateInfo vertex_input_state_create_info(
            const VkVertexInputBindingDescription* bindings, uint32_t bindings_count,
            const VkVertexInputAttributeDescription* attributes, uint32_t attributes_count);

        VkPipelineViewportStateCreateInfo viewport_state_create_info(const VkViewport* viewport, const VkRect2D* scissor);
    }

    namespace rendering
    {
        VkRenderingAttachmentInfo color_attachment_info(VkImageView view, const VkClearValue* clear,
            VkImageLayout layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);

        VkRenderingAttachmentInfo depth_attachment_info(VkImageView view,
            VkImageLayout layout = VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL);

        VkRenderingInfo info(VkExtent2D render_extent, const VkRenderingAttachmentInfo* color_attachment,
            const VkRenderingAttachmentInfo* depth_attachment);
    }

    namespace render_pass
    {
        VkRenderPassBeginInfo begin_info(VkRenderPass render_pass, VkFramebuffer frame_buffer, VkRect2D rect,
            const VkClearValue* clear_values, uint32_t clear_count);

        VkRenderPassCreateInfo create_info(
            const std::vector<VkAttachmentDescription>& attachments,
            const std::vector<VkSubpassDescription>& subpasses,
            const std::vector<VkSubpassDependency>& dependencies);

        VkRenderPassCreateInfo create_info(const VkAttachmentDescription* attachments,
            const VkSubpassDescription* subpasses, const VkSubpassDependency* dependencies);

        VkRenderPassCreateInfo create_info(
            const VkAttachmentDescription* attachments, uint32_t attachment_count,
            const VkSubpassDescription* subpasses, uint32_t subpass_count,
            const VkSubpassDependency* dependencies, uint32_t dependencies_count);
    }

    namespace sampler
    {
        VkSamplerCreateInfo create_info(VkBool32 linear, float anisotropy, float mip_levels);

        VkSamplerCreateInfo create_info_linear(VkSampleCountFlagBits msaa = VK_SAMPLE_COUNT_1_BIT);

        VkSamplerCreateInfo create_info_nearest(VkSampleCountFlagBits msaa = VK_SAMPLE_COUNT_1_BIT);
    }

    namespace semaphore
    {
        VkSemaphoreCreateInfo create_info(VkSemaphoreCreateFlags flags = 0);

        VkSemaphoreSubmitInfo submit_info(VkPipelineStageFlags2 stage_mask, VkSemaphore semaphore);
    }

    namespace subpass
    {
        VkSubpassDependency dependency();

        VkSubpassDescription description(
            const VkAttachmentReference* input_attachment_refs, uint32_t input_attachment_refs_count,
            const VkAttachmentReference* color_attachment_refs, uint32_t color_attachment_refs_count,
            const uint32_t* preserve_attachment_refs, uint32_t preserve_attachment_refs_count);
    }

    namespace swapchain
    {
        VkSwapchainCreateInfoKHR create_info_KHR(VkSurfaceKHR surface,
            VkSurfaceCapabilitiesKHR capabilities, VkSurfaceFormatKHR format,
            uint32_t surface_width, uint32_t surface_height,
            VkPresentModeKHR present_mode, VkSwapchainKHR old_swapchain, uint32_t min_image_count);
    }

    namespace vertex_input
    {
        VkVertexInputAttributeDescription attribute_description(uint32_t binding, uint32_t location, uint32_t offset, VkFormat format);

        VkVertexInputBindingDescription binding_description(uint32_t binding, uint32_t stride, VkVertexInputRate input_rate);
    }
}
