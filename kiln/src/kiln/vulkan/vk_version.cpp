#include <filesystem>
#include <format>
#include <vulkan/vulkan.h>
#include "kiln/logging.hpp"
#include "kiln/platform.hpp"
//-----------------------------------
#include "kiln/vulkan/vk_version.hpp"

using namespace kiln;
using namespace vulkan;

vk_version::vk_version() : vk_version(0)
{
}

vk_version::vk_version(uint32_t value) :
    variant(VK_API_VERSION_VARIANT(value)),
    major(VK_API_VERSION_MAJOR(value)),
    minor(VK_API_VERSION_MINOR(value)),
    patch(VK_API_VERSION_PATCH(value)),
    value(value)
{
}

vk_version::vk_version(uint8_t variant, uint8_t major, uint16_t minor, uint16_t patch) :
    variant(variant), major(major), minor(minor), patch(patch),
    value(VK_MAKE_API_VERSION(variant, major, minor, patch))
{
}

std::string vk_version::string() const
{
    return std::format("{}.{}.{}", major, minor, patch);
}

std::string vk_version::string_variant() const
{
    return std::format("{}.{}.{}.{}", variant, major, minor, patch);
}

bool vk_version::operator==(const vk_version& other) const
{
    return value == other.value;
}

bool vk_version::operator!=(const vk_version& other) const
{
    return value != other.value;
}

bool vk_version::operator<(const vk_version& other) const
{
    return value < other.value;
}

bool vk_version::operator>(const vk_version& other) const
{
    return value > other.value;
}

bool vk_version::operator<=(const vk_version& other) const
{
    return value <= other.value;
}

bool vk_version::operator>=(const vk_version& other) const
{
    return value >= other.value;
}

const vk_version vulkan::api_1_0 = vk_version(0, 1, 0, 0);
const vk_version vulkan::api_1_1 = vk_version(0, 1, 1, 0);
const vk_version vulkan::api_1_2 = vk_version(0, 1, 2, 0);
const vk_version vulkan::api_1_3 = vk_version(0, 1, 3, 0);

vk_version vulkan::api_version_direct;
const vk_version vulkan::api_version_header = vulkan::api_header_version_complete;
vk_version vulkan::api_version_platform;

bool vulkan::check_version_mismatch()
{
    // Comparisons should be done based on the value from the header.

    const auto log = logging::get_logger(log_name::vulkan);
    const auto mismatch_direct     = api_version_header != api_version_direct;
    const auto mismatch_vulkan_api = api_version_header != api_version_platform;

    if (mismatch_direct)
    {
        log->info(
            "Version returned by \"vkEnumerateInstanceVersion\" did not match the version reported by the Vulkan API header. "
            "Header: {}, Vulkan API: {}",
            api_version_header.string(),
            api_version_direct.string());
    }

    if (mismatch_vulkan_api)
    {
        log->info(
            "Version returned by \"vkEnumerateInstanceVersion\" retrieved via OS calls did not match the version reported by the Vulkan API header. "
            "Header: {}, Platform: {}",
            api_version_header.string(),
            api_version_platform.string());
    }

    return mismatch_direct && mismatch_vulkan_api;
}

void vulkan::collect_versions()
{
    api_version_direct   = get_vulkan_version(version_fetch_method::vulkan_api);
    api_version_platform = get_vulkan_version(version_fetch_method::windows_api);
}

//-----------------------------------------------------------------------------
#pragma region get_vulkan_version

namespace
{
    constexpr char vk_enumerate_instance_version[] = "vkEnumerateInstanceVersion";

#ifdef OS_WINDOWS
    /**
    *\brief The name of the expected Vulkan module.
    *\note The path for module should be "C:\Windows\System32\vulkan-1.dll"
    */
    const std::wstring windows_vulkan_module = L"vulkan-1.dll";

    /**
    *\brief Uses the Windows API to find "vulkan-1.dll" loaded in the process. If the library
    *       isn't loaded then the function pointer for \c vkEnumerateInstanceVersion will be
    *       returned as \c nullptr.
    */
    PFN_vkEnumerateInstanceVersion get_vulkan_version_windows_api()
    {
        // Figure out what our process ID is.
        const auto proc_id = GetCurrentProcessId();

        // Open a handle for the process
        auto proc = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
            FALSE, proc_id);

        if (proc == nullptr)
            return { };

        // Enumerate the loaded modules within the process.
        DWORD cb_needed;
        auto proc_modules = std::vector<HMODULE>(128);

        HMODULE vulkan_module = nullptr;
        if (EnumProcessModules(proc, proc_modules.data(), static_cast<DWORD>(proc_modules.size()), &cb_needed))
        {
            TCHAR module_name[MAX_PATH];
            constexpr auto name_size = static_cast<DWORD>(sizeof(module_name) / sizeof(TCHAR));
            const auto module_count = static_cast<uint32_t>(cb_needed / sizeof(HMODULE));

            for (auto idx = 0u; idx < module_count; idx++)
            {
                if (!GetModuleFileNameEx(proc, proc_modules[idx], module_name, name_size))
                    continue;

                const auto module_path = std::filesystem::path(module_name);
                const auto file_name = module_path.filename();
                if (file_name == windows_vulkan_module)
                {
                    vulkan_module = proc_modules[idx];
                    break;
                }
            }
        }

        CloseHandle(proc);
        proc = nullptr;

        if (vulkan_module == nullptr)
        {
            vulkan_module = LoadLibrary(windows_vulkan_module.c_str());

            // If the module is still nullptr after directly loading it, then
            // we can assume that Vulkan isn't available so, we'll just return
            // nullptr for the address location of "vkEnumerateInstanceVersion(uint32_t*)"
            if (vulkan_module == nullptr)
                return nullptr;
        }

        const FARPROC func_ptr = GetProcAddress(vulkan_module, vk_enumerate_instance_version);
        return (PFN_vkEnumerateInstanceVersion)(func_ptr);
    }
#endif

    /**
    *\brief Uses the Vulkan API to find the address of "vkEnumerateInstanceVersion(uint32_t*)".
    *       If the pointer returned is \c nullptr then the function was not able to be found in
    *       the API.
    */
    PFN_vkEnumerateInstanceVersion get_vulkan_version_vulkan_api()
    {
        const auto target = reinterpret_cast<void*>(vkGetInstanceProcAddr(nullptr, vk_enumerate_instance_version));
        return reinterpret_cast<PFN_vkEnumerateInstanceVersion>(target);
    }
}

vk_version vulkan::get_vulkan_version(const version_fetch_method method)
{
    PFN_vkEnumerateInstanceVersion func = nullptr;
    switch (method)
    {
    case version_fetch_method::use_default:
    case version_fetch_method::vulkan_api:
        func = get_vulkan_version_vulkan_api();
        break;
    case version_fetch_method::windows_api:
        if (current_os == os_platform::windows)
        {
            func = get_vulkan_version_windows_api();
        }
        break;
    }

    if (func == nullptr)
        return { };

    uint32_t version;
    const auto result = func(&version);
    if (result != VK_SUCCESS)
    {
        return { };
    }

    return version;
}

#pragma endregion