#include <cstdint>
#include <vulkan/vulkan.h>
#include "vk_init.hpp"

VkPresentInfoKHR vk_init::present_info(const VkSwapchainKHR* swapchains, uint32_t swapchains_count,
    const uint32_t* images, VkResult* present_result,
    const VkSemaphore* wait_semaphores, uint32_t wait_semaphores_count)
{
    return VkPresentInfoKHR
    {
        .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
        .pNext = nullptr,
        .waitSemaphoreCount = wait_semaphores_count,
        .pWaitSemaphores = wait_semaphores,
        .swapchainCount = swapchains_count,
        .pSwapchains = swapchains,
        .pImageIndices = images,
        .pResults = present_result,
    };
}

VkSubmitInfo vk_init::submit_info(const VkCommandBuffer* command_buffers, uint32_t command_buffer_count,
    const VkSemaphore* signal_semaphores, uint32_t signal_semaphore_count,
    const VkSemaphore* wait_semaphores, uint32_t wait_semaphore_count,
    const VkPipelineStageFlags* wait_flag)
{
    return VkSubmitInfo
    {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .pNext = nullptr,
        .waitSemaphoreCount = wait_semaphore_count,
        .pWaitSemaphores = wait_semaphores,
        .pWaitDstStageMask = wait_flag,
        .commandBufferCount = command_buffer_count,
        .pCommandBuffers = command_buffers,
        .signalSemaphoreCount = signal_semaphore_count,
        .pSignalSemaphores = signal_semaphores,
    };
}

VkSubmitInfo2 vk_init::submit_info2(const VkCommandBufferSubmitInfo* cmd, const VkSemaphoreSubmitInfo* signal_semaphore_info,
    const VkSemaphoreSubmitInfo* wait_semaphore_info)
{
    return VkSubmitInfo2
    {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2,
        .pNext = nullptr,
        .flags = 0,

        .waitSemaphoreInfoCount = static_cast<uint32_t>(wait_semaphore_info == nullptr ? 0 : 1),
        .pWaitSemaphoreInfos = wait_semaphore_info,

        .commandBufferInfoCount = 1,
        .pCommandBufferInfos = cmd,

        .signalSemaphoreInfoCount = static_cast<uint32_t>(signal_semaphore_info == nullptr ? 0 : 1),
        .pSignalSemaphoreInfos = signal_semaphore_info,
    };
}

//-----------------------------------------------------------------------------
#pragma region Application

VkApplicationInfo vk_init::application::info()
{
    return VkApplicationInfo
    {
        .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
        .pNext = nullptr,
        .pApplicationName = "",
        .applicationVersion = VK_MAKE_VERSION(0, 1, 0),
        .pEngineName = "kiln",
        .engineVersion = VK_MAKE_VERSION(0, 1, 0),
        .apiVersion = VK_API_VERSION_1_3,
    };
}

#pragma endregion
//-----------------------------------------------------------------------------
#pragma region Attachments

VkAttachmentDescription vk_init::attachments::color(VkFormat format)
{
    return VkAttachmentDescription
    {
        .flags = 0,

        .format = format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,

        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,

        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
    };
}

VkAttachmentReference vk_init::attachments::color_ref(uint32_t index)
{
    return VkAttachmentReference
    {
        .attachment = index,
        .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
    };
}

#pragma endregion
//-----------------------------------------------------------------------------
#pragma region Buffer

VkBufferCreateInfo vk_init::buffer::create_info(const VkDeviceSize size, const VkBufferUsageFlags usage)
{
    return VkBufferCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .size = size,
        .usage = usage,
        //.sharingMode =
        //.queueFamilyIndexCount =
        //.pQueueFamilyIndices = 
    };
}


#pragma endregion
//-----------------------------------------------------------------------------
#pragma region Command

VkCommandBufferAllocateInfo vk_init::command::buffer_allocate_info(VkCommandPool pool, uint32_t count)
{
    return buffer_allocate_info(pool, count, true);
}

VkCommandBufferAllocateInfo vk_init::command::buffer_allocate_info(VkCommandPool pool, uint32_t count, bool is_primary)
{
    return VkCommandBufferAllocateInfo
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .pNext = nullptr,
        .commandPool = pool,
        .level = is_primary ? VK_COMMAND_BUFFER_LEVEL_PRIMARY : VK_COMMAND_BUFFER_LEVEL_SECONDARY,
        .commandBufferCount = count,
    };
}

VkCommandBufferBeginInfo vk_init::command::buffer_begin_info(VkCommandBufferUsageFlags flags, const VkCommandBufferInheritanceInfo* inherit_info)
{
    return VkCommandBufferBeginInfo
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext = nullptr,
        .flags = flags,
        .pInheritanceInfo = inherit_info,
    };
}

VkCommandBufferInheritanceInfo vk_init::command::buffer_inheritance_info()
{
    return VkCommandBufferInheritanceInfo
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO,
        .pNext = nullptr,
    };
}

VkCommandBufferInheritanceRenderingInfo vk_init::command::buffer_inheritance_rendering_info(VkSampleCountFlagBits sampling)
{
    return VkCommandBufferInheritanceRenderingInfo
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_RENDERING_INFO,
        .pNext = nullptr,
        .flags = 0,

        .viewMask = 0,

        .colorAttachmentCount = 0,
        .pColorAttachmentFormats = nullptr,

        .depthAttachmentFormat = VK_FORMAT_UNDEFINED,
        .stencilAttachmentFormat = VK_FORMAT_UNDEFINED,
        .rasterizationSamples = sampling,
    };
}

VkCommandBufferSubmitInfo vk_init::command::buffer_submit_info(const VkCommandBuffer cmd)
{
    return VkCommandBufferSubmitInfo
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO,
        .pNext = nullptr,
        .commandBuffer = cmd,
        .deviceMask = 0,
    };
}

VkCommandPoolCreateInfo vk_init::command::pool_create_info(uint32_t queue_family_index, VkCommandPoolCreateFlags flags)
{
    return VkCommandPoolCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .pNext = nullptr,
        .flags = flags,
        .queueFamilyIndex = queue_family_index,
    };
}

#pragma endregion
//-----------------------------------------------------------------------------
#pragma region Debug

VkDebugReportCallbackCreateInfoEXT vk_init::debug::report_callback_create_info_EXT(PFN_vkDebugReportCallbackEXT callback,
    void* user_data)
{
    return VkDebugReportCallbackCreateInfoEXT
    {
        .sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT,
        .pNext = nullptr,
        .flags = VK_DEBUG_REPORT_INFORMATION_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT |
                 VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT | VK_DEBUG_REPORT_ERROR_BIT_EXT |
                 VK_DEBUG_REPORT_DEBUG_BIT_EXT,
        .pfnCallback = callback,
        .pUserData = user_data,
    };
}

#pragma endregion
//-----------------------------------------------------------------------------
#pragma region Descriptor Pool

VkDescriptorPoolCreateInfo vk_init::descriptor_pool::create_info(const std::vector<VkDescriptorPoolSize>& pool_sizes, uint32_t max_sets)
{
    return create_info(pool_sizes.data(), static_cast<uint32_t>(pool_sizes.size()), max_sets);
}

VkDescriptorPoolCreateInfo vk_init::descriptor_pool::create_info(const VkDescriptorPoolSize* pool_sizes,
    uint32_t pool_sizes_count, uint32_t max_sets)
{
    return VkDescriptorPoolCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .maxSets = max_sets,
        .poolSizeCount = pool_sizes_count,
        .pPoolSizes = pool_sizes,
    };
}

#pragma endregion
//-----------------------------------------------------------------------------
#pragma region Descriptor Set

VkDescriptorSetAllocateInfo vk_init::descriptor_set::allocate_info(VkDescriptorPool pool, const VkDescriptorSetLayout* layout)
{
    return allocate_info(pool, layout, 1);
}

VkDescriptorSetAllocateInfo vk_init::descriptor_set::allocate_info(VkDescriptorPool pool,
    const VkDescriptorSetLayout* layouts, uint32_t layouts_count)
{
    return VkDescriptorSetAllocateInfo
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .pNext = nullptr,
        .descriptorPool = pool,
        .descriptorSetCount = layouts_count,
        .pSetLayouts = layouts,
    };
}

void vk_init::descriptor_set::destroy_layout(VkDevice device, VkDescriptorSetLayout* layout)
{
    if (layout == nullptr) return;

    vkDestroyDescriptorSetLayout(device, *layout, nullptr);
    *layout = nullptr;
}

VkDescriptorSetLayoutBinding vk_init::descriptor_set::layout_binding(uint32_t binding,
    VkDescriptorType types, uint32_t type_count,
    VkShaderStageFlags shader_stage, const VkSampler* sampler)
{
    return VkDescriptorSetLayoutBinding
    {
        .binding = binding,
        .descriptorType = types,
        .descriptorCount = type_count,
        .stageFlags = shader_stage,
        .pImmutableSamplers = sampler,
    };
}

VkDescriptorSetLayoutCreateInfo vk_init::descriptor_set::layout_create_info(const VkDescriptorSetLayoutBinding* bindings,
    uint32_t binding_count)
{
    return VkDescriptorSetLayoutCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .bindingCount = binding_count,
        .pBindings = bindings,
    };
}

VkWriteDescriptorSet vk_init::descriptor_set::write(VkDescriptorType type, uint32_t binding, VkDescriptorSet dst_set,
    const VkDescriptorBufferInfo* info, uint32_t descriptor_count, const VkDescriptorImageInfo* images)
{
    return VkWriteDescriptorSet
    {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .pNext = nullptr,

        .dstSet = dst_set,
        .dstBinding = binding,
        .dstArrayElement = 0,
        .descriptorCount = descriptor_count,
        .descriptorType = type,
        .pImageInfo = images,
        .pBufferInfo = info,
        .pTexelBufferView = nullptr,
    };
}



#pragma endregion
//-----------------------------------------------------------------------------
#pragma region Device

namespace vk_init::device
{
    constexpr const char* BASE_EXTENSIONS[] =
    {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME
    };

    constexpr int BASE_EXTENSION_COUNT = 1;

    constexpr int BASE_LAYER_COUNT = 0;

    constexpr const char* DEBUG_EXTENSIONS[] =
    {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME,
    };

    constexpr int DEBUG_EXTENSION_COUNT = 1;

    constexpr const char* DEBUG_LAYERS[] =
    {
        "VK_LAYER_LUNARG_standard_validation"
    };

    constexpr int DEBUG_LAYER_COUNT = 1;
}

VkDeviceCreateInfo vk_init::device::create_info(const VkDeviceQueueCreateInfo* info,
    uint32_t size, const VkPhysicalDeviceFeatures* features, bool debug)
{
    auto create_info = VkDeviceCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        .pNext = nullptr,
        .queueCreateInfoCount = size,
        .pQueueCreateInfos = info,
        .pEnabledFeatures = features,
    };

    if (debug)
    {
        create_info.ppEnabledExtensionNames = DEBUG_EXTENSIONS;
        create_info.enabledExtensionCount = DEBUG_EXTENSION_COUNT;
        //create_info.ppEnabledLayerNames = DEBUG_LAYERS;
        //create_info.enabledLayerCount = DEBUG_LAYER_COUNT;
    }
    else
    {
        create_info.ppEnabledExtensionNames = BASE_EXTENSIONS;
        create_info.enabledExtensionCount = BASE_EXTENSION_COUNT;
        //create_info.ppEnabledLayerNames = nullptr;
        //create_info.enabledLayerCount = BASE_LAYER_COUNT;
    }

    return create_info;
}

VkDeviceQueueCreateInfo vk_init::device::queue_create_info(uint32_t queue_family_index, const float* priority,
    uint32_t queue_count)
{
    return VkDeviceQueueCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .queueFamilyIndex = queue_family_index,
        .queueCount = queue_count,
        .pQueuePriorities = priority,
    };
}

#pragma endregion
//-----------------------------------------------------------------------------
#pragma region Frame Buffer

VkFramebufferCreateInfo vk_init::frame_buffer::create_info(VkRenderPass render_pass, uint32_t surface_width, uint32_t surface_height,
    const std::vector<VkImageView>& attachments)
{
    return VkFramebufferCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .renderPass = render_pass,
        .attachmentCount = static_cast<uint32_t>(attachments.size()),
        .pAttachments = attachments.data(),
        .width = surface_width,
        .height = surface_height,
        .layers = 1,
    };
}

#pragma endregion
//-----------------------------------------------------------------------------
#pragma region Fence

VkFenceCreateInfo vk_init::fence::create_info(const VkFenceCreateFlags flags)
{
    return VkFenceCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
        .pNext = nullptr,
        .flags = flags,
    };
}

#pragma endregion
//-----------------------------------------------------------------------------
#pragma region Image

VkImageCreateInfo vk_init::image::create_info(uint32_t image_width, uint32_t image_height, VkFormat format,
    VkImageUsageFlags usage, uint32_t mip_levels, VkSampleCountFlagBits samples)
{
    const auto extent = VkExtent3D{ .width = image_width, .height = image_height, .depth = 1 };
    return create_info(extent, format, usage, mip_levels, samples);
}

VkImageCreateInfo vk_init::image::create_info(VkExtent3D extent, VkFormat format, VkImageUsageFlags usage_flags)
{
    return create_info(extent, format, usage_flags, 1, VK_SAMPLE_COUNT_1_BIT);
}

VkImageCreateInfo vk_init::image::create_info(VkExtent3D extent, VkFormat format, VkImageUsageFlags usage,
    uint32_t mip_levels, VkSampleCountFlagBits samples)
{
    return VkImageCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .imageType = VK_IMAGE_TYPE_2D,
        .format = format,
        .extent = extent,
        .mipLevels = mip_levels,
        .arrayLayers = 1,
        .samples = samples,
        .tiling = VK_IMAGE_TILING_OPTIMAL,
        .usage = usage,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        //.queueFamilyIndexCount = 0,
        //.pQueueFamilyIndices = nullptr,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
    };
}

VkImageSubresourceRange vk_init::image::subresource_range(VkImageAspectFlags aspect_mask)
{
    return VkImageSubresourceRange
    {
        .aspectMask = aspect_mask,
        .baseMipLevel = 0,
        .levelCount = VK_REMAINING_MIP_LEVELS,
        .baseArrayLayer = 0,
        .layerCount = VK_REMAINING_ARRAY_LAYERS,
    };
}

VkImageViewCreateInfo vk_init::image::view_create_info(VkImage image, VkFormat format,
    VkImageAspectFlags aspect_mask)
{
    return view_create_info(image, format, aspect_mask, 1);
}

VkImageViewCreateInfo vk_init::image::view_create_info(VkImage image, VkFormat format,
    VkImageAspectFlags aspect_mask, uint32_t mip_levels)
{
    auto create_info = VkImageViewCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .image = image,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = format,
    };

    create_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    create_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    create_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    create_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

    create_info.subresourceRange.aspectMask = aspect_mask;
    create_info.subresourceRange.baseMipLevel = 0;
    create_info.subresourceRange.levelCount = mip_levels;
    create_info.subresourceRange.baseArrayLayer = 0;
    create_info.subresourceRange.layerCount = 1;

    return create_info;
}

#pragma endregion
//-----------------------------------------------------------------------------
#pragma region Instance

VkInstanceCreateInfo vk_init::instance::create_info(const VkApplicationInfo* app_info,
    const std::vector<const char*>& layers, const std::vector<const char*>& extensions)
{
    return VkInstanceCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .pApplicationInfo = app_info,
        .enabledLayerCount = static_cast<uint32_t>(layers.size()),
        .ppEnabledLayerNames = layers.data(),
        .enabledExtensionCount = static_cast<uint32_t>(extensions.size()),
        .ppEnabledExtensionNames = extensions.data(),
    };
}

VkInstanceCreateInfo vk_init::instance::create_info(const VkApplicationInfo* app_info,
    const char** layers, uint32_t layer_count, const char** extensions, uint32_t extension_count)
{
    return VkInstanceCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .pApplicationInfo = app_info,
        .enabledLayerCount = layer_count,
        .ppEnabledLayerNames = layers,
        .enabledExtensionCount = extension_count,
        .ppEnabledExtensionNames = extensions,
    };
}

#pragma endregion
//-----------------------------------------------------------------------------
#pragma region Pipleine

VkPipelineColorBlendStateCreateInfo vk_init::pipeline::color_blend_state_create_info(
    const VkPipelineColorBlendAttachmentState* attachments, uint32_t attachments_count)
{
    return VkPipelineColorBlendStateCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .logicOpEnable = VK_FALSE,
        .logicOp = VK_LOGIC_OP_CLEAR,
        .attachmentCount = attachments_count,
        .pAttachments = attachments,
        //.blendConstants = 
    };
}

void vk_init::pipeline::destroy(VkDevice device, VkPipeline* pipeline)
{
    if (pipeline == nullptr) return;

    vkDestroyPipeline(device, *pipeline, nullptr);
    *pipeline = nullptr;
}

void vk_init::pipeline::destroy_layout(VkDevice device, VkPipelineLayout* layout)
{
    if (layout == nullptr) return;

    vkDestroyPipelineLayout(device, *layout, nullptr);
    *layout = nullptr;
}

VkPipelineDepthStencilStateCreateInfo vk_init::pipeline::depth_stencil_state_create_info()
{
    return VkPipelineDepthStencilStateCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .depthTestEnable = VK_FALSE,
        .depthWriteEnable = VK_TRUE,
        .depthCompareOp = VK_COMPARE_OP_LESS,
        .depthBoundsTestEnable = VK_FALSE,
        .stencilTestEnable = VK_FALSE,
        //.front =
        //.back =
        .minDepthBounds = 0.f,
        .maxDepthBounds = 1.f,
    };
}

VkPipelineDynamicStateCreateInfo vk_init::pipeline::dynamic_state_create_info(const VkDynamicState* states, uint32_t states_count)
{
    return VkPipelineDynamicStateCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .dynamicStateCount = states_count,
        .pDynamicStates = states,
    };
}

VkGraphicsPipelineCreateInfo vk_init::pipeline::graphics_create_info(
    const VkPipelineShaderStageCreateInfo* shader_stages, uint32_t shader_stages_count,
    const VkPipelineVertexInputStateCreateInfo* vertex_input_info,
    const VkPipelineInputAssemblyStateCreateInfo* input_assembly,
    const VkPipelineViewportStateCreateInfo* viewport,
    const VkPipelineRasterizationStateCreateInfo* rasterizer,
    const VkPipelineMultisampleStateCreateInfo* multisampling,
    const VkPipelineDepthStencilStateCreateInfo* depth_stencil,
    const VkPipelineColorBlendStateCreateInfo* color_blending,
    const VkPipelineDynamicStateCreateInfo* dynamic_state,
    VkPipelineLayout layout,
    VkRenderPass render_pass)
{
    return VkGraphicsPipelineCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .stageCount = shader_stages_count,
        .pStages = shader_stages,
        .pVertexInputState = vertex_input_info,
        .pInputAssemblyState = input_assembly,
        .pTessellationState = nullptr,
        .pViewportState = viewport,
        .pRasterizationState = rasterizer,
        .pMultisampleState = multisampling,
        .pDepthStencilState = depth_stencil,
        .pColorBlendState = color_blending,
        .pDynamicState = dynamic_state,
        .layout = layout,
        .renderPass = render_pass,
        .subpass = 0,
        .basePipelineHandle = nullptr,
        .basePipelineIndex = 0,
    };
}

VkPipelineInputAssemblyStateCreateInfo vk_init::pipeline::input_assembly_state_create_info(bool triangles)
{
    return VkPipelineInputAssemblyStateCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .topology = triangles ? VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST : VK_PRIMITIVE_TOPOLOGY_LINE_STRIP,
        .primitiveRestartEnable = VK_FALSE,
    };
}

VkPipelineLayoutCreateInfo vk_init::pipeline::layout_create_info(const VkDescriptorSetLayout* layouts, uint32_t layouts_count,
    const VkPushConstantRange* push_constants, uint32_t push_constants_count)
{
    return VkPipelineLayoutCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .setLayoutCount = layouts_count,
        .pSetLayouts = layouts,
        .pushConstantRangeCount = push_constants_count,
        .pPushConstantRanges = push_constants,
    };
}

VkPipelineMultisampleStateCreateInfo vk_init::pipeline::multisample_state_create_info(VkSampleCountFlagBits samples)
{
    return VkPipelineMultisampleStateCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .rasterizationSamples = samples,
        .sampleShadingEnable = VK_FALSE,
        .minSampleShading = 1.f,
        .pSampleMask = nullptr,
        .alphaToCoverageEnable = VK_FALSE,
        .alphaToOneEnable = VK_FALSE,
    };
}

VkPipelineRasterizationStateCreateInfo vk_init::pipeline::rasterization_state_create_info(bool fill)
{
    return VkPipelineRasterizationStateCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        //.depthClampEnable = VK_FALSE,
        //.rasterizerDiscardEnable = VK_FALSE,
        .polygonMode = fill ? VK_POLYGON_MODE_FILL : VK_POLYGON_MODE_LINE,
        .cullMode = VK_CULL_MODE_NONE,
        .frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
        //.depthBiasEnable = VK_FALSE,
        //.depthBiasConstantFactor = 0.f,
        //.depthBiasClamp = 0.f,
        //.depthBiasSlopeFactor = 0.f,
        .lineWidth = 1.f
    };
}

VkPipelineShaderStageCreateInfo vk_init::pipeline::shader_stage_create_info(const VkShaderStageFlagBits stage, const VkShaderModule shader,
    const char* entry_name)
{
    return VkPipelineShaderStageCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .stage = stage,
        .module = shader,
        .pName = entry_name,
        .pSpecializationInfo = nullptr,
    };
}

VkPipelineVertexInputStateCreateInfo vk_init::pipeline::vertex_input_state_create_info(
    const std::vector<VkVertexInputBindingDescription>& bindings,
    const std::vector<VkVertexInputAttributeDescription>& attributes)
{
    return vertex_input_state_create_info(
        bindings.data(), static_cast<uint32_t>(bindings.size()),
        attributes.data(), static_cast<uint32_t>(attributes.size()));
}

VkPipelineVertexInputStateCreateInfo vk_init::pipeline::vertex_input_state_create_info(
    const VkVertexInputBindingDescription* bindings, uint32_t bindings_count,
    const VkVertexInputAttributeDescription* attributes, uint32_t attributes_count)
{
    return VkPipelineVertexInputStateCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .vertexBindingDescriptionCount = bindings_count,
        .pVertexBindingDescriptions = bindings,
        .vertexAttributeDescriptionCount = attributes_count,
        .pVertexAttributeDescriptions = attributes,
    };
}

VkPipelineViewportStateCreateInfo vk_init::pipeline::viewport_state_create_info(const VkViewport* viewport, const VkRect2D* scissor)
{
    return VkPipelineViewportStateCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .viewportCount = 1,
        .pViewports = viewport,
        .scissorCount = 1,
        .pScissors = scissor,
    };
}

#pragma endregion
//-----------------------------------------------------------------------------
#pragma region Rendering


VkRenderingAttachmentInfo  vk_init::rendering::color_attachment_info(VkImageView view, const VkClearValue* clear,
    VkImageLayout layout /*  = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL */)
{
    return VkRenderingAttachmentInfo
    {
        .sType = VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO,
        .pNext = nullptr,
        .imageView = view,
        .imageLayout = layout,
        //.resolveMode
        //.resolveImageView
        //.resolveImageLayout
        .loadOp = clear != nullptr ? VK_ATTACHMENT_LOAD_OP_CLEAR : VK_ATTACHMENT_LOAD_OP_LOAD,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .clearValue = clear != nullptr ? *clear : VkClearValue{ },
    };
}

VkRenderingAttachmentInfo vk_init::rendering::depth_attachment_info(const VkImageView view,
    const VkImageLayout layout /* = VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL */)
{
    return VkRenderingAttachmentInfo
    {
        .sType = VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO,
        .pNext = nullptr,
        .imageView = view,
        .imageLayout = layout,
        //.resolveMode
        //.resolveImageView
        //.resolveImageLayout
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .clearValue = VkClearValue
        {
            .depthStencil = VkClearDepthStencilValue
            {
                .depth = 0.f,
            },
        },
    };
}

VkRenderingInfo vk_init::rendering::info(VkExtent2D render_extent, const VkRenderingAttachmentInfo* color_attachment,
    const VkRenderingAttachmentInfo* depth_attachment)
{
    return VkRenderingInfo
    {
        .sType = VK_STRUCTURE_TYPE_RENDERING_INFO,
        .pNext = nullptr,
        .flags = 0,

        .renderArea = VkRect2D
        {
            .offset = VkOffset2D{.x = 0, .y = 0, },
            .extent = render_extent,
        },

        .layerCount = 1,
        //.viewMask = ,
        .colorAttachmentCount = 1,
        .pColorAttachments = color_attachment,
        .pDepthAttachment = depth_attachment,
        .pStencilAttachment = nullptr,
    };
}

#pragma endregion
//-----------------------------------------------------------------------------
#pragma region Render Pass

VkRenderPassBeginInfo vk_init::render_pass::begin_info(VkRenderPass render_pass, VkFramebuffer frame_buffer, VkRect2D rect,
    const VkClearValue* clear_values, uint32_t clear_count)
{
    return VkRenderPassBeginInfo
    {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
        .pNext = nullptr,
        .renderPass = render_pass,
        .framebuffer = frame_buffer,
        .renderArea = rect,
        .clearValueCount = clear_count,
        .pClearValues = clear_values,
    };
}

VkRenderPassCreateInfo vk_init::render_pass::create_info(
    const std::vector<VkAttachmentDescription>& attachments,
    const std::vector<VkSubpassDescription>& subpasses,
    const std::vector<VkSubpassDependency>& dependencies)
{
    return create_info(
        attachments.data(), static_cast<uint32_t>(attachments.size()),
        subpasses.data(), static_cast<uint32_t>(subpasses.size()),
        dependencies.data(), static_cast<uint32_t>(dependencies.size()));
}

VkRenderPassCreateInfo vk_init::render_pass::create_info(const VkAttachmentDescription* attachments,
    const VkSubpassDescription* subpasses, const VkSubpassDependency* dependencies)
{
    return create_info(attachments, 1, subpasses, 1, dependencies, 1);
}

VkRenderPassCreateInfo vk_init::render_pass::create_info(
    const VkAttachmentDescription* attachments, uint32_t attachment_count,
    const VkSubpassDescription* subpasses, uint32_t subpass_count,
    const VkSubpassDependency* dependencies, uint32_t dependencies_count)
{
    return VkRenderPassCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .attachmentCount = attachment_count,
        .pAttachments = attachments,
        .subpassCount = subpass_count,
        .pSubpasses = subpasses,
        .dependencyCount = dependencies_count,
        .pDependencies = dependencies,
    };
}

#pragma endregion
//-----------------------------------------------------------------------------
#pragma region Sampler

VkSamplerCreateInfo vk_init::sampler::create_info(VkBool32 linear, float anisotropy, float mip_levels)
{
    const auto filter = linear ? VK_FILTER_LINEAR : VK_FILTER_NEAREST;

    return VkSamplerCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .magFilter = filter,
        .minFilter = filter,
        .mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST,
        .addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .mipLodBias = 0.f,
        .anisotropyEnable = anisotropy > 1.f ? VK_TRUE : VK_FALSE,
        .maxAnisotropy = anisotropy,
        .compareEnable = VK_FALSE,
        .compareOp = VK_COMPARE_OP_ALWAYS,
        .minLod = 0.f,
        .maxLod = 0.f,
        .borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK,
        // Un-normalized coordinates cannot be used if anisotropy is enabled.
        .unnormalizedCoordinates = VK_TRUE,
    };
}


VkSamplerCreateInfo vk_init::sampler::create_info_linear(VkSampleCountFlagBits msaa)
{
    const auto enable_anisotropy = msaa > VK_SAMPLE_COUNT_1_BIT;

    return VkSamplerCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .magFilter = VK_FILTER_LINEAR,
        .minFilter = VK_FILTER_LINEAR,
        .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
        .addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .mipLodBias = 0.f,
        .anisotropyEnable = static_cast<VkBool32>(enable_anisotropy),
        .maxAnisotropy = static_cast<float>(msaa),
        .compareEnable = VK_FALSE,
        .compareOp = VK_COMPARE_OP_ALWAYS,
        .minLod = 0.f,
        .maxLod = 0.f,
        .borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK,
        // Unnormalized coordinates cannot be used if anisotropy is enabled.
        .unnormalizedCoordinates = enable_anisotropy != VK_FALSE ? VK_FALSE : VK_TRUE,
    };
}

VkSamplerCreateInfo vk_init::sampler::create_info_nearest(VkSampleCountFlagBits msaa)
{
    const auto enable_anisotropy = msaa > VK_SAMPLE_COUNT_1_BIT;

    return VkSamplerCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,

        .magFilter = VK_FILTER_NEAREST,
        .minFilter = VK_FILTER_NEAREST,
        .mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST,
        .addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .mipLodBias = 0.f,
        .anisotropyEnable = static_cast<VkBool32>(enable_anisotropy),
        .maxAnisotropy = static_cast<float>(msaa),
        .compareEnable = VK_FALSE,
        .compareOp = VK_COMPARE_OP_ALWAYS,
        .minLod = 0.f,
        .maxLod = 0.f,
        .borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK,
        // Unnormalized coordinates cannot be used if anisotropy is enabled.
        .unnormalizedCoordinates = enable_anisotropy != VK_FALSE ? VK_FALSE : VK_TRUE,
    };
}

#pragma endregion
//-----------------------------------------------------------------------------
#pragma region Semaphore

VkSemaphoreCreateInfo vk_init::semaphore::create_info(VkSemaphoreCreateFlags flags)
{
    return VkSemaphoreCreateInfo
    {
        .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
        .pNext = nullptr,
        .flags = flags,
    };
}

VkSemaphoreSubmitInfo vk_init::semaphore::submit_info(const VkPipelineStageFlags2 stage_mask, const VkSemaphore semaphore)
{
    return VkSemaphoreSubmitInfo
    {
        .sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO,
        .pNext = nullptr,
        .semaphore = semaphore,
        .value = 1,
        .stageMask = stage_mask,
        .deviceIndex = 0,
    };
}

#pragma endregion
//-----------------------------------------------------------------------------
#pragma region Subpass

VkSubpassDependency vk_init::subpass::dependency()
{
    return VkSubpassDependency
    {
        .srcSubpass = VK_SUBPASS_EXTERNAL,
        .dstSubpass = 0,

        .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,

        .srcAccessMask = 0,
        .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
    };
}

VkSubpassDescription vk_init::subpass::description(
    const VkAttachmentReference* input_attachment_refs, uint32_t input_attachment_refs_count,
    const VkAttachmentReference* color_attachment_refs, uint32_t color_attachment_refs_count,
    const uint32_t* preserve_attachment_refs, uint32_t preserve_attachment_refs_count)
{
    return VkSubpassDescription
    {
        .flags = 0,

        .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,

        .inputAttachmentCount = input_attachment_refs_count,
        .pInputAttachments = input_attachment_refs,

        .colorAttachmentCount = color_attachment_refs_count,
        .pColorAttachments = color_attachment_refs,

        .pResolveAttachments = nullptr,
        .pDepthStencilAttachment = nullptr,

        .preserveAttachmentCount = preserve_attachment_refs_count,
        .pPreserveAttachments = preserve_attachment_refs,
    };
}

#pragma endregion
//-----------------------------------------------------------------------------
#pragma region Swapchain

VkSwapchainCreateInfoKHR vk_init::swapchain::create_info_KHR(VkSurfaceKHR surface,
    VkSurfaceCapabilitiesKHR capabilities, VkSurfaceFormatKHR format,
    uint32_t surface_width, uint32_t surface_height,
    VkPresentModeKHR present_mode, VkSwapchainKHR old_swapchain, uint32_t min_image_count)
{
    return VkSwapchainCreateInfoKHR
    {
        .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
        .pNext = nullptr,
        .flags = 0,

        .surface = surface,
        .minImageCount = min_image_count,
        .imageFormat = format.format,
        .imageColorSpace = format.colorSpace,
        .imageExtent = VkExtent2D{.width = surface_width, .height = surface_height },
        .imageArrayLayers = 1,
        .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT,
        .imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
        //.queueFamilyIndexCount = 0,
        //.pQueueFamilyIndices = nullptr,
        .preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR,
        .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        .presentMode = present_mode,
        .oldSwapchain = old_swapchain,
    };
}

#pragma endregion
//-----------------------------------------------------------------------------
#pragma region Vertex Input

VkVertexInputAttributeDescription vk_init::vertex_input::attribute_description(uint32_t binding, uint32_t location,
    uint32_t offset, VkFormat format)
{
    return VkVertexInputAttributeDescription
    {
        .location = location,
        .binding = binding,
        .format = format,
        .offset = offset,
    };
}

VkVertexInputBindingDescription vk_init::vertex_input::binding_description(uint32_t binding, uint32_t stride, VkVertexInputRate input_rate)
{
    return VkVertexInputBindingDescription
    {
        .binding = binding,
        .stride = stride,
        .inputRate = input_rate,
    };
}

#pragma endregion
//-----------------------------------------------------------------------------