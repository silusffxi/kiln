#include <cstdint>
#include <string>
#include <tuple>
#include <unordered_set>
#include <vector>
#include <vulkan/vulkan.h>
#include "kiln/logging.hpp"
#include "kiln/vulkan/vk_instance.h"
#include "kiln/vulkan/vk_version.hpp"
//------------------------------------
#include "kiln/vulkan/vk_physical_device.h"

using namespace kiln;
using namespace vulkan;

namespace
{
    struct queue_candidate
    {
        uint32_t queue_family;
        uint32_t queue_count;
    };
}

vk_physical_device::vk_physical_device(const std::unique_ptr<vk_instance>& instance) :
    vk_physical_device(instance.get(), default_physical_device_search_predicate)
{
}

vk_physical_device::vk_physical_device(const vk_instance* instance) :
    vk_physical_device(instance, default_physical_device_search_predicate)
{
}

vk_physical_device::vk_physical_device(const std::unique_ptr<vk_instance>& instance,
    const std::function<bool(VkPhysicalDevice, const VkPhysicalDeviceProperties*)>& search_predicate) :
    vk_physical_device(instance.get(), search_predicate)
{
}

vk_physical_device::vk_physical_device(const vk_instance* instance,
    const std::function<bool(VkPhysicalDevice, const VkPhysicalDeviceProperties*)>& search_predicate)
{
    const auto log = logging::get_logger(log_name::vulkan);

    const auto devices = enumerate_physical_devices(*instance);
    log->debug("Found {} possible physical devices.", devices.size());

    for (auto& dev : devices)
    {
        VkPhysicalDeviceProperties props;
        vkGetPhysicalDeviceProperties(dev, &props);

        if (props.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
        {
            _physical_device = dev;
            _properties = props;
            break;
        }

        log->debug("Skipped physical device \"{}\". Device is not a discrete GPU.",
            std::string(props.deviceName));
    }

    if (_physical_device == nullptr)
    {
        log->critical("Failed to locate a suitable physical device.");
        return;
    }

    _api_version = vk_version(_properties.apiVersion);
    _device_name = _properties.deviceName;

    // NVIDIA version appears to be packed in the "driverVersion" property as:
    // the top 10 bits are the major version number (e.g. 546) and the next
    // 8 bits are the minor version number (e.g. 65).

    //_driver_version.major = _properties.driverVersion >> 22;
    //_driver_version.minor = (_properties.driverVersion << 10) >> 14;

    _driver_version.major = VK_VERSION_MAJOR(_properties.driverVersion);
    _driver_version.minor = VK_VERSION_MINOR(_properties.driverVersion);
    _driver_version.patch = VK_VERSION_PATCH(_properties.driverVersion);

    auto [compute_queue_family, graphics_queue_family, queue_family_props] = enumerate_queue_families(_physical_device);
    _queue_families = queue_family_props;

    if (compute_queue_family >= 0)
    {
        _queue_family_compute = compute_queue_family;
        log->debug("Using queue family {} for compute ({} available queues)",
            compute_queue_family,
            queue_family_props.at(compute_queue_family).queueCount);
    }

    if (graphics_queue_family >= 0)
    {
        _queue_family_graphics = graphics_queue_family;
        log->debug("Using queue family {} for graphics ({} available queues)",
            graphics_queue_family,
            queue_family_props.at(graphics_queue_family).queueCount);
    }

    if (_queue_family_compute < 0 || _queue_family_graphics < 0)
        return;

    vkGetPhysicalDeviceFeatures(_physical_device, &_features);
    vkGetPhysicalDeviceMemoryProperties(_physical_device, &_memory);

    populate_additional_physical_device_features();
    populate_additional_physical_device_properties();
}

vk_physical_device::~vk_physical_device()
{
    if (_physical_device != nullptr)
        _physical_device = nullptr;

    logging::get_logger(log_name::vulkan)->debug("VkPhysicalDevice released.");
}

VkFormatProperties vk_physical_device::get_format_properties(VkFormat format) const
{
    VkFormatProperties props;
    vkGetPhysicalDeviceFormatProperties(_physical_device, format, &props);

    return props;
}

VkQueueFamilyProperties* vk_physical_device::get_queue_family_properties(uint32_t queue_family)
{
    if (queue_family >= _queue_families.size())
        return nullptr;

    return &_queue_families[queue_family];
}

VkSampleCountFlagBits vk_physical_device::get_supported_msaa() const
{
    if (_features.samplerAnisotropy == VK_FALSE)
        return VK_SAMPLE_COUNT_1_BIT;

    const auto counts = _properties.limits.framebufferColorSampleCounts & _properties.limits.framebufferDepthSampleCounts;
    if (counts & VK_SAMPLE_COUNT_64_BIT) return VK_SAMPLE_COUNT_64_BIT;
    if (counts & VK_SAMPLE_COUNT_32_BIT) return VK_SAMPLE_COUNT_32_BIT;
    if (counts & VK_SAMPLE_COUNT_16_BIT) return VK_SAMPLE_COUNT_16_BIT;
    if (counts & VK_SAMPLE_COUNT_8_BIT) return VK_SAMPLE_COUNT_8_BIT;
    if (counts & VK_SAMPLE_COUNT_4_BIT) return VK_SAMPLE_COUNT_4_BIT;
    if (counts & VK_SAMPLE_COUNT_2_BIT) return VK_SAMPLE_COUNT_2_BIT;

    return VK_SAMPLE_COUNT_1_BIT;
}

uint32_t vk_physical_device::get_queue_family_queue_count(int32_t queue_family) const
{
    if (queue_family < 0 ||
        queue_family >= static_cast<int32_t>(_queue_families.size()))
    {
        return 0u;
    }

    return _queue_families.at(queue_family).queueCount;
}

void vk_physical_device::populate_additional_physical_device_features()
{
    // If we don't have at least Vulkan 1.1 then we can't do
    // anything with vkGetPhysicalDeviceFeatures2.
    if (_api_version < vulkan::api_1_1) return;

    // Vulkan 1.1 is required in order to use "vkGetPhysicalDeviceFeatures2".
    // v1.1 also included the "VkPhysicalDeviceVulkan11Features" struct.
    //
    // Configure the "VkPhysicalDeviceFeatures2" chain so that we can pull in features
    // supported for the physical device for Vulkan 1.0, 1.1, 1.2, 1.3, etc.
    _features2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
    _features2.pNext = &_vulkan_11_features;

    _vulkan_11_features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES;

    if (_api_version >= vulkan::api_1_2)
    {
        _vulkan_11_features.pNext = &_vulkan_12_features;
        _vulkan_12_features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES;
    }

    if (_api_version >= vulkan::api_1_3)
    {
        _vulkan_12_features.pNext = &_vulkan_13_features;
        _vulkan_13_features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_3_FEATURES;
    }

    vkGetPhysicalDeviceFeatures2(_physical_device, &_features2);
}

void vk_physical_device::populate_additional_physical_device_properties()
{
    // If we don't have at least Vulkan 1.1 then we can't do
    // anything with vkGetPhysicalDeviceProperties2.
    if (_api_version < vulkan::api_1_1) return;

    _properties2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
    _properties2.pNext = &_vulkan_11_props;

    _vulkan_11_props.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_PROPERTIES;

    if (_api_version >= vulkan::api_1_2)
    {
        _vulkan_11_props.pNext = &_vulkan_12_props;
        _vulkan_12_props.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_PROPERTIES;
    }

    if (_api_version >= vulkan::api_1_3)
    {
        _vulkan_12_props.pNext = &_vulkan_13_props;
        _vulkan_13_props.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_3_PROPERTIES;
    }

    vkGetPhysicalDeviceProperties2(_physical_device, &_properties2);
}

bool vk_physical_device::default_physical_device_search_predicate(VkPhysicalDevice device, const VkPhysicalDeviceProperties* props)
{
    return props->deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU;
}

std::vector<VkPhysicalDevice> vk_physical_device::enumerate_physical_devices(VkInstance instance)
{
    uint32_t count;
    vkEnumeratePhysicalDevices(instance, &count, nullptr);

    std::vector<VkPhysicalDevice> devices(count);
    vkEnumeratePhysicalDevices(instance, &count, devices.data());

    return devices;
}

std::tuple<int32_t, int32_t, std::vector<VkQueueFamilyProperties>> vk_physical_device::enumerate_queue_families(
    VkPhysicalDevice device)
{
    const auto log = logging::get_logger(log_name::vulkan);

    uint32_t count;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &count, nullptr);

    std::vector<VkQueueFamilyProperties> queue_family_props(count);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &count, queue_family_props.data());

    log->trace("Located {} available queues families.", count);

    // Enumerate our available queues and see which ones have the features
    // we're looking for.

    std::vector<queue_candidate> compute_candidates;
    std::vector<queue_candidate> graphics_candidates;
    for (auto idx = 0u; idx < count; idx++)
    {
        const auto& props = queue_family_props[idx];
        if (props.queueCount <= 0)
            continue;

        if (props.queueFlags & VK_QUEUE_COMPUTE_BIT)
        {
            compute_candidates.emplace_back(queue_candidate{ idx, props.queueCount });
        }

        if (props.queueFlags & VK_QUEUE_GRAPHICS_BIT)
        {
            graphics_candidates.emplace_back(queue_candidate{ idx, props.queueCount });
        }
    }

    log->debug("Compute queue family candidates: {}", compute_candidates.size());
    log->debug("Graphics queue family candidates: {}", graphics_candidates.size());

    // Once we have enumerated our possible queue family options, we'll want to select
    // the appropriate queue families for what we're looking for.
    //
    // We'll select the graphics queue family first, since that's going to be our "primary"
    // queue family to use.

    std::unordered_set<int32_t> queue_families_selected;
    int32_t compute_family = -1, graphics_family = -1;

    // Select the graphics queue family first.
    if (!graphics_candidates.empty())
    {
        uint32_t queue_count = 0;
        int32_t possible_family = 0;

        // Look for the queue family that has
        // the highest number of queues.
        for (const auto& candidate : graphics_candidates)
        {
            auto family = static_cast<int32_t>(candidate.queue_family);
            if (candidate.queue_count > queue_count && !queue_families_selected.contains(family))
            {
                queue_count = candidate.queue_count;
                possible_family = family;
            }
        }

        graphics_family = possible_family;
        queue_families_selected.emplace(graphics_family);
    }

    // Then select which queue family to use for compute.
    if (!compute_candidates.empty())
    {
        uint32_t queue_count = 0;
        int32_t possible_family = -1;

        // Look for the queue family that has the highest number of queues and
        // is already not in use as the graphics queue.
        for (const auto& candidate : compute_candidates)
        {
            auto family = static_cast<int32_t>(candidate.queue_family);
            if (candidate.queue_count > queue_count &&
                !queue_families_selected.contains(family))
            {
                queue_count = candidate.queue_count;
                possible_family = family;
            }
        }

        compute_family = possible_family;
        queue_families_selected.emplace(compute_family);
    }

    if (compute_family < 0)
    {
        log->warn("Failed to select a queue family for compute.");
    }

    if (graphics_family < 0)
    {
        log->warn("Failed to select a queue family for graphics.");
    }

    return { compute_family, graphics_family, queue_family_props };
}
