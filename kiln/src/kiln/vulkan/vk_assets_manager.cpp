#include <cstdint>
#include <filesystem>
#include <ranges>
#include <utility>
#include <fastgltf/core.hpp>
#include <fastgltf/glm_element_traits.hpp>
#include "kiln/logging.hpp"
#include "kiln/shaders.hpp"
#include "kiln/vulkan/vk_asset_types.h"
//----------------------------------------
#include "kiln/vulkan/vk_assets_manager.h"

namespace fs = std::filesystem;
using namespace kiln;
using namespace vulkan;

vk_assets_manager::vk_assets_manager(const std::unique_ptr<vk_device>& device, const std::unique_ptr<vk_immediate>& immediate,
    const std::filesystem::path& assets_root) :
    vk_assets_manager(device.get(), immediate.get(), assets_root)
{
}

vk_assets_manager::vk_assets_manager(vk_device* device, vk_immediate* immediate, std::filesystem::path assets_root) :
    _log(logging::get_logger(log_name::vulkan)),
    _device(device), _immediate(immediate), _assets_root(std::move(assets_root))
{
}

vk_assets_manager::~vk_assets_manager()
{
    if (!_shaders.empty())
    {
        for (auto& shader : _shaders | std::views::values)
        {
            shader.reset();
            shader = nullptr;
        }
        _shaders.clear();
    }
}

vk_shader* vk_assets_manager::get_shader(const std::string& name)
{
    if (_shaders.contains(name))
        return _shaders.at(name).get();

    const auto* bin = shaders::get_shader(name);
    if (bin == nullptr)
    {
        _log->warn("Shader Asset: \"{}\" does not exist", name);
        return nullptr;
    }

    auto shader = vk_shader::from_shader_binary(_device, *bin);
    if (shader == nullptr)
    {
        _log->warn("Shader Asset: failed to load \"{}\"", name);
        return nullptr;
    }

    auto [_, added] = _shaders.try_emplace(name, std::move(shader));
    if (!added)
        return nullptr;

    return _shaders.at(name).get();
}

load_gltf_meshes_result vk_assets_manager::load_gltf_meshes(const std::filesystem::path& file_path)
{
    std::vector<uint32_t> indices;
    std::vector<vertex>   vertices;

    fastgltf::Asset  gltf;
    fastgltf::Parser parser;

    std::vector<std::shared_ptr<mesh_asset>> meshes;

    const auto target_file = _assets_root / file_path;

    if (!fs::exists(target_file))
    {
        _log->warn("GLTF file does not exist at \"{}\"", target_file.string());
        return { };
    }

    _log->debug("Loading GLTF file: \"{}\"", target_file.string());
    fastgltf::GltfDataBuffer data;
    data.loadFromFile(target_file);

    constexpr auto gltf_options = fastgltf::Options::LoadGLBBuffers | fastgltf::Options::LoadExternalBuffers;

    auto load = parser.loadGltfBinary(&data, target_file.parent_path(), gltf_options);
    if (load)
        gltf = std::move(load.get());
    else
    {
        _log->error("Failed to load GLTF file: \"{}\"", fastgltf::to_underlying(load.error()));
        return { };
    }

    for (fastgltf::Mesh& mesh : gltf.meshes)
    {
        const auto current_index = static_cast<uint32_t>(meshes.size());

        auto new_mesh = std::make_shared<mesh_asset>();
        new_mesh->name = mesh.name;

        _log->debug("Loading Mesh: {} ({})", new_mesh->name, current_index);

        // Clear the mesh arrays for each mesh.
        // This is to avoid accidentally merging meshes.
        indices.clear();
        vertices.clear();

        for (auto&& p : mesh.primitives)
        {
            auto surface = geo_surface
            {
                .start_index = static_cast<uint32_t>(indices.size()),
                .count       = static_cast<uint32_t>(gltf.accessors[p.indicesAccessor.value()].count),
            };

            auto initial_vtx = vertices.size();

            // Load indices
            {
                fastgltf::Accessor& index_accessor = gltf.accessors[p.indicesAccessor.value()];
                indices.reserve(indices.size() + index_accessor.count);

                fastgltf::iterateAccessor<uint32_t>(gltf, index_accessor, [&](const uint32_t idx)
                {
                    indices.emplace_back(idx + initial_vtx);
                });
            }

            // Load vertex positions
            {
                fastgltf::Accessor& pos_accessor = gltf.accessors[p.findAttribute("POSITION")->second];
                vertices.resize(vertices.size() + pos_accessor.count);

                fastgltf::iterateAccessorWithIndex<glm::vec3>(gltf, pos_accessor,
                    [&](const glm::vec3 v, const size_t index)
                    {
                        vertices[initial_vtx + index] = vertex
                        {
                            .position = v,
                            .uv_x = 0,
                            .normal = glm::vec3(1.f, 0.f, 0.f),
                            .uv_y = 0,
                            .color = glm::vec4{ 1.f },
                        };
                    });
            }

            // Load vertex normals
            auto normals = p.findAttribute("NORMAL");
            if (normals != p.attributes.end())
            {
                fastgltf::iterateAccessorWithIndex<glm::vec3>(gltf, gltf.accessors[(*normals).second],
                    [&](const glm::vec3 v, const size_t index)
                    {
                        vertices[initial_vtx + index].normal = v;
                    });
            }

            // Load UVs
            auto uv = p.findAttribute("TEXCOORD_0");
            if (uv != p.attributes.end())
            {
                fastgltf::iterateAccessorWithIndex<glm::vec2>(gltf, gltf.accessors[(*uv).second],
                    [&](const glm::vec2 v, const size_t index)
                    {
                        vertices[initial_vtx + index].uv_x = v.x;
                        vertices[initial_vtx + index].uv_y = v.y;
                    });
            }

            // Load vertex colors
            auto colors = p.findAttribute("COLOR_0");
            if (colors != p.attributes.end())
            {
                fastgltf::iterateAccessorWithIndex<glm::vec4>(gltf, gltf.accessors[(*colors).second],
                    [&](const glm::vec4 v, const size_t index)
                    {
                        vertices[initial_vtx + index].color = v;
                    });
            }

            new_mesh->surfaces.emplace_back(surface);
        }

        constexpr bool override_colors = true;
        if (override_colors)
        {
            for (vertex& vtx : vertices)
            {
                vtx.color = glm::vec4(vtx.normal, 1.f);
            }
        }

        new_mesh->mesh_buffers = _immediate->upload_mesh(indices, vertices);
        meshes.emplace_back(std::move(new_mesh));
    }

    _log->info("Mesh loaded: {}", target_file.string());
    return meshes;
}
