#include <cstdint>
#include <utility>
#include <vulkan/vulkan.h>
#include "kiln/logging.hpp"
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_utilities.h"
#include "vk_init.hpp"
//-----------------------------------
#include "kiln/vulkan/vk_immediate.h"

using namespace kiln;
using namespace vulkan;

namespace
{
    constexpr uint64_t fence_wait_time = static_cast<uint64_t>(vk_timeout_seconds) * 999LLU;
}

vk_immediate::vk_immediate(vk_device* device, vk_memory_allocator* allocator) :
    _log(logging::get_logger(log_name::vulkan)),
    _device(device), _allocator(allocator),
    _cmd_pool(_device->create_command_pool(nullptr)),
    _cmd_buffer(_device->create_command_buffer(_cmd_pool)),
    _fence(_device->create_fence())
{
}

vk_immediate::~vk_immediate()
{
    if (!_uploaded_meshes.empty())
    {
        _uploaded_meshes.clear();
    }
}

void vk_immediate::buffer_copy(VkBuffer src, VkDeviceSize src_size, VkBuffer dst) const
{
    submit([&](VkCommandBuffer cmd) -> bool
    {
        const auto copy_region = VkBufferCopy
        {
            .srcOffset = 0,
            .dstOffset = 0,
            .size = src_size,
        };

        vkCmdCopyBuffer(cmd, src, dst, 1, &copy_region);
        return true;
    });
}

void vk_immediate::buffer_copy(const VkBuffer src, const VkBuffer dst, const VkBufferCopy& copy) const
{
    submit([&](VkCommandBuffer cmd) -> bool
    {
        vkCmdCopyBuffer(cmd, src, dst, 1, &copy);
        return true;
    });
}

void vk_immediate::submit(const immediate_submit_func& submit_func) const
{
    vk_result_check(vkResetFences(*_device, 1, &_fence),
        "Failed to reset fences for immediate submit.");

    vk_result_check(vkResetCommandBuffer(_cmd_buffer, 0),
        "Failed to reset command buffer for immediate submit.");

    const auto buffer_begin_info = vk_init::command::buffer_begin_info(
        VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT, nullptr);

    vk_result_check(vkBeginCommandBuffer(_cmd_buffer, &buffer_begin_info),
        "Failed to begin command buffer for immediate submit.");

    if (!submit_func(_cmd_buffer)) { return; }

    vk_result_check(vkEndCommandBuffer(_cmd_buffer),
        "Failed to end command buffer for immediate submit.");

    // Submit the command buffer to the queue and execute it.
    // The render fence will now block until the graphics commands finish execution.

    const auto buffer_submit_info = vk_init::command::buffer_submit_info(_cmd_buffer);
    const auto submit_info = vk_init::submit_info2(&buffer_submit_info, nullptr, nullptr);

    vk_result_check(vkQueueSubmit2(_device->get_graphics_queue(), 1, &submit_info, _fence),
        "Queue submit for immediate submit failed.");

    vk_result_check(vkWaitForFences(*_device, 1, &_fence, true, fence_wait_time),
        "Failed to wait for fences for immediate submit.");
}

gpu_mesh_buffers* vk_immediate::upload_mesh(std::span<uint32_t> indices, std::span<vertex> vertices)
{
    const auto index_buffer_size  = indices.size() * sizeof(uint32_t);
    const auto vertex_buffer_size = vertices.size() * sizeof(vertex);

    auto vertex_buffer = _allocator->create_buffer(vertex_buffer_size,
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
        VMA_MEMORY_USAGE_GPU_ONLY);

    vertex_buffer->set_name("mesh_vertex_buffer");

    auto index_buffer = _allocator->create_buffer(index_buffer_size,
        VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        VMA_MEMORY_USAGE_GPU_ONLY);

    index_buffer->set_name("mesh_index_buffer");

    auto staging_buffer = _allocator->create_buffer(index_buffer_size + vertex_buffer_size,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VMA_MEMORY_USAGE_CPU_ONLY);

    void* data = staging_buffer->allocation_info().pMappedData;
    memcpy(data, vertices.data(), vertex_buffer_size);
    memcpy(static_cast<char*>(data) + vertex_buffer_size, indices.data(), index_buffer_size);

    const auto vertex_copy = VkBufferCopy
    {
        .srcOffset = 0,
        .dstOffset = 0,
        .size = vertex_buffer_size,
    };

    const auto index_copy = VkBufferCopy
    {
        .srcOffset = vertex_buffer_size,
        .dstOffset = 0,
        .size = index_buffer_size,
    };

    buffer_copy(staging_buffer->buffer(), vertex_buffer->buffer(), vertex_copy);
    buffer_copy(staging_buffer->buffer(), index_buffer->buffer(), index_copy);

    staging_buffer.reset();
    staging_buffer = nullptr;

    _uploaded_meshes.emplace_back(std::make_unique<gpu_mesh_buffers>(std::move(index_buffer), std::move(vertex_buffer)));
    const auto& added = _uploaded_meshes.back();

    return added.get();
}