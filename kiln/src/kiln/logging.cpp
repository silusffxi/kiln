#include <filesystem>
#include <memory>
#include <string>
#include <unordered_set>
#include <vector>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/ansicolor_sink.h>
#include <spdlog/sinks/daily_file_sink.h>
#include <spdlog/sinks/null_sink.h>
//
#include "kiln/logging.hpp"

using namespace kiln;

namespace
{
    constexpr char log_format_console[] = "[\x1b[1;30m%Y-%m-%d %H:%M:%S.%e\x1b[0m]<\x1b[1;4;37m%n\x1b[0m>(%^%l%$) %v";
    constexpr char log_format_file[] = "[%Y-%m-%d %H:%M:%S.%e]<%n>(%^%l%$) %v";

    /**
    *\brief All the available log names.
    */
    std::vector log_names =
    {
        std::string(log_name::kiln),
        std::string(log_name::vulkan),
    };

    /**
    *\brief Contains a collection of loggers that have been created.
    */
    std::unordered_set<std::string> created_loggers;
}

log_mode    logging::_configured_mode = log_mode::none;
std::string logging::_default_log_name;

std::filesystem::path logging::_log_directory = L"";

void logging::configure(const logging_config& config)
{
    _default_log_name = config.default_log_name;
    switch (config.mode)
    {
    case log_mode::console:
        configure_for_console();
        break;
    case log_mode::file:
        configure_for_file(config.root_directory);
        break;
    case log_mode::none:
        configure_for_none();
        break;
    }
}

void logging::configure_for_console()
{
    _configured_mode = log_mode::console;
    _default_log_name = std::string(log_name::kiln);

    for (const auto& name : log_names)
    {
        create_console_logger(name);
    }
}

void logging::configure_for_file(const std::filesystem::path& root_directory)
{
    _configured_mode = log_mode::file;

    _log_directory = root_directory / L"logs" / L"vk2d.log";
    for (const auto& name : log_names)
    {
        create_file_logger(name, _log_directory.string());
    }
}

void logging::configure_for_none()
{
    _configured_mode = log_mode::none;
    for (const auto& name : log_names)
    {
        const auto log = spdlog::create<spdlog::sinks::null_sink_st>(name);
        log->set_level(spdlog::level::trace);
    }
}

std::shared_ptr<spdlog::logger> logging::get_default_logger()
{
    return get_logger(_default_log_name);
}

std::shared_ptr<spdlog::logger> logging::get_logger(const std::string& name)
{
    switch (_configured_mode)
    {
    case log_mode::console:
        return get_console_logger(name);
    default:
        return get_file_logger(_default_log_name);
    }
}

void logging::create_console_logger(const std::string& name)
{
    const auto log = spdlog::create<spdlog::sinks::ansicolor_stdout_sink_mt>(name);
    log->set_level(spdlog::level::trace);
    log->set_pattern(log_format_console);

    created_loggers.emplace(name);
}

void logging::create_file_logger(const std::string& name, const std::string& log_dir)
{
    const auto log = spdlog::create<spdlog::sinks::daily_file_format_sink_mt>(
        name,
        log_dir,
        0, 0,  // when the log should rotate
        false, // truncate
        7      // max number of (days) files
    );

    log->set_level(spdlog::level::trace);
    log->set_pattern(log_format_file);

    created_loggers.emplace(name);
}

std::shared_ptr<spdlog::logger> logging::get_console_logger(const std::string& name)
{
    if (!created_loggers.contains(name))
        create_console_logger(name);

    return spdlog::get(name);
}

std::shared_ptr<spdlog::logger> logging::get_file_logger(const std::string& name)
{
    if (!created_loggers.contains(name))
        create_file_logger(name, _log_directory.string());

    return spdlog::get(name);
}
