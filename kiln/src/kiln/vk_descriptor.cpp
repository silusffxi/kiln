#include <utility>
#include <vulkan/vulkan.h>
#include "kiln/utilities.hpp"
#include "kiln/vulkan/vk_device.h"
//------------------------------------
#include "kiln/vulkan/vk_descriptor.h"

using namespace kiln;
using namespace vulkan;

vk_descriptor::vk_descriptor(vk_device* device, VkDescriptorSetLayout layout, uint32_t max_sets,
    const std::span<descriptor_allocator::pool_size_ratio>& pool_ratios) :
    vk_descriptor(device, layout, std::make_unique<descriptor_allocator>(device, max_sets, pool_ratios))
{
}

vk_descriptor::vk_descriptor(vk_device* device, VkDescriptorSetLayout layout, std::unique_ptr<descriptor_allocator> allocator) :
    _device(device), _layout(layout), _desc_allocator(std::move(allocator))
{
}

vk_descriptor::~vk_descriptor()
{
    if (_layout != nullptr)
    {
        vkDestroyDescriptorSetLayout(*_device, _layout, nullptr);
        _layout = nullptr;
    }

    free_unique_ptr(_desc_allocator);
}

std::tuple<VkDescriptorSet, uint32_t> vk_descriptor::allocate_set()
{
    const auto set_index = _sets.size();
    _sets.emplace_back(_desc_allocator->allocate(_layout));
    return { _sets.back(), set_index };
}

std::tuple<VkDescriptorSet, uint32_t> vk_descriptor::allocate_set(const std::function<void(VkDescriptorSet)>& update)
{
    const auto set_index = _sets.size();
    _sets.emplace_back(_desc_allocator->allocate(_layout));
    const auto set = _sets.back();
    update(set);

    return { _sets.back(), set_index };
}

VkDescriptorSetLayout vk_descriptor::layout() const { return _layout; }

VkDescriptorSet vk_descriptor::set(const uint32_t index) const
{
    if (index >= _sets.size())
    {
        return nullptr;
    }

    if (index == 0 && _sets.empty())
    {
        return nullptr;
    }

    return _sets[index];
}
