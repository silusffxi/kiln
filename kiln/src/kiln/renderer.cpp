#include <memory>
#include <glm/glm.hpp>
#include <SDL2/SDL.h>
#include "kiln/logging.hpp"
#include "kiln/structs.hpp"
#include "kiln/types.h"
//--------------------------
#include "kiln/renderer.hpp"

using namespace kiln;

renderer::renderer(SDL_Window* window, graphics_api selected_api) :
    _log(logging::get_logger(log_name::kiln)),
    _window(window), _selected_api(selected_api),
    _cam(std::make_unique<camera>())
{
    update_window_size();
    _keyboard = SDL_GetKeyboardState(&_keyboard_size);
}

graphics_api renderer::api() const { return _selected_api; }

glm::vec4& renderer::clear_color()
{
    return _clear_color;
}

camera* renderer::default_camera() const
{
    return _cam.get();
}

void renderer::frame_begin()
{
    update_window_size();

    if (_paused) return;

    on_frame_begin();
}

void renderer::frame_end()
{
    if (_paused) return;

    on_frame_end();
}

void renderer::handle_event(const SDL_Event* e)
{
    on_event(e);
}

bool renderer::is_paused() const
{
    return _paused;
}

void renderer::pause()
{
    _log->debug("Rendering paused.");
    on_pause();
}

void renderer::resume()
{
    _log->debug("Rendering resumed.");
    on_resume();
}

void renderer::resize()
{
    update_window_size();
    on_resize(_window, &_window_size);
}

void renderer::on_event(const SDL_Event* e)
{
}

void renderer::on_frame_begin()
{
}

void renderer::on_frame_end()
{
}

void renderer::on_pause()
{
}

void renderer::on_resume()
{
}

void renderer::on_resize(SDL_Window* window, const window_size_data* size)
{
}

const window_size_data& renderer::window_size() const
{
    return _window_size;
}

void renderer::update_window_size()
{
    int w, h;
    SDL_GetWindowSize(_window, &w, &h);
    _window_size.update(w, h);
}
