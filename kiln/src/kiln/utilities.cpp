#include <filesystem>
#include <string>
#include "kiln/platform.hpp"
#include "kiln/types.h"
//---------------------------
#include "kiln/utilities.hpp"

using namespace kiln;

shader_type kiln::file_ext_to_shader_type(const std::string& file_ext)
{
    if (file_ext == ".comp") return shader_type::compute;
    if (file_ext == ".frag") return shader_type::fragment;
    if (file_ext == ".geom") return shader_type::geometry;
    if (file_ext == ".tesc") return shader_type::tess_control;
    if (file_ext == ".tese") return shader_type::tess_eval;
    if (file_ext == ".vert") return shader_type::vertex;
    if (file_ext == ".hlsl") return shader_type::hlsl;

    return shader_type::unknown;
}

shader_type kiln::file_name_to_shader_type(const std::string& file_name)
{
    if (file_name.ends_with(".comp") || file_name.ends_with(".comp.spv")) return shader_type::compute;
    if (file_name.ends_with(".frag") || file_name.ends_with(".frag.spv")) return shader_type::fragment;
    if (file_name.ends_with(".geom") || file_name.ends_with(".geom.spv")) return shader_type::geometry;
    if (file_name.ends_with(".tesc") || file_name.ends_with(".tesc.spv")) return shader_type::tess_control;
    if (file_name.ends_with(".tese") || file_name.ends_with(".tese.spv")) return shader_type::tess_eval;
    if (file_name.ends_with(".vert") || file_name.ends_with(".vert.spv")) return shader_type::vertex;
    if (file_name.ends_with(".hlsl")) return shader_type::hlsl;

    return shader_type::unknown;
}

std::filesystem::path kiln::get_current_binary_path()
{
#if defined(OS_WINDOWS)
    TCHAR raw_path[MAX_PATH];
    GetModuleFileName(nullptr, raw_path, MAX_PATH);
    return { raw_path };
#else
    //TODO: Verify this works on other platforms other than Linux.
    char raw_path[PATH_MAX];
    readlink("/proc/self/exe", raw_path, PATH_MAX);
    return { raw_path };
#endif
}

std::filesystem::path kiln::get_root_directory()
{
    return get_current_binary_path().parent_path();
}
