#include <memory>
#include <string>
#include <SDL2/SDL.h>
#include <vulkan/vulkan.h>
#include "kiln/logging.hpp"
#include "kiln/shaders.hpp"
#include "kiln/utilities.hpp"
#include "kiln/vulkan/vk_config.h"
#include "kiln/vulkan/vk_debug_reporter.h"
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_instance.h"
#include "kiln/vulkan/vk_physical_device.h"
#include "kiln/vulkan/vk_memory_allocator.h"
#include "kiln/vulkan/vk_renderer.h"
#include "kiln/vulkan/vk_surface.h"
#include "kiln/vulkan/vk_swapchain.h"
#include "kiln/vulkan/vk_assets_manager.h"
//--------------------
#include "kiln/kiln.h"

using namespace kiln;
using namespace vulkan;

namespace
{
    std::unique_ptr<vk_debug_reporter>  g_debug_reporter;
    std::unique_ptr<vk_instance>        g_instance;
    std::unique_ptr<vk_physical_device> g_gpu;
    std::unique_ptr<vk_device>          g_device;
    std::unique_ptr<vk_renderer>        g_renderer;
    std::unique_ptr<vk_surface>         g_surface;
    std::unique_ptr<vk_swapchain>       g_swapchain;

    std::unique_ptr<vk_memory_allocator> g_memory_allocator;

    std::unique_ptr<vk_immediate>      g_immediate;
    std::unique_ptr<vk_assets_manager> g_assets_manager;
}

void kiln::initialize_for_vulkan(SDL_Window* window, const vk_config* config, vk_instance_config* instance_config)
{
    const auto log = logging::get_logger(log_name::vulkan);

    if (g_instance == nullptr)
    {
        g_instance = std::make_unique<vk_instance>(window, instance_config);
        collect_versions();
        check_version_mismatch();
    }

    if (g_debug_reporter == nullptr)
    {
        g_debug_reporter = std::make_unique<vk_debug_reporter>(g_instance);
    }

    if (g_gpu == nullptr)
    {
        g_gpu = std::make_unique<vk_physical_device>(g_instance);

        const auto gpu_name = std::string(g_gpu->properties()->deviceName);
        const auto& driver_version = g_gpu->driver_version();
        const auto& vk_api_version = g_gpu->api_version();

        log->info("Vulkan API Version (reported by GPU driver): {}.{}.{}",
            vk_api_version.major, vk_api_version.minor, vk_api_version.patch);

        log->info("Selected GPU: {}", gpu_name);
        log->info("GPU driver version: {}.{}.{} (may be inaccurate)",
            driver_version.major, driver_version.minor, driver_version.patch);
    }

    if (g_device == nullptr)
    {
        g_device = std::make_unique<vk_device>(g_gpu, instance_config->debug_enabled());
    }

    if (g_memory_allocator == nullptr)
    {
        g_memory_allocator = std::make_unique<vk_memory_allocator>(g_instance, g_gpu, g_device);
    }

    if (g_surface == nullptr)
    {
        g_surface = std::make_unique<vk_surface>(window, g_instance, g_gpu);
    }

    if (g_swapchain == nullptr)
    {
        g_swapchain = std::make_unique<vk_swapchain>(config, g_gpu, g_device, g_surface, g_memory_allocator->allocator());
    }

    if (g_immediate == nullptr)
    {
        g_immediate = std::make_unique<vk_immediate>(g_device.get(), g_memory_allocator.get());
    }

    if (g_assets_manager == nullptr)
    {
        g_assets_manager = std::make_unique<vk_assets_manager>(g_device, g_immediate, config->assets_root_directory);
    }

    // Renderer should be the last thing created as it will depend on everything created before it.

    if (g_renderer == nullptr)
    {
        g_renderer = std::make_unique<vk_renderer>(vk_renderer_args
        {
            .window    = window,
            .instance  = g_instance.get(),
            .gpu       = g_gpu.get(),
            .device    = g_device.get(),
            .allocator = g_memory_allocator.get(),
            .surface   = g_surface.get(),
            .swapchain = g_swapchain.get(),

            .immediate = g_immediate.get(),
            .assets    = g_assets_manager.get(),
        });
    }
}

void kiln::shutdown()
{
    g_device->wait_queue_idle();
    g_device->wait_idle();

    free_unique_ptr(g_renderer);
    free_unique_ptr(g_assets_manager);
    free_unique_ptr(g_immediate);
    free_unique_ptr(g_swapchain);
    free_unique_ptr(g_surface);
    free_unique_ptr(g_memory_allocator);
    free_unique_ptr(g_device);
    free_unique_ptr(g_gpu);
    free_unique_ptr(g_debug_reporter);
    free_unique_ptr(g_instance);
}

vk_assets_manager* vulkan::assets()
{
    return g_assets_manager.get();
}

vk_device* vulkan::get_device()
{
    return g_device.get();
}

vk_physical_device* vulkan::get_gpu()
{
    return g_gpu.get();
}

vk_instance* vulkan::get_instance()
{
    return g_instance.get();
}

vk_memory_allocator* vulkan::get_memory_allocator()
{
    return g_memory_allocator.get();
}

vk_renderer* vulkan::get_renderer()
{
    return g_renderer.get();
}

vk_surface* vulkan::get_surface()
{
    return g_surface.get();
}

vk_swapchain* vulkan::get_swapchain()
{
    return g_swapchain.get();
}

vk_immediate* vulkan::immediate()
{
    return g_immediate.get();
}
