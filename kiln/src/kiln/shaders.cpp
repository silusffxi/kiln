#include <cctype>
#include <filesystem>
#include <fstream>
#include <ranges>
#include <string>
#include "kiln/logging.hpp"
#include "kiln/utilities.hpp"
#include "kiln/types.h"
//-------------------------
#include "kiln/shaders.hpp"

namespace fs = std::filesystem;
using namespace kiln;

bool shaders::_initialized = false;

kiln_logger shaders::_log = nullptr;

fs::path shaders::_shaders_root_dir = L"";

std::unordered_map<std::string, shaders::binary> shaders::_shader_binaries;

void shaders::initialize(const std::filesystem::path& shaders_dir_path)
{
    if (_initialized) return;

    _log = logging::get_logger(log_name::kiln);

    if (!fs::exists(shaders_dir_path))
    {
        _log->warn(
            "Shaders directory at \"{}\" does not exist. Shaders directory will not be set.",
            shaders_dir_path.string());
        return;
    }

    _shaders_root_dir = shaders_dir_path;
    _initialized = true;
}

shaders::binary* shaders::get_shader(const std::string& name)
{
    if (!_shader_binaries.contains(name))
        return nullptr;

    return &_shader_binaries.at(name);
}

void shaders::load_all()
{
    auto shader_files = std::unordered_map<std::filesystem::path, file_entry>();

    // Recursively locate all the shaders that we should load.
    for (auto& entry : fs::directory_iterator(_shaders_root_dir))
    {
        if (entry.is_directory())
        {
            collect_shader_files_in_directory(L"", entry.path(), shader_files);
        }
    }

    _log->info("Located {} shader files.", shader_files.size());

    for (auto& entry : shader_files | std::views::values)
    {
        // Filter out file types we can't (or don't want to) handle:
        if (entry.file_type == shader_file_type::glsl_source)
        {
            _log->info("Skipping GLSL source file: \"{}\"", entry.path.string());
            continue;
        }

        if (entry.file_type == shader_file_type::hlsl_source)
        {
            _log->info("Skipping HLSL source file: \"{}\"", entry.path.string());
            continue;
        }

        if (entry.file_type == shader_file_type::hlsl_binary)
        {
            _log->info("Skipping HLSL binary file: \"{}\"", entry.path.string());
            continue;
        }

        // Load the shader binary:
        if (entry.file_type == shader_file_type::glsl_binary)
        {
            _log->info("Shader {} -- Loading GLSL binary: \"{}\"...", entry.name, entry.path.string());

            _shader_binaries.try_emplace(entry.name, binary
            {
                .name = entry.name,
                .path = entry.path,
                .type = entry.type,
                .spirv = read_spv_file(entry.path, true),
            });
        }

        if (entry.file_type == shader_file_type::u32_binary)
        {
            _log->info("Shader {} -- Loading GLSL binary (text) \"{}\"...", entry.name, entry.path.string());

            _shader_binaries.try_emplace(entry.name, binary
            {
                .name = entry.name,
                .path = entry.path,
                .type = entry.type,
                .spirv = read_u32_file(entry.path, true),
            });
        }
    }
}

std::vector<uint32_t> shaders::read_spv_file(const std::filesystem::path& path, bool is_absolute_path)
{
    const auto target_path = is_absolute_path
        ? path
        : _shaders_root_dir / path;

    auto spv_file = std::ifstream(target_path, std::ios::ate | std::ios::binary);
    if (!spv_file.is_open())
        return { };

    const auto file_size = spv_file.tellg();
    const auto spv_binary = std::vector<uint32_t>(file_size / sizeof(uint32_t));

    // Move the read to the beginning of the file.
    spv_file.seekg(0);

    // Read the file content into the vector.
    spv_file.read((char*)spv_binary.data(), file_size);

    // Close the file handle now that it's been read into memory.
    spv_file.close();

    return spv_binary;
}

std::vector<uint32_t> shaders::read_u32_file(const std::filesystem::path& path, bool is_absolute_path)
{
    const auto target_path = is_absolute_path
        ? path
        : _shaders_root_dir / path;

    if (!fs::exists(target_path))
        return { };

    std::fstream file(target_path);

    std::string content;
    content.assign(
        std::istreambuf_iterator<char>(file),
        std::istreambuf_iterator<char>());

    std::string current_word;
    current_word.reserve(10);

    auto in_comment = false;
    std::vector<uint32_t> binary;
    for (const auto& c : content)
    {
        if (c == '\n')
        {
            in_comment = false;
            continue;
        }

        if (in_comment) continue;
        if (c == '\t') continue;

        if (c == '/')
        {
            in_comment = true;
            continue;
        }

        if (isdigit(c) || c == 'x')
        {
            current_word += c;
            continue;
        }

        if (c == ',')
        {
            binary.emplace_back(std::stoul(current_word, nullptr, 16));
            current_word.clear();
        }
    }

    return binary;
}

void shaders::collect_shader_files_in_directory(const std::filesystem::path& parent_path, const std::filesystem::path& dir_path,
    std::unordered_map<std::filesystem::path, file_entry>& shader_files)
{
    const auto dir_name = (parent_path / dir_path.stem()).string();
    _log->debug("Loading shaders in directory: {}", dir_name);

    for (auto& entry : fs::directory_iterator(dir_path))
    {
        if (entry.is_directory())
        {
            collect_shader_files_in_directory(dir_path, entry.path(), shader_files);
            continue;
        }

        // Skip anything that isn't "regular" file.
        if (!entry.is_regular_file())
            continue;

        const auto& path = entry.path();
        const auto file_name = path.filename();
        const auto file_ext = file_name.extension();

        const auto shader_type = file_name_to_shader_type(file_name.string());

        shader_file_type file_type;
        if      (file_ext == ".spv") file_type = shader_file_type::glsl_binary;
        else if (file_ext == ".u32") file_type = shader_file_type::u32_binary;
        else
        {
            if (shader_type == shader_type::unknown)
                continue;

            if (shader_type == shader_type::hlsl)
                file_type = shader_file_type::hlsl_source;
            else
                file_type = shader_file_type::glsl_source;
        }

        shader_files.try_emplace(entry.path(), file_entry
        {
            .name = std::format("{}/{}", dir_name, entry.path().filename().string()),
            .path = entry.path(),
            .type = shader_type,
            .file_type = file_type,
        });
    }
}
