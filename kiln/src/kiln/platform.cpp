#include "kiln/platform.hpp"

namespace
{
    enum class vt_mode
    {
        invalid,
        disable,
        enable,
    };

    bool vt_already_enabled = false;

    bool set_virtual_terminal_mode(vt_mode mode)
    {
#ifdef OS_WINDOWS
        const auto std_out = GetStdHandle(STD_OUTPUT_HANDLE);
        if (std_out == INVALID_HANDLE_VALUE)
            return false;

        DWORD output_mode = 0;
        if (!GetConsoleMode(std_out, &output_mode))
            return false;

        // If we were asked to enable virtual terminal processing, then
        // we want to check if it's already enabled. If it is already
        // enabled then we don't want to do anything.
        //
        // We're also taking this approach so that if virtual terminal
        // processing was enabled originally it won't be disabled when
        // we exit.
        if (mode == vt_mode::enable)
        {
            vt_already_enabled = (output_mode & ENABLE_VIRTUAL_TERMINAL_PROCESSING) != 0;
            if (vt_already_enabled)
                return true;
        }

        switch (mode)
        {
        case vt_mode::enable:
            if ((output_mode & ENABLE_VIRTUAL_TERMINAL_PROCESSING) == 0)
                SetConsoleMode(std_out, output_mode | ENABLE_VIRTUAL_TERMINAL_PROCESSING);
            return true;
        case vt_mode::disable:
            // Since virtual terminal processing was enabled when it was requested we don't want
            // to disable it here. We'll just leave it enabled since that's how we originally
            // saw it.
            if (vt_already_enabled) return true;

            if ((output_mode & ENABLE_VIRTUAL_TERMINAL_PROCESSING) != 0)
                SetConsoleMode(std_out, output_mode & ~ENABLE_VIRTUAL_TERMINAL_PROCESSING);
            return true;
        default:
            return false;
        }
#else
        (void)mode; // Mode won't actually be used when we're not targeting Windows.
        return true;
#endif
    }
}

const char* kiln::os_platform_str(os_platform value)
{
    switch (value)
    {
    case os_platform::windows:
        return "windows";
    case os_platform::macos:
        return "macos";
    case os_platform::android:
        return "android";
    case os_platform::linux_generic:
        return "linux_generic";
    default:
        break;
    }

    return "unknown";
}

bool kiln::virtual_terminal::enable()
{
#ifdef OS_WINDOWS
    return set_virtual_terminal_mode(vt_mode::enable);
#else
    return true;
#endif
}

bool kiln::virtual_terminal::disable()
{
#ifdef OS_WINDOWS
    return set_virtual_terminal_mode(vt_mode::disable);
#else
    return false;
#endif
}

