#pragma once
#include <filesystem>
#include <memory>
#include <string>
#include <spdlog/logger.h>

namespace kiln
{
    typedef std::shared_ptr<spdlog::logger> kiln_logger;

    enum class log_mode
    {
        none,
        console,
        file,
    };

    namespace log_name
    {
        constexpr char kiln[] = "kiln";
        constexpr char vulkan[] = "vulkan";
    }

    struct logging_config
    {
        std::string default_log_name = std::string(log_name::kiln);

        log_mode mode = log_mode::console;

        std::filesystem::path root_directory;
    };

    class logging
    {
        static log_mode    _configured_mode;
        static std::string _default_log_name;

        static std::filesystem::path _log_directory;

    public:
        static void configure(const logging_config& config);
        static void configure_for_console();
        static void configure_for_file(const std::filesystem::path& root_directory);
        static void configure_for_none();

        static std::shared_ptr<spdlog::logger> get_default_logger();
        static std::shared_ptr<spdlog::logger> get_logger(const std::string& name);

    private:
        static void create_console_logger(const std::string& name);
        static void create_file_logger(const std::string& name, const std::string& log_dir);

        static std::shared_ptr<spdlog::logger> get_console_logger(const std::string& name);
        static std::shared_ptr<spdlog::logger> get_file_logger(const std::string& name);
    };
}