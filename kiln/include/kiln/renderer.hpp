#pragma once
#include <cstdint>
#include <memory>
#include <glm/glm.hpp>
#include <SDL2/SDL.h>
#include "kiln/camera.h"
#include "kiln/logging.hpp"
#include "kiln/structs.hpp"
#include "kiln/types.h"

namespace kiln
{
    class renderer
    {
        kiln_logger _log;

        SDL_Window*      _window;
        window_size_data _window_size = { };

        /**
        *\brief The graphics API in use by the renderer.
        */
        graphics_api _selected_api = graphics_api::unknown;

        std::unique_ptr<camera> _cam;

        int _keyboard_size = 0;
        const uint8_t* _keyboard = nullptr;

        glm::vec4 _clear_color = { 0.f, 0.f, 0.f, 1.f };

        bool _paused = false;

    public:
        renderer(SDL_Window* window, graphics_api selected_api);
        virtual ~renderer() = default;
        renderer(renderer&&) = default;

        renderer& operator=(renderer&&) = default;

        /**
        *\brief Returns the graphics API in use.
        */
        [[nodiscard]] graphics_api api() const;

        glm::vec4& clear_color();

        [[nodiscard]] camera* default_camera() const;

        void frame_begin();
        void frame_end();

        void handle_event(const SDL_Event* e);

        [[nodiscard]] bool is_paused() const;

        void pause();
        void resume();

        void resize();

    protected:
        virtual void on_event(const SDL_Event* e);

        virtual void on_frame_begin();
        virtual void on_frame_end();

        virtual void on_pause();
        virtual void on_resume();

        virtual void on_resize(SDL_Window* window, const window_size_data* size);

        [[nodiscard]] const window_size_data& window_size() const;

    private:
        void update_window_size();
    };
}