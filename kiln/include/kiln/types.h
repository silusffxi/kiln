///\file types.h
///\brief Contains graphics API agnostic types.
#pragma once

/**
*\brief Specifies the graphics API in use.
*\note Most of the APIs listed are \b NOT supported.
*/
typedef enum
{
    kiln_graphics_api_unknown = 0, ///< Graphics API is not known.
    kiln_graphics_api_dx9,         ///< DirectX 9
    kiln_graphics_api_dx10,        ///< DirectX 10
    kiln_graphics_api_dx11,        ///< DirectX 11
    kiln_graphics_api_dx12,        ///< DirectX 12
    kiln_graphics_api_opengl,      ///< OpenGL (latest - likely will be 4.6)
    kiln_graphics_api_vulkan,      ///< Vulkan
} kiln_graphics_api;

/**
*\brief Available screen modes for a Kiln renderer.
*/
typedef enum
{
    kiln_screen_mode_unknown,         ///< Invalid screen mode. The screen mode shouldn't ever be this value.
    kiln_screen_mode_immediate,       ///< No limits on the rate of frame rendering.
    kiln_screen_mode_vsync,           ///< This equates to "VK_PRESENT_MODE_FIFO_KHR" for Vulkan.
    kiln_screen_mode_vsync_relaxed,   ///< Similar to V-Sync.
    kiln_screen_mode_triple_buffered,
} kiln_screen_mode;

typedef enum
{
    kiln_shader_type_unknown = 0,  ///< Shader type is not known.
    kiln_shader_type_compute,      ///< Compute shader
    kiln_shader_type_fragment,     ///< Fragment shader
    kiln_shader_type_geometry,     ///< Geometry shader
    kiln_shader_type_tess_control, ///< Tesselation control shader
    kiln_shader_type_tess_eval,    ///< Tesselation evaluation shader
    kiln_shader_type_vertex,       ///< Vertex shader

    kiln_shader_type_hlsl,         ///< HLSL shader. (HLSL shaders aren't likely to be supported, but we'll still want to identify them.)
    kiln_shader_type_max,          ///< Maximum number of shader types.
} kiln_shader_type;

#ifdef __cplusplus
namespace kiln
{
    enum class graphics_api
    {
        unknown = kiln_graphics_api_unknown,
        dx9     = kiln_graphics_api_dx9,
        dx10    = kiln_graphics_api_dx10,
        dx11    = kiln_graphics_api_dx11,
        dx12    = kiln_graphics_api_dx12,
        opengl  = kiln_graphics_api_opengl,
        vulkan  = kiln_graphics_api_vulkan,
    };

    enum class screen_mode
    {
        unknown         = kiln_screen_mode_unknown,       ///< Invalid screen mode. The screen mode shouldn't ever be this value.
        immediate       = kiln_screen_mode_immediate,     ///< No limits on the rate of frame rendering.
        vsync           = kiln_screen_mode_vsync,         ///< This equates to "VK_PRESENT_MODE_FIFO_KHR" for Vulkan.
        vsync_relaxed   = kiln_screen_mode_vsync_relaxed, ///< Similar to V-Sync.
        triple_buffered = kiln_screen_mode_triple_buffered,
    };

    enum class shader_type
    {
        unknown      = kiln_shader_type_unknown,
        compute      = kiln_shader_type_compute,
        fragment     = kiln_shader_type_fragment,
        geometry     = kiln_shader_type_geometry,
        tess_control = kiln_shader_type_tess_control,
        tess_eval    = kiln_shader_type_tess_eval,
        vertex       = kiln_shader_type_vertex,
        hlsl         = kiln_shader_type_hlsl,
    };

    enum class shader_file_type
    {
        unknown,     ///< Unknown shader file type.
        hlsl_source, ///< A HLSL source file.
        glsl_source, ///< A GLSL source file.
        hlsl_binary, ///< A compiled HLSL binary.
        glsl_binary, ///< A compiled GLSL binary.

        /// This is a compiled GLSL shader binary. But rather than being a binary file,
        /// it is, effectively, a text file that contains a list of 32-bit integers in
        /// hex format that represent the binary.
        u32_binary,
    };
}
#endif