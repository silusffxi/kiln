#pragma once
#include <cstdint>

namespace kiln
{
    /**
    *\brief Uniform way to manage window sizing information.
    */
    struct window_size_data
    {
        uint32_t width;
        uint32_t height;

        uint32_t width_prev;
        uint32_t height_prev;

        float width_f;
        float height_f;

        /**
        *\brief Updates the window sizing information.
        *\returns \c true if the size actually changed, otherwise \c false.
        */
        bool update(int w, int h)
        {
            const auto next_width = static_cast<uint32_t>(w);
            const auto next_height = static_cast<uint32_t>(h);

            const auto changed = width != next_width || height != next_height;
            if (changed)
            {
                width_prev = width;
                height_prev = height;

                width_f = static_cast<float>(w);
                height_f = static_cast<float>(h);
            }

            width = next_width;
            height = next_height;

            return changed;
        }
    };
}
