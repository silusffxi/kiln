///\file platform.hpp
///\brief Provides includes and functionality for the platform where the hosting application is running.
#pragma once
#if defined(_WIN32)
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <Windows.h>
#include <Psapi.h>
#undef WIN32_LEAN_AND_MEAN
#undef NOMINMAX
#else
#include <sysexits.h>
#include <unistd.h>
#include <linux/limits.h>
#endif

// This might be a more reliable way to identify CPU-architecture:
// https://stackoverflow.com/a/66249936
#if (defined(_WIN64) && _WIN64) || (defined(_WIN32) && _WIN32)
#if defined(_WIN64) && _WIN64
#define PLATFORM_X64
#else
#define PLATFORM_X86
#endif
#elif defined(__GNUC__) && __GNUC__
#if __x86_64__ || __ppc64__
#define PLATFORM_X64
#else
#define PLATFORM_X86
#endif
#endif

namespace kiln
{
    /**
    *\brief Possible CPU architectures
    */
    enum class cpu_arch
    {
        unknown, ///< Unknown CPU architecture
        i386,    ///< 32-bit x86
        amd64,   ///< 64-bit x86 (also known as x86-64)
        max,     ///< Number of cpu_arch values that exist.
    };

    /**
    *\brief Possible platforms where the hosting application may be running.
    */
    enum class os_platform
    {
        unknown,       ///< Unknown OS
        windows,       ///< Microsoft Windows
        macos,         ///< Apple macOS
        android,       ///< Google Android
        linux_generic, ///< Linux; no specific distro.
        max,           ///< Number of os_platform values that exist.
    };

    // Some documentation about OS identification via preprocessor macros:
    // https://sourceforge.net/p/predef/wiki/OperatingSystems/
    //
    // Related StackOverflow answer:
    // https://stackoverflow.com/a/8249232
    //
#if defined(_WIN32) || defined(_WIN64) || defined(__CYGWIN__)
#define OS_WINDOWS
    constexpr os_platform current_os = os_platform::windows;
#elif defined(__APPLE__) && defined(__MACH__)
#define OS_MACOS
    constexpr os_platform current_os = os_platform::macos;
#elif defined(__ANDROID__)
#define OS_ANDROID
    constexpr os_platform current_os = os_platform::android;
#elif defined(__linux__) || defined(__linux) || defined(linux) || defined(__gnu_linux__)
#define OS_LINUX
    constexpr os_platform current_os = os_platform::linux_generic;
#else
#define OS_UNKNOWN
    constexpr os_platform current_os = os_platform::unknown;
#endif

#if defined(PLATFORM_X64)
    typedef unsigned long long platform_ptr_t;
    typedef platform_ptr_t* platform_ptr;
    constexpr int pointer_size = sizeof(void*);
#elif defined(PLATFORM_X86)
    typedef unsigned         platform_ptr_t;
    typedef platform_ptr_t* platform_ptr;
    constexpr int pointer_size = sizeof(void*);
#endif

    constexpr int pointer_size_bits = pointer_size * 8;

    /**
    *\brief Returns the value of os_platform as a wide string.
    *\param value The platform value to convert to a string.
    *\return The string representation of the value.
    */
    const char* os_platform_str(os_platform value);

    namespace virtual_terminal
    {
        /**
        *\brief Enables virtual terminal and ANSI escape sequence processing in standard out.
        */
        bool enable();

        /**
        *\brief Disables virtual terminal and ANSI escape sequence processing in standard out.
        */
        bool disable();
    }
}
