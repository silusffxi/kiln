#pragma once
#include <cstdint>
#include <filesystem>
#include <string>
#include <unordered_map>
#include <vector>
#include "kiln/logging.hpp"
#include "kiln/types.h"

namespace kiln
{
    class shaders
    {
        struct binary;
        struct file_entry
        {
            std::string           name;
            std::filesystem::path path;
            shader_type           type;
            shader_file_type      file_type;
        };

        static bool _initialized;

        static kiln_logger _log;

        static std::filesystem::path _shaders_root_dir;

        /**
        *\brief Contains the collection of shader files that were found the last time \c load_all was run.
        *       If \c load_all has not been run then this will be empty.
        *\note This will only be populated/updated when 
        */
        static std::unordered_map<std::string, binary> _shader_binaries;

    public:
        struct binary
        {
            std::string           name;
            std::filesystem::path path;
            shader_type           type;

            std::vector<uint32_t> spirv;
        };

        /**
        *\brief Initializes the shader system.
        *\param shaders_dir_path Sets the root directory where shaders should be loaded from.
        */
        static void initialize(const std::filesystem::path& shaders_dir_path);

        /**
        *\brief Returns a shader with the specified name. If the shader doesn't exist, then this will return \c nullptr.
        */
        static binary* get_shader(const std::string& name);

        /**
        *\brief Loads all the shaders in the assets directory.
        */
        static void load_all();

        /**
        *\brief Read a .spv file from disk and returns the contents as a vector of \c uint32_t.
        *\param path The path to the .spv file to load.
        *\param is_absolute_path Indicates if path provided is relative to the shaders directory or if it should be
        *                        treated as an absolute path.
        */
        static std::vector<uint32_t> read_spv_file(const std::filesystem::path& path, bool is_absolute_path = false);

        /**
        *\brief Read a .u32 file from disk and returns the contents as a vector of \c uint32_t.
        *\param path The path to the .spv file to load.
        *\param is_absolute_path Indicates if path provided is relative to the shaders directory or if it should be
        *                        treated as an absolute path.
        */
        static std::vector<uint32_t> read_u32_file(const std::filesystem::path& path, bool is_absolute_path = false);

    private:
        /**
        *\brief Loads all the shaders from the specified directory.
        */
        static void collect_shader_files_in_directory(const std::filesystem::path& parent_path, const std::filesystem::path& dir_path,
            std::unordered_map<std::filesystem::path, file_entry>& shader_files);
    };
}
