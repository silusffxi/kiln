///\file api.h
///\brief Provides macros for helping define exports
#pragma once
#ifdef __cplusplus
#define KILN_EXTERN extern "C"
#else
#define KILN_EXTERN
#endif

#ifdef _WIN32
#if defined(_USRDLL) && defined(KILN_EXPORTS)
#define VK2D_API __declspec(dllexport)
#elif defined(KILN_IMPORTS)
#define KILN_API __declspec(dllimport)
#else
#define KILN_API
#endif
#endif

#define KILN_STRUCT_PTR(type) typedef struct type##_t *type
