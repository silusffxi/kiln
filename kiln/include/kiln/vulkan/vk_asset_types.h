#pragma once
#ifdef __cplusplus
#include <cstdint>
#else
#include <stdint.h>
#endif
#include "kiln/vulkan/gpu_mesh_buffers.h"
#include "kiln/vulkan/vk_utilities.h"

#ifdef __cplusplus
#include <memory>
#include <string>
#include <vector>
#include "kiln/utilities.hpp"

namespace kiln::vulkan
{
    struct geo_surface
    {
        uint32_t start_index;
        uint32_t count;
    };

    class mesh_asset
    {
    public:
        std::string              name;
        std::vector<geo_surface> surfaces;
        gpu_mesh_buffers*        mesh_buffers = nullptr;

        mesh_asset() = default;
        ~mesh_asset() = default;
        mesh_asset(const mesh_asset&) = default;
        mesh_asset(mesh_asset&&) = default;

        mesh_asset& operator=(const mesh_asset&) = default;
        mesh_asset& operator=(mesh_asset&&) = default;
    };
}
#endif
