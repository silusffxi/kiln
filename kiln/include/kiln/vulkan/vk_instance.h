#pragma once
#include <SDL2/SDL.h>
#include <vulkan/vulkan.h>

#ifdef __cplusplus
#include <string_view>
#include <vector>
#include "kiln/vulkan/vk_instance_config.hpp"
#include "kiln/vulkan/structs.hpp"

namespace kiln::vulkan
{
    class vk_instance
    {
        SDL_Window* _window;

        VkInstance _instance = nullptr;

        std::vector<vk_extension_properties> _instance_extensions;
        std::vector<vk_layer_properties> _instance_layers;

        std::unordered_map<std::string, vk_layer_properties> _layers_in_use;

        /// Contains function pointers to Vulkan debugging functionality.
        struct
        {
            PFN_vkCreateDebugReportCallbackEXT  fn_create_debug_report_callback = nullptr;
            PFN_vkDestroyDebugReportCallbackEXT fn_destroy_debug_report_callback = nullptr;
        } _debug;

    public:
        vk_instance(SDL_Window* window, vk_instance_config* config);
        ~vk_instance();

        [[nodiscard]] const std::vector<vk_layer_properties>& available_layers() const;
        [[nodiscard]] const std::vector<vk_extension_properties>& extensions() const;
        [[nodiscard]] const std::unordered_map<std::string, vk_layer_properties>& layers_in_use() const;

        // Debug Reporting
        //----------------

        VkResult create_debug_report_callback(const VkDebugReportCallbackCreateInfoEXT* pCreateInfo,
            const VkAllocationCallbacks* pAllocator, VkDebugReportCallbackEXT* pCallback) const;

        void destroy_debug_report_callback(VkDebugReportCallbackEXT callback,
            const VkAllocationCallbacks* pAllocator) const;

        /**
        *@brief Attempts to get the pointer to a function in the Vulkan API.
        *
        *@details This should be used to acquire the pointer to functions that aren't explicitly provided
        *        by the Vulkan header, but are still present in the API. For example, the function \c vkCreateDebugReportCallbackEXT
        *        is not directly accessible. The pointer to that function should be able to be obtained using this
        *        function.
        *        Usage:
        *@code{.cpp}
        *const auto ptr_func = get_function_pointer<PFN_vkCreateDebugReportCallbackEXT>("vkCreateDebugReportCallbackEXT");
        *@endcode
        *       Note the usage of the \c PFN_ type for the type parameter. The return value should also be \b checked \b for \c nullptr.
        *
        *@tparam FunctionPointer The function pointer type specified by the Vulkan API.
        *@param func_name The name of the function to attempt to locate.
        *@return A pointer to the function if it can be found, otherwise, \c nullptr.
        */
        template<typename FunctionPointer>
        FunctionPointer get_function_pointer(const std::string_view& func_name)
        {
            if (_instance == nullptr || func_name.empty())
                return nullptr;

            const auto ptr = vkGetInstanceProcAddr(_instance, func_name.data());

            // If the requested function was not found, then null can just be returned
            // since attempting to do the cast isn't important.
            if (ptr == nullptr)
                return nullptr;

            return reinterpret_cast<FunctionPointer>(ptr);
        }

        operator VkInstance() const { return _instance; }

    private:
        /**
        *\brief Locate and store pointers to various functions that the instance should provide.
        */
        void setup_function_pointers();

        static void populate_instance_extensions(std::vector<vk_extension_properties>* extensions,
            SDL_Window* window, bool enable_debug);

        static void populate_instance_layers(std::vector<vk_layer_properties>* layers);
    };
}
#endif