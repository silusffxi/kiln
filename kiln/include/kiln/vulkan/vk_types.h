#pragma once
#ifdef __cplusplus
#include <cstdint>
#else
#include <stdint.h>
#endif
#include <vulkan/vulkan.h>

typedef enum
{
    kiln_vk_blend_mode_none = 0,
    kiln_vk_blend_mode_additive,
    kiln_vk_blend_mode_alpha_blend,
} kiln_vk_blend_mode;

#ifdef __cplusplus
#include <glm/glm.hpp>

namespace kiln::vulkan
{
    enum class blend_mode
    {
        none        = kiln_vk_blend_mode_none,
        additive    = kiln_vk_blend_mode_additive,
        alpha_blend = kiln_vk_blend_mode_alpha_blend,
    };

    /**
    *\brief List of available sample count values.
    */
    constexpr VkSampleCountFlagBits vk_sample_counts[] =
    {
        VK_SAMPLE_COUNT_1_BIT,
        VK_SAMPLE_COUNT_2_BIT,
        VK_SAMPLE_COUNT_4_BIT,
        VK_SAMPLE_COUNT_8_BIT,
        VK_SAMPLE_COUNT_16_BIT,
        VK_SAMPLE_COUNT_32_BIT,
        VK_SAMPLE_COUNT_64_BIT,
    };

    constexpr uint32_t vk_sample_counts_count = static_cast<uint32_t>(sizeof(vk_sample_counts) / sizeof(VkSampleCountFlagBits));

    /**
    *\brief Generic set of push constants to use with compute shaders.
    */
    struct compute_push_constants
    {
        glm::vec4 data1;
        glm::vec4 data2;
        glm::vec4 data3;
        glm::vec4 data4;
    };

    struct gpu_draw_push_constants
    {
        glm::mat4       world_matrix;
        VkDeviceAddress vertex_buffer;
    };

    struct vertex
    {
        glm::vec3 position;
        float uv_x;
        glm::vec3 normal;
        float uv_y;
        glm::vec4 color;
    };
}
#endif