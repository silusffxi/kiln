#pragma once
#ifdef __cplusplus
#include <cstdint>
#else
#include <stdint.h>
#endif
#include <vulkan/vulkan.h>

#ifdef __cplusplus
#include <functional>
#include <unordered_set>
#include "kiln/logging.hpp"
#include "kiln/vulkan/structs.hpp"
#include "kiln/vulkan/vk_compat.h"
#include "kiln/vulkan/vk_instance.h"
#include "kiln/vulkan/vk_version.hpp"

namespace kiln::vulkan
{
    class vk_physical_device
    {
        VkPhysicalDevice _physical_device = nullptr;

        VkPhysicalDeviceFeatures   _features = { };
        VkPhysicalDeviceProperties _properties = { };

        VkPhysicalDeviceFeatures2 _features2 = { };
        VkPhysicalDeviceVulkan11Features _vulkan_11_features = { };
        VkPhysicalDeviceVulkan12Features _vulkan_12_features = { };
        VkPhysicalDeviceVulkan13Features _vulkan_13_features = { };

        VkPhysicalDeviceProperties2 _properties2 = { };
        VkPhysicalDeviceVulkan11Properties _vulkan_11_props = { };
        VkPhysicalDeviceVulkan12Properties _vulkan_12_props = { };
        VkPhysicalDeviceVulkan13Properties _vulkan_13_props = { };

        VkPhysicalDeviceMemoryProperties _memory = { };

        vk_version _api_version = { };

        std::string       _device_name;
        vk_driver_version _driver_version;

        std::vector<VkQueueFamilyProperties> _queue_families;

        int32_t _queue_family_compute = -1;
        int32_t _queue_family_graphics = -1;

        std::unordered_set<VkPresentModeKHR> _supported_present_modes;

    public:
        vk_physical_device(const std::unique_ptr<vk_instance>& instance);
        vk_physical_device(const vk_instance* instance);

        vk_physical_device(const std::unique_ptr<vk_instance>& instance,
            const std::function<bool(VkPhysicalDevice, const VkPhysicalDeviceProperties*)>& search_predicate);

        vk_physical_device(const vk_instance* instance,
            const std::function<bool(VkPhysicalDevice, const VkPhysicalDeviceProperties*)>& search_predicate);

        ~vk_physical_device();

        [[nodiscard]] int32_t compute_queue_family() const { return _queue_family_compute; }
        [[nodiscard]] int32_t graphics_queue_family() const { return _queue_family_graphics; }

        VkPhysicalDeviceFeatures* features() { return &_features; }
        VkPhysicalDeviceLimits* limits() { return &_properties.limits; }
        VkPhysicalDeviceMemoryProperties* memory_properties() { return &_memory; }
        VkPhysicalDeviceProperties* properties() { return &_properties; }

#ifdef KILN_HAVE_VULKAN_1_1
        [[nodiscard]] const VkPhysicalDeviceFeatures2* features2() const { return &_features2; }
        [[nodiscard]] const VkPhysicalDeviceProperties2* properties2() const { return &_properties2; }
        [[nodiscard]] const VkPhysicalDeviceVulkan11Features* features_vulkan_11() const { return &_vulkan_11_features; }
        [[nodiscard]] const VkPhysicalDeviceVulkan11Properties* properties_vulkan_11() const { return &_vulkan_11_props; }
#endif
#ifdef KILN_HAVE_VULKAN_1_2
#endif
        [[nodiscard]] const VkPhysicalDeviceVulkan12Features* features_vulkan_12() const { return &_vulkan_12_features; }
        [[nodiscard]] const VkPhysicalDeviceVulkan12Properties* properties_vulkan_12() const { return &_vulkan_12_props; }
#ifdef KILN_HAVE_VULKAN_1_3
        [[nodiscard]] const VkPhysicalDeviceVulkan13Features* features_vulkan_13() const { return &_vulkan_13_features; }
        [[nodiscard]] const VkPhysicalDeviceVulkan13Properties* properties_vulkan_13() const { return &_vulkan_13_props; }
#endif

        [[nodiscard]] const vk_version& api_version() const { return _api_version; }
        [[nodiscard]] const std::string& name() const { return _device_name; }
        [[nodiscard]] const vk_driver_version& driver_version() const { return _driver_version; }

        /**
        *\brief Returns the format properties for a given format.
        */
        [[nodiscard]] VkFormatProperties get_format_properties(VkFormat format) const;

        VkQueueFamilyProperties* get_queue_family_properties(uint32_t queue_family);

        [[nodiscard]] uint32_t get_compute_queue_count() const { return get_queue_family_queue_count(_queue_family_compute); }
        [[nodiscard]] uint32_t get_graphics_queue_count() const { return get_queue_family_queue_count(_queue_family_graphics); }

        [[nodiscard]] VkSampleCountFlagBits get_supported_msaa() const;

        /**
        *\brief Indicates if the physical device supports buffer device address.
        *       Refer to the Vulkan documentation for more information about this feature:
        *       https://registry.khronos.org/vulkan/specs/1.3-extensions/man/html/VK_KHR_buffer_device_address.html
        *
        *\note Vulkan API v1.2 must be used and the physical device must actually support this feature
        *      in order for this method to return \c true.
        */
        [[nodiscard]] bool supports_buffer_device_address() const
        {
#if KILN_HAVE_VULKAN_1_2
            return _vulkan_12_features.bufferDeviceAddress;
#else
            return false;
#endif
        }

        /**
        *\brief Indicates if the physical device supports dynamic rendering.
        *       Refer to the Vulkan documentation for more information about this feature:
        *       https://registry.khronos.org/vulkan/specs/1.3-extensions/man/html/VK_KHR_dynamic_rendering.html
        *
        *\note Vulkan API v1.3 must be used and the physical device must actually support this feature in
        *      order for this method to return \c true.
        */
        [[nodiscard]] bool supports_dynamic_rendering() const
        {
#if KILN_HAVE_VULKAN_1_3
            return _vulkan_13_features.dynamicRendering;
#else
            return false;
#endif
        }

        /**
        *\brief
        * Indicates if the physical device supports synchronization2.
        * Refer to the Vulkan documentation for more information about this feature:
        *\li https://docs.vulkan.org/guide/latest/extensions/VK_KHR_synchronization2.html
        *\li https://registry.khronos.org/vulkan/specs/1.3-extensions/man/html/VK_KHR_synchronization2.html
        * .
        * \note Vulkan API v1.3 must be used and the physical device must actually support this feature in
        *       order for this method to return \c true.
        */
        [[nodiscard]] bool supports_synchronization2() const
        {
#if KILN_HAVE_VULKAN_1_3
            return _vulkan_13_features.synchronization2;
#else
            return false;
#endif
        }

        operator VkPhysicalDevice() const { return _physical_device; }

    private:
        /**
        *\brief Returns the number of queues available for a given queue family.
        */
        [[nodiscard]] uint32_t get_queue_family_queue_count(int32_t queue_family) const;

        /**
        *\brief Populates additional physical device features that are acquired via \c vkGetPhysicalDeviceFeatures2.
        */
        void populate_additional_physical_device_features();

        /**
        *\brief Populates additional physical device properties that are acquired via \c vkGetPhysicalDeviceProperties2.
        */
        void populate_additional_physical_device_properties();

        static bool default_physical_device_search_predicate(VkPhysicalDevice device, const VkPhysicalDeviceProperties* props);

        static std::vector<VkPhysicalDevice> enumerate_physical_devices(VkInstance instance);

        static std::tuple<int32_t, int32_t, std::vector<VkQueueFamilyProperties>> enumerate_queue_families(
            VkPhysicalDevice device);
    };
}
#endif