#pragma once
#ifdef __cplusplus
#endif
#include <vulkan/vulkan.h>
#include "kiln/vulkan/allocated_image.h"
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_shader.h"
#include "kiln/vulkan/vk_types.h"

#ifdef __cplusplus
#include <memory>
#include <string>

namespace kiln::vulkan
{
    class compute_effect
    {
        std::string _name;

        vk_device* _device;

        VkPipelineLayout _layout;
        VkPipeline       _pipeline;

        compute_push_constants _push_constants = { };

    public:
        compute_effect(std::string name, vk_device* device, VkPipelineLayout layout, VkPipeline pipeline);
        ~compute_effect();

        [[nodiscard]] std::string name() const;
        [[nodiscard]] VkPipelineLayout layout() const;
        [[nodiscard]] VkPipeline pipeline() const;
        [[nodiscard]] compute_push_constants* push_constants();

        void draw(VkCommandBuffer cmd, VkDescriptorSet draw_image_desc_set, const allocated_image* draw_image) const;

        static std::unique_ptr<compute_effect> create(const std::string& name, vk_device* device,
            VkDescriptorSetLayout desc_set_layout, const vk_shader* shader);
    };
}
#endif