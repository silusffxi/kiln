#pragma once
#include <string>
#include <unordered_map>
#include <vector>

namespace kiln::vulkan
{
    class vk_instance_config
    {
        bool _debug_enabled = false;

        std::unordered_map<std::string, bool> _optional_layers;

    public:
        /**
        *\brief Indicates if debugging features should be enabled for the instance.
        */
        bool& debug_enabled();

        /**
        *\brief Returns a list of enabled, optional, Vulkan instance layers.
        */
        std::vector<std::string> get_enabled_optional_layers();

        /**
        * \brief Includes an optional instance layer with the specified name if it is
        *        an available layer.
        */
        void include_optional_layer(const std::string& layer_name);

        /**
        *\brief Indicates that layers for Epic Online Services (EOS) should be
        *       included in the \c VkInstance.
        */
        void include_eos();

        /**
        *\brief Indicates the NVIDIA Optimus Vulkan layer(s) should be
        *       included in the \c VkInstance.
        *
        *\details Refer to this URL for more information about NVIDIA Optimus:
        *         https://www.nvidia.com/en-us/geforce/technologies/optimus/
        */
        void include_nv_optimus();

        /**
        *\brief Indicates the OBS (Open Broadcaster Software) hook layer(s) should
        *       be included in the \c VkInstance.
        *
        *\details Refer to the OBS GitHub repository for more information about this layer
        *         and how it works.
        *         https://github.com/obsproject/obs-studio/blob/master/plugins/win-capture/graphics-hook/vulkan-capture.h
        */
        void include_obs_hook();

        /**
        *\brief Indicates the Rockstar Games Social Club layer(s) should be included
        *       in the \c VkInstance.
        */
        void include_rgsc();

        /**
        *\brief Indicates the RTSS (Riva Tuner Statistics Server) layer(s) should
        *       be included in the \c VkInstance.
        */
        void include_rtss();

        /**
        *\brief Indicates the Steam related layer(s) should be included in the \c VkInstance.
        *
        *\details This includes both the Steam overlay layer and the Steam caching layer.
        */
        void include_steam();

        /**
        *\brief Indicates that the "VK_LAYER_LUNARG_api_dump" layer should be included in the \c VkInstance.
        *
        *\details This should only be enabled when a detailed report of all Vulkan API calls is needed.
        *         Having this layer enabled will create a lot of verbose debug output.
        */
        void include_vulkan_api_dump();

        /**
        *\brief Indicates the "VK_LAYER_LUNARG_monitor" layer should be included in the \c VkInstance.
        *
        *\details This will alter the window's title bar to include an FPS counter.
        */
        void include_vulkan_monitor();

        /**
        *\brief Indicates that the "VK_LAYER_LUNARG_gfxreconstruct" layer should be included in the \c VkInstance.
        *
        *\details GFXReconstruct provides tools for the capture and replay of graphics API calls. Refer to the
        *         project's GitHub page for more information: https://github.com/LunarG/gfxreconstruct
        */
        void include_vulkan_gfx_recconstruct();

        /**
        *\brief Indicates that the "VK_LAYER_KHRONOS_profiles" layer should be included in the \c VkInstance.
        *
        *\details Refer to the Vulkan documentation for more information about this layer:
        *         https://vulkan.lunarg.com/doc/view/latest/linux/profiles_layer.html
        */
        void include_vulkan_profiles();

    private:
        void add_or_enable_instance_layer(const std::string& layer_name);
    };
}