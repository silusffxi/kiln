#pragma once
#include <SDL2/SDL.h>
#include "kiln/vulkan/vk_instance.h"
#include "kiln/vulkan/vk_physical_device.h"
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_memory_allocator.h"
#include "kiln/vulkan/vk_surface.h"
#include "kiln/vulkan/vk_swapchain.h"
#include "kiln/vulkan/vk_immediate.h"
#include "kiln/vulkan/descriptor_allocator.h"
#include "kiln/vulkan/vk_frame_data.h"
#include "kiln/vulkan/compute_effect.h"
#include "kiln/vulkan/allocated_image.h"
#include "kiln/vulkan/vk_shader.h"
#include "kiln/vulkan/vk_render_extension.h"
#include "kiln/vulkan/sampler_manager.h"
#include "kiln/vulkan/vk_pipeline.h"
#include "kiln/vulkan/vk_descriptor.h"
#include "kiln/vulkan/vk_assets_manager.h"

#ifdef __cplusplus
#include <functional>
#include <memory>
#include <unordered_map>
#include <vector>
#include "kiln/logging.hpp"
#include "kiln/renderer.hpp"

namespace kiln::vulkan
{
    struct vk_renderer_args
    {
        SDL_Window* window;

        vk_instance*         instance;
        vk_physical_device*  gpu;
        vk_device*           device;
        vk_memory_allocator* allocator;
        vk_surface*          surface;
        vk_swapchain*        swapchain;

        vk_immediate*      immediate;
        vk_assets_manager* assets;
    };

    class vk_renderer : public renderer
    {
    public:
        /**
        *\brief The maximum number of render extensions that can be attached to a renderer.
        */
        static constexpr size_t max_render_extensions = 30;

    private:
        kiln_logger _log;

        SDL_Window* _window; ///< The window hosting the renderer.

        vk_instance*         _instance;
        vk_physical_device*  _gpu;
        vk_device*           _device;
        vk_memory_allocator* _allocator;
        vk_surface*          _surface;
        vk_swapchain*        _swapchain;

        /**
        *\brief Provides a way to invoke immediate commands.
        */
        vk_immediate* _immediate;

        /**
        *\brief Provides access to asset loading and retrieval functionality.
        */
        vk_assets_manager* _assets;

        /**
        *\brief Manager for \c VkSampler instances.
        */
        std::unique_ptr<sampler_manager> _samplers;

        /**
        *\brief Contains data related to in-flight frames.
        */
        struct
        {
            uint32_t                   current_index = 0;
            vk_frame_data*             current       = nullptr;
            std::vector<vk_frame_data> frames; ///< Data required for each "in-flight" frame.
        } _frame_data;

        /**
        *\brief Contains status about information about the current frame or all frames being rendered.
        */
        struct
        {
            bool     in_progress = false;
            uint32_t count = 0;
        } _frame_status;

        std::unique_ptr<vk_descriptor> _draw_image_descriptor = nullptr;
        std::unique_ptr<vk_descriptor> _sampler_descriptor    = nullptr;
        std::unique_ptr<vk_descriptor> _texture_descriptor    = nullptr;

        std::unique_ptr<vk_pipeline> _colored_triangle = nullptr;
        std::unique_ptr<vk_pipeline> _mesh_pipeline    = nullptr;

        /**
        *\brief List of compute effects.
        */
        std::vector<std::unique_ptr<compute_effect>> _compute_effects;
        compute_effect* _selected_background = nullptr;

        /**
        *\brief Current image from the swapchain being used for rendering.
        */
        vk_swapchain_image* _swapchain_image = nullptr;

        /**
        *\brief Depth image.
        */
        std::unique_ptr<allocated_image> _depth_image = nullptr;

        /**
        *\brief Intermediate image to be drawn to during rendering rather than drawing
        *       directly to the swapchain.
        */
        std::unique_ptr<allocated_image> _draw_image = nullptr;

        struct
        {
            /**
            *\brief Pre-allocate the constructor args to pass to new render extensions so that we don't have to
            *       keep re-allocating them any time we want to create a new extension.
            */
            vk_render_extension_create_args create_args = { };

            /**
            *\brief The collection of render extensions that have been added to the renderer.
            */
            std::vector<std::unique_ptr<vk_render_extension>> list;

            /**
            *\brief The render extensions keyed by name.
            */
            std::unordered_map<std::string, vk_render_extension*> by_name;
        } _render_extensions;

    public:
        vk_renderer(const vk_renderer_args& args);
        ~vk_renderer() override;

        void add_extension(const std::function<std::unique_ptr<vk_render_extension>(vk_render_extension_create_args&)>& ext_factory);
        void add_extension(std::unique_ptr<vk_render_extension> ext);

        [[nodiscard]] compute_effect* background_effect() const;
        std::vector<std::unique_ptr<compute_effect>>& background_effects();
        void set_background_effect(compute_effect* effect);

        void draw_background() const;
        void draw_geometry() const;
        void draw_geometry(const std::unique_ptr<gpu_mesh_buffers>& mesh) const;
        void draw_geometry(const gpu_mesh_buffers* mesh) const;
        void draw_mesh(const mesh_asset* mesh, bool world_transform = false) const;

        /**
        *\brief Returns the extension with the specified name. If the extension doesn't exist, then \c nullptr will be returned.
        */
        [[nodiscard]] vk_render_extension* get_extension(const std::string& name) const;

        [[nodiscard]] const std::unordered_map<std::string, vk_render_extension*>& get_extensions() const;

        [[nodiscard]] vk_immediate* immediate() const;

    protected:
        void on_event(const SDL_Event* e) override;

        void on_frame_begin() override;
        void on_frame_end() override;

    private:
        void apply_world_transform(glm::mat4& world_matrix) const;

        void draw_background(VkCommandBuffer cmd) const;
        void draw_geometry(VkCommandBuffer cmd, const gpu_mesh_buffers* mesh) const;
        void draw_mesh(VkCommandBuffer cmd, const mesh_asset* mesh, bool world_transform) const;

        /**
        *\brief Draws the clear color to the specified image.
        *\param cmd The command buffer to use.
        *\param image The image that should be drawn to.
        *\param layout The layout of the image that should be drawn to.
        */
        void draw_clear(VkCommandBuffer cmd, VkImage image, VkImageLayout layout);

        void next_frame();

        void queue_submit(VkCommandBuffer cmd, const vk_frame_data* frame) const;
        void queue_present(const vk_frame_data* frame) const;

        void render_extensions_frame_begin() const;
        void render_extensions_frame_end(VkCommandBuffer cmd) const;

        void initialize_compute_effects();
        void initialize_descriptors();

        void initialize_pipelines();
        void initialize_mesh_pipeline();
        void initialize_triangle_pipeline();
    };
}
#endif