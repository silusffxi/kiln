#pragma once
#include <cstdint>
#include <string>
#include <vulkan/vulkan.h>

namespace kiln::vulkan
{
    /**
    *\brief Version that is compatible with the Vulkan version specification.
    */
    struct vk_version
    {
        /// The variant of the version. This should always 0 for standard Vulkan.
        uint8_t  variant;
        uint8_t  major;
        uint16_t minor;
        uint16_t patch;

        /// The full version value as a single \c uint32_t.
        uint32_t value;

        vk_version();
        vk_version(uint32_t value);
        vk_version(uint8_t variant, uint8_t major, uint16_t minor, uint16_t patch);
        ~vk_version() = default;
        vk_version(const vk_version&) = default;
        vk_version(vk_version&&) = default;

        vk_version& operator=(const vk_version&) = default;
        vk_version& operator=(vk_version&&) = default;

        /// Returns the string representation of the version.
        [[nodiscard]] std::string string() const;

        /// Returns the string representation of the version with the variant number included.
        [[nodiscard]] std::string string_variant() const;

        bool operator==(const vk_version& other) const;
        bool operator!=(const vk_version& other) const;

        bool operator<(const vk_version& other) const;
        bool operator>(const vk_version& other) const;

        bool operator<=(const vk_version& other) const;
        bool operator>=(const vk_version& other) const;
    };

    /**
    *\brief Header value \c VK_HEADER_VERSION_COMPLETE.
    */
    constexpr uint32_t api_header_version_complete = VK_HEADER_VERSION_COMPLETE;

    extern const vk_version api_1_0;
    extern const vk_version api_1_1;
    extern const vk_version api_1_2;
    extern const vk_version api_1_3;

    /**
    *\brief Vulkan API as reported by calling \c PFN_vkEnumerateInstanceVersion located by the
    *       operating system or platform.
    *
    * - \b Linux: Accomplished by calling \c dlopen then using \c dlsym to locate and
    *      return the pointer to \c vkEnumerateInstanceVersion.
    * - \b Windows: Accomplished by calling \c EnumProcessModules, locating "vulkan-1.dll", then using
    *      \c LoadLibrary and \c GetProcAddress to return the pointer to \c vkEnumerateInstanceVersion
    */
    extern vk_version api_version_direct;

    /**
    *\brief Vulkan API version as reported by the header.
    */
    extern const vk_version api_version_header;

    /**
    *\brief Vulkan API as reported by the API itself (using \c vkEnumerateInstanceVersion).
    */
    extern vk_version api_version_platform;

    /**
    *\brief Methodology to use to retrieve the version number for the loaded
    *       Vulkan library. The Vulkan library should be identified with a file
    *       name like \b vulkan-1.dll, \b vulkan-1.so, or \b vulkan-1.dylib.
    */
    enum class version_fetch_method
    {
        use_default = 0, ///< Use the default method for getting the loaded version of Vulkan.
        vulkan_api,      ///< Use the Vulkan API to get the loaded version of Vulkan.
        windows_api,     ///< Use the Windows API to get the loaded version of Vulkan.
    };

    /**
    *\brief Checks a few different sources for the Vulkan API version. The version defined by the Vulkan API header
    *       will be "preferred" among the possible sources. If there is a mismatch, this will return false.
    *       Mismatches, and their sources, will be logged.
    */
    bool check_version_mismatch();

    /**
    *\brief Collect the version information for the Vulkan API from a few different sources.
    */
    void collect_versions();

    /**
    *\brief Retrieves the Vulkan API version from the loaded Vulkan library.
    *       Refer to this Stack Overflow answer: https://stackoverflow.com/a/65382710
    *
    *\param method Defines the method to use to fetch the Vulkan API version. This will be "use_default" unless
    *              otherwise specified.
    */
    vk_version get_vulkan_version(version_fetch_method method = version_fetch_method::use_default);
}