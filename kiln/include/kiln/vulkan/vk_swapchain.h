#pragma once
#ifdef __cplusplus
#include <cstdint>
#else
#include <stdint.h>
#endif
#include <vulkan/vulkan.h>
#include <vma/vk_mem_alloc.h>
#include "kiln/vulkan/vk_config.h"
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_physical_device.h"
#include "kiln/vulkan/vk_surface.h"
#include "kiln/vulkan/vk_render_image.h"

#ifdef __cplusplus
#pragma once
#include <atomic>
#include <memory>
#include <vector>
#include "kiln/logging.hpp"

namespace kiln::vulkan
{
    struct vk_swapchain_image : vk_render_image
    {
        uint32_t index;
    };

    class vk_swapchain
    {
        kiln_logger _log;

        const vk_config*    _config;
        vk_physical_device* _physical_device;
        vk_device*          _device;
        vk_surface*         _surface;
        VmaAllocator        _allocator;

        VkSwapchainKHR _swapchain = nullptr;

        VkPresentModeKHR _present_mode = VK_PRESENT_MODE_IMMEDIATE_KHR;

        std::vector<vk_swapchain_image> _images;

        VkExtent2D _swapchain_extent = { };

        VkFormat _swapchain_image_format = VK_FORMAT_UNDEFINED;
        std::atomic_uint32_t _swapchain_image_index = 0;

        /// Indicates that the swapchain needs to be rebuilt due to something
        /// like a window resize or minimize/restore event.
        std::atomic_uint8_t _rebuild_request_count = 0;

    public:
        vk_swapchain(const vk_config* config, const std::unique_ptr<vk_physical_device>& gpu,
            const std::unique_ptr<vk_device>& device, const std::unique_ptr<vk_surface>& surface,
            VmaAllocator allocator);
        vk_swapchain(const vk_config* config, vk_physical_device* physical_device,
            vk_device* device, vk_surface* surface, VmaAllocator allocator);
        ~vk_swapchain();

        /**
        *\brief Returns the index of the next swapchain image.
        */
        vk_swapchain_image* acquire_next_image(VkSemaphore semaphore);

        /**
        *\brief Returns the \c VkSwapchainKHR instance wrapped by this instance.
        */
        [[nodiscard]] VkSwapchainKHR get_vk_swapchain() const { return _swapchain; }

        VkExtent2D image_extent() const;
        VkFormat   image_format() const;

        /**
        *\brief Returns the collection of swapchain images.
        */
        std::vector<vk_swapchain_image>& images();

        /**
        *\brief Returns the present mode used by the swapchain.
        */
        [[nodiscard]] VkPresentModeKHR present_mode() const { return _present_mode; }

        /**
        *\brief If a rebuild was previously requested, process it now. If no rebuild was requested
        *       then calling this method will have no effect.
        */
        void process_rebuild();

        /**
        *\brief Requests that the swapchain be rebuilt the next time an image is requested.
        */
        void request_rebuild();

        /**
        *\brief Indicates if the swapchain needs to be rebuilt or not.
        */
        [[nodiscard]] bool requires_rebuild() const { return _rebuild_request_count > 0; }

        operator VkSwapchainKHR() const { return _swapchain; }

    private:
        /**
        *\brief Constructs the swapchain. This exists separate from the constructor in order to allow
        *       for the swapchain to be rebuilt arbitrarily.
        */
        void build();

        /**
        *\brief Releases/frees the resources associated with the swapchain. This exists separate from
        *       the deconstructor in order allow for the swapchain to be rebuilt arbitrarily.
        */
        void destroy();

        /**
        *\brief Rebuilds the swapchain.
        */
        void rebuild();
    };
}

#endif