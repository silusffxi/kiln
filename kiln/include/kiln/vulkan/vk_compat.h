///<\file  vk_compat.h
///<\brief Contains compatibility definitions to make working with
///        different versions of the Vulkan API easier.
///
///\details This only really covers stuff that we need compatibility
///         definitions for with regard to Kiln. This shouldn't attempt to cover
///         the entire range definitions required to cover the entire Vulkan API.
#pragma once
#include <vulkan/vulkan.h>

// Vulkan API v1.0 Compatibility Definitions
// (Provided as a sanity check to make sure Vulkan is actually included.)
//-----------------------------------------------------------------------------
#ifndef VK_VERSION_1_0
#define KILN_HAVE_VULKAN_1_0 0 // NOLINT(modernize-macro-to-enum)
#else
#define KILN_HAVE_VULKAN_1_0 1 // NOLINT(modernize-macro-to-enum)
#endif

// Vulkan API v1.1 Compatibility Definitions
//-----------------------------------------------------------------------------
#ifndef VK_VERSION_1_1
#define KILN_HAVE_VULKAN_1_1 0 // NOLINT(modernize-macro-to-enum)

constexpr VkStructureType VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2 = VK_STRUCTURE_TYPE_MAX_ENUM;
constexpr VkStructureType VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES = VK_STRUCTURE_TYPE_MAX_ENUM;

typedef struct VkPhysicalDeviceVulkan11Features {
    VkStructureType sType = VK_STRUCTURE_TYPE_MAX_ENUM;
    void* pNext;
} VkPhysicalDeviceVulkan11Features;

typedef struct VkPhysicalDeviceVulkan11Properties {
    VkStructureType sType = VK_STRUCTURE_TYPE_MAX_ENUM;
    void* pNext;
} VkPhysicalDeviceVulkan11Properties;
#else
#define KILN_HAVE_VULKAN_1_1 1 // NOLINT(modernize-macro-to-enum)
#endif

// Vulkan API v1.2 Compatibility Definitions
//-----------------------------------------------------------------------------
#ifndef VK_VERSION_1_2
#define KILN_HAVE_VULKAN_1_2 0 // NOLINT(modernize-macro-to-enum)

constexpr VkStructureType VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES = VK_STRUCTURE_TYPE_MAX_ENUM;

typedef struct VkPhysicalDeviceVulkan12Features {
    VkStructureType sType = VK_STRUCTURE_TYPE_MAX_ENUM;
    void* pNext;
} VkPhysicalDeviceVulkan12Features;

typedef struct VkPhysicalDeviceVulkan12Properties {
    VkStructureType sType = VK_STRUCTURE_TYPE_MAX_ENUM;
    void* pNext;
} VkPhysicalDeviceVulkan12Properties;
#else
#define KILN_HAVE_VULKAN_1_2 1 // NOLINT(modernize-macro-to-enum)
#endif

// Vulkan API v1.3 Compatibility Definitions
//-----------------------------------------------------------------------------
#ifndef VK_VERSION_1_3
#define KILN_HAVE_VULKAN_1_3 0 // NOLINT(modernize-macro-to-enum)

constexpr VkStructureType VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_3_FEATURES = VK_STRUCTURE_TYPE_MAX_ENUM;

typedef struct VkPhysicalDeviceVulkan13Features {
    VkStructureType sType = VK_STRUCTURE_TYPE_MAX_ENUM;
    void* pNext;
} VkPhysicalDeviceVulkan13Features;

typedef struct VkPhysicalDeviceVulkan13Properties {
    VkStructureType sType = VK_STRUCTURE_TYPE_MAX_ENUM;
    void* pNext;
} VkPhysicalDeviceVulkan13Properties;
#else
#define KILN_HAVE_VULKAN_1_3 1 // NOLINT(modernize-macro-to-enum)
#endif

/*
// Vulkan API v1.4 Compatibility Definitions
//-----------------------------------------------------------------------------
// Hypothetical Vulkan version for testing the compatibility layer. This will
// be moved in a future version of Kiln.
#ifndef VK_VERSION_1_4
#define KILN_HAVE_VULKAN_1_4 0  // NOLINT(modernize-macro-to-enum)

constexpr VkStructureType VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_4_FEATURES = VK_STRUCTURE_TYPE_MAX_ENUM;

typedef struct VkPhysicalDeviceVulkan14Features {
    VkStructureType sType = VK_STRUCTURE_TYPE_MAX_ENUM;
    void*           pNext;
} VkPhysicalDeviceVulkan14Features;

typedef struct VkPhysicalDeviceVulkan14Properties {
    VkStructureType sType = VK_STRUCTURE_TYPE_MAX_ENUM;
    void*           pNext;
} VkPhysicalDeviceVulkan14Properties;
#else
#define KILN_HAVE_VULKAN_1_4 1 // NOLINT(modernize-macro-to-enum)
#endif
*/

#ifdef __cplusplus
namespace kiln
{
    /**
    *\brief Indicates that Vulkan 1.0 API support is available.
    *       (This is mostly provided as a sanity check.)
    */
    constexpr bool have_vulkan_10 = KILN_HAVE_VULKAN_1_0;

    /**
    *\brief Indicates that Vulkan 1.1 API support is available.
    */
    constexpr bool have_vulkan_11 = KILN_HAVE_VULKAN_1_1;

    /**
    *\brief Indicates that Vulkan 1.2 API support is available.
    */
    constexpr bool have_vulkan_12 = KILN_HAVE_VULKAN_1_2;

    /**
    *\brief Indicates that Vulkan 1.3 API support is available.
    */
    constexpr bool have_vulkan_13 = KILN_HAVE_VULKAN_1_3;
}
#endif