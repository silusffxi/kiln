#pragma once
#include <vulkan/vulkan.h>
#include "kiln/api.h"

/**
*\brief An image that is used for rendering. This can be used to represent
*       something like the draw image or a swapchain image.
*/
typedef struct kiln_vk_render_image
{
    VkExtent2D extent_2d = { };
    VkExtent3D extent_3d = { };

    VkImage     image = nullptr;
    VkImageView view  = nullptr;

    VkImageLayout layout      = VK_IMAGE_LAYOUT_UNDEFINED;
    VkImageLayout prev_layout = VK_IMAGE_LAYOUT_UNDEFINED;
} kiln_vk_render_image;

/**
*\brief Transitions a \c kiln_vk_render_image to a new layout.
*/
KILN_EXTERN void kiln_transition_vk_render_image(kiln_vk_render_image* img, VkCommandBuffer cmd, VkImageLayout next_layout);

#ifdef __cplusplus
namespace kiln::vulkan
{
    /**
    *\brief An image that is used for rendering. This can be used to represent
    *       something like the draw image or a swapchain image.
    */
    struct vk_render_image : kiln_vk_render_image
    {
        void transition_to(VkCommandBuffer cmd, VkImageLayout next_layout);
    };
}
#endif