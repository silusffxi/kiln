#pragma once
#ifdef __cplusplus
#endif
#include <vulkan/vulkan.h>
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_shader.h"

#ifdef __cplusplus
#include <functional>
#include <memory>

namespace kiln::vulkan
{
    class vk_pipeline
    {
        vk_device* _device;

        VkPipelineLayout _layout;
        VkPipeline       _pipeline;

    public:
        vk_pipeline(vk_device* device, VkPipelineLayout layout, VkPipeline pipeline);
        vk_pipeline(vk_device* device, const std::function<void(VkPipelineLayout*, VkPipeline*)>& create_func);
        ~vk_pipeline();

        [[nodiscard]] VkPipelineLayout layout() const;
        [[nodiscard]] VkPipeline pipeline() const;

        static std::unique_ptr<vk_pipeline> create_compute_pipeline(vk_device* device, VkDescriptorSetLayout desc_set_layout,
            const vk_shader* shader);
    };
}
#endif