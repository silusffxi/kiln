#pragma once
#ifdef __cplusplus
#include <cstdint>
#else
#incldue <stdint.h>
#endif
#include <vulkan/vulkan.h>
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/descriptor_allocator.h"

#ifdef __cplusplus
#include <functional>
#include <memory>
#include <tuple>
#include <vector>

namespace kiln::vulkan
{
    class vk_descriptor
    {
        vk_device* _device;

        VkDescriptorSetLayout _layout;
        
        std::unique_ptr<descriptor_allocator> _desc_allocator;

        std::vector<VkDescriptorSet> _sets;

    public:
        vk_descriptor(vk_device* device, VkDescriptorSetLayout layout, uint32_t max_sets,
            const std::span<descriptor_allocator::pool_size_ratio>& pool_ratios);
        vk_descriptor(vk_device* device, VkDescriptorSetLayout layout, std::unique_ptr<descriptor_allocator> allocator);
        ~vk_descriptor();

        /**
        *\brief Allocates and returns a new descriptor set.
        */
        std::tuple<VkDescriptorSet, uint32_t> allocate_set();

        /**
        *\brief Allocates a new descriptor set, applies the provided update function, and returns the set.
        */
        std::tuple<VkDescriptorSet, uint32_t> allocate_set(const std::function<void(VkDescriptorSet)>& update);

        /**
        *\brief Returns the \c VkDescriptorSetLayout in use.
        */
        [[nodiscard]] VkDescriptorSetLayout layout() const;

        /**
        *\brief Returns the descriptor set with the specified index, if one is provided.
        *       If the set doesn't exist, then \c nullptr will be returned.
        */
        [[nodiscard]] VkDescriptorSet set(uint32_t index = 0) const;
    };
}
#endif
