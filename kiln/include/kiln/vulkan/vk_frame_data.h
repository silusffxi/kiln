#pragma once
#ifdef __cplusplus
#include <cstdint>
#else
#include <stdint.h>
#endif
#include <vulkan/vulkan.h>
#include "kiln/api.h"
#include "kiln/vulkan/vk_device.h"

KILN_EXTERN uint32_t kiln_vulkan_max_frames_in_flight;

#ifdef __cplusplus
namespace kiln::vulkan
{
    /**
    *\brief The maximum number of frames that can be in flight (being rendered or drawn to)
    *       at a time. By default, this should probably only be 2, but more may be
    *       used if desired.
    */
    constexpr uint32_t max_frames_in_flight = 2;

    /**
    *\brief Contains data related to each "in-flight" frame.
    *\note Command pools, command buffers, semaphores, and fences owned by a given \c vk_frame_data
    *      instance will not need to be explicitly freed/destroyed. The device that created aforementioned
    *      structures will handle releasing those resources.
    */
    struct vk_frame_data
    {
        VkCommandPool   cmd_pool;
        VkCommandBuffer cmd_buffer;

        VkSemaphore swapchain_semaphore;
        VkSemaphore renderer_semaphore;

        VkFence render_fence;

        vk_frame_data();
        vk_frame_data(vk_device* device);

        static std::vector<vk_frame_data> create_and_populate(vk_device* device, uint32_t count);
        static void populate(vk_device* device, std::vector<vk_frame_data>* frames);
    };
}
#endif