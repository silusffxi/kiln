#pragma once
#ifdef __cplusplus
#include <cstdint>
#else
#include <stdint.h>
#endif
#include <SDL2/SDL.h>
#include <vulkan/vulkan.h>
#include "kiln/vulkan/vk_instance.h"
#include "kiln/vulkan/vk_physical_device.h"
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_surface.h"
#include "kiln/vulkan/vk_swapchain.h"
#include "kiln/vulkan/allocated_image.h"
#include "kiln/vulkan/vk_render_image.h"

#ifdef __cplusplus
#include <string>
#include <vector>

namespace kiln::vulkan
{
    /**
    *\brief Arguments that should be provided when created any render extension.
    */
    struct vk_render_extension_create_args
    {
        vk_instance*        instance  = nullptr;
        vk_physical_device* gpu       = nullptr;
        vk_device*          device    = nullptr;
        vk_surface*         surface   = nullptr;
        vk_swapchain*       swapchain = nullptr;
    };

    /**
    *\brief Provides a module that allows a renderer to be extended without directly making changes
    *       to the renderer itself.
    */
    class vk_render_extension
    {
        std::string _name;

        vk_instance*        _instance;
        vk_physical_device* _gpu;
        vk_device*          _device;
        vk_surface*         _surface;
        vk_swapchain*       _swapchain;

        VkSampleCountFlagBits _max_supported_msaa = VK_SAMPLE_COUNT_1_BIT;

        /**
        *\brief Indicates if the extension is enabled or not.
        */
        bool _enabled = true;

        /**
        *\brief Indicates that a frame is being drawn to.
        */
        bool _frame_in_progress = false;

        /**
        *\brief Contains data required for secondary command buffers.
        */
        struct
        {
            VkCommandBufferInheritanceRenderingInfo inherit_render_info;
            VkCommandBufferInheritanceInfo          inherit_info;
            VkCommandBufferBeginInfo                begin_info;

            std::vector<VkCommandBuffer> buffers;
        } _cmd;

        struct
        {
            uint32_t current_index = 0;
        } _frame_data;

        vk_render_image* _render_image    = nullptr;
        vk_render_image* _swapchain_image = nullptr;

    public:
        vk_render_extension(const std::string& name, const vk_render_extension_create_args& args);

        vk_render_extension(const std::string& name, vk_instance* instance, vk_physical_device* gpu, vk_device* device,
            vk_surface* surface, vk_swapchain* swapchain);
        virtual ~vk_render_extension() = default;

        void disable();
        void enable();
        [[nodiscard]] bool is_enabled() const;

        void frame_begin(vk_render_image* target_image, vk_render_image* swapchain_image);

        void frame_end(VkCommandBuffer parent);

        /**
        *\brief Propagate events the render extension.
        */
        void handle_event(const SDL_Event* e);

        std::string name();

    protected:
        [[nodiscard]] VkCommandBuffer current_command_buffer() const;

        virtual void on_event(const SDL_Event* e);
        virtual void on_frame_begin(VkCommandBuffer cmd, vk_render_image* target_image, vk_render_image* swapchain_image);
        virtual void on_frame_end(VkCommandBuffer cmd, vk_render_image* target_image, vk_render_image* swapchain_image);

        [[nodiscard]] vk_instance*        instance() const;
        [[nodiscard]] vk_physical_device* gpu() const;
        [[nodiscard]] vk_device*          device() const;
        [[nodiscard]] vk_surface*         surface() const;
        [[nodiscard]] vk_swapchain*       swapchain() const;
        [[nodiscard]] vk_render_image*    render_image() const;

    private:
        void command_buffer_begin() const;
        void command_buffer_end() const;

        void next_frame();
    };

    class noop_render_extension : public vk_render_extension
    {
    public:
        noop_render_extension(vk_instance* instance, vk_physical_device* gpu, vk_device* device,
            vk_surface* surface, vk_swapchain* swapchain) :
            vk_render_extension("noop", instance, gpu, device, surface, swapchain)
        {
        }
    };
}
#endif