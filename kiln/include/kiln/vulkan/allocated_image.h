#pragma once
#ifdef __cplusplus
#include <cstdint>
#else
#include <stdint.h>
#endif
#include <vulkan/vulkan.h>
#include "vma/vk_mem_alloc.h"
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_memory_allocator.h"
#include "kiln/vulkan/vk_render_image.h"

#ifdef __cplusplus
#include <memory>

namespace kiln::vulkan
{
    class allocated_image
    {
    public:
        static constexpr VkFormat DEFAULT_IMAGE_FORMAT = VK_FORMAT_R16G16B16A16_SFLOAT;

        static constexpr VkImageUsageFlags DEFAULT_IMAGE_USAGE =
            VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_STORAGE_BIT |
            VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;

        static constexpr VkFormat DEPTH_IMAGE_FORMAT = VK_FORMAT_D32_SFLOAT;

        static constexpr VkImageUsageFlags DEPTH_IMAGE_USAGE = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;

    private:
        vk_device*   _device;
        VmaAllocator _allocator;

        uint32_t _width;
        uint32_t _height;

        /**
        *\brief The internal representation of the image.
        */
        vk_render_image _internal_image = { };

        VmaAllocation     _alloc;
        VmaAllocationInfo _alloc_info;

        VkFormat          _format;
        VkImageUsageFlags _usage;

    public:
        allocated_image(vk_device* device, VmaAllocator allocator, uint32_t width, uint32_t height,
            VkImage image, VkImageView view, VmaAllocation alloc, const VmaAllocationInfo& alloc_info,
            VkFormat format, VkImageUsageFlags usage);
        ~allocated_image();

        [[nodiscard]] float aspect_ratio() const;

        [[nodiscard]] uint32_t width() const;
        [[nodiscard]] uint32_t height() const;

        [[nodiscard]] float width_f() const;
        [[nodiscard]] float height_f() const;

        [[nodiscard]] VkImage image() const;
        [[nodiscard]] VkImageView view() const;

        [[nodiscard]] const VkExtent2D& extent_2d() const;
        [[nodiscard]] const VkExtent3D& extent_3d() const;

        [[nodiscard]] VmaAllocation allocation() const;
        [[nodiscard]] VmaAllocationInfo allocation_info() const;

        [[nodiscard]] VkFormat format() const;
        [[nodiscard]] VkImageUsageFlags usage() const;

        vk_render_image* render_image();

        [[nodiscard]] VkImageLayout layout() const;

        void transition_to(VkCommandBuffer cmd, VkImageLayout next_layout);

        static std::unique_ptr<allocated_image> create_depth_image(vk_device* device, VmaAllocator allocator,
            uint32_t width, uint32_t height);

        static std::unique_ptr<allocated_image> create_depth_image(vk_device* device, const vk_memory_allocator* allocator,
            uint32_t width, uint32_t height);

        /**
        *\brief Create the intermediate image used for drawing rather than drawing directly to the swapchain.
        */
        static std::unique_ptr<allocated_image> create_draw_image(vk_device* device, VmaAllocator allocator,
            uint32_t width, uint32_t height);

        /**
        *\brief Create the intermediate image used for drawing rather than drawing directly to the swapchain.
        */
        static std::unique_ptr<allocated_image> create_draw_image(vk_device* device, const vk_memory_allocator* allocator,
            uint32_t width, uint32_t height);
    };
}
#endif