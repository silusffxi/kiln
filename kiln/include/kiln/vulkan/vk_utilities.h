#pragma once
#ifdef __cplusplus
#include <cstdint>
#else
#include <stdint.h>
#endif
#include <vulkan/vulkan.h>
#include "kiln/api.h"

/**
*\brief Performs a check on a \c VkResult check returned by a Vulkan API call. While this function ca
*/
KILN_EXTERN VkResult kiln_vk_result_check(VkResult result, const char* error_message,
    const char* src_file, int line, const char* func);

#ifdef __cplusplus
#include <string>

namespace kiln::vulkan
{
    /**
    *\brief Copies one image to another.
    */
    void copy_image_to_image(VkCommandBuffer cmd, VkImage src, VkExtent2D src_size,
        VkImage dst, VkExtent2D dst_size);

    /**
    *\brief Indicates the application should abort when an error is returned from a Vulkan API call.
    */
    void set_abort_on_vulkan_error();

    /**
    *\brief Runs a check on the VkResult to determine if it succeeded or not.
    */
    VkResult result_check(VkResult result, const std::string& error_message = "",
        const char* src_file = "", int line = 0, const char* func = "");

    void transition_image(VkCommandBuffer cmd, VkImage image, VkImageLayout current_layout, VkImageLayout next_layout);
}
#endif

/**
*\brief Checks the \c VkResult passed as the first argument to determine if the call was successful or not.
*       If the result does not indicate success then the application will exit.
*/
#ifdef __cplusplus
constexpr uint32_t vk_timeout_seconds = 1'000'000'000;

#define vk_result_check(ptr, err_msg) kiln::vulkan::result_check(ptr, err_msg, __FILE__, __LINE__, #ptr)
#else
const uint32_t vk_timeout_seconds = 1'000'000'000;

#define vk_result_check(ptr, err_msg) kiln_vk_result_check(ptr, err_msg, __FILE__, __LINE__, #ptr)
#endif
