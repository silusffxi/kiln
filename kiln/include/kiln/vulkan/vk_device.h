#pragma once
#ifdef __cplusplus
#include <cstdint>
#else
#include <stdint.h>
#endif
#include <vulkan/vulkan.h>

#ifdef __cplusplus
#include <memory>
#include <unordered_map>
#include <vector>
#include "kiln/logging.hpp"
#include "kiln/vulkan/vk_physical_device.h"

namespace kiln::vulkan
{
    class vk_device
    {
        kiln_logger _log;
        VkDevice _device = nullptr;

        VkQueue       _compute_queue = nullptr;
        VkCommandPool _compute_cmd_pool = nullptr;

        VkQueue       _graphics_queue = nullptr;
        VkCommandPool _graphics_cmd_pool = nullptr;

        std::vector<VkQueue> _compute_queues;
        std::vector<VkQueue> _graphics_queues;

        /**
        *\brief Additional command pools that were created with the device.
        */
        std::unordered_map<uint32_t, VkCommandPool> _cmd_pools;

        int32_t _compute_queue_family_index = -1;
        int32_t _graphics_queue_family_index = -1;

        /// Fences that are owned by this logical device instance.
        std::vector<VkFence> _fences;

        /// Semaphores that are owned by this logical device instance.
        std::vector<VkSemaphore> _semaphores;

        /// Shader modules that were created by the device.
        std::vector<VkShaderModule> _shader_modules;

    public:
        vk_device(const std::unique_ptr<vk_physical_device>& phys_device, bool enable_debug);
        vk_device(vk_physical_device* phys_device, bool enable_debug);
        ~vk_device();

        VkCommandBuffer create_command_buffer(VkCommandPool pool, bool primary = true) const;

        /**
        *\brief Create a command pool.
        *\note A command pool created this way will be tracked by the \c vk_device instance that created it.
        *      The caller will not have to make a call to \c vkDestroyCommandPool in order to release the associated resources.
        */
        VkCommandPool create_command_pool(const VkAllocationCallbacks* alloc_callbacks);

        /**
        *\brief Create a command pool.
        *\note A command pool created this way will be tracked by the \c vk_device instance that created it.
        *      The caller will not have to make a call to \c vkDestroyCommandPool in order to release the associated resources.
        */
        VkCommandPool create_command_pool(uint32_t queue_family, const VkAllocationCallbacks* alloc_callbacks);

        /**
        *\brief Create a command pool.
        *\note A command pool created this way will be tracked by the \c vk_device instance that created it.
        *      The caller will not have to make a call to \c vkDestroyCommandPool in order to release the associated resources.
        */
        VkCommandPool create_command_pool(const VkCommandPoolCreateInfo& create_info,
            const VkAllocationCallbacks* alloc_callbacks);

        /**
        *\brief Creates a fence. This \c vk_device instance will be responsible for destroying the fence.
        */
        VkFence create_fence(VkFenceCreateFlags flags = 0);

        VkCommandBuffer create_graphics_command_buffer(bool primary = true) const;

        /**
        *\brief Creates a semaphore. This \c vk_device instance will be responsible for destroying the semaphore.
        */
        VkSemaphore create_semaphore(VkSemaphoreCreateFlags flags = 0);

        /**
        *\brief Creates a shader module from the specified shader binary.
        */
        VkShaderModule create_shader_module(const std::vector<uint32_t>& shader_binary);

        [[nodiscard]] VkQueue get_graphics_queue() const;
        [[nodiscard]] VkCommandPool get_graphics_command_pool() const;

        [[nodiscard]] VkQueue get_compute_queue(uint32_t index) const;
        [[nodiscard]] VkQueue get_graphics_queue(uint32_t index) const;

        /**
        *\brief Resets the specified fence.
        */
        void reset_fence(VkFence fence) const;

        /**
        *\brief Waits for the specified fence.
        */
        void wait_for_fence(VkFence fence, uint32_t wait_seconds = 1) const;

        /**
        *\brief Waits for the specified semaphore.
        */
        void wait_for_semaphore(VkSemaphore semaphore);

        void wait_idle() const;
        void wait_queue_idle() const;

        operator VkDevice() const { return _device; }

    private:
        VkDevice create_device(vk_physical_device* gpu, const std::vector<VkDeviceQueueCreateInfo>& queues,
            bool enable_debug) const;
    };
}
#endif