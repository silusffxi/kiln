#pragma once
#ifdef __cplusplus
#include <cstdint>
#else
#include <stdint.h>
#endif
#include <vulkan/vulkan.h>
#include "kiln/vulkan/vk_device.h"

#ifdef __cplusplus
#include <memory>
#include <span>
#include "kiln/logging.hpp"

namespace kiln::vulkan
{
    class descriptor_allocator
    {
        kiln_logger _log;

        vk_device* _device;

        /// Maximum number of descriptor sets that can be allocated.
        uint32_t _max_sets;

        /// The descriptor pool used to allocate descriptor sets.
        VkDescriptorPool _pool = nullptr;

        /// Contains the descriptor sets that have been allocated by this allocator.
        std::vector<VkDescriptorSet> _allocated_sets;

    public:
        struct pool_size_ratio
        {
            VkDescriptorType type;
            float ratio;
        };

        descriptor_allocator(vk_device* device, uint32_t max_sets, const std::span<pool_size_ratio>& pool_ratios);
        ~descriptor_allocator();

        VkDescriptorSet allocate(VkDescriptorSetLayout layout);
        VkDescriptorSet allocate(const VkDescriptorSetLayout* layout);

        std::vector<VkDescriptorSet> allocate(const std::vector<VkDescriptorSetLayout>& layouts);
        std::vector<VkDescriptorSet> allocate(const VkDescriptorSetLayout* layouts, uint32_t layouts_count);

        [[nodiscard]] VkDescriptorPool pool() const;

        //void reset_pool() const;

    private:
        void allocate(const VkDescriptorSetLayout* layouts, uint32_t layouts_count,
            VkDescriptorSet* sets);
    };
}
#endif