#pragma once
#include "kiln/types.h"

#ifdef __cplusplus
#include <filesystem>

namespace kiln::vulkan
{
    /**
    *\brief Contains configuration information required to initialize Kiln with Vulkan rendering.
    */
    struct vk_config
    {
        /**
        *\brief The root directory for assets.
        */
        std::filesystem::path assets_root_directory;

        /**
        *\brief The screen mode that the swapchain should use for presenting frames.
        */
        screen_mode screen_mode = screen_mode::immediate;
    };
}
#endif