#pragma once
#ifdef __cplusplus
#include <cstdint>
#else
#include <stdbool.h>
#include <stdint.h>
#endif
#include <vulkan/vulkan.h>
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_memory_allocator.h"
#include "kiln/vulkan/vk_types.h"
#include "kiln/vulkan/gpu_mesh_buffers.h"

typedef bool(*pfn_kiln_immediate_submit)(VkCommandBuffer cmd);

#ifdef __cplusplus
#include <functional>
#include <span>
#include <vector>
#include "kiln/logging.hpp"

namespace kiln::vulkan
{
    typedef std::function<bool(VkCommandBuffer cmd)> immediate_submit_func;

    /**
    *\brief Provides a way to execute Vulkan commands immediately without needing to
    *       be actually rendering a frame. Also provides functionality for common "immediate"
    *       operations like texture uploads, buffer copy, etc.
    */
    class vk_immediate
    {
        kiln_logger _log;

        vk_device*           _device;
        vk_memory_allocator* _allocator;

        VkCommandPool   _cmd_pool;
        VkCommandBuffer _cmd_buffer;

        VkFence _fence;

        bool _log_submits = false;

        std::vector<std::unique_ptr<gpu_mesh_buffers>> _uploaded_meshes;

    public:
        vk_immediate(vk_device* device, vk_memory_allocator* allocator);
        ~vk_immediate();
        vk_immediate(const vk_immediate&) = default;
        vk_immediate(vk_immediate&&) = default;

        vk_immediate& operator=(const vk_immediate&) = default;
        vk_immediate& operator=(vk_immediate&&) = default;

        /**
        *\brief Helper function to make copying buffers a bit easier.
        */
        void buffer_copy(VkBuffer src, VkDeviceSize src_size, VkBuffer dst) const;

        /**
        *\brief Helper function to make copying buffers a bit easier.
        */
        void buffer_copy(VkBuffer src, VkBuffer dst, const VkBufferCopy& copy) const;

        /**
        *\brief Submits a function using a command buffer immediately without waiting for the renderer
        *       to be in process of rendering a frame.
        */
        void submit(const immediate_submit_func& submit_func) const;

        /**
        *\brief Uploads a mesh to the GPU.
        */
        gpu_mesh_buffers* upload_mesh(std::span<uint32_t> indices, std::span<vertex> vertices);
    };
}
#endif