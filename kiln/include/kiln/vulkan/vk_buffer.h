#pragma once
#ifdef __cplusplus
#endif
#include <vulkan/vulkan.h>
#include <vma/vk_mem_alloc.h>

#ifdef __cplusplus
#include <string>

namespace kiln::vulkan
{
    class vk_buffer
    {
        VmaAllocator _allocator;

        VkBuffer          _buffer;
        VmaAllocation     _allocation;
        VmaAllocationInfo _alloc_info;

        VkDeviceAddress _buffer_address = 0;

    public:
        vk_buffer(VmaAllocator allocator, VkBuffer buffer, VmaAllocation allocation,
            const VmaAllocationInfo& alloc_info, VkDeviceAddress address);
        ~vk_buffer();

        /**
        *\brief Returns the allocation for the buffer.
        */
        [[nodiscard]] VmaAllocation allocation() const;

        /**
        *\brief Returns the allocation info for the buffer.
        */
        [[nodiscard]] const VmaAllocationInfo& allocation_info() const;

        /**
        *\brief Returns the actual \c VkBuffer instance for the buffer.
        */
        [[nodiscard]] VkBuffer buffer() const;

        /**
        *\brief Returns the address of the buffer.
        */
        [[nodiscard]] VkDeviceAddress buffer_address() const;

        /**
        *\brief Sets a name for the buffer's allocation.
        */
        void set_name(const std::string& name);

        operator VkBuffer() const { return _buffer; }
    };
}
#endif