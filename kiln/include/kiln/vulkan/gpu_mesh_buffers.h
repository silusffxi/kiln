#pragma once
#ifdef __cplusplus
#include <cstdint>
#else
#include <stdint.h>
#endif
#include "kiln/vulkan/vk_memory_allocator.h"
#include "kiln/vulkan/vk_buffer.h"
#include "kiln/vulkan/vk_types.h"

#ifdef __cplusplus
#include <memory>
#include <span>
#include <utility>

namespace kiln::vulkan
{
    class gpu_mesh_buffers
    {
        std::unique_ptr<vk_buffer> _index_buffer  = nullptr;
        std::unique_ptr<vk_buffer> _vertex_buffer = nullptr;

    public:
        gpu_mesh_buffers(std::unique_ptr<vk_buffer> index_buffer, std::unique_ptr<vk_buffer> vertex_buffer) :
            _index_buffer(std::move(index_buffer)), _vertex_buffer(std::move(vertex_buffer))
        {
        }

        ~gpu_mesh_buffers()
        {
            if (_vertex_buffer != nullptr)
            {
                _vertex_buffer.reset();
                _vertex_buffer = nullptr;
            }

            if (_index_buffer != nullptr)
            {
                _index_buffer.reset();
                _index_buffer = nullptr;
            }
        }

        [[nodiscard]] vk_buffer* index_buffer() const { return _index_buffer.get(); }
        [[nodiscard]] vk_buffer* vertex_buffer() const { return _vertex_buffer.get(); }

        [[nodiscard]] VkDeviceAddress index_buffer_address() const { return _index_buffer->buffer_address(); }
        [[nodiscard]] VkDeviceAddress vertex_buffer_address() const { return _vertex_buffer->buffer_address(); }
    };
}
#endif