#pragma once
#ifdef __cplusplus
#endif
#include <vulkan/vulkan.h>
#include "kiln/vulkan/vk_physical_device.h"
#include "kiln/vulkan/vk_device.h"

#ifdef __cplusplus
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include "kiln/logging.hpp"

namespace kiln::vulkan
{
    /**
    *\brief Manager for \c VkSampler instances.
    */
    class sampler_manager
    {
        kiln_logger _log;

        vk_physical_device* _gpu;
        vk_device*          _device;

        VkSampleCountFlagBits _min_supported = VK_SAMPLE_COUNT_1_BIT;
        VkSampleCountFlagBits _max_supported = VK_SAMPLE_COUNT_1_BIT;

        /**
        *\brief Contains all the supported, and unique, sample counts.
        */
        std::unordered_set<VkSampleCountFlagBits> _supported_sample_counts;

        /**
        *\brief This is, effectively a copy of \c _supported_sample_counts. However, it is
        *       in a \c std::vector instance to make working with the supported sample counts
        *       a bit easier outside the class.
        */
        std::vector<VkSampleCountFlagBits> _supported_sample_counts_v;

        /**
        *\brief Contains \c VkSampler instances using "linear".
        */
        std::unordered_map<VkSampleCountFlagBits, VkSampler> _samplers_linear;

        /**
        *\brief Contains \c VkSampler instances using "nearest".
        */
        std::unordered_map<VkSampleCountFlagBits, VkSampler> _samplers_nearest;

    public:
        sampler_manager(vk_physical_device* gpu, vk_device* device);
        ~sampler_manager();

        /**
        *\brief Indicates if a given sample count is supported or not.
        */
        [[nodiscard]] bool is_supported(uint32_t sample_count) const;

        /**
        *\brief Indicates if a given sample count is supported or not.
        */
        [[nodiscard]] bool is_supported(VkSampleCountFlagBits sample_count) const;

        /**
        *\brief Returns a \c VkSampler, using a linear algorithm, for the specified sample count.
        *       \c nullptr will be returned if the sample count is not supported or if the sample
        *       could not be created. If no sample count (or VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM) is provided,
        *       then the maximum value supported by the GPU will be used.
        */
        VkSampler linear(VkSampleCountFlagBits sample_count = VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM);

        /**
        *\brief Returns a \c VkSampler, using a nearest algorithm, for the specified sample count.
        *       \c nullptr will be returned if the sample count is not supported or if the sample
        *       could not be created. If no sample count (or VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM) is provided,
        *       then the maximum value supported by the GPU will be used.
        */
        VkSampler nearest(VkSampleCountFlagBits sample_count = VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM);

        /**
        *\brief Returns all the supported sample counts.
        */
        const std::vector<VkSampleCountFlagBits>& supported_sample_counts() const;

    private:
        VkSampler create_sampler(VkSampleCountFlagBits sample_count, const VkSamplerCreateInfo* create_info) const;
    };
}
#endif