#pragma once
#ifdef __cplusplus
#include <cstdint>
#else
#include <stdint.h>
#endif
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_immediate.h"
#include "kiln/vulkan/vk_shader.h"
#include "kiln/vulkan/vk_asset_types.h"

#ifdef __cplusplus
#include <filesystem>
#include <memory>
#include <optional>
#include <unordered_map>
#include "kiln/logging.hpp"

namespace kiln::vulkan
{
    typedef std::optional<std::vector<std::shared_ptr<mesh_asset>>> load_gltf_meshes_result;

    class vk_assets_manager
    {
        kiln_logger _log;

        vk_device*    _device;
        vk_immediate* _immediate;

        std::filesystem::path _assets_root;

        std::unordered_map<std::string, std::unique_ptr<vk_shader>> _shaders;

    public:
        vk_assets_manager(const std::unique_ptr<vk_device>& device, const std::unique_ptr<vk_immediate>& immediate,
            const std::filesystem::path& assets_root);
        vk_assets_manager(vk_device* device, vk_immediate* immediate, std::filesystem::path assets_root);
        ~vk_assets_manager();

        /**
        *\brief Returns a shader with the specified name. If the shader hasn't been loaded yet,
        *       it will be loaded. If the shader doesn't exist then \c nullptr will be returned.
        */
        vk_shader* get_shader(const std::string& name);

        /**
        *\brief Loads a GLTF mesh from disk.
        */
        load_gltf_meshes_result load_gltf_meshes(const std::filesystem::path& file_path);
    };
}
#endif
