#pragma once
#include <cstdint>
#include <string>
#include <vulkan/vulkan.h>

namespace kiln::vulkan
{
    /**
    *\brief VK2D representation of \c VkExtensionProperties.
    */
    struct vk_extension_properties
    {
        uint32_t    spec_version;
        std::string name;
    };

    /**
    *\brief VK2D representation of \c VkLayerProperties.
    */
    struct vk_layer_properties
    {
        uint32_t spec_version;
        uint32_t implementation_version;

        std::string name;
        std::string desc;
    };

    /**
    *\brief Version object used to represent the driver version of a physical device.
    *
    *\details This is needed because the driver version for a physical values can contain
    *         values larger than 255 for a single part of the version number.
    */
    struct vk_driver_version
    {
        uint32_t major;
        uint32_t minor;
        uint32_t patch;
    };

    /**
    *\brief This is, more or less, a "prototype" definition of any given Vulkan struct
    *       where the first field is \c sType and the second field is \c pNext. This
    *       struct was created in order to allow a "generic" struct type to represent
    *       a given Vulkan struct without needing to do a bunch of extra logic.
    *
    *\note This is used in conjunction with the function \c link_device_create_info in
    *      vk_device.cpp to make it a bit easier from the caller's perspective to specify
    *      a list of available create info structs that need to all be passed to \c vkCreateDevice
    *      using each create info structs \c pNext field.
    */
    struct VkStructPrototype
    {
        VkStructureType sType;
        void* pNext;
    };
}