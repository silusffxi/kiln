#pragma once
#include <memory>
#include <spdlog/logger.h>
#include <vulkan/vulkan.h>
#include "kiln/vulkan/vk_instance.h"

namespace kiln::vulkan
{
    /**
    *\brief Debug reporting facilities for Vulkan.
    */
    class vk_debug_reporter
    {
        std::shared_ptr<spdlog::logger> _log;

        vk_instance* _instance;

        VkDebugReportCallbackEXT _debug_report_callback = nullptr;

        bool _debug_enabled = false;
        bool _error_enabled = false;
        bool _info_enabled = false;
        bool _warn_enabled = false;
        bool _warn_perf_enabled = false;

    public:
        vk_debug_reporter(const std::unique_ptr<vk_instance>& instance);
        vk_debug_reporter(vk_instance* instance);
        ~vk_debug_reporter();

        vk_debug_reporter(const vk_debug_reporter&) = default;
        vk_debug_reporter(vk_debug_reporter&&) = default;

        vk_debug_reporter& operator=(const vk_debug_reporter&) = default;
        vk_debug_reporter& operator=(vk_debug_reporter&&) = default;

        bool& debug_enabled() { return _debug_enabled; }
        bool& error_enabled() { return _error_enabled; }
        bool& info_enabled() { return _info_enabled; }
        bool& warn_enabled() { return _warn_enabled; }
        bool& warn_perf_enabled() { return _warn_perf_enabled; }

        /**
        *\brief Invoked when a debug report is received from the Vulkan API.
        */
        void handle_debug_report(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT object_type,
            uint64_t object, size_t location, int32_t message_code,
            const char* layer_prefix, const char* message, void* user_data) const;

    private:
        static std::string_view debug_object_type_to_string(VkDebugReportObjectTypeEXT obj_type);
    };
}