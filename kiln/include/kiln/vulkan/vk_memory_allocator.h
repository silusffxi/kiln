#pragma once
#ifdef __cplusplus
#endif
#include <vulkan/vulkan.h>
#include <vma/vk_mem_alloc.h>
#include "kiln/vulkan/vk_instance.h"
#include "kiln/vulkan/vk_physical_device.h"
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_buffer.h"

#ifdef __cplusplus
#include <memory>

namespace kiln::vulkan
{
    /**
    *\brief Provides a wrapper around \c VmaAllocator.
    */
    class vk_memory_allocator
    {
        vk_instance*        _instance;
        vk_physical_device* _gpu;
        vk_device*          _device;

        VmaAllocator _allocator = nullptr;

    public:
        vk_memory_allocator(const std::unique_ptr<vk_instance>& instance,
            const std::unique_ptr<vk_physical_device>& gpu, const std::unique_ptr<vk_device>& device);

        vk_memory_allocator(vk_instance* instance, vk_physical_device* gpu, vk_device* device);
        ~vk_memory_allocator();

        /**
        *\brief Returns the wrapped allocator instance.
        */
        [[nodiscard]] VmaAllocator allocator() const;

        std::unique_ptr<vk_buffer> create_buffer(size_t size, VkBufferUsageFlags usage, VmaMemoryUsage memory_usage);

        /**
        *\brief Creates a temporary staging buffer.
        */
        std::unique_ptr<vk_buffer> create_staging_buffer(size_t size, VkBufferUsageFlags usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VmaMemoryUsage memory_usage = VMA_MEMORY_USAGE_CPU_ONLY);
    };
}
#endif