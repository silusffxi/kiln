#pragma once
#ifdef __cplusplus
#endif
#include <SDL2/SDL.h>
#include <vulkan/vulkan.h>
#include "kiln/vulkan/vk_instance.h"
#include "kiln/vulkan/vk_physical_device.h"
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_surface.h"
#include "kiln/vulkan/vk_swapchain.h"
#include "kiln/vulkan/vk_render_extension.h"
#include "kiln/vulkan/vk_immediate.h"
#include "kiln/imgui/imgui.h"

#ifdef __cplusplus
#include "kiln/logging.hpp"

namespace kiln::vulkan
{
    class imgui_vk_render_extension : public vk_render_extension
    {
        kiln_logger _log;

        SDL_Window* _window;

        vk_instance*        _instance;
        vk_physical_device* _gpu;
        vk_device*          _device;
        vk_surface*         _surface;
        vk_swapchain*       _swapchain;

        vk_immediate* _immediate;

        VkDescriptorPool _desc_pool = nullptr;

        ImGuiContext* _context = nullptr;

    public:
        imgui_vk_render_extension(SDL_Window* window, vk_immediate* immediate, const vk_render_extension_create_args& args);
        ~imgui_vk_render_extension() override;

    protected:
        void on_event(const SDL_Event* e) override;

        void on_frame_begin(VkCommandBuffer cmd, vk_render_image* target_image, vk_render_image* swapchain_image) override;
        void on_frame_end(VkCommandBuffer cmd, vk_render_image* target_image, vk_render_image* swapchain_image) override;

    private:
        static void apply_dark_style();
        static void setup_imgui();
    };
}
#endif