#pragma once
#include <vulkan/vulkan.h>
#include "kiln/types.h"
#include "kiln/vulkan/vk_device.h"

#ifdef __cplusplus
#include <filesystem>
#include <memory>
#include <string>
#include "kiln/shaders.hpp"

namespace kiln::vulkan
{
    class vk_shader
    {
        /**
        *\brief The device that was to create the shader module.
        */
        vk_device* _device;

        /**
        *\brief The actual \c VkShaderModule for the shader.
        */
        VkShaderModule _mod;

        /**
        *\brief The name of the shader.
        */
        std::string _name;

        /**
        *\brief The path to the original file.
        */
        std::filesystem::path _file_path;

        /**
        *\brief The type of the shader.
        */
        shader_type _type;

    public:
        vk_shader(vk_device* device, VkShaderModule mod, const shaders::binary& shader_def);
        ~vk_shader() = default;
        vk_shader(const vk_shader&) = default;
        vk_shader(vk_shader&&) = default;

        vk_shader& operator=(const vk_shader&) = default;
        vk_shader& operator=(vk_shader&&) = default;

        [[nodiscard]] VkShaderModule shader_module() const;
        [[nodiscard]] const std::string& name() const;
        [[nodiscard]] const std::filesystem::path& file_path() const;
        [[nodiscard]] const shader_type& type() const;

        static std::unique_ptr<vk_shader> from_shader_binary(vk_device* device, const shaders::binary& def);
    };
}
#endif