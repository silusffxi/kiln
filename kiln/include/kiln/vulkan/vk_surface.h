#pragma once
#include <SDL2/SDL.h>
#include <vulkan/vulkan.h>

#ifdef __cplusplus
#pragma once
#include <vector>
#include "kiln/vulkan/vk_instance.h"
#include "kiln/vulkan/vk_physical_device.h"

namespace kiln::vulkan
{
    class vk_surface
    {
        vk_instance* _instance;
        vk_physical_device* _physical_device;

        VkSurfaceKHR _surface = nullptr;

        VkSurfaceFormatKHR _surface_format =
        {
            .format = VK_FORMAT_B8G8R8A8_SRGB,
            //.format = VK_FORMAT_B8G8R8A8_UNORM,
            .colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR,
        };

        VkSurfaceCapabilitiesKHR _capabilities = { };

        std::vector<VkSurfaceFormatKHR> _supported_formats;
        std::vector<VkPresentModeKHR>   _supported_present_modes;

    public:
        vk_surface(SDL_Window* window, const std::unique_ptr<vk_instance>& instance, const std::unique_ptr<vk_physical_device>& gpu);
        vk_surface(SDL_Window* window, vk_instance* instance, vk_physical_device* gpu);
        ~vk_surface();

        [[nodiscard]] const VkSurfaceCapabilitiesKHR& capabilities() const { return _capabilities; }
        [[nodiscard]] const VkSurfaceFormatKHR& format() const { return _surface_format; }

        [[nodiscard]] uint32_t width() const { return _capabilities.currentExtent.width; }
        [[nodiscard]] uint32_t height() const { return _capabilities.currentExtent.height; }
        [[nodiscard]] VkExtent2D extent() const { return _capabilities.currentExtent; }

        std::vector<VkSurfaceFormatKHR>& supported_formats() { return _supported_formats; }
        std::vector<VkPresentModeKHR>& supported_present_modes() { return _supported_present_modes; }

        /**
        *\brief Check to see if the provided present mode is support.
        *\returns \c true if the mode is supported, otherwise \c false.
        */
        [[nodiscard]] bool is_present_mode_supported(VkPresentModeKHR mode) const;

        /**
        *\brief Updates the surface capabilities. This should be called when the window size changes.
        */
        void update_capabilities();

        operator VkSurfaceKHR() const { return _surface; }

    private:
        static std::vector<VkSurfaceFormatKHR> enumerate_surface_formats(VkPhysicalDevice physical_device, VkSurfaceKHR surface);
        static std::vector<VkPresentModeKHR> enumerate_surface_present_modes(VkPhysicalDevice physical_device, VkSurfaceKHR surface);
    };
}
#endif