#pragma once
#include <SDL2/SDL.h>
#include "kiln/vulkan/vk_config.h"
#include "kiln/vulkan/vk_device.h"
#include "kiln/vulkan/vk_instance.h"
#include "kiln/vulkan/vk_physical_device.h"
#include "kiln/vulkan/vk_memory_allocator.h"
#include "kiln/vulkan/vk_renderer.h"
#include "kiln/vulkan/vk_surface.h"
#include "kiln/vulkan/vk_swapchain.h"
#include "kiln/vulkan/vk_immediate.h"
#include "kiln/vulkan/vk_assets_manager.h"

#ifdef __cplusplus
#include "kiln/vulkan/vk_instance_config.hpp"

namespace kiln
{
    /**
    *\brief Initializes the Kiln library for Vulkan and creates a renderer instance.
    */
    void initialize_for_vulkan(SDL_Window* window, const vulkan::vk_config* config,  vulkan::vk_instance_config* instance_config);

    /**
    *\brief Releases all the resources used by Kiln.
    */
    void shutdown();

    namespace vulkan
    {
        /**
        *\brief Returns the assets manager instance.
        */
        vk_assets_manager* assets();

        /**
        *\brief Returns the global Vulkan logical device.
        */
        vk_device* get_device();

        /**
        *\brief Returns the physical device (GPU) that was selected by Vulkan.
        */
        vk_physical_device* get_gpu();

        /**
        *\brief Returns the global Vulkan instance.
        */
        vk_instance* get_instance();

        /**
        *\brief Returns the global memory allocator.
        */
        vk_memory_allocator* get_memory_allocator();

        /**
        *\brief Returns the global renderer for Vulkan.
        */
        vk_renderer* get_renderer();

        /**
        *\brief Returns the window surface created from the SDL window.
        */
        vk_surface* get_surface();

        /**
        *\brief Returns the global swapchain for Vulkan.
        */
        vk_swapchain* get_swapchain();

        /**
        *\brief Returns the structure to perform immediate submits.
        */
        vk_immediate* immediate();
    }
}
#endif
