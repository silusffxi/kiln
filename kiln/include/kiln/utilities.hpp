#pragma once
#include <filesystem>
#include <memory>
#include <string>
#include <vector>
#include "kiln/types.h"

namespace kiln
{
    /**
    *\brief Returns the shader type for the given file extension.
    */
    shader_type file_ext_to_shader_type(const std::string& file_ext);

    /**
    *\brief Returns the shader type based on the file name.
    */
    shader_type file_name_to_shader_type(const std::string& file_name);

    /**
    *\brief Helper to explicitly free instances of \c std::unique_ptr.
    */
    template<typename T>
    void free_unique_ptr(std::unique_ptr<T>& ptr)
    {
        if (ptr != nullptr)
        {
            ptr.reset();
            ptr = nullptr;
        }
    }

    /**
    *\brief Returns the path of the currently executing binary.
    */
    std::filesystem::path get_current_binary_path();

    /**
    *\brief Returns the path of the directory where the binary is executing from.
    */
    std::filesystem::path get_root_directory();
}
