#pragma once
#ifdef __cplusplus
#include <cstdint>
#else
#include <stdint.h>
#endif

#ifdef __cplusplus
#include <glm/glm.hpp>

namespace kiln
{
    /**
    *\brief Generic camera.
    */
    class camera
    {
        glm::vec3 _pos = glm::vec3();

    public:
        camera() = default;
        ~camera() = default;
        camera(const camera&) = default;
        camera(camera&&) = default;

        camera& operator=(const camera&) = default;
        camera& operator=(camera&&) = default;

        glm::vec3& pos();
    };
}
#endif